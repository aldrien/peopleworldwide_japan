-- MySQL dump 10.13  Distrib 5.6.33, for Linux (x86_64)
--
-- Host: localhost    Database: peopleworldwide_production
-- ------------------------------------------------------
-- Server version	5.6.33-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `academic_abilities`
--

DROP TABLE IF EXISTS `academic_abilities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `academic_abilities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `en_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `jp_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_updated_by` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `academic_abilities`
--

LOCK TABLES `academic_abilities` WRITE;
/*!40000 ALTER TABLE `academic_abilities` DISABLE KEYS */;
INSERT INTO `academic_abilities` VALUES (1,'IQ test:IQ','-','sakiko.admin','2016-10-24 05:51:50','2016-11-04 09:17:08'),(2,'PWA Japanese skill test','PWJ日本語能力テスト','admin','2016-10-24 05:51:50','2016-10-24 05:51:50'),(3,'IQ test:Calculation ','-','sakiko.admin','2016-11-04 09:17:28','2016-11-04 09:17:28');
/*!40000 ALTER TABLE `academic_abilities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `applicant_academic_abilities`
--

DROP TABLE IF EXISTS `applicant_academic_abilities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `applicant_academic_abilities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `academic_ability_id` int(11) DEFAULT NULL,
  `score` mediumint(9) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_applicant_academic` (`applicant_id`,`academic_ability_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `applicant_academic_abilities`
--

LOCK TABLES `applicant_academic_abilities` WRITE;
/*!40000 ALTER TABLE `applicant_academic_abilities` DISABLE KEYS */;
INSERT INTO `applicant_academic_abilities` VALUES (1,27,1,80,'2016-11-04 09:19:40','2016-11-04 09:19:40'),(2,27,3,100,'2016-11-04 09:19:40','2016-11-04 09:19:40'),(3,23,1,90,'2016-11-04 09:32:31','2016-11-04 09:32:31'),(4,23,3,100,'2016-11-04 09:32:31','2016-11-04 09:32:31'),(5,25,1,50,'2016-11-04 09:33:44','2016-11-04 09:33:44'),(6,25,3,100,'2016-11-04 09:33:44','2016-11-04 09:33:44'),(7,26,1,60,'2016-11-04 09:34:39','2016-11-04 09:34:39'),(8,26,3,90,'2016-11-04 09:34:39','2016-11-04 09:34:39'),(9,29,1,60,'2016-11-04 09:35:13','2016-11-04 09:35:13'),(10,29,3,90,'2016-11-04 09:35:13','2016-11-04 09:35:13'),(11,28,1,40,'2016-11-04 09:36:25','2016-11-04 09:36:25'),(12,28,3,90,'2016-11-04 09:36:25','2016-11-04 09:36:25'),(13,8,1,90,'2016-11-04 09:37:54','2016-11-04 09:37:54'),(14,8,3,100,'2016-11-04 09:37:54','2016-11-04 09:37:54'),(15,13,1,60,'2016-11-04 09:38:59','2016-11-04 09:38:59'),(16,13,3,90,'2016-11-04 09:38:59','2016-11-04 09:38:59'),(17,14,1,100,'2016-11-04 09:39:37','2016-11-04 09:39:37'),(18,14,3,100,'2016-11-04 09:39:37','2016-11-04 09:39:37'),(19,32,1,70,'2016-11-04 09:41:02','2016-11-04 09:41:02'),(20,32,3,90,'2016-11-04 09:41:02','2016-11-04 09:41:02'),(21,30,1,40,'2016-11-04 09:42:08','2016-11-04 09:42:08'),(22,30,3,100,'2016-11-04 09:42:08','2016-11-04 09:42:08'),(23,21,1,60,'2016-11-04 09:43:28','2016-11-04 09:43:28'),(24,21,3,100,'2016-11-04 09:43:28','2016-11-04 09:43:28'),(25,34,1,70,'2016-11-05 15:49:51','2016-11-05 15:49:51'),(26,34,3,90,'2016-11-05 15:49:51','2016-11-05 15:49:51'),(27,11,1,50,'2016-11-05 15:50:57','2016-11-05 15:50:57'),(28,11,3,90,'2016-11-05 15:50:57','2016-11-05 15:50:57'),(29,7,1,80,'2016-11-05 15:52:35','2016-11-05 15:52:35'),(30,7,3,80,'2016-11-05 15:52:35','2016-11-05 15:52:35'),(31,17,1,50,'2016-11-05 15:55:37','2016-11-05 15:55:37'),(32,17,3,90,'2016-11-05 15:55:37','2016-11-05 15:55:37'),(33,16,1,1,'2016-11-05 15:56:41','2016-11-05 15:56:41'),(34,16,3,1,'2016-11-05 15:56:41','2016-11-05 15:56:41'),(35,10,1,70,'2016-11-05 15:57:36','2016-11-05 15:57:36'),(36,10,3,80,'2016-11-05 15:57:36','2016-11-05 15:57:36'),(37,35,1,70,'2016-11-05 15:58:39','2016-11-05 15:58:39'),(38,35,3,90,'2016-11-05 15:58:39','2016-11-05 15:58:39'),(39,3,1,70,'2016-11-05 16:01:05','2016-11-05 16:02:58'),(40,3,3,90,'2016-11-05 16:01:05','2016-11-05 16:02:58'),(41,4,1,70,'2016-11-05 16:01:41','2016-11-05 16:01:41'),(42,4,3,90,'2016-11-05 16:01:41','2016-11-05 16:01:41'),(43,2,1,80,'2016-11-05 16:02:28','2016-11-05 16:02:28'),(44,2,3,80,'2016-11-05 16:02:28','2016-11-05 16:02:28'),(45,6,1,80,'2016-11-05 16:03:36','2016-11-05 16:03:36'),(46,6,3,80,'2016-11-05 16:03:36','2016-11-05 16:03:36'),(47,33,1,70,'2016-11-05 16:04:23','2016-11-05 16:04:23'),(48,33,3,80,'2016-11-05 16:04:23','2016-11-05 16:04:23'),(49,1,1,80,'2016-11-05 16:05:15','2016-11-05 16:05:15'),(50,1,3,90,'2016-11-05 16:05:15','2016-11-05 16:05:15'),(51,36,1,70,'2016-11-05 16:06:40','2016-11-05 16:06:40'),(52,36,3,90,'2016-11-05 16:06:40','2016-11-05 16:06:40'),(53,39,1,50,'2016-11-05 16:08:12','2016-11-05 16:08:12'),(54,39,3,90,'2016-11-05 16:08:12','2016-11-05 16:08:12'),(55,22,1,1,'2016-11-05 16:12:08','2016-11-05 16:13:46'),(56,22,3,1,'2016-11-05 16:12:08','2016-11-05 16:13:46'),(57,5,1,80,'2016-11-05 16:14:48','2016-11-05 16:14:48'),(58,5,3,90,'2016-11-05 16:14:48','2016-11-05 16:14:48'),(59,31,1,1,'2016-11-05 16:16:21','2016-11-05 16:16:21'),(60,31,3,1,'2016-11-05 16:16:21','2016-11-05 16:16:21'),(61,20,1,60,'2016-11-05 16:17:25','2016-11-05 16:17:25'),(62,20,3,80,'2016-11-05 16:17:25','2016-11-05 16:17:25'),(63,15,1,60,'2016-11-05 16:18:28','2016-11-05 16:18:28'),(64,15,3,90,'2016-11-05 16:18:28','2016-11-05 16:18:28'),(65,40,1,90,'2016-11-05 16:19:32','2016-11-05 16:19:32'),(66,40,3,1,'2016-11-05 16:19:32','2016-11-05 16:19:32'),(67,38,1,70,'2016-11-05 16:20:54','2016-11-05 16:20:54'),(68,38,3,90,'2016-11-05 16:20:54','2016-11-05 16:20:54'),(69,18,1,30,'2016-11-05 16:21:50','2016-11-05 16:21:50'),(70,18,3,70,'2016-11-05 16:21:50','2016-11-05 16:21:50'),(71,37,1,70,'2016-11-05 16:22:58','2016-11-05 16:22:58'),(72,37,3,90,'2016-11-05 16:22:58','2016-11-05 16:22:58'),(73,19,1,70,'2016-11-05 16:24:01','2016-11-05 16:24:01'),(74,19,3,90,'2016-11-05 16:24:01','2016-11-05 16:25:13'),(75,24,1,70,'2016-11-05 16:25:58','2016-11-05 16:25:58'),(76,24,3,70,'2016-11-05 16:25:58','2016-11-05 16:25:58'),(77,9,1,80,'2016-11-05 16:26:59','2016-11-05 16:26:59'),(78,9,3,90,'2016-11-05 16:26:59','2016-11-05 16:26:59'),(79,12,1,70,'2016-11-05 16:28:08','2016-11-05 16:28:08'),(80,12,3,90,'2016-11-05 16:28:08','2016-11-05 16:28:08');
/*!40000 ALTER TABLE `applicant_academic_abilities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `applicant_aptitude_tests`
--

DROP TABLE IF EXISTS `applicant_aptitude_tests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `applicant_aptitude_tests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `aptitude_test_id` int(11) DEFAULT NULL,
  `score` mediumint(9) DEFAULT NULL,
  `show_in_report` tinyint(1) DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_applicant_aptitude` (`applicant_id`,`aptitude_test_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `applicant_aptitude_tests`
--

LOCK TABLES `applicant_aptitude_tests` WRITE;
/*!40000 ALTER TABLE `applicant_aptitude_tests` DISABLE KEYS */;
INSERT INTO `applicant_aptitude_tests` VALUES (1,27,4,72,0,'2016-11-04 09:19:40','2016-11-04 09:20:42'),(2,23,4,84,0,'2016-11-04 09:32:31','2016-11-04 09:32:31'),(3,25,4,91,0,'2016-11-04 09:33:44','2016-11-04 09:33:44'),(4,26,4,91,0,'2016-11-04 09:34:39','2016-11-04 09:34:39'),(5,29,4,75,0,'2016-11-04 09:35:13','2016-11-04 09:35:13'),(6,28,4,88,0,'2016-11-04 09:36:25','2016-11-04 09:36:25'),(7,8,4,78,0,'2016-11-04 09:37:54','2016-11-04 09:37:54'),(8,13,4,88,0,'2016-11-04 09:38:59','2016-11-04 09:38:59'),(9,14,4,91,0,'2016-11-04 09:39:37','2016-11-04 09:39:37'),(10,32,4,84,0,'2016-11-04 09:41:02','2016-11-04 09:41:02'),(11,30,4,88,0,'2016-11-04 09:42:08','2016-11-04 09:42:08'),(12,21,4,88,0,'2016-11-04 09:43:28','2016-11-04 09:43:28'),(13,34,4,94,0,'2016-11-05 15:49:51','2016-11-05 15:49:51'),(14,11,4,75,0,'2016-11-05 15:50:57','2016-11-05 15:50:57'),(15,7,4,75,0,'2016-11-05 15:52:35','2016-11-05 15:52:35'),(16,17,4,69,0,'2016-11-05 15:55:37','2016-11-05 15:55:37'),(17,16,4,100,0,'2016-11-05 15:56:41','2016-11-05 15:56:41'),(18,10,4,72,0,'2016-11-05 15:57:36','2016-11-05 15:57:36'),(19,35,4,81,0,'2016-11-05 15:58:39','2016-11-05 15:58:39'),(20,3,4,78,0,'2016-11-05 16:01:05','2016-11-05 16:02:58'),(21,4,4,78,0,'2016-11-05 16:01:41','2016-11-05 16:01:41'),(22,2,4,66,0,'2016-11-05 16:02:28','2016-11-05 16:02:28'),(23,6,4,75,0,'2016-11-05 16:03:36','2016-11-05 16:03:36'),(24,33,4,97,0,'2016-11-05 16:04:23','2016-11-05 16:04:23'),(25,1,4,88,0,'2016-11-05 16:05:15','2016-11-05 16:05:15'),(26,36,4,88,0,'2016-11-05 16:06:40','2016-11-05 16:06:40'),(27,39,4,100,0,'2016-11-05 16:08:12','2016-11-05 16:08:12'),(28,22,4,84,0,'2016-11-05 16:12:08','2016-11-05 16:13:46'),(29,5,4,94,0,'2016-11-05 16:14:48','2016-11-05 16:14:48'),(30,31,4,72,0,'2016-11-05 16:16:21','2016-11-05 16:16:21'),(31,20,4,84,0,'2016-11-05 16:17:25','2016-11-05 16:17:25'),(32,15,4,91,0,'2016-11-05 16:18:28','2016-11-05 16:18:28'),(33,40,4,94,0,'2016-11-05 16:19:32','2016-11-05 16:19:32'),(34,38,4,81,0,'2016-11-05 16:20:54','2016-11-05 16:20:54'),(35,18,4,73,0,'2016-11-05 16:21:50','2016-11-05 16:21:50'),(36,37,4,88,0,'2016-11-05 16:22:58','2016-11-05 16:22:58'),(37,19,4,97,0,'2016-11-05 16:24:01','2016-11-06 06:54:04'),(38,24,4,69,0,'2016-11-05 16:25:58','2016-11-05 16:25:58'),(39,9,4,94,0,'2016-11-05 16:26:59','2016-11-05 16:26:59'),(40,12,4,94,0,'2016-11-05 16:28:08','2016-11-05 16:28:08');
/*!40000 ALTER TABLE `applicant_aptitude_tests` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `applicant_files`
--

DROP TABLE IF EXISTS `applicant_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `applicant_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `file_type_id` int(11) DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `document_file_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `document_content_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `document_file_size` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_applicant` (`applicant_id`,`file_type_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `applicant_files`
--

LOCK TABLES `applicant_files` WRITE;
/*!40000 ALTER TABLE `applicant_files` DISABLE KEYS */;
/*!40000 ALTER TABLE `applicant_files` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `applicant_project_histories`
--

DROP TABLE IF EXISTS `applicant_project_histories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `applicant_project_histories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `notes` text COLLATE utf8_unicode_ci,
  `last_updated_by` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_applicant` (`applicant_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=122 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `applicant_project_histories`
--

LOCK TABLES `applicant_project_histories` WRITE;
/*!40000 ALTER TABLE `applicant_project_histories` DISABLE KEYS */;
INSERT INTO `applicant_project_histories` VALUES (1,1,1,'No notes.','sakiko.partner','2016-11-04 03:34:01','2016-11-04 03:34:01'),(2,2,1,'No notes.','sakiko.partner','2016-11-04 03:52:14','2016-11-04 03:52:14'),(3,3,1,'No notes.','sakiko.partner','2016-11-04 04:09:56','2016-11-04 04:09:56'),(4,1,2,'No notes.','admin','2016-11-04 04:19:18','2016-11-04 04:19:18'),(5,1,1,'No notes.','admin','2016-11-04 04:19:59','2016-11-04 04:19:59'),(6,4,1,'No notes.','sakiko.partner','2016-11-04 04:33:19','2016-11-04 04:33:19'),(7,6,1,'No notes.','sakiko.partner','2016-11-04 05:03:59','2016-11-04 05:03:59'),(8,7,1,'No notes.','sakiko.partner','2016-11-04 05:13:57','2016-11-04 05:13:57'),(9,8,1,'No notes.','sakiko.partner','2016-11-04 05:18:21','2016-11-04 05:18:21'),(10,9,2,'No notes.','yoshida.partner','2016-11-04 05:18:34','2016-11-04 05:18:34'),(11,5,2,'No notes.','yoshida.partner','2016-11-04 05:20:40','2016-11-04 05:20:40'),(12,10,1,'No notes.','sakiko.partner','2016-11-04 05:23:19','2016-11-04 05:23:19'),(13,11,1,'No notes.','sakiko.partner','2016-11-04 05:36:43','2016-11-04 05:36:43'),(14,12,2,'No notes.','yoshida.partner','2016-11-04 05:45:49','2016-11-04 05:45:49'),(15,13,1,'No notes.','sakiko.partner','2016-11-04 05:51:43','2016-11-04 05:51:43'),(16,14,1,'No notes.','sakiko.partner','2016-11-04 05:59:04','2016-11-04 05:59:04'),(17,15,2,'No notes.','yoshida.partner','2016-11-04 05:59:50','2016-11-04 05:59:50'),(18,16,1,'No notes.','sakiko.partner','2016-11-04 06:05:08','2016-11-04 06:05:08'),(19,17,1,'No notes.','sakiko.partner','2016-11-04 06:12:33','2016-11-04 06:12:33'),(20,18,2,'No notes.','yoshida.partner','2016-11-04 06:18:39','2016-11-04 06:18:39'),(21,19,2,'No notes.','yoshida.partner','2016-11-04 06:24:47','2016-11-04 06:24:47'),(22,20,2,'No notes.','yoshida.partner','2016-11-04 06:31:37','2016-11-04 06:31:37'),(23,21,2,'No notes.','yoshida.partner','2016-11-04 06:40:42','2016-11-04 06:40:42'),(24,22,2,'No notes.','yoshida.partner','2016-11-04 06:48:43','2016-11-04 06:48:43'),(25,23,1,'No notes.','sakiko.partner','2016-11-04 06:49:47','2016-11-04 06:49:47'),(26,24,2,'No notes.','yoshida.partner','2016-11-04 06:57:23','2016-11-04 06:57:23'),(27,25,1,'No notes.','sakiko.partner','2016-11-04 06:59:07','2016-11-04 06:59:07'),(28,26,1,'No notes.','sakiko.partner','2016-11-04 07:05:02','2016-11-04 07:05:02'),(29,27,1,'No notes.','sakiko.partner','2016-11-04 07:13:06','2016-11-04 07:13:06'),(30,28,1,'No notes.','sakiko.partner','2016-11-04 07:21:55','2016-11-04 07:21:55'),(31,29,1,'No notes.','sakiko.partner','2016-11-04 07:27:58','2016-11-04 07:27:58'),(32,30,1,'No notes.','sakiko.partner','2016-11-04 07:35:08','2016-11-04 07:35:08'),(33,31,2,'No notes.','yoshida.partner','2016-11-04 07:43:27','2016-11-04 07:43:27'),(34,32,2,'No notes.','yoshida.partner','2016-11-04 07:47:41','2016-11-04 07:47:41'),(35,33,1,'No notes.','sakiko.partner','2016-11-04 07:55:30','2016-11-04 07:55:30'),(36,34,1,'No notes.','sakiko.partner','2016-11-04 07:59:49','2016-11-04 07:59:49'),(37,35,1,'No notes.','sakiko.partner','2016-11-04 08:04:51','2016-11-04 08:04:51'),(38,36,2,'No notes.','yoshida.partner','2016-11-04 08:10:07','2016-11-04 08:10:07'),(39,37,2,'No notes.','yoshida.partner','2016-11-04 08:18:56','2016-11-04 08:18:56'),(40,38,2,'No notes.','yoshida.partner','2016-11-04 08:26:03','2016-11-04 08:26:03'),(41,39,2,'No notes.','yoshida.partner','2016-11-04 08:32:19','2016-11-04 08:32:19'),(42,40,2,'No notes.','yoshida.partner','2016-11-04 08:41:04','2016-11-04 08:41:04'),(43,27,3,'No notes.','sakiko.admin','2016-11-04 13:03:33','2016-11-04 13:03:33'),(44,8,3,'No notes.','sakiko.admin','2016-11-04 13:03:50','2016-11-04 13:03:50'),(45,30,3,'No notes.','sakiko.admin','2016-11-04 13:04:05','2016-11-04 13:04:05'),(46,14,3,'No notes.','sakiko.admin','2016-11-04 13:04:36','2016-11-04 13:04:36'),(47,26,3,'No notes.','sakiko.admin','2016-11-04 13:04:45','2016-11-04 13:04:45'),(48,13,3,'No notes.','sakiko.admin','2016-11-04 13:04:54','2016-11-04 13:04:54'),(49,25,3,'No notes.','sakiko.admin','2016-11-04 13:05:53','2016-11-04 13:05:53'),(50,29,3,'No notes.','sakiko.admin','2016-11-04 13:06:02','2016-11-04 13:06:02'),(51,32,3,'No notes.','sakiko.admin','2016-11-04 13:06:22','2016-11-04 13:06:22'),(52,23,4,'No notes.','sakiko.admin','2016-11-05 15:45:59','2016-11-05 15:45:59'),(53,28,4,'No notes.','sakiko.admin','2016-11-05 15:46:33','2016-11-05 15:46:33'),(54,31,5,'No notes.','sakiko.admin','2016-11-05 16:38:21','2016-11-05 16:38:21'),(55,36,5,'No notes.','sakiko.admin','2016-11-05 16:38:32','2016-11-05 16:38:32'),(56,37,5,'No notes.','sakiko.admin','2016-11-05 16:38:42','2016-11-05 16:38:42'),(57,38,5,'No notes.','sakiko.admin','2016-11-05 16:40:44','2016-11-05 16:40:44'),(58,39,5,'No notes.','sakiko.admin','2016-11-05 16:40:54','2016-11-05 16:40:54'),(59,40,5,'No notes.','sakiko.admin','2016-11-05 16:41:04','2016-11-05 16:41:04'),(60,5,5,'No notes.','sakiko.admin','2016-11-05 16:41:19','2016-11-05 16:41:19'),(61,9,5,'No notes.','sakiko.admin','2016-11-05 16:41:30','2016-11-05 16:41:30'),(62,12,5,'No notes.','sakiko.admin','2016-11-05 16:41:41','2016-11-05 16:41:41'),(63,15,5,'No notes.','sakiko.admin','2016-11-05 16:41:51','2016-11-05 16:41:51'),(64,18,5,'No notes.','sakiko.admin','2016-11-05 16:42:02','2016-11-05 16:42:02'),(65,19,5,'No notes.','sakiko.admin','2016-11-05 16:42:12','2016-11-05 16:42:12'),(66,20,5,'No notes.','sakiko.admin','2016-11-05 16:42:23','2016-11-05 16:42:23'),(67,21,5,'No notes.','sakiko.admin','2016-11-05 16:42:34','2016-11-05 16:42:34'),(68,22,5,'No notes.','sakiko.admin','2016-11-05 16:42:45','2016-11-05 16:42:45'),(69,24,5,'No notes.','sakiko.admin','2016-11-05 16:42:54','2016-11-05 16:42:54'),(70,1,5,'No notes.','sakiko.admin','2016-11-05 16:43:18','2016-11-05 16:43:18'),(71,2,5,'No notes.','sakiko.admin','2016-11-05 16:43:32','2016-11-05 16:43:32'),(72,3,5,'No notes.','sakiko.admin','2016-11-05 16:43:44','2016-11-05 16:43:44'),(73,4,5,'No notes.','sakiko.admin','2016-11-05 16:43:54','2016-11-05 16:43:54'),(74,6,5,'No notes.','sakiko.admin','2016-11-05 16:44:05','2016-11-05 16:44:05'),(75,7,5,'No notes.','sakiko.admin','2016-11-05 16:44:16','2016-11-05 16:44:16'),(76,10,5,'No notes.','sakiko.admin','2016-11-05 16:44:26','2016-11-05 16:44:26'),(77,11,5,'No notes.','sakiko.admin','2016-11-05 16:44:36','2016-11-05 16:44:36'),(78,16,5,'No notes.','sakiko.admin','2016-11-05 16:44:46','2016-11-05 16:44:46'),(79,17,5,'No notes.','sakiko.admin','2016-11-05 16:44:58','2016-11-05 16:44:58'),(80,33,5,'No notes.','sakiko.admin','2016-11-05 16:45:09','2016-11-05 16:45:09'),(81,34,5,'No notes.','sakiko.admin','2016-11-05 16:45:19','2016-11-05 16:45:19'),(82,35,5,'No notes.','sakiko.admin','2016-11-05 16:45:29','2016-11-05 16:45:29'),(83,25,11,'No notes.','admin','2016-11-07 11:39:36','2016-11-07 11:39:36'),(84,27,11,'No notes.','admin','2016-11-07 11:39:43','2016-11-07 11:39:43'),(85,32,11,'No notes.','admin','2016-11-07 11:39:48','2016-11-07 11:39:48'),(86,8,11,'No notes.','admin','2016-11-07 11:39:53','2016-11-07 11:39:53'),(87,13,11,'No notes.','admin','2016-11-07 11:40:00','2016-11-07 11:40:00'),(88,29,8,'No notes.','pwjstaff_admin','2016-11-08 02:28:06','2016-11-08 02:28:06'),(89,23,7,'No notes.','pwjstaff_admin','2016-11-08 02:31:28','2016-11-08 02:31:28'),(90,28,7,'No notes.','pwjstaff_admin','2016-11-08 02:31:42','2016-11-08 02:31:42'),(91,25,8,'No notes.','pwjstaff_admin','2016-11-08 02:33:48','2016-11-08 02:33:48'),(92,7,8,'No notes.','pwjstaff_admin','2016-11-08 02:39:14','2016-11-08 02:39:14'),(93,5,8,'No notes.','pwjstaff_admin','2016-11-08 02:39:28','2016-11-08 02:39:28'),(94,9,8,'No notes.','pwjstaff_admin','2016-11-08 02:39:45','2016-11-08 02:39:45'),(95,12,8,'No notes.','pwjstaff_admin','2016-11-08 02:40:00','2016-11-08 02:40:00'),(96,1,7,'No notes.','pwjstaff_admin','2016-11-08 02:44:43','2016-11-08 02:44:43'),(97,11,7,'No notes.','pwjstaff_admin','2016-11-08 02:44:59','2016-11-08 02:44:59'),(98,17,7,'No notes.','pwjstaff_admin','2016-11-08 02:45:21','2016-11-08 02:45:21'),(99,33,7,'No notes.','pwjstaff_admin','2016-11-08 02:45:34','2016-11-08 02:45:34'),(100,39,7,'No notes.','pwjstaff_admin','2016-11-08 02:45:48','2016-11-08 02:45:48'),(101,40,7,'No notes.','pwjstaff_admin','2016-11-08 02:45:59','2016-11-08 02:45:59'),(102,24,7,'No notes.','pwjstaff_admin','2016-11-08 02:46:11','2016-11-08 02:46:11'),(103,2,9,'No notes.','pwjstaff_admin','2016-11-08 02:47:35','2016-11-08 02:47:35'),(104,3,9,'No notes.','pwjstaff_admin','2016-11-08 02:47:42','2016-11-08 02:47:42'),(105,4,9,'No notes.','pwjstaff_admin','2016-11-08 02:47:51','2016-11-08 02:47:51'),(106,21,9,'No notes.','pwjstaff_admin','2016-11-08 02:48:00','2016-11-08 02:48:00'),(107,6,4,'No notes.','pwjstaff_admin','2016-11-08 02:48:10','2016-11-08 02:48:10'),(108,16,4,'No notes.','pwjstaff_admin','2016-11-08 02:48:17','2016-11-08 02:48:17'),(109,34,4,'No notes.','pwjstaff_admin','2016-11-08 02:48:30','2016-11-08 02:48:30'),(110,35,4,'No notes.','pwjstaff_admin','2016-11-08 02:48:39','2016-11-08 02:48:39'),(111,31,4,'No notes.','pwjstaff_admin','2016-11-08 02:48:53','2016-11-08 02:48:53'),(112,37,4,'No notes.','pwjstaff_admin','2016-11-08 02:49:03','2016-11-08 02:49:03'),(113,38,4,'No notes.','pwjstaff_admin','2016-11-08 02:49:11','2016-11-08 02:49:11'),(114,19,4,'No notes.','pwjstaff_admin','2016-11-08 02:49:22','2016-11-08 02:49:22'),(115,22,4,'No notes.','pwjstaff_admin','2016-11-08 02:49:34','2016-11-08 02:49:34'),(116,15,6,'No notes.','pwjstaff_admin','2016-11-08 02:50:39','2016-11-08 02:50:39'),(117,18,6,'No notes.','pwjstaff_admin','2016-11-08 02:50:49','2016-11-08 02:50:49'),(118,20,6,'No notes.','pwjstaff_admin','2016-11-08 02:50:58','2016-11-08 02:50:58'),(119,10,6,'No notes.','pwjstaff_admin','2016-11-08 02:54:48','2016-11-08 02:54:48'),(120,36,6,'No notes.','pwjstaff_admin','2016-11-08 02:54:56','2016-11-08 02:54:56'),(121,32,6,'No notes.','pwjstaff_admin','2016-11-08 02:55:40','2016-11-08 02:55:40');
/*!40000 ALTER TABLE `applicant_project_histories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `applicants`
--

DROP TABLE IF EXISTS `applicants`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `applicants` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `custom_code` varchar(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  `creator_id` int(11) DEFAULT NULL,
  `edit_status` tinyint(1) DEFAULT '0',
  `last_updated_by` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `religion_id` int(11) DEFAULT NULL,
  `status` varchar(20) COLLATE utf8_unicode_ci DEFAULT 'Waiting',
  `email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` tinytext COLLATE utf8_unicode_ci,
  `gender` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birth_date` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `age` mediumint(9) DEFAULT NULL,
  `height` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `weight` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `marital_status` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_academic` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dominant_hand` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `english_skill` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `jlpt_level` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_work_related_expirience` tinyint(1) DEFAULT NULL,
  `profile_file_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `profile_content_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `profile_file_size` int(11) DEFAULT NULL,
  `wholebody_file_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `wholebody_content_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `wholebody_file_size` int(11) DEFAULT NULL,
  `short_notes` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notes` text COLLATE utf8_unicode_ci,
  `show_notes_in_admin_pdf` tinyint(1) DEFAULT '1',
  `show_notes_in_partner_pdf` tinyint(1) DEFAULT '0',
  `show_notes_in_client_pdf` tinyint(1) DEFAULT '0',
  `admin_remarks` text COLLATE utf8_unicode_ci,
  `show_remarks_in_admin_pdf` tinyint(1) DEFAULT '1',
  `show_remarks_in_partner_pdf` tinyint(1) DEFAULT '0',
  `show_remarks_in_client_pdf` tinyint(1) DEFAULT '0',
  `show_client_comments` tinyint(1) DEFAULT '0',
  `client_comments` text COLLATE utf8_unicode_ci,
  `self_introduction_video_link` text COLLATE utf8_unicode_ci,
  `aptitude_test_link` text COLLATE utf8_unicode_ci,
  `psychometric_test_link` text COLLATE utf8_unicode_ci,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `searchable_by_idx` (`creator_id`,`project_id`,`country_id`,`religion_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `applicants`
--

LOCK TABLES `applicants` WRITE;
/*!40000 ALTER TABLE `applicants` DISABLE KEYS */;
INSERT INTO `applicants` VALUES (1,'B00016',3,0,'pwjstaff_admin','Zin Ko Ko Latt',7,10,5,'Waiting','hninthetthetwai01@gmail.com','+959970307289','8 ward, Hnin Si Street, North Dangon Township, Myanmar','Male','1996/05/13',20,'167','58','Single','Junior school graduate','Right','No','No',0,'B-16.jpg','image/jpeg',6055,NULL,NULL,NULL,'','<p>現在はドーナッツ屋さんのレジのスタッフを１年間ほどしています。<br />\r\n日本で働きたい理由は、少しでも多くお金を稼ぎ家族に仕送りをしたいからだそうです。</p>',1,0,0,'',1,0,0,0,'','https://onedrive.live.com/?authkey=%21ABEfnfNBPriktkc&cid=7DDE2CFD3DB0EE0F&id=7DDE2CFD3DB0EE0F%215539&parId=7DDE2CFD3DB0EE0F%215538&o=OneUp','https://onedrive.live.com/?authkey=%21AADwqSSvWaQ0bqM&cid=7DDE2CFD3DB0EE0F&id=7DDE2CFD3DB0EE0F%215495&parId=7DDE2CFD3DB0EE0F%215472&o=OneUp','https://1drv.ms/b/s!Ag_usD39LN59rCeMoi9uXvy_k-9s','2016-11-04 03:34:01','2016-11-08 05:56:41'),(2,'B00018',3,0,'pwjstaff_admin','Thet Thet Aung',9,10,5,'Waiting','thetthetaung90@gmail.com','+959792541271','No.66, Thatkala Township, Myanmar','Female','1990/06/01',26,'163','64','Single','High school graduate','Right','No','No',0,'B-18.jpg','image/jpeg',6902,NULL,NULL,NULL,'','<p>現在か家族が経営しているレストランを手伝っています。過去に７年ほど工場で勤務しており、こちらで深夜勤務の経験があります。<br />\r\n日本で働きたい理由は経験を積みながらお金を稼ぎ、家族に仕送りをするためだそうです。<br />\r\n将来は両親の仕事を手伝っていきたいとのことです。</p>',1,0,0,'',1,0,0,0,'','https://onedrive.live.com/?cid=7dde2cfd3db0ee0f&id=7DDE2CFD3DB0EE0F%215540&authkey=%21AMrV90KQdLHEe2c','https://onedrive.live.com/?authkey=%21AMPvKxFZqmIk2fM&cid=7DDE2CFD3DB0EE0F&id=7DDE2CFD3DB0EE0F%215496&parId=7DDE2CFD3DB0EE0F%215472&o=OneUp','https://1drv.ms/b/s!Ag_usD39LN59rCmq8qoFC9QSv2I4','2016-11-04 03:52:14','2016-11-08 05:52:31'),(3,'B00019',3,0,'pwjstaff_admin','Theint Theint Soe',9,10,5,'Waiting','malthedarlay@gmail.com','+959791039628','No.(18)Kawa, Thatkala Township, Myanmar','Female','1990/12/15',25,'154','47','Single','University graduate','Right','No','No',0,'B-19.jpg','image/jpeg',6426,NULL,NULL,NULL,'','<p>現在は３年弱ほど美容師をしています。<br />\r\n過去に２年間ほどホテルのウエイトレスをしていました。<br />\r\n日本で働きたい理由は、稼いだお金を家族に仕送りするためです。<br />\r\nまた、将来の夢は自分の美容室を開くことだそうです。</p>',1,0,0,'',1,0,0,0,'','https://onedrive.live.com/?cid=7dde2cfd3db0ee0f&id=7DDE2CFD3DB0EE0F%215543&authkey=%21AP%2DeH%2DtCE9x4zfc','https://onedrive.live.com/?cid=7dde2cfd3db0ee0f&id=7DDE2CFD3DB0EE0F%215496&authkey=%21AMPvKxFZqmIk2fM','https://1drv.ms/b/s!Ag_usD39LN59rCgrrrHnbvnWn6tq','2016-11-04 04:09:56','2016-11-08 05:52:44'),(4,'B00020',3,0,'pwjstaff_admin','Thet Mar Min',9,10,5,'Waiting','thetmarmin8592@gmail.com','+959780283315','No.286, Banyaroo StreetTapinshwehtee Street, East Dangon, Myanmar','Female','1992/05/08',24,'163','50','Single','University graduate','Right','No','No',0,'B-20.jpg','image/jpeg',8848,NULL,NULL,NULL,'','<p>現在は４年間強海洋学校で経理のお仕事をしながら、実家の服屋のお手伝いもしています。<br />\r\n日本で働きたい理由は、家族の生活のために仕送りをしつつ、自立のために自分を磨きたいからだそうです。<br />\r\n稼いだお金は、家族に仕送りをする一方で、将来自身のレストランを開くために貯金したいとのことです。　</p>',1,0,0,'',1,0,0,0,'','https://1drv.ms/v/s!Ag_usD39LN59qynh2s16EFDB9ess','https://onedrive.live.com/?authkey=%21AFoQswzpG%2DxcqmQ&cid=7DDE2CFD3DB0EE0F&id=7DDE2CFD3DB0EE0F%215497&parId=7DDE2CFD3DB0EE0F%215472&o=OneUp','https://1drv.ms/b/s!Ag_usD39LN59rCqtbOFkGbd_plni','2016-11-04 04:33:19','2016-11-08 05:52:56'),(5,'B00046',NULL,0,'pwjstaff_admin','Aung Thu Min',8,10,5,'Waiting','aungthumin69@gmail.com','+959792294611','No.(2)Ward, Korkate Township, Myanmar','Male','1996/06/30',20,'160','52','Single','Junior school graduate','Right','No','No',1,'B-46.jpg','image/jpeg',6312,NULL,NULL,NULL,'','<p>現在は自営で４年間ほどドライバーをしています。<br />\r\n過去には２年間ほど車の洗車や修理のお仕事の経験もあり、そちらで清掃の経験もあります。<br />\r\n経験を積みながら、家族に仕送りをするために日本で働きたいそうです。また貯金もして、将来は携帯電話のお店を開きたいそうです。　</p>',1,0,0,'',1,0,0,0,'','https://1drv.ms/v/s!Ag_usD39LN59q3OiyUrHj6r8KAqO','https://1drv.ms/v/s!Ag_usD39LN59rA1tvqLshHvn5491','https://1drv.ms/b/s!Ag_usD39LN59rEoRyr0oQ1xc2yFi','2016-11-04 05:02:24','2016-11-08 04:36:53'),(6,'B00021',3,0,'pwjstaff_admin','Thiha Hlaing',4,10,5,'Waiting','thewisevanes@gmail.com','+959250795763','No.(774,C) Bo Tay Za Street, Saw Bwar Gyi Kone Ward, 10 Mile, Insein Township, Myanmar','Male','1986/05/07',30,'163','54','Single','University graduate','Right','No','No',0,'B-21.jpg','image/jpeg',7530,NULL,NULL,NULL,'','<p>現在は金融関係の会社のマネージャー兼スーパーバイザーをしています。<br />\r\n過去にホテルのウエイターや、２年ほどカタールのレストランでウエイターとしての勤務経験があります。<br />\r\n日本で働きたい理由は、経験や知識を習得しながらお金を稼ぎ、将来自分のレストランを開きたいそうです。</p>',1,0,0,'',1,0,0,0,'','https://onedrive.live.com/?cid=7dde2cfd3db0ee0f&id=7DDE2CFD3DB0EE0F%215544&authkey=%21AESWRtsg6x%2Dbfko','https://onedrive.live.com/?cid=7dde2cfd3db0ee0f&id=7DDE2CFD3DB0EE0F%215499&authkey=%21AIbrMANVMk%2DjJ8s','https://1drv.ms/b/s!Ag_usD39LN59rCyqYEME3h0-X2Nt','2016-11-04 05:03:59','2016-11-08 05:49:02'),(7,'B00022',3,0,'pwjstaff_admin','Kyaw Naing Win',8,10,5,'Waiting','kyawnaingwin94@gmail.com','+959454194470','No.106, Kayin Su Village, Nattaline Township, Myanmar','Male','1994/05/05',22,'172','59','Single','High school graduate','Right','Conversational','No',1,'B-22.jpg','image/jpeg',7294,NULL,NULL,NULL,'','<p>現在は１年間ほどレストランのお掃除の仕事をしています。<br />\r\n過去に３年ほど船舶の会社の倉庫での勤務経験があります。<br />\r\n日本の生活を経験しながら、自分のチャンスを探していきたいそうです。 稼いだお金は、将来自分のレストランを開くために貯金していきたいとのことです。</p>',1,0,0,'',1,0,0,0,'','https://onedrive.live.com/redir?resid=7DDE2CFD3DB0EE0F!5546&authkey=!AMSWeV1H6pFCGeo&ithint=video%2cmp4','https://onedrive.live.com/?cid=7dde2cfd3db0ee0f&id=7DDE2CFD3DB0EE0F%215500&authkey=%21AMnlwgBKsh4zGKg','https://1drv.ms/b/s!Ag_usD39LN59rCvpW5XKCOsbr6Zx','2016-11-04 05:13:57','2016-11-08 04:36:32'),(8,'C00004',3,0,'admin','Khun Khun',11,10,5,'Failed','khunkhun97@gmail.com','+959799471495','No, 42, Kandawlay Village, So Saing Township, Myanmar','Male','1997/01/01',19,'163','55','Single','Junior school graduate','Right','No','No',0,'B-23.jpg','image/jpeg',6480,NULL,NULL,NULL,'','',1,0,0,'<p>■　2016年11月07日（月）【有限会社アイ・エイ・シー】　不採用</p>',1,0,0,0,'','https://onedrive.live.com/?cid=7dde2cfd3db0ee0f&id=7DDE2CFD3DB0EE0F%215547&authkey=%21ALS4Q4JaIBjq9Dc','https://onedrive.live.com/?authkey=%21ALWByWxxrImKS%5Fg&cid=7DDE2CFD3DB0EE0F&id=7DDE2CFD3DB0EE0F%215501&parId=7DDE2CFD3DB0EE0F%215472&o=OneUp','https://onedrive.live.com/?authkey=%21ALWByWxxrImKS%5Fg&cid=7DDE2CFD3DB0EE0F&id=7DDE2CFD3DB0EE0F%215501&parId=7DDE2CFD3DB0EE0F%215472&o=OneUp','2016-11-04 05:18:21','2016-11-07 11:39:53'),(9,'B00047',NULL,0,'pwjstaff_admin','Than Kywe Oo',8,10,5,'Waiting','thankyweoo90@gmail.com','+959776311916','Shwe myaing thiri,  ward,Mawlamyaing Township,  Myanmar','Male','1990/04/20',26,'162','54','Single','University drop-out','Right','No','No',0,'B-47.jpg','image/jpeg',6907,NULL,NULL,NULL,'','<p>現在は塗装のお仕事をしています。<br />\r\n過去に４年間、タイにあるコットンの製造工場で働いた経験もあります。<br />\r\n日本で働きたい理由は日本語を勉強するため、日本での生活から経験や知識を得るためだそうです。<br />\r\n稼いだお金は家族に仕送りをするとともに将来電気屋を開くために使いたいとのことです。　</p>',1,0,0,'',1,0,0,0,'','https://1drv.ms/v/s!Ag_usD39LN59q3Ll4JwGiYAIWoKJ','https://1drv.ms/v/s!Ag_usD39LN59rA7qgObnJYWZK6RU','https://1drv.ms/b/s!Ag_usD39LN59rEs0SxC3SdD-0H5O','2016-11-04 05:18:34','2016-11-08 04:37:22'),(10,'B00024',3,0,'pwjstaff_admin','Nay Zaw Htway',6,10,5,'Waiting','nayzawhtwe87@gmail.com','+959791452254','No. 18, Hninsipan Street, Yay Kyaw, Pazutadaung Township, Myanmar','Male','1987/06/06',29,'170','64','Single','University graduate','Right','No','No',1,'B-24.jpg','image/jpeg',5652,NULL,NULL,NULL,'','<p>現在はバスとタクシーのドライバーをしています。<br />\r\n過去には７年ほど食材の配送をする船での勤務経験や１年ほどショッピングセンターの清掃員の経験があります。<br />\r\n日本で働きたい理由は、家族に仕送りをするためと自身の生活の向上のためだそうです。<br />\r\nまた、稼いだお金を使って、帰国後は日常品を販売する小さなお店を開きたいとのことです。</p>',1,0,0,'',1,0,0,0,'','https://onedrive.live.com/?cid=7dde2cfd3db0ee0f&id=7DDE2CFD3DB0EE0F%215624&ithint=video,mp4&authkey=!AC3gOiyBHs4ge5o','https://onedrive.live.com/?cid=7dde2cfd3db0ee0f&id=7DDE2CFD3DB0EE0F%215473&authkey=%21ALWz0GTIm%2Dbt6Ck','https://1drv.ms/b/s!Ag_usD39LN59rC5POR6so3KuqU4H','2016-11-04 05:23:19','2016-11-08 05:53:35'),(11,'B00025',3,0,'pwjstaff_admin','Kyaw Myo Naung',7,10,5,'Waiting','yuya38536@gmail.com','+959259458566','Ngalonetin ward, Mahtithar Village, Chaung Oo Township, Myanmar','Male','1995/05/05',21,'172','56','Single','Junior school graduate','Right','No','No',0,'B-25.jpg','image/jpeg',5720,NULL,NULL,NULL,'','<p>現在は６年間ほど耕作のお仕事をしています。<br />\r\n以前に３年間ほど大工のお仕事の経験もあります。<br />\r\n日本で働きたい理由は、家族への仕送りのため、自身の人生を良い方向へ持っていくために何かしたいからだそうです。<br />\r\n将来は機械のお店を開きたいとのことです。</p>',1,0,0,'',1,0,0,0,'','https://onedrive.live.com/?cid=7dde2cfd3db0ee0f&id=7DDE2CFD3DB0EE0F%215548&authkey=%21APU4ChkyLnqGJBk','https://onedrive.live.com/?cid=7dde2cfd3db0ee0f&id=7DDE2CFD3DB0EE0F%215475&ithint=video,mp4&authkey=!AI8d1Shd6--OlEI','https://1drv.ms/b/s!Ag_usD39LN59rC9qT2jv-EJVbmoB','2016-11-04 05:36:43','2016-11-08 05:56:55'),(12,'B00048',NULL,0,'pwjstaff_admin','Thiha Aung',8,10,5,'Waiting','thihaaung91@gmail.com','+959454434216','Taw Kuu Village,  Mudon Township,  Myanmar','Male','1991/05/01',25,'154','52','Married','Junior school graduate','Right','No','No',0,'B-48.jpg','image/jpeg',7136,NULL,NULL,NULL,'','<p>現在は日本語の勉強のみしています。　<br />\r\n以前は９年間ほどタイで大工のお仕事をしていました。そちらで深夜勤務の経験もあります。<br />\r\n結婚はしていますが、子供はいません。<br />\r\n日本で働きたい理由は、家族への仕送りのと貯金のためだそうです。<br />\r\n貯金したお金を使って将来は、生活用品を販売するお店を開くことが夢です。</p>',1,0,0,'',1,0,0,0,'','https://1drv.ms/v/s!Ag_usD39LN59q3RKDQM8XzUKR5UI','https://1drv.ms/v/s!Ag_usD39LN59rA_Z-LYJEb2_UHed','https://1drv.ms/b/s!Ag_usD39LN59rE35OQPokauxIqNs','2016-11-04 05:45:49','2016-11-08 06:12:25'),(13,'C00005',3,0,'admin','Phyo Wai Oo',11,10,5,'Failed','kophyp1992@gmail.com','+959960848660','Kanchaung Village, Kanchaung Ward, Yegyi Township, Myanmar','Male','1992/11/10',23,'162','54','Single','Junior school drop out','Right','No','No',1,'B-26.jpg','image/jpeg',6937,NULL,NULL,NULL,'','',1,0,0,'<p>■　2016年11月07日（月）【有限会社アイ・エイ・シー】　不採用</p>',1,0,0,0,'','https://onedrive.live.com/?cid=7dde2cfd3db0ee0f&id=7DDE2CFD3DB0EE0F%215549&ithint=video,mp4&authkey=!AMkmS3sFYDMiTAw','https://onedrive.live.com/?cid=7dde2cfd3db0ee0f&id=7DDE2CFD3DB0EE0F%215642&authkey=%21ACAUXNfSdxxC0tw','https://1drv.ms/b/s!Ag_usD39LN59rDBKwAhJirlTPz80','2016-11-04 05:51:43','2016-11-07 11:40:00'),(14,'A00002',3,0,'admin','Min Min Soe',3,10,5,'Confirmed','minminsoe91@gmail.com','+95943085104','No.Nya/3,under mingalardon Street, Phawt Kan, Insein Township, Myanmar','Male','1991/02/01',25,'165','55','Single','Junior school graduate','Right','No','No',1,'B-27.jpg','image/jpeg',6245,NULL,NULL,NULL,'内定②','',1,0,0,'<p>■　2016年11月07日（月）【有限会社アイ・エイ・シー】　採用</p>',1,0,0,0,'','https://onedrive.live.com/?cid=7dde2cfd3db0ee0f&id=7DDE2CFD3DB0EE0F%215551&ithint=video,mp4&authkey=!AIPjJ6YF_BkhygQ','https://onedrive.live.com/?cid=7dde2cfd3db0ee0f&id=7DDE2CFD3DB0EE0F%215474&authkey=%21AA%2D%2DAxAO%5FE5Ou0g','https://1drv.ms/b/s!Ag_usD39LN59rDGf58IpusIyUqlP','2016-11-04 05:59:04','2016-11-07 12:11:44'),(15,'B00049',NULL,0,'pwjstaff_admin','Min Zayar Kyaw',6,10,3,'Waiting','boy.newstyle.2010@gmail.com','+959975650086','No.(53),156th Street,  Tamwe township,  Myanmar','Male','1990/11/18',25,'175','62','Married','University graduate','Right','No','No',0,'B-49.jpg','image/jpeg',6936,NULL,NULL,NULL,'','<p>現在は携帯電話の販売店で販売や商品の入れ替えなどのお仕事をしています。<br />\r\n以前３か月間ほどタイのココナッツ精製工場での勤務経験があります。<br />\r\n日本語を勉強しながら、家族に仕送りをするために日本で働きたいそうです。また、稼いだお金は貯金もして将来携帯電話のお店を開くためにつかいたいそうです。</p>',1,0,0,'',1,0,0,0,'','https://1drv.ms/v/s!Ag_usD39LN59q3ZER32yafex-TUL','https://1drv.ms/v/s!Ag_usD39LN59rBGodEZwhXRan1a_','https://1drv.ms/b/s!Ag_usD39LN59rEkNXK2lxNlGr3eh','2016-11-04 05:59:50','2016-11-08 05:55:01'),(16,'B00030',3,0,'pwjstaff_admin','Myo Min Tun',4,10,5,'Waiting','myomintun91@gmail.com','+959262150867','No. 972,(9)ward, Tapinshwehtee Street, North Dangon, Myanmar','Male','1991/12/21',24,'170','65','Single','Junior school graduate','Right','No','No',1,'B-30.jpg','image/jpeg',4997,NULL,NULL,NULL,'','<p>現在は車屋のスタッフとして働いています。<br />\r\n過去にマレーシアで４年間ほどホテルの中のレストランのウェイターをしていました。こちらで、深夜勤務と清掃業務の経験があります。<br />\r\n日本で働きたい理由は、お金を稼ぎ家族に仕送りをするためだそうです。また、稼いだお金を貯めて将来は自分車のショールームを開きたいとのことです。</p>',1,0,0,'',1,0,0,0,'','https://onedrive.live.com/?authkey=%21AMYcwiTd%5Fem5LXA&cid=7DDE2CFD3DB0EE0F&id=7DDE2CFD3DB0EE0F%215550&parId=7DDE2CFD3DB0EE0F%215538&o=OneUp','https://onedrive.live.com/redir?resid=7DDE2CFD3DB0EE0F!5627&authkey=!AHZi_t0tWzZt5co&ithint=video%2cmp4','https://1drv.ms/b/s!Ag_usD39LN59rDOJDHqjNlFbLref','2016-11-04 06:05:08','2016-11-08 05:49:18'),(17,'B00031',3,0,'pwjstaff_admin','Mg Mg Zaw',7,10,5,'Waiting','mgmgzaw92@gmail.com','+95931551197','No.(5),4th Floor, Kyauk Myaung Gyi Quarter, Tamwe Township, Myanmar','Male','1992/01/01',24,'165','57','Single','Junior school graduate','Right','No','No',1,'B-31.jpg','image/jpeg',6092,NULL,NULL,NULL,'','<p>現在は６年弱ほど建設のお仕事をしており、こちらで清掃業務の経験もあります。過去に３年間ほどスーパーマーケットで洋服の販売をしていた経験もあります。<br />\r\n日本で働きたい理由は、家族に仕送りをしながら自身の生活を改善していきたいからだそうです。<br />\r\n仕送りをする一方で、将来自身の洋服屋を開くために貯金もしていきたいそうです。　</p>',1,0,0,'',1,0,0,0,'','https://onedrive.live.com/?authkey=%21AP2I6lV8VUImGew&cid=7DDE2CFD3DB0EE0F&id=7DDE2CFD3DB0EE0F%215552&parId=7DDE2CFD3DB0EE0F%215538&o=OneUp','https://onedrive.live.com/?cid=7dde2cfd3db0ee0f&id=7DDE2CFD3DB0EE0F%215478&authkey=%21AKc96jZ2%5FiEy%5FH4','https://1drv.ms/b/s!Ag_usD39LN59rDUJdnD612tw_uCD','2016-11-04 06:12:33','2016-11-08 05:57:09'),(18,'B00050',NULL,0,'pwjstaff_admin','Raj Kumar',6,10,4,'Waiting','rajkumar90@gmail.com','+959794136186','No.(152),Htawinbell Ward,  Sein Chay Street,(North),  Okkalarpa Township,  Myanmar','Male','1990/09/06',26,'167','53','Single','University graduate','Right','No','No',0,'B-50.jpg','image/jpeg',6299,NULL,NULL,NULL,'','<p>現在お仕事はしていませんが、以前には倉庫での従業員やバイクの部品の販売の経験があります。３年ほど、マレーシアの機械工場での勤務経験もあります。マレーシア語も話すことができます。<br />\r\n家族に仕送りをするために日本で働きたいそうです。<br />\r\nまた、貯金もして将来バイクのお店を開くのに使いたいそうです。</p>',1,0,0,'',1,0,0,0,'','https://1drv.ms/v/s!Ag_usD39LN59q3VFp4fecEnthiu1','https://1drv.ms/v/s!Ag_usD39LN59q30wX_LpXCsFOcYx','https://1drv.ms/b/s!Ag_usD39LN59rExL702CkTFxQmUp','2016-11-04 06:18:39','2016-11-08 05:55:14'),(19,'B00051',NULL,0,'pwjstaff_admin','Soe Min Tun',4,10,5,'Waiting','soemintun91@gmail.com','+959264572650','39 Street,Middle Block,  Kyauktadar Township,  Myanmar','Male','1991/02/05',25,'180','72','Single','University graduate','Right','No','No',0,'B-51.jpg','image/jpeg',6092,NULL,NULL,NULL,'','<p>現在は３年ほど建設会社で働いており、こちらで深夜勤務の経験もあります。<br />\r\n以前は、車の部品のお店でマーケティングやセールスのお仕事もした経験があります。<br />\r\n日本で働きたい理由は、日本の「ルールを守る文化」を体験しながらお金を稼ぎ家族に仕送りをしたいそうです。<br />\r\nまた、稼いだお金は将来車の販売のお店を開くために使い、<br />\r\n残りは子供たちに寄付をしたいそうです。</p>',1,0,0,'',1,0,0,0,'','https://1drv.ms/v/s!Ag_usD39LN59q2TyZLyC_Z78kMaQ','https://1drv.ms/v/s!Ag_usD39LN59rAJ136rxDfziaDvv','https://1drv.ms/b/s!Ag_usD39LN59rE5ZcLLrCjMDAdTj','2016-11-04 06:24:47','2016-11-08 05:51:20'),(20,'B00052',NULL,0,'pwjstaff_admin','Min Thein Kyaw',6,10,5,'Waiting','mintheinkyaw92@gmail.com','+959448049343','Building(6),Room(104),  Shwe Htee Housing,Thamine,  Mayankone Township,  Myanmar','Male','1992/07/26',24,'177','63','Single','High school graduate','Right','Conversational','No',0,'B-52.jpg','image/jpeg',5046,NULL,NULL,NULL,'','<p>現在は外国からのビジネスマン専用の車のドライバーをしています。<br />\r\n以前にタクシーのドライバーの経験も２年ほどあります。<br />\r\nお金を稼ぎ家族に仕送りするために日本で働きたいそうです。<br />\r\nまた、貯金もして将来自身のコーヒーショップを開きたいそうです。</p>',1,0,0,'',1,0,0,0,'','https://1drv.ms/v/s!Ag_usD39LN59q2OvvreQ2Y_-ZBoJ','https://1drv.ms/v/s!Ag_usD39LN59rAGrRl5g4CAckDn2','https://1drv.ms/b/s!Ag_usD39LN59rEh9Vzv3didOHVaz','2016-11-04 06:31:37','2016-11-08 05:55:33'),(21,'B00053',NULL,0,'pwjstaff_admin','Phyu Phyu Aye',9,10,5,'Waiting','maylay980912@gmail.com','+959421740373','No.(603),(B),San Yeik,  Nyain(6) Street, Hledan,Kamayut Township,  Myanmar','Female','1989/06/01',27,'154','60','Single','University graduate','Right','No','No',0,'B-53.jpg','image/jpeg',6081,NULL,NULL,NULL,'','<p>現在は８年間ほど美容師をしています。こちらで深夜勤務の経験もあります。<br />\r\n家族に仕送りをするために日本で働きたいそうです。<br />\r\nまた、稼いだお金は将来自分の美容室を開くために使いたいそうです。</p>',1,0,0,'',1,0,0,0,'','https://1drv.ms/v/s!Ag_usD39LN59q2IQFWpkMrkBkAYs','https://1drv.ms/v/s!Ag_usD39LN59q3rperyu4Imqxwew','','2016-11-04 06:40:42','2016-11-08 05:53:09'),(22,'B00055',NULL,0,'pwjstaff_admin','Aung Min Thu',4,10,5,'Waiting','aungminthu!@gmail.com','+959792815538','No.(26), 2 Floor (B),  Zanihla Street, Tamwe,  Township,  Myanmar','Male','1996/08/23',20,'170','68','Single','Junior school graduate','Right','No','No',0,'B-55.jpg','image/jpeg',5877,NULL,NULL,NULL,'','<p>現在は建設会社で主にコンクリートを練るお仕事をしています。以前は７年間ほど農業のお仕事をしていました。<br />\r\nお金を稼ぐために日本で働きたいそうで、そのお金で将来自身の車の部品を販売するお店を開きたいそうです。</p>',1,0,0,'',1,0,0,0,'','https://1drv.ms/v/s!Ag_usD39LN59q2duHD8gDZXtiwti','https://1drv.ms/v/s!Ag_usD39LN59rBAbkfIifMP06271','https://1drv.ms/b/s!Ag_usD39LN59rEbYLnOkut3iOjw1','2016-11-04 06:48:43','2016-11-08 05:51:38'),(23,'B00001',3,0,'pwjstaff_admin','Than Naing Swe',7,10,5,'Waiting','tinnaingswe85@gmail.com','+959791593930','Tuu Yin Bot Village, Tuu Yin Bot Tract, Myingyan Township, Myanmar','Male','1985/05/31',31,'162','65','Married','Junior school graduate','Right','No','No',1,'B-01.jpg','image/jpeg',6907,NULL,NULL,NULL,'','<p>現在はドライバーのお仕事をしています。<br />\r\n過去にシンガポールで２年ほど船の塗装の経験、７年間ほどの建設会社でのポリッシャーの経験があります。結婚しており、４歳の娘が一人います。<br />\r\n日本語を学ぶために日本で仕事がしたいそうで、<br />\r\n日本で稼いだお金は子供の将来のために使いたいそうです。</p>',1,0,0,'',1,0,0,0,'','https://onedrive.live.com/redir?resid=7DDE2CFD3DB0EE0F!5601&authkey=!AFhOeKonpEctV1M&ithint=video%2cmp4','https://onedrive.live.com/?cid=7dde2cfd3db0ee0f&id=7DDE2CFD3DB0EE0F%215487&authkey=%21ABlXHEmJf%5FBNndQ','https://1drv.ms/b/s!Ag_usD39LN59rDqIUMloE8XbTv8e','2016-11-04 06:49:47','2016-11-08 05:56:15'),(24,'B00056',NULL,0,'pwjstaff_admin','Soe Thiha',7,10,5,'Waiting','soethina@gmail.com','+959263163422','No.12/5 Floor (A),  Pon Nya Wohtana Street,  Tamwe Township,  Myanmar','Male','1996/01/03',20,'175','68','Single','Junior school graduate','Right','No','No',0,'B-56.jpg','image/jpeg',5240,NULL,NULL,NULL,'','<p>現在は３年強ほど車の整備のお仕事をしています。こちらで深夜勤務の経験もあります。<br />\r\n稼いだお金を家族に仕送りするために日本で働きたいそうです。<br />\r\nまた、貯金もして将来は車の部品を販売するお店を開きたいとのことです。</p>',1,0,0,'',1,0,0,0,'','https://1drv.ms/v/s!Ag_usD39LN59q2Xvsp-OTssfurAu','https://1drv.ms/v/s!Ag_usD39LN59q3-x9myOid0j_tCN','https://1drv.ms/b/s!Ag_usD39LN59rEVMQC6wOA4OHM2c','2016-11-04 06:57:23','2016-11-08 06:01:28'),(25,'B00004',3,0,'admin','Tun Myo Aung',8,10,5,'Failed','tunmyoaung@gmail.com','+959777700259','No-42,41-Street, Bo Htataung Township, Myanmar','Male','1981/02/01',35,'162','75','Single','University drop-out','Right','No','No',0,'B-04.jpg','image/jpeg',6304,NULL,NULL,NULL,'','<p>現在は建設会社で４年間ほど部屋の内装のお仕事をしています。<br />\r\n以前に韓国でお酒の工場での機械オペレーター、携帯電話の部品の組み立てのお仕事の経験があります。韓国には３年ほど滞在していました。<br />\r\n日本で経験を積みながら、家族の生活を支えるために日本で働きたいそうです。将来の夢はホテルの経営をすることです。</p>',1,0,0,'<p>■　2016年11月07日（月）【有限会社アイ・エイ・シー】　不採用</p>',0,0,0,0,'','https://onedrive.live.com/?cid=7dde2cfd3db0ee0f&id=7DDE2CFD3DB0EE0F%215554&authkey=%21AKSRkAaWh7k40rk','https://onedrive.live.com/?authkey=%21AM3OQmkrz1pgBg8&cid=7DDE2CFD3DB0EE0F&id=7DDE2CFD3DB0EE0F%215490&parId=7DDE2CFD3DB0EE0F%215472&o=OneUp','https://1drv.ms/b/s!Ag_usD39LN59rB0hLFYLFmG5aEl2','2016-11-04 06:59:07','2016-11-08 06:31:47'),(26,'A00001',3,0,'admin','Moe Win',3,10,5,'Confirmed','moewin83@gmail.com','+959254805101','Kan Pyo Village, Pauk Township, Myanmar','Male','1983/06/01',33,'170','70','Single','University drop-out','Right','No','No',0,'B-06.jpg','image/jpeg',6511,NULL,NULL,NULL,'内定①','',1,0,0,'<p>■　2016年11月07日（月）【有限会社アイ・エイ・シー】　採用</p>',1,0,0,1,'','https://onedrive.live.com/?cid=7dde2cfd3db0ee0f&id=7DDE2CFD3DB0EE0F%215623&authkey=%21AMPSsS8p%2DQA2yOw','https://onedrive.live.com/?cid=7dde2cfd3db0ee0f&id=7DDE2CFD3DB0EE0F%215650&authkey=%21AD8epY4DH%5F%5FnAkI','https://1drv.ms/b/s!Ag_usD39LN59rB-gQJJ1-xvzBsfE','2016-11-04 07:05:02','2016-11-07 12:11:31'),(27,'C00002',3,0,'admin','Aung Kyi San',11,10,5,'Failed','aungkyisan86@gmail.com','+959457338937','A Lal Kyun Village, Alal Kyun Tract, Tap Kone Tract Township, Myanmar','Male','1986/07/07',30,'165','65','Single','University drop-out','Right','No','No',0,'B-08.jpg','image/jpeg',6697,NULL,NULL,NULL,'','',1,0,0,'<p>■　2016年11月07日（月）【有限会社アイ・エイ・シー】　不採用</p>',1,0,0,0,'','https://onedrive.live.com/?cid=7dde2cfd3db0ee0f&id=7DDE2CFD3DB0EE0F%215553&authkey=%21AFsWcGCOytJ6iHE','https://onedrive.live.com/?cid=7dde2cfd3db0ee0f&id=7DDE2CFD3DB0EE0F%215489&authkey=%21APG5UpQmlTzZCRg','https://1drv.ms/b/s!Ag_usD39LN59rCF4gbWw-AT-WVx5','2016-11-04 07:13:06','2016-11-07 11:39:43'),(28,'B00012',3,0,'pwjstaff_admin','Win Ko Ko',7,10,5,'Waiting','limlimoo7w@gmail.com','+959784636402','Phar Aung Village, 7 Quarter, Mawlamyaing Township, Myanmar','Male','1992/06/14',24,'172','75','Single','Junior school graduate','Right','No','No',0,'B-12.jpg','image/jpeg',4207,NULL,NULL,NULL,'','<p>現在はエアコンの設置や溶接のお仕事をしています。過去に６年ほどタイでお仕事の経験があります。２年間はのシーフードの製造工場勤務、４年間は溶接工として働いており、こちらで深夜勤務も経験しています。<br />\r\n日本でお金を稼ぎ、家族に仕送りをしたいそうです。<br />\r\n帰国後は、自分のビジネスをしたいとのことでした。</p>',1,0,0,'',1,0,0,0,'','https://onedrive.live.com/?cid=7dde2cfd3db0ee0f&id=7DDE2CFD3DB0EE0F%215541&ithint=video,mp4&authkey=!AOKkykVDlvhrZZ0','https://onedrive.live.com/?cid=7dde2cfd3db0ee0f&id=7DDE2CFD3DB0EE0F%215493&authkey=%21AIh3mQe%2DS%2DGRTcw','https://1drv.ms/b/s!Ag_usD39LN59rCW4bl_Vetu_J5pX','2016-11-04 07:21:55','2016-11-08 05:56:27'),(29,'B00011',3,0,'pwjstaff_admin','Zar Ni Maung',8,10,5,'Waiting','zarnimaung636@gmail.com','+95945796549','Ma Ti Thar Village, Ngar Lone,Tin Tract, Chaung Oo Township, Myanmar','Male','1991/10/02',25,'175','65','Single','Junior school graduate','Right','No','No',0,'B-11.jpg','image/jpeg',5979,NULL,NULL,NULL,'','<p>現在は実家の農業を手伝いながら自営で配送などのドライバーをしています。<br />\r\n以前に２年間ほどマレーシアのパイプの加工工場での就業経験があります。<br />\r\n日本で働きたい理由はお金を稼せぎ家族に仕送りをするためで、帰国したら「農業を発展させる」ために何かしていきたいとのことです。</p>',1,0,0,'<p>■　2016年11月07日（月）【有限会社アイ・エイ・シー】　第１補欠</p>',0,0,0,1,'','https://onedrive.live.com/?cid=7dde2cfd3db0ee0f&id=7DDE2CFD3DB0EE0F%215555&authkey=%21AEUZjrzROSNUNRc','https://onedrive.live.com/?cid=7dde2cfd3db0ee0f&id=7DDE2CFD3DB0EE0F%215492&authkey=%21ABTaRP3j7BnltQk','https://1drv.ms/b/s!Ag_usD39LN59rCTb2uZPa0tR4REc','2016-11-04 07:27:58','2016-11-08 06:14:35'),(30,'A00003',3,0,'jidouka','Min Aung Min Tun',3,10,5,'Confirmed','minaungmintun93@gmail.com','+959777291779','3-Taungsun Village, Chaung Sone Township, Myanmar','Male','1983/06/15',33,'162','56','Single','University graduate','Right','No','No',0,'B-38.jpg','image/jpeg',6587,NULL,NULL,NULL,'内定③','',1,0,0,'<p>■　2016年11月07日（月）【有限会社アイ・エイ・シー】　採用</p>',1,0,0,1,'','https://onedrive.live.com/?cid=7dde2cfd3db0ee0f&id=7DDE2CFD3DB0EE0F%215557&ithint=video,mp4&authkey=!AJgycVn4jDu1qT8','https://onedrive.live.com/?cid=7dde2cfd3db0ee0f&id=7DDE2CFD3DB0EE0F%215482&authkey=%21AKQ56%2DaoyMBDNVQ','https://1drv.ms/b/s!Ag_usD39LN59rBb5haMqQN019kd5','2016-11-04 07:35:08','2016-11-08 06:31:26'),(31,'B00035',NULL,0,'pwjstaff_admin','Min Saw Nyunt',4,10,5,'Waiting','minsawnyunt@gmail.com','+959778139213','Thahtaykone Village,  Thanapin Township,  Myanmar','Male','1991/07/25',25,'165','59','Married','Junior school graduate','Right','No','No',0,'B-35.jpg','image/jpeg',7474,NULL,NULL,NULL,'','<p>現在は実家の農業のお手伝いをしています。<br />\r\n以前は４年弱ほどマレーシアの印刷工業での勤務経験があります。こちらで深夜勤務も経験しています。<br />\r\n結婚６年目ですが、子供はいません。<br />\r\nお金を稼ぐために日本で働きたいそうで、<br />\r\n稼いだお金は家族に仕送りする一方で自身の将来のために使いたいそうです。</p>',1,0,0,'',1,0,0,0,'','https://1drv.ms/v/s!Ag_usD39LN59q2u2tDRqIfemPg2Y','https://1drv.ms/v/s!Ag_usD39LN59qmnbzNY5XUEJaXR6','https://1drv.ms/b/s!Ag_usD39LN59rEQyR_fgHCt6CCuW','2016-11-04 07:43:27','2016-11-08 05:50:00'),(32,'B00036',NULL,0,'pwjstaff_admin','Thura Tun',6,10,5,'Failed','thuratun94@gmail.com','+959256066098','Ingyin Myaing(3)street,  47 Ward,North Dagon,  Myanmar','Male','1994/09/24',22,'167','57','Single','Junior school drop out','Right','No','No',0,'B-36.jpg','image/jpeg',6106,NULL,NULL,NULL,'','<p>現在は２年ほど、車の販売のお仕事をしています。<br />\r\nお金を稼ぐために日本で働きたく、<br />\r\n稼いだお金は家族に仕送りする一方で、<br />\r\n自分が教育をきちんと受けることができなかったので孤児の教育支援に使いたいそうです。　</p>',1,0,0,'<p>■　2016年11月07日（月）【有限会社アイ・エイ・シー】　不採用</p>',1,0,0,0,'','https://1drv.ms/v/s!Ag_usD39LN59q2kzaTSTnpG71zLB','https://1drv.ms/v/s!Ag_usD39LN59qmdnxVxrZIMquQKM','https://onedrive.live.com/?cid=7DDE2CFD3DB0EE0F&id=7DDE2CFD3DB0EE0F%215689&parId=7DDE2CFD3DB0EE0F%215652&o=OneUp','2016-11-04 07:47:41','2016-11-08 05:54:04'),(33,'B00032',3,0,'pwjstaff_admin','Tin Ko Ko',7,10,5,'Waiting','tintinkoko90@gmail.com','+959973749162','No.(50), North, Tarpon, Nattaline Township, Myanmar','Male','1990/04/21',26,'167','54','Single','Junior school graduate','Right','No','No',0,'B-32.jpg','image/jpeg',5252,NULL,NULL,NULL,'','<p>現在は父とともに農業と大工のお仕事をしています。<br />\r\n以前は３年ほどシンガポールで船の修理やパイプ加工、組み立てなどのお仕事をしていました。<br />\r\n日本は「発展している」というイメージがあり、お金を稼ぐために日本で働きたいそうです。稼いだお金は貯金をして、将来洋服の生地を販売するお店を開きたいそうです。</p>',1,0,0,'',1,0,0,0,'','https://onedrive.live.com/redir?resid=7DDE2CFD3DB0EE0F!5606&authkey=!AKPQR7wB4VJxdx0&ithint=video%2cmp4','https://onedrive.live.com/?cid=7dde2cfd3db0ee0f&id=7DDE2CFD3DB0EE0F%215477&authkey=%21ANKph5mAfmUPdkw','https://1drv.ms/b/s!Ag_usD39LN59rDTd2H-8JR1603JL','2016-11-04 07:55:30','2016-11-08 05:57:23'),(34,'B00033',3,0,'pwjstaff_admin','Aung Lin Tun',4,10,5,'Waiting','Myat.Myat2012@gmail.com','+989790250171','No.(50), Hmyawlint(2) Street, Shwe myaing thiri ward, Mawlamyaing Township, Myanmar','Male','1987/11/04',29,'168','68','Single','University graduate','Right','No','No',0,'B-33.jpg','image/jpeg',6893,NULL,NULL,NULL,'','<p>現在はハイネケンとの合弁会社で働いています。こちらでは様々な国の同僚と関わっており、たまに深夜勤務も経験しています。<br />\r\n日本語の勉強をするために、日本で働きたく、最終的に学んだ日本語をミャンマーの子供たちに教えたいそうです。<br />\r\n日本で稼いだお金は、将来何等かの商売をするために使いたいそうです。</p>',1,0,0,'',1,0,0,0,'','https://onedrive.live.com/?cid=7dde2cfd3db0ee0f&id=7DDE2CFD3DB0EE0F%215608&authkey=%21AOBHmLNd3y5Y1XA','https://onedrive.live.com/?cid=7dde2cfd3db0ee0f&id=7DDE2CFD3DB0EE0F%215476&authkey=%21AD4N8bLvpcRcx1M','https://1drv.ms/b/s!Ag_usD39LN59rDa1ujPwDPXkIPxl','2016-11-04 07:59:49','2016-11-08 05:49:33'),(35,'B00034',3,0,'pwjstaff_admin','Saw Shar Kaw Htoo',4,10,5,'Waiting','sawkawsharhtoo90@gmail.com','+959777154158','Phado(4) eard, Kyauk Ta Khar Township, Myanmar','Male','1990/01/02',26,'162','54','Single','Junior school graduate','Right','No','No',0,'B-34.jpg','image/jpeg',6931,NULL,NULL,NULL,'','<p>現在は実家の農業のお手伝いをしています。<br />\r\n以前は２年間ほどタイで溶接のお仕事をしていました。<br />\r\n日本語を学ぶために日本で働きたいそうです。<br />\r\n稼いだお金は家族に仕送りをする一方で、将来帰国したら自身の商売をするために使いたいそうです。　　</p>',1,0,0,'',1,0,0,0,'','https://onedrive.live.com/?cid=7dde2cfd3db0ee0f&id=7DDE2CFD3DB0EE0F%215610&authkey=%21AMEo8WW8lKIoh%2DA','https://onedrive.live.com/?cid=7dde2cfd3db0ee0f&id=7DDE2CFD3DB0EE0F%215480&ithint=video,mp4&authkey=!AN2blE19hbSNT9o','https://1drv.ms/b/s!Ag_usD39LN59rDdzlmdcUUY_BOK6','2016-11-04 08:04:51','2016-11-08 05:49:47'),(36,'B00037',NULL,1,'jidouka','Aung Ko Ko',6,10,5,'Waiting','aungkoko97@gmail.com','+959793892908','No.(25),Part(1),West Myo,  Pat Street,  Sayarsan west/north(Bahan),  Myanmar','Male','1992/07/05',24,'167','60','Single','Junior school graduate','Right','No','No',0,'B-37.jpg','image/jpeg',6701,NULL,NULL,NULL,'','<p>現在は日本語のお勉強のみをしています。<br />\r\n以前に、電気工事のお仕事や車の部品の販売のお仕事の経験があります。<br />\r\nお金を稼ぐために日本で働きたいそうで、稼いだお金は家族に仕送りをするそうです。<br />\r\n将来の夢は自身のレストランを開くことです。　</p>',1,0,0,'',1,0,0,0,'','https://1drv.ms/v/s!Ag_usD39LN59q20CUzqu1dSUoCEO','https://1drv.ms/v/s!Ag_usD39LN59qmwgKTjoGLz-RMWT','https://1drv.ms/b/s!Ag_usD39LN59rDjDONryf9ZrarqN','2016-11-04 08:10:07','2016-11-08 06:32:40'),(37,'B00039',NULL,0,'pwjstaff_admin','Saw Htee Khu Wah',4,10,5,'Waiting','sawhteekhuwah90@gmail.com','+959777384867','Se sone kone Village,Phado,  City,   Kyauk Ta Khar Township,  Myanmar','Male','1990/09/22',26,'165','55','Single','Junior school drop out','Right','No','N5',0,'B-39.jpg','image/jpeg',5899,NULL,NULL,NULL,'','<p>現在は実家の農業のお手伝いをしながら、村の人と一緒に大工のお仕事もしています。<br />\r\n家族のサポートのために日本で仕事がしたく、稼いだお金を仕送りしたいそうです。将来の夢は建築材料のお店を作りたいそうです。　</p>',1,0,0,'',1,0,0,0,'','https://1drv.ms/v/s!Ag_usD39LN59q2z6O9d8LvwTNowB','https://1drv.ms/v/s!Ag_usD39LN59qmuGpE4CSgqQ5y_i','https://1drv.ms/b/s!Ag_usD39LN59rBWYRJppaOEB2fvm','2016-11-04 08:18:56','2016-11-08 05:50:20'),(38,'B00041',NULL,0,'pwjstaff_admin','Pyae Phyo Paing Soe',4,10,5,'Waiting','pyaephyopaingsoe89@gmail.com','+959263683412','No.(479)/ka,Wai pon la (1),  Street,  20 Ward,Hlaing Thar Yar,  Township,  Myanmar','Male','1989/06/04',27,'167','56','Single','University graduate','Right','No','No',0,'B-41.jpg','image/jpeg',7140,NULL,NULL,NULL,'','<p>現在はヤンゴンの喫茶店で働いています。<br />\r\n以前はマレーシアのチョコレートの製造機械の洗浄のお仕事をしていました。<br />\r\nお金を稼ぐために日本で働きたいそうで、将来は自身の喫茶店やお店を開きたいとのことです。　</p>',1,0,0,'',1,0,0,0,'','https://1drv.ms/v/s!Ag_usD39LN59q2-uLPWce1D78AdF','https://1drv.ms/v/s!Ag_usD39LN59qm0VVphrbj3H_BLo','https://1drv.ms/b/s!Ag_usD39LN59rBfAv6i9qQibrq1D','2016-11-04 08:26:03','2016-11-08 05:51:03'),(39,'B00042',NULL,0,'pwjstaff_admin','Aung Ko Ko',7,10,5,'Waiting','aungkoko92@gmail.com','+959421028094','No.(670(b)Maharbandoola,  Street,  (na-gyi(14)ward,Panchan(12,  ),Shwe Pyi Thar,  Myanmar','Male','1997/06/05',19,'163','50','Single','Junior school graduate','Right','No','No',0,'B-42.jpg','image/jpeg',6738,NULL,NULL,NULL,'','<p>現在はエアコンの設置のお仕事をしています。<br />\r\nこちらで深夜勤務の経験もあります。<br />\r\nお金を稼ぐために日本で働きたく、貯めたお金で日本語の勉強をして将来はミャンマーで通訳のお仕事をしたいそうです。</p>',1,0,0,'',1,0,0,0,'','https://1drv.ms/v/s!Ag_usD39LN59q3DjViy6eh9U1BOP','https://1drv.ms/v/s!Ag_usD39LN59rAxOZEtC56fTIbZj','https://1drv.ms/b/s!Ag_usD39LN59rBjKnhQlDNLuR6VX','2016-11-04 08:32:19','2016-11-08 05:58:13'),(40,'B00043',NULL,0,'pwjstaff_admin','Nay Lin Soe',7,10,5,'Waiting','naylinsoe@gmail.com','+959423714815','Pyi Sin Kone Village,Son,  Kone Ward,  Moe Nyo Township,  Myanmar','Male','1987/02/13',29,'177','68','Single','High school graduate','Right','No','No',0,'B-43.jpg','image/jpeg',7664,NULL,NULL,NULL,'','<p>現在は実家の農業のお手伝いをしながら、豆の栽培や販売のお仕事もしています。こちらで時々深夜勤務も経験しています。<br />\r\n日本でお金を稼ぎ、家族の生活を支えていきたいとのことです。<br />\r\nまた、将来の夢は自分の商売をすることだそうです。</p>',1,0,0,'',1,0,0,0,'','https://1drv.ms/v/s!Ag_usD39LN59q3FYiuVuG41aek9D','https://1drv.ms/v/s!Ag_usD39LN59q3zcFwNTZj0_8b3W','https://1drv.ms/b/s!Ag_usD39LN59rEfstHoWqPwe6kyw','2016-11-04 08:41:04','2016-11-08 06:01:14');
/*!40000 ALTER TABLE `applicants` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aptitude_tests`
--

DROP TABLE IF EXISTS `aptitude_tests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aptitude_tests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `en_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `jp_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_updated_by` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aptitude_tests`
--

LOCK TABLES `aptitude_tests` WRITE;
/*!40000 ALTER TABLE `aptitude_tests` DISABLE KEYS */;
INSERT INTO `aptitude_tests` VALUES (1,'Food processor','食品工場','admin','2016-10-24 05:51:50','2016-10-24 05:51:50'),(2,'Construction','建設業','admin','2016-10-24 05:51:50','2016-10-24 05:51:50'),(3,'Car maintenace','自動車整備','admin','2016-10-24 05:51:50','2016-10-24 05:51:50'),(4,'Card Test','カードテスト','sakiko.admin','2016-11-04 09:20:22','2016-11-04 09:20:22');
/*!40000 ALTER TABLE `aptitude_tests` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ckeditor_assets`
--

DROP TABLE IF EXISTS `ckeditor_assets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ckeditor_assets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_file_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `data_content_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data_file_size` int(11) DEFAULT NULL,
  `data_fingerprint` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `assetable_id` int(11) DEFAULT NULL,
  `assetable_type` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_ckeditor_assetable` (`assetable_type`,`assetable_id`) USING BTREE,
  KEY `idx_ckeditor_assetable_type` (`assetable_type`,`type`,`assetable_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ckeditor_assets`
--

LOCK TABLES `ckeditor_assets` WRITE;
/*!40000 ALTER TABLE `ckeditor_assets` DISABLE KEYS */;
INSERT INTO `ckeditor_assets` VALUES (1,'pww_japan.png','image/png',40046,'0b2fc152b42c4caca8ef9f496b03dd69',1,'User','Ckeditor::Picture',2841,624,'2016-10-24 07:02:12','2016-10-24 07:02:12');
/*!40000 ALTER TABLE `ckeditor_assets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `countries`
--

DROP TABLE IF EXISTS `countries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `countries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `en_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `jp_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country_code` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_updated_by` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `countries`
--

LOCK TABLES `countries` WRITE;
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
INSERT INTO `countries` VALUES (1,'Bangladesh','バングラデシュ','BD','admin','2016-10-24 05:51:50','2016-10-24 05:51:50'),(2,'Bhutan','ブータン','BT','admin','2016-10-24 05:51:50','2016-10-24 05:51:50'),(3,'Cambodia','カンボジア','KH','admin','2016-10-24 05:51:50','2016-10-24 05:51:50'),(4,'China','中華人民共和国','CN','admin','2016-10-24 05:51:50','2016-10-24 05:51:50'),(5,'Indonesia','インドネシア','ID','admin','2016-10-24 05:51:50','2016-10-24 05:51:50'),(6,'India','インド','IN','admin','2016-10-24 05:51:50','2016-10-24 05:51:50'),(7,'Japan','日本','JP','admin','2016-10-24 05:51:50','2016-10-24 05:51:50'),(8,'Laos','ラオス','LA','admin','2016-10-24 05:51:50','2016-10-24 05:51:50'),(9,'Malaysia','マレーシア','MY','admin','2016-10-24 05:51:50','2016-10-24 05:51:50'),(10,'Myanmar','ミャンマー','MM','admin','2016-10-24 05:51:50','2016-10-24 05:51:50'),(11,'Mongolia','モンゴル','MN','admin','2016-10-24 05:51:50','2016-10-24 05:51:50'),(12,'Nepal','ネパール','NP','admin','2016-10-24 05:51:50','2016-10-24 05:51:50'),(13,'Palau','パラオ','PW','admin','2016-10-24 05:51:50','2016-10-24 05:51:50'),(14,'Pakistan','パキスタン','PK','admin','2016-10-24 05:51:50','2016-10-24 05:51:50'),(15,'Philippines','フィリピン','PH','admin','2016-10-24 05:51:50','2016-10-24 05:51:50'),(16,'Republic of Korea','韓国','KR','admin','2016-10-24 05:51:50','2016-10-24 05:51:50'),(17,'Singapore','シンガポール','SG','admin','2016-10-24 05:51:50','2016-10-24 05:51:50'),(18,'Sri Lanka','スリランカ','LK','admin','2016-10-24 05:51:50','2016-10-24 05:51:50'),(19,'Thailand','タイ','TH','admin','2016-10-24 05:51:50','2016-10-24 05:51:50'),(20,'Viet Nam','ベトナム','VN','admin','2016-10-24 05:51:50','2016-10-24 05:51:50'),(21,'Qatar','カタール','QR','admin','2016-11-04 04:43:51','2016-11-04 04:43:51');
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `education_backgrounds`
--

DROP TABLE IF EXISTS `education_backgrounds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `education_backgrounds` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL,
  `date` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `university` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_applicant` (`applicant_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `education_backgrounds`
--

LOCK TABLES `education_backgrounds` WRITE;
/*!40000 ALTER TABLE `education_backgrounds` DISABLE KEYS */;
INSERT INTO `education_backgrounds` VALUES (1,1,'2013/03','B.E.H.S(1) ','Graduate','2016-11-04 03:34:01','2016-11-06 00:51:56'),(2,2,'2006/03','B.E.H.S(1) thatkala ','Graduate','2016-11-04 03:52:14','2016-11-04 03:52:14'),(3,3,'2011/04','Yangon East University ','Graduate','2016-11-04 04:09:56','2016-11-06 00:59:42'),(4,4,'2012/03','Yangon East University ','Graduate','2016-11-04 04:33:19','2016-11-04 04:33:19'),(5,5,'2007/03','B.E.H.S-Taw Kuu','Graduate','2016-11-04 05:02:24','2016-11-04 05:20:40'),(7,6,'2007/04','Dangon University ','Graduate','2016-11-04 05:03:59','2016-11-04 05:03:59'),(8,7,'2011/03','B.E.H.S(Tarpon) ','Graduate','2016-11-04 05:13:57','2016-11-04 05:13:57'),(9,8,'2013/03','B.E.H.S(5) North Okkalapa ','Graduate','2016-11-04 05:18:21','2016-11-04 05:18:21'),(10,9,'2008/04','Mawlamyine University','Drop-out','2016-11-04 05:18:34','2016-11-06 06:19:14'),(11,10,'2012/04','Dangon University ','Graduate','2016-11-04 05:23:19','2016-11-04 05:23:19'),(12,11,'2011/03','B.E.H.S(Khwal)Ngalonetin ','Graduate','2016-11-04 05:36:43','2016-11-06 01:26:36'),(13,12,'2007/03','B.E.H.S-Taw Kuu','Graduate','2016-11-04 05:45:49','2016-11-04 05:45:49'),(14,13,'2008/03','B.E.H.S(1)Yegyi','Graduate','2016-11-04 05:51:43','2016-11-04 05:51:43'),(15,14,'2007/03','B.E.H.S(2) Kyaukpadaung ','Graduate','2016-11-04 05:59:04','2016-11-04 05:59:04'),(16,15,'2013/04','(B.Sc)Mawlamyine UniversityBotany','Graduate','2016-11-04 05:59:50','2016-11-06 06:34:43'),(17,16,'2008/03','B.E.H.S-Yan Byal ','Graduate','2016-11-04 06:05:08','2016-11-04 06:05:08'),(18,17,'2008/03','B.E.H.S-Maung Taw ','Graduate','2016-11-04 06:12:33','2016-11-04 06:12:33'),(19,18,'2013/03','Taungoo University','Graduate','2016-11-04 06:18:39','2016-11-04 06:18:39'),(20,19,'2013/04','Sittway University','Graduate','2016-11-04 06:24:47','2016-11-06 06:50:32'),(21,20,'2012/03','Dagon University','Graduate','2016-11-04 06:31:37','2016-11-06 06:58:09'),(22,21,'2015/04','Sittway University','Graduate','2016-11-04 06:40:42','2016-11-06 07:02:44'),(23,22,'2013/03','B.E.H.S (1) Tamwe','Graduate','2016-11-04 06:48:43','2016-11-04 06:48:43'),(24,23,'2001/03','B.E.H.S(2)Myingyan ','Graduate','2016-11-04 06:49:47','2016-11-04 06:49:47'),(25,24,'2012/03','B.E.H.S (2) Tamwe','Graduate','2016-11-04 06:57:23','2016-11-04 06:57:23'),(26,25,'1998/04','Kyauk Se University ','Drop-out','2016-11-04 06:59:07','2016-11-04 06:59:07'),(27,26,'2015/04','Pakokku University ','Drop-out','2016-11-04 07:05:02','2016-11-04 07:05:02'),(28,27,'2006/03','B.E.H.S(1)Tap Kone ','Graduate','2016-11-04 07:13:06','2016-11-04 07:13:06'),(29,28,'2009/03','B.E.H.S(2) Mawlamyaing','Graduate','2016-11-04 07:21:55','2016-11-04 07:21:55'),(30,29,'2009/03','B.E.H.S-Chaung Oo ','Graduate','2016-11-04 07:27:58','2016-11-04 07:27:58'),(31,30,'2005/04','University of Mawlamyine','Graduate','2016-11-04 07:35:08','2016-11-04 07:35:08'),(32,31,'2007/03','B.E.H.S-Thanapin','Graduate','2016-11-04 07:43:27','2016-11-04 07:43:27'),(33,32,'2014/03','B.E.H.S-Sataung','Drop-out','2016-11-04 07:47:41','2016-11-04 07:47:41'),(34,33,'2006/03','B.E.H.H-Nattaline ','Graduate','2016-11-04 07:55:30','2016-11-04 07:55:30'),(35,34,'2008/04','Yangon Institute of Economics ','Graduate','2016-11-04 07:59:49','2016-11-04 07:59:49'),(36,35,'2006/03','B.E.H.S-Phado','Graduate','2016-11-04 08:04:51','2016-11-04 08:04:51'),(37,36,'2011/03','B.E.H.S-Yankin','Graduate','2016-11-04 08:10:07','2016-11-04 08:10:07'),(38,37,'1999/03','B.E.H.S-Phado','Drop-out','2016-11-04 08:18:56','2016-11-04 08:18:56'),(39,38,'2010/03','West University,B.A (Philosophy)','Graduate','2016-11-04 08:26:03','2016-11-04 08:26:03'),(40,39,'2008/03','B.E.H.S Shwe Pyi Thar','Graduate','2016-11-04 08:32:19','2016-11-04 08:32:19'),(41,40,'2008/03','B.E.H.S-yay kin','Graduate','2016-11-04 08:41:04','2016-11-04 08:41:04');
/*!40000 ALTER TABLE `education_backgrounds` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `family_backgrounds`
--

DROP TABLE IF EXISTS `family_backgrounds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `family_backgrounds` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL,
  `father_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mother_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `husband_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `wife_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `father_job_id` int(11) DEFAULT NULL,
  `mother_job_id` int(11) DEFAULT NULL,
  `husband_job_id` int(11) DEFAULT NULL,
  `wife_job_id` int(11) DEFAULT NULL,
  `father_phone_number` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mother_phone_number` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `husband_phone_number` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `wife_phone_number` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `brother_count` smallint(6) DEFAULT NULL,
  `sister_count` smallint(6) DEFAULT NULL,
  `daugther_count` smallint(6) DEFAULT NULL,
  `son_count` smallint(6) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_searchables` (`applicant_id`,`father_job_id`,`mother_job_id`,`husband_job_id`,`wife_job_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `family_backgrounds`
--

LOCK TABLES `family_backgrounds` WRITE;
/*!40000 ALTER TABLE `family_backgrounds` DISABLE KEYS */;
INSERT INTO `family_backgrounds` VALUES (1,1,'Myo Win ','Mya Than ','','',NULL,NULL,NULL,NULL,'+9594321259','+9594321259','','',NULL,NULL,NULL,NULL,'2016-11-04 03:34:01','2016-11-06 00:53:48'),(2,2,'Chain Khwi','Tin Aye ','','',35,35,NULL,NULL,'+959786719073','+959786719073','','',NULL,NULL,NULL,NULL,'2016-11-04 03:52:14','2016-11-06 07:27:47'),(3,3,'Win Htay ','Myint Myint Naing ','','',35,NULL,NULL,NULL,'+959791039629','+955257095','','',NULL,NULL,NULL,NULL,'2016-11-04 04:09:56','2016-11-06 07:28:22'),(4,4,'Zaw Min','Tin May San ','','',20,NULL,NULL,NULL,'+959776871465','+959261775077','','',NULL,NULL,NULL,NULL,'2016-11-04 04:33:19','2016-11-04 04:33:19'),(5,5,'Kyi Htay ','Ma Gyi','','',3,NULL,NULL,NULL,'+95978446070','+95978446070','','',1,1,NULL,NULL,'2016-11-04 05:02:24','2016-11-06 06:10:09'),(6,6,'Thaung Hlaing ','','','',NULL,NULL,NULL,NULL,'+959795600848','','','',NULL,NULL,NULL,NULL,'2016-11-04 05:03:59','2016-11-06 07:28:40'),(7,7,'Aye Tun ','Khian Pu ','','',25,NULL,NULL,NULL,'+959423711806','+959423711806','','',NULL,NULL,NULL,NULL,'2016-11-04 05:13:57','2016-11-06 07:29:02'),(8,8,'San Tun ','Ma Noat ','','',25,25,NULL,NULL,'+959794378113','+959794378113','','',NULL,NULL,NULL,NULL,'2016-11-04 05:18:21','2016-11-04 05:18:21'),(9,9,'Zaw Win','','','',19,NULL,NULL,NULL,'+9592559937','','','',NULL,1,NULL,NULL,'2016-11-04 05:18:34','2016-11-06 06:19:14'),(10,10,'Hla Sein ','','','',NULL,NULL,NULL,NULL,'+959791452254','','','',NULL,NULL,NULL,NULL,'2016-11-04 05:23:19','2016-11-04 05:23:19'),(11,11,'Aung Min ','Tin ','','',NULL,NULL,NULL,NULL,'+9597732343214','+9597732343214','','',NULL,NULL,NULL,NULL,'2016-11-04 05:36:43','2016-11-04 05:36:43'),(12,12,'Aung Myin Htay','Aye Aye Khine','','',19,19,NULL,NULL,'+959970062191','+959970062191','','',NULL,1,NULL,NULL,'2016-11-04 05:45:49','2016-11-06 07:33:37'),(13,13,'Thein Lwin','Khin Moe Myat ','','',NULL,NULL,NULL,NULL,'+95931152270','+95931152270','','',NULL,NULL,NULL,NULL,'2016-11-04 05:51:43','2016-11-04 05:51:43'),(14,14,'Aung Myalang','Than Mylnt Htay ','','',35,NULL,NULL,NULL,'+959795711440','+959795711440','','',NULL,NULL,NULL,NULL,'2016-11-04 05:59:04','2016-11-04 05:59:04'),(15,15,'Kyaw Lwin','','','',35,NULL,NULL,NULL,'+959975650086','','','',NULL,NULL,NULL,1,'2016-11-04 05:59:50','2016-11-06 06:34:43'),(16,16,'San Hla Aung','Htway May ','','',NULL,NULL,NULL,NULL,'+959250457240','+959250457240','','',NULL,NULL,NULL,NULL,'2016-11-04 06:05:08','2016-11-04 06:05:08'),(17,17,'Maung Hla ','Aye Tin ','','',NULL,NULL,NULL,NULL,'+95933621907','+95933621907','','',NULL,3,NULL,NULL,'2016-11-04 06:12:33','2016-11-04 06:12:33'),(18,18,'Du Ta Nat','','','',25,NULL,NULL,NULL,'+95943028629','','','',4,1,NULL,NULL,'2016-11-04 06:18:39','2016-11-04 07:10:10'),(19,19,'Mg Khin','Tin Tin','','',NULL,NULL,NULL,NULL,'+959963552740','+959963552740','','',2,NULL,NULL,NULL,'2016-11-04 06:24:47','2016-11-06 07:34:24'),(20,20,'Myint Thein','Win Win Than','','',3,15,NULL,NULL,'+959262043378','+959974716866','','',NULL,2,NULL,NULL,'2016-11-04 06:31:37','2016-11-06 06:58:09'),(21,21,'Maung Kyaw Thein','Oo Hmya Thein','','',25,35,NULL,NULL,'+959250838913','+959250838913','','',1,2,NULL,NULL,'2016-11-04 06:40:42','2016-11-06 07:02:44'),(22,22,'Khin Zaw Naing','Cho Cho Lwin','','',25,NULL,NULL,NULL,'+959973311240','+959960333984','','',1,2,NULL,NULL,'2016-11-04 06:48:43','2016-11-06 07:07:37'),(23,23,'Win Theln ','San Htay ','','',NULL,NULL,NULL,NULL,'+959791593930','+959791593930','','',NULL,NULL,NULL,NULL,'2016-11-04 06:49:47','2016-11-04 06:49:47'),(24,24,'','Thein Kyu','','',NULL,NULL,NULL,NULL,'','+95926313422','','',NULL,2,NULL,NULL,'2016-11-04 06:57:23','2016-11-06 07:12:02'),(25,25,'Pike Htwe ','','','',NULL,NULL,NULL,NULL,'+959444024804','','','',NULL,NULL,NULL,NULL,'2016-11-04 06:59:07','2016-11-08 06:03:52'),(26,26,'Aung Thit ','Ngwe Shln ','','',NULL,NULL,NULL,NULL,'+959258745815','+959258745815','','',NULL,NULL,NULL,NULL,'2016-11-04 07:05:02','2016-11-04 07:05:02'),(27,27,'Shwe Kha','Kyi Aye ','','',NULL,NULL,NULL,NULL,'+959791107229','+959791107229','','',NULL,NULL,NULL,NULL,'2016-11-04 07:13:06','2016-11-04 07:13:06'),(28,28,'Theln Oo ','Nyunt Theln ','','',NULL,NULL,NULL,NULL,'+959785419462','+959780306351','','',NULL,NULL,NULL,NULL,'2016-11-04 07:21:55','2016-11-04 07:21:55'),(29,29,'Seln Min ','San Win ','','',NULL,NULL,NULL,NULL,'+959440294077','+959787942784','','',NULL,NULL,NULL,NULL,'2016-11-04 07:27:58','2016-11-04 07:27:58'),(30,30,'Thein Aung','Tin Win ','','',NULL,NULL,NULL,NULL,'+959976364151','+959970438845','','',NULL,NULL,NULL,NULL,'2016-11-04 07:35:08','2016-11-04 07:35:08'),(31,31,'Than Htay','Ohn May','','',NULL,25,NULL,NULL,'','+959778139213','','',4,1,NULL,NULL,'2016-11-04 07:43:27','2016-11-06 03:15:19'),(32,32,'Thit Lwin','Phone Kyi','','',NULL,NULL,NULL,NULL,'+959795600607','+959795600607','','',NULL,NULL,NULL,NULL,'2016-11-04 07:47:41','2016-11-04 07:47:41'),(33,33,'Tin Win','San San Oo ','','',NULL,NULL,NULL,NULL,'+959423688164','+959423688164','','',NULL,NULL,NULL,NULL,'2016-11-04 07:55:30','2016-11-04 07:55:30'),(34,34,'Kyaw Myint','Khin Ohn Tun ','','',NULL,NULL,NULL,NULL,'','','','',NULL,NULL,NULL,NULL,'2016-11-04 07:59:49','2016-11-04 07:59:49'),(35,35,'Saw Thein Lwin ','Naw Moe Thar','','',NULL,NULL,NULL,NULL,'+959777154158','+959777154158','','',NULL,NULL,NULL,NULL,'2016-11-04 08:04:51','2016-11-06 07:30:29'),(36,36,'Tint Lwin','Khin Nyo Nyo','','',35,NULL,NULL,NULL,'+959420124647','+95973214717','','',1,1,NULL,NULL,'2016-11-04 08:10:07','2016-11-06 07:31:00'),(37,37,'Saw Kyar Myin','Naw Than Yin','','',NULL,NULL,NULL,NULL,'+959791531517','+959791531517','','',4,NULL,NULL,NULL,'2016-11-04 08:18:56','2016-11-06 07:31:49'),(38,38,'Tln Tun','San Mya Nu','','',NULL,NULL,NULL,NULL,'+959455305910','+959254432716','','',NULL,NULL,NULL,NULL,'2016-11-04 08:26:03','2016-11-04 08:26:03'),(39,39,'Kyaw Myint','Phyo Thiha Kyaw','','',15,2,NULL,NULL,'+959421028097','+959421028097','','',NULL,2,NULL,NULL,'2016-11-04 08:32:19','2016-11-06 07:32:30'),(40,40,'Tun Aung','Khin Win Myint','','',NULL,NULL,NULL,NULL,'+959452336378','+959452336378','','',1,NULL,NULL,NULL,'2016-11-04 08:41:04','2016-11-06 07:32:55');
/*!40000 ALTER TABLE `family_backgrounds` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `file_types`
--

DROP TABLE IF EXISTS `file_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `file_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `en_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `jp_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_updated_by` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `file_types`
--

LOCK TABLES `file_types` WRITE;
/*!40000 ALTER TABLE `file_types` DISABLE KEYS */;
INSERT INTO `file_types` VALUES (1,'Passport copy','パスポートコピー','admin','2016-10-24 05:51:50','2016-10-24 05:51:50'),(2,'ID copy','身分証明書','admin','2016-10-24 05:51:50','2016-10-24 05:51:50'),(3,'Health check','健康診断書','admin','2016-10-24 05:51:50','2016-10-24 05:51:50');
/*!40000 ALTER TABLE `file_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `job_types`
--

DROP TABLE IF EXISTS `job_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `en_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `jp_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_updated_by` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job_types`
--

LOCK TABLES `job_types` WRITE;
/*!40000 ALTER TABLE `job_types` DISABLE KEYS */;
INSERT INTO `job_types` VALUES (1,'waiter','ウエイター','admin','2016-10-24 05:51:50','2016-10-24 05:51:50'),(2,'waitress','ウエイトレス','admin','2016-10-24 05:51:50','2016-10-24 05:51:50'),(3,'driver','運転手','admin','2016-10-24 05:51:50','2016-10-24 05:51:50'),(4,'care worker','介護福祉士','admin','2016-10-24 05:51:50','2016-10-24 05:51:50'),(5,'confectioner','菓子屋','admin','2016-10-24 05:51:50','2016-10-24 05:51:50'),(6,'garment factory worker','縫製工場作業員','admin','2016-10-24 05:51:50','2016-10-24 05:51:50'),(7,'nurse','看護士・看護師','admin','2016-10-24 05:51:50','2016-10-24 05:51:50'),(8,'mechanic','機械工','admin','2016-10-24 05:51:50','2016-10-24 05:51:50'),(9,'engineer, engine driver','機関士','admin','2016-10-24 05:51:50','2016-10-24 05:51:50'),(10,'engineer','技術者','admin','2016-10-24 05:51:50','2016-10-24 05:51:50'),(11,'soldier, serviceman','軍人','admin','2016-10-24 05:51:50','2016-10-24 05:51:50'),(12,'guard, security guard','警備員、ガードマン','admin','2016-10-24 05:51:50','2016-10-24 05:51:50'),(13,'construction worker','建設作業員','admin','2016-10-24 05:51:50','2016-10-24 05:51:50'),(14,'builder','建築業者','admin','2016-10-24 05:51:50','2016-10-24 05:51:50'),(15,'public servant','公務員','admin','2016-10-24 05:51:50','2016-10-24 05:51:50'),(16,'cook','コック','admin','2016-10-24 05:51:50','2016-10-24 05:51:50'),(17,'plaster','左官','admin','2016-10-24 05:51:50','2016-10-24 05:51:50'),(18,'craftsman, artisan','職人','admin','2016-10-24 05:51:50','2016-10-24 05:51:50'),(19,'carpenter','大工','admin','2016-10-24 05:51:50','2016-10-24 05:51:50'),(20,'cab driver, taxi driver','タクシー運転手','admin','2016-10-24 05:51:50','2016-10-24 05:51:50'),(21,'interpreter','通訳','admin','2016-10-24 05:51:50','2016-10-24 05:51:50'),(22,'truck driver','トラック運転手','admin','2016-10-24 05:51:50','2016-10-24 05:51:50'),(23,'butcher','肉屋','admin','2016-10-24 05:51:50','2016-10-24 05:51:50'),(24,'grdener','庭師','admin','2016-10-24 05:51:50','2016-10-24 05:51:50'),(25,'farmer','農家、農夫','admin','2016-10-24 05:51:50','2016-10-24 05:51:50'),(26,'bartender, barman','バーテンダー','admin','2016-10-24 05:51:50','2016-10-24 05:51:50'),(27,'plumber','配管工','admin','2016-10-24 05:51:50','2016-10-24 05:51:50'),(28,'florist','花屋','admin','2016-10-24 05:51:50','2016-10-24 05:51:50'),(29,'hairdresser, beautician','美容師','admin','2016-10-24 05:51:50','2016-10-24 05:51:50'),(30,'Cleaning staff','清掃員','admin','2016-10-24 05:51:50','2016-11-04 04:21:21'),(31,'maid','メイド','admin','2016-10-24 05:51:50','2016-10-24 05:51:50'),(32,'welder','溶接工','admin','2016-10-24 05:51:50','2016-10-24 05:51:50'),(33,'fisherman','漁師','admin','2016-10-24 05:51:50','2016-10-24 05:51:50'),(34,'foreman','作業長、班長','admin','2016-10-24 05:51:50','2016-10-24 05:51:50'),(35,'worker','工員','admin','2016-10-24 05:51:50','2016-10-24 05:51:50'),(36,'manual labor','肉体労働','admin','2016-10-24 05:51:50','2016-10-24 05:51:50'),(37,'factory, works, plant, mill','工場','admin','2016-10-24 05:51:50','2016-10-24 05:51:50');
/*!40000 ALTER TABLE `job_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects`
--

DROP TABLE IF EXISTS `projects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `partner_id` int(11) DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `job_type_id` int(11) DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'In-progress',
  `no_of_needed_applicants` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `notes` text COLLATE utf8_unicode_ci,
  `document_file_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `document_content_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `document_file_size` int(11) DEFAULT NULL,
  `last_email_sent` datetime DEFAULT NULL,
  `last_updated_by` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_user` (`partner_id`,`client_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects`
--

LOCK TABLES `projects` WRITE;
/*!40000 ALTER TABLE `projects` DISABLE KEYS */;
INSERT INTO `projects` VALUES (3,'00017IAC','有限会社アイ・エイ・シー',3,4,7,35,'Confirmed','9','<p>5</p>\r\n','',NULL,NULL,NULL,NULL,'admin','2016-11-04 13:02:35','2016-11-07 11:19:40'),(4,'00011IBS','株式会社アイ・ビー・エス様',3,4,10,35,'In-progress','9','<p>ssssssss</p>\r\n','',NULL,NULL,NULL,NULL,'pwjstaff_admin','2016-11-05 15:45:28','2016-11-08 06:18:22'),(5,'★ﾋﾞﾙｸﾘｰﾆﾝｸﾞ候補者','ビルクリーニング',3,4,7,35,'Waiting','55','<p>NPO CLEAN-Building cleaners&nbsp;</p>\r\n','',NULL,NULL,NULL,NULL,'admin','2016-11-05 16:37:59','2016-11-07 11:37:57'),(6,'00020Koryo','株式会社コウリョウ様',3,4,7,35,'In-progress','6','<p>Koryo Co.,Ltd&nbsp;</p>\r\n','',NULL,NULL,NULL,NULL,'pwjstaff_admin','2016-11-05 16:48:40','2016-11-08 06:17:59'),(7,'00021Gokou','株式会社互幸ワークス様　',3,4,7,35,'In-progress','9','<p>Gokou Works Co., Ltd</p>\r\n','',NULL,NULL,NULL,NULL,'pwjstaff_admin','2016-11-05 16:50:45','2016-11-08 06:17:42'),(8,'00022Fuyoushokai','株式会社芙蓉商会様',3,4,7,35,'In-progress','6','<p>Fuyoshokai Co., Ltd</p>\r\n','',NULL,NULL,NULL,NULL,'pwjstaff_admin','2016-11-05 16:52:52','2016-11-08 06:17:31'),(9,'00018SBM','三洋ビル管理株式会社様',3,4,7,35,'In-progress','9','<p>SBM Co., Ltd</p>\r\n','',NULL,NULL,NULL,NULL,'pwjstaff_admin','2016-11-05 16:55:09','2016-11-08 06:18:06'),(10,'00023Kyoei','共栄フード株式会社様　',7,8,7,37,'In-progress','12','<p><strong><u>Bread factory workers &nbsp;Kyoei</u></strong><strong><u>　</u></strong><strong><u>food Co., Ltd </u></strong></p>\r\n\r\n<p><strong>&lt;&lt;Order for 4 females&gt;&gt;</strong><strong>　　　<u>★</u></strong><strong><u>Please upload 16 CVs on Mku. </u></strong></p>\r\n\r\n<p>【Working Hours】</p>\r\n\r\n<p>8 hours a day (including a 1-hour break)</p>\r\n\r\n<p>Night or daytime shift depends</p>\r\n\r\n<p>【Requirement】</p>\r\n\r\n<p>(1) Must have Working experience in food factory</p>\r\n\r\n<p>(2) Must be single</p>\r\n\r\n<p>(3) Must be right handed</p>\r\n\r\n<p>(4) Must have eye sight over 0.7(allowed to wear glasses but not allowed to wear contact lenses)</p>\r\n\r\n<p>【Salary】</p>\r\n\r\n<p>￥142,000~185,000</p>\r\n\r\n<p>(May differ depending on working hour Overtime)</p>\r\n\r\n<p>【Contract Duration】</p>\r\n\r\n<p><u>＜＜＜</u>&nbsp;<u>3 years</u>&nbsp;<u>＞＞＞</u></p>\r\n\r\n<p>【Holiday】</p>\r\n\r\n<p>8 days in a month</p>\r\n\r\n<p>(Paid holiday after 6 months)</p>\r\n\r\n<p>【Housing】</p>\r\n\r\n<p>▪Dormitory provided and utility costs cost to be deducted from salary.</p>\r\n\r\n<p>&nbsp;(\\20,000-\\30,000)</p>\r\n\r\n<p>【Minimum Experience / Skills】</p>\r\n\r\n<p>▪must be open to night shifts</p>\r\n\r\n<p>▪Must take Japanese lesson after Job Confirmation.</p>\r\n\r\n<p>【Age】</p>\r\n\r\n<p>19 ～ 22</p>\r\n\r\n<p>【Work Place】</p>\r\n\r\n<p>Saitama, Japan</p>\r\n','',NULL,NULL,NULL,NULL,'pwjstaff_admin','2016-11-07 09:46:52','2016-11-08 06:17:21'),(11,'★不採用-候補者','ビルクリーニング',3,4,7,35,'Waiting','100','<p>1</p>\r\n','',NULL,NULL,NULL,NULL,'admin','2016-11-07 11:19:17','2016-11-07 11:37:48');
/*!40000 ALTER TABLE `projects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pww_logos`
--

DROP TABLE IF EXISTS `pww_logos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pww_logos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `status` tinyint(1) DEFAULT '1',
  `logo_file_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `logo_content_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `logo_file_size` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pww_logos`
--

LOCK TABLES `pww_logos` WRITE;
/*!40000 ALTER TABLE `pww_logos` DISABLE KEYS */;
INSERT INTO `pww_logos` VALUES (1,'PWW Japan Main','<div>〒105-0001 東京都港区虎ノ⾨2-7-5 BUREX虎ノ⾨7F</div>\n            <div>〒105-0001 7F BUREX Toranom on, 2-7-5 Minato-ku, Tokyo Japan</div>\n            <div>TEL:+81-3-3539-2237 FAX:+81-3-3539-2238</div>',1,'pww-japan.png','image/png',40046,'2016-10-24 05:51:49','2016-10-24 05:51:49');
/*!40000 ALTER TABLE `pww_logos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `religions`
--

DROP TABLE IF EXISTS `religions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `religions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `en_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `jp_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_updated_by` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `religions`
--

LOCK TABLES `religions` WRITE;
/*!40000 ALTER TABLE `religions` DISABLE KEYS */;
INSERT INTO `religions` VALUES (1,'-','-','admin','2016-10-24 05:51:50','2016-10-24 05:51:50'),(2,'Christianity','キリスト教','admin','2016-10-24 05:51:50','2016-10-24 05:51:50'),(3,'Islam','イスラム教','admin','2016-10-24 05:51:50','2016-10-24 05:51:50'),(4,'Hinduism','ヒンドゥー教','admin','2016-10-24 05:51:50','2016-10-24 05:51:50'),(5,'Buddhism','仏教','admin','2016-10-24 05:51:50','2016-10-24 05:51:50'),(6,'Other','その他','admin','2016-10-24 05:51:50','2016-10-24 05:51:50');
/*!40000 ALTER TABLE `religions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `requested_to_be_deleted_applicants`
--

DROP TABLE IF EXISTS `requested_to_be_deleted_applicants`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `requested_to_be_deleted_applicants` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `partner_id` int(11) DEFAULT NULL,
  `reason` text COLLATE utf8_unicode_ci,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_applicant` (`applicant_id`) USING BTREE,
  KEY `idx_user` (`partner_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `requested_to_be_deleted_applicants`
--

LOCK TABLES `requested_to_be_deleted_applicants` WRITE;
/*!40000 ALTER TABLE `requested_to_be_deleted_applicants` DISABLE KEYS */;
/*!40000 ALTER TABLE `requested_to_be_deleted_applicants` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schema_migrations`
--

DROP TABLE IF EXISTS `schema_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schema_migrations` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  UNIQUE KEY `unique_schema_migrations` (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schema_migrations`
--

LOCK TABLES `schema_migrations` WRITE;
/*!40000 ALTER TABLE `schema_migrations` DISABLE KEYS */;
INSERT INTO `schema_migrations` VALUES ('20160627073122'),('20160628055156'),('20160628055336'),('20160628064720'),('20160628065525'),('20160628065549'),('20160629085136'),('20160630073724'),('20160704012957'),('20160704013743'),('20160712065001'),('20160714082131'),('20160715013511'),('20160715013537'),('20160715014634'),('20160715014646'),('20160801144319'),('20160816060528'),('20160905022311'),('20160930014016');
/*!40000 ALTER TABLE `schema_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_digest` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fullname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `role` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `image_file_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_content_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_file_size` int(11) DEFAULT NULL,
  `pww_logo_id` int(11) DEFAULT '1',
  `last_updated_by` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'admin','$2a$10$s3p4jf44FfjT4kw1UJ2qMeS8zfhz1Y5Hs3pQkLJzIXGhvk895Oaea','Administrator','admin',1,'kinji_shimizu@peopleworldwide.jp','','','pww-logo.png','image/png',11894,1,'skywork','2016-10-24 05:51:49','2016-11-07 01:07:42'),(2,'jidouka','$2a$10$cHn8znYZqaGXLD6fzn.HAOF27mG6vWOP/Ld01eAzIxmcP3SPjY/9W','skyjidouka(skyarch admin)','admin',1,'jidouka@skyarch.net','+81-3-6743-1100','skyarch jidouka admin account',NULL,NULL,NULL,1,'skywork','2016-10-27 02:12:41','2016-11-07 01:09:01'),(3,'pwjstaff_partner','$2a$10$jn.z6Am8phtiq6DpAl8u7ubxuanTMEwEQnyoEjH8p0WFfYDCWxpSu','Pwj staff partner','partner',1,'doc@peopleworldwide.jp','','','people-worldwide-big.png','image/png',32516,1,'admin','2016-11-04 01:13:38','2016-11-07 12:21:31'),(4,'pwjstaff_client','$2a$10$ues/ymh9r3/XQbYoA/pyLOO8QC8IV9G2IfyGu/xArqa2G6ndnEsvu','Pwj Staff Client','client',1,'doc@peopleworldwide.jp','','','people-worldwide-big.png','image/png',32516,1,'admin','2016-11-04 01:51:11','2016-11-07 12:21:41'),(6,'pwjstaff_admin','$2a$10$mS/C1LYPCRIhYzBNlVbMfeB02vU1D1cOTOF3/IVSQJ0Al8DZBRfpq','Pwj staff Admin','admin',1,'doc@peopleworldwide.jp','','','people-worldwide-big.png','image/png',32516,1,'admin','2016-11-04 09:12:42','2016-11-07 12:22:33'),(7,'triduc','$2a$10$LTwRPF4Y1zl6cgLf/sm4CemD6iW8Lyl52Gu.dtcyIAgkSBVNswvUy','TRI DUC MANPOWER DEVELOPMENT & CONSTRUCTION JSC','partner',1,'triducmdcgroup@gmail.com','','','triducmanpower.jpg','image/jpeg',14355,1,'admin','2016-11-07 09:50:10','2016-11-07 12:21:15'),(8,'kyoei','$2a$10$WSEcYoTOnRWzq8BUc2xvSeoDeerC361Jvbkwdx8fPME50PLTBmWGm','共栄フード株式会社','client',1,'staff@peopleworldwide.jp','','','kyoei.png','image/png',3461,1,'admin','2016-11-07 09:57:48','2016-11-07 09:57:48'),(9,'tkss','$2a$10$m4xCQyUD7VNhSjpw7JURfum00jdoAiOhF..gtis7lLQ04MFAASPvW','Thu Kha Su San Services Co.,Ltd.','partner',1,'thukhasusan2012@gmail.com','','','thu-kha-su-san-services-co-ltd.jpg','image/jpeg',106362,1,'admin','2016-11-07 11:29:09','2016-11-07 12:20:26');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `work_experiences`
--

DROP TABLE IF EXISTS `work_experiences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `work_experiences` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL,
  `job_type_id` int(11) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `entering_date` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `leaving_date` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `up_to_present` tinyint(1) DEFAULT '0',
  `company_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `monthly_salary` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_applicant_nature` (`applicant_id`,`job_type_id`,`country_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `work_experiences`
--

LOCK TABLES `work_experiences` WRITE;
/*!40000 ALTER TABLE `work_experiences` DISABLE KEYS */;
INSERT INTO `work_experiences` VALUES (1,1,35,10,'2014/07','2015/07',0,'J\'Donuts ',NULL,'150','2016-11-04 03:34:01','2016-11-04 03:34:01'),(3,2,35,10,'2009/08','2015/05',0,'Asia Optical Co., Ltd ',NULL,'200','2016-11-04 03:52:14','2016-11-04 03:52:14'),(4,3,29,10,'2012/12','2016/04',0,'Pan Thakhin(Beauty Centre)',NULL,'200','2016-11-04 04:09:56','2016-11-04 04:09:56'),(5,4,35,10,'2012/02','2016/08',0,'Zay Ya Theikdi Meritime Training Centre Co., Ltd',NULL,'200','2016-11-04 04:33:19','2016-11-04 04:33:19'),(6,5,3,10,'2012/06','2016/06',0,'Own business',NULL,'300','2016-11-04 05:02:24','2016-11-04 05:42:42'),(8,6,35,10,'2009/05','2011/03',0,'KandawGyi Palace Hotel',NULL,'200','2016-11-04 05:03:59','2016-11-04 05:03:59'),(10,7,35,17,'2013/03','2016/04',0,'PPL-Shipyard Pte. Ltd ',NULL,'400','2016-11-04 05:13:57','2016-11-04 05:13:57'),(11,8,25,10,'2012/05','2014/03',0,'Own Business ',NULL,'','2016-11-04 05:18:21','2016-11-04 05:18:21'),(12,9,37,19,'2010/05','2015/05',0,'South East Taxil Company',NULL,'400','2016-11-04 05:18:34','2016-11-04 06:12:40'),(13,10,20,10,'2014/08','2016/04',0,'34 Bus Car ',NULL,'','2016-11-04 05:23:19','2016-11-06 01:19:07'),(14,11,25,10,'2014/10',NULL,1,'Own Business',NULL,'200','2016-11-04 05:36:43','2016-11-06 01:26:36'),(15,12,19,19,'2007/03','2016/03',0,'Buddy',NULL,'300','2016-11-04 05:45:49','2016-11-06 06:23:30'),(16,13,35,10,'2010/03','2016/03',0,'Naing Group',NULL,'200','2016-11-04 05:51:43','2016-11-04 05:51:43'),(17,14,NULL,NULL,'','',0,'',NULL,'','2016-11-04 05:59:04','2016-11-04 05:59:04'),(18,15,35,10,'2014/03',NULL,1,'Mobile',NULL,'200','2016-11-04 05:59:50','2016-11-06 06:34:43'),(19,16,35,10,'2015/03',NULL,1,'Car Workship(Nyi Than)',NULL,'200','2016-11-04 06:05:08','2016-11-04 06:05:08'),(20,17,14,10,'2010/07',NULL,1,'Family Construction Group Co., Ltd ',NULL,'200','2016-11-04 06:12:33','2016-11-04 06:12:33'),(21,18,37,9,'2014/06',NULL,1,'Taung Pyar Tan Factory',NULL,'500','2016-11-04 06:18:39','2016-11-04 07:10:10'),(23,19,13,10,'2009/04','2012/04',0,'Nay La Thit Sar Construction',NULL,'200','2016-11-04 06:24:47','2016-11-04 06:24:47'),(24,20,3,10,'2015/02',NULL,1,'Ooredoo Myanmar',NULL,'250','2016-11-04 06:31:37','2016-11-04 06:31:37'),(25,21,29,10,'2008/06',NULL,1,'Sydney(Beatyt Spa)',NULL,'200','2016-11-04 06:40:42','2016-11-06 07:02:44'),(26,22,25,10,'2013/06','2015/06',0,'agriculture',NULL,'150','2016-11-04 06:48:43','2016-11-04 06:48:43'),(27,23,18,17,'2005/01','2007/01',0,'Glenn Marine Shipyard',NULL,'400','2016-11-04 06:49:47','2016-11-06 00:42:39'),(28,23,13,17,'2008/10','2015/10',0,'Woh Hup Construction Co., Ltd',NULL,'700','2016-11-04 06:49:47','2016-11-06 00:42:39'),(29,24,35,10,'2013/11','2016/11',0,'Ko Aung Car Audio',NULL,'300','2016-11-04 06:57:23','2016-11-04 06:57:23'),(30,25,13,10,'2014/01','2015/03',0,'Phwe Pyi Soe Co., Ltd',NULL,'300','2016-11-04 06:59:07','2016-11-04 06:59:07'),(31,26,37,9,'2006/01','2014/09',0,'Carlorino Handbag',NULL,'300','2016-11-04 07:05:02','2016-11-04 07:05:02'),(32,27,20,10,'2008/02','2013/02',0,'Own Business',NULL,'200','2016-11-04 07:13:06','2016-11-04 07:13:06'),(33,27,19,10,'2014/09','2016/09',0,'Construction Co.,Ltd',NULL,'150','2016-11-04 07:13:06','2016-11-04 07:13:06'),(34,28,35,19,'2008/06','2010/04',0,'Prantalay Co., Ltd',NULL,'','2016-11-04 07:21:55','2016-11-04 07:21:55'),(35,28,35,19,'2010/04','2015/03',0,'Prantalay Co., Ltd',NULL,'','2016-11-04 07:21:55','2016-11-04 07:21:55'),(36,29,37,9,'2011/09','2013/07',0,'Glasfil Polymer Sendirian Berhad Co., Ltd0',NULL,'300','2016-11-04 07:27:58','2016-11-04 12:17:27'),(37,30,NULL,NULL,'','',0,'',NULL,'','2016-11-04 07:35:08','2016-11-04 07:35:08'),(38,31,35,9,'2011/09','2016/02',0,'Ban Way',NULL,'250','2016-11-04 07:43:27','2016-11-04 07:43:27'),(39,32,NULL,NULL,'','',0,'',NULL,'','2016-11-04 07:47:41','2016-11-04 07:47:41'),(40,33,27,17,'2013/03','2016/03',0,'PPL-Shipyard Pte. Ltd',NULL,'500','2016-11-04 07:55:30','2016-11-06 03:01:18'),(41,34,35,10,'2011/02','2016/08',0,'IBTC Company ',NULL,'300','2016-11-04 07:59:49','2016-11-04 07:59:49'),(42,35,32,19,'2013/10','2015/10',0,'ORAM',NULL,'300','2016-11-04 08:04:51','2016-11-04 08:04:51'),(45,38,37,9,'2010/05','2015/05',0,'Guanchong Company',NULL,'300','2016-11-04 08:26:03','2016-11-06 03:30:02'),(48,40,25,10,'2008/11','2015/11',0,'Own business',NULL,'','2016-11-04 08:41:04','2016-11-06 06:06:04'),(50,6,1,21,'2012/02','2014/02',0,'The Anuil Room(Tovnado Tower Restaurant) ',NULL,'400','2016-11-06 01:12:59','2016-11-06 01:12:59');
/*!40000 ALTER TABLE `work_experiences` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-11-08 15:46:51
