# Available Assertions

Assertion -- Purpose

assert( test, [msg] ) Ensures that test is true.
assert_not( test, [msg] ) Ensures that test is false.
assert_equal( expected, actual, [msg] ) Ensures that expected == actual is true.
assert_not_equal( expected, actual, [msg] ) Ensures that expected != actual is true.
assert_same( expected, actual, [msg] )  Ensures that expected.equal?(actual) is true.
assert_not_same( expected, actual, [msg] )  Ensures that expected.equal?(actual) is false.
assert_nil( obj, [msg] )  Ensures that obj.nil? is true.
assert_not_nil( obj, [msg] )  Ensures that obj.nil? is false.
assert_match( regexp, string, [msg] ) Ensures that a string matches the regular expression.
assert_no_match( regexp, string, [msg] )  Ensures that a string doesn't match the regular expression.
assert_in_delta( expecting, actual, [delta], [msg] )  Ensures that the numbers expected and actual are within delta of each other.
assert_not_in_delta( expecting, actual, [delta], [msg] )  Ensures that the numbers expected and actual are not within delta of each other.
assert_throws( symbol, [msg] ) { block }  Ensures that the given block throws the symbol.
assert_raises( exception1, exception2, ... ) { block }  Ensures that the given block raises one of the given exceptions.
assert_nothing_raised( exception1, exception2, ... ) { block }  Ensures that the given block doesn't raise one of the given exceptions.
assert_instance_of( class, obj, [msg] ) Ensures that obj is an instance of class.
assert_not_instance_of( class, obj, [msg] ) Ensures that obj is not an instance of class.
assert_kind_of( class, obj, [msg] ) Ensures that obj is or descends from class.
assert_not_kind_of( class, obj, [msg] ) Ensures that obj is not an instance of class and is not descending from it.
assert_respond_to( obj, symbol, [msg] ) Ensures that obj responds to symbol.
assert_not_respond_to( obj, symbol, [msg] ) Ensures that obj does not respond to symbol.
assert_operator( obj1, operator, [obj2], [msg] )  Ensures that obj1.operator(obj2) is true.
assert_not_operator( obj1, operator, [obj2], [msg] )  Ensures that obj1.operator(obj2) is false.
assert_send( array, [msg] ) Ensures that executing the method listed in array[1] on the object in array[0] with the parameters of array[2 and up] is true. This one is weird eh?
flunk( [msg] )  Ensures failure. This is useful to explicitly mark a test that isn't finished yet.
Because of the modular nature of the testing framework, it is possible to create your own assertions. In fact, that's exactly what Rails does. It includes some specialized assertions to make your life easier.


# Rails Specific Assertions
  Rails adds some custom assertions of its own to the test/unit framework:

Assertion -- Purpose
assert_difference(expressions, difference = 1, message = nil) {...} Test numeric difference between the return value of an expression as a result of what is evaluated in the yielded block.
assert_no_difference(expressions, message = nil, &amp;block)  Asserts that the numeric result of evaluating an expression is not changed before and after invoking the passed in block.
assert_recognizes(expected_options, path, extras={}, message=nil) Asserts that the routing of the given path was handled correctly and that the parsed options (given in the expected_options hash) match path. Basically, it asserts that Rails recognizes the route given by expected_options.
assert_generates(expected_path, options, defaults={}, extras = {}, message=nil) Asserts that the provided options can be used to generate the provided path. This is the inverse of assert_recognizes. The extras parameter is used to tell the request the names and values of additional request parameters that would be in a query string. The message parameter allows you to specify a custom error message for assertion failures.
assert_response(type, message = nil)  Asserts that the response comes with a specific status code. You can specify :success to indicate 200-299, :redirect to indicate 300-399, :missing to indicate 404, or :error to match the 500-599 range. You can also pass an explicit status number or its symbolic equivalent. For more information, see full list of status codes and how their mapping works.
assert_redirected_to(options = {}, message=nil) Assert that the redirection options passed in match those of the redirect called in the latest action. This match can be partial, such that assert_redirected_to(controller: "weblog") will also match the redirection of redirect_to(controller: "weblog", action: "show") and so on. You can also pass named routes such as assert_redirected_to root_path and Active Record objects such as assert_redirected_to @article.
assert_template(expected = nil, message=nil)  Asserts that the request was rendered with the appropriate template file.