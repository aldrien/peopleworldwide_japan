=======================================================================================
RAILS UNIT TEST (MINITEST, RSPEC...)

  Building Rails
    -http://buildingrails.com/a/rails_unit_testing_with_minitest_for_beginners
    -http://buildingrails.com/a/rails_functional_testing_controllers_for_beginners_part_1
    -http://buildingrails.com/a/rails_functional_testing_controllers_for_beginners_part_2

  Rails 4.1 Testing with Test::Unit, MiniTest, and Rspec
    -http://www.ironhorserails.com/posts/1

=======================================================================================
CAPYBARA CHEAT SHEET REFERENCE

  GitHubGist: Capybara Actions & Finders
    -https://gist.github.com/tomas-stefano/6652111

  Ruby MiniTest Cheat Sheet, Unit and Spec reference 
    -http://danwin.com/2013/03/ruby-minitest-cheat-sheet/

  Capybara Cheat Sheet
    -https://www.launchacademy.com/codecabulary/learn-test-driven-development/rspec/capybara-cheat-sheet

  6 Ways to Remove Pain From Feature Testing in Ruby on Rails
    -https://teamgaslight.com/blog/6-ways-to-remove-pain-from-feature-testing-in-ruby-on-rails

=======================================================================================
CANCAN ABILITY TEST

  Fast Rails Tests With CanCan
    -http://blog.steveklabnik.com/posts/2011-12-12-fast-rails-tests-with-cancan


=======================================================================================
TEST AUTOMATION SETUP using GUARD (auto run test and auto update view)
  
  RailsCase - Guard
    -http://railscasts.com/episodes/264-guard?autoplay=true

  GitHub Documentation:
    
    Guard
    -https://github.com/guard/guard

    Guard::LiveReload
      -https://github.com/guard/guard-livereload