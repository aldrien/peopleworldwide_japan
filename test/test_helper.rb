ENV["RAILS_ENV"] = "test"
require File.expand_path("../../config/environment", __FILE__)
require "rails/test_help"
require "minitest/rails"
require 'minitest/autorun'

# To add Capybara feature tests add `gem "minitest-rails-capybara"`
# to the test group in the Gemfile and uncomment the following:
require "minitest/rails/capybara"
require 'capybara-screenshot/minitest'
require "minitest/reporters"

# Uncomment for awesome colorful output
require "minitest/pride"

Minitest::Reporters.use!(
  Minitest::Reporters::SpecReporter.new,
  ENV,
  Minitest.backtrace_filter
)

class ActiveSupport::TestCase
  ActiveRecord::Migration.check_pending!

  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all
  
  # Logs in a test user.
  def authenticate_user_login(username, password)
    user = User.find_by(username: username).try(:authenticate, password)
    if user.status == true
      session[:username] = user.username.downcase
      session[:user_id] = user.id
      session[:role] = user.role

      flash.keep[:status] = "success"      
    else
      flash.keep[:status] = "error"      
    end
  end

  # Helper for ALL feature test that includes or requires login
  def feature_login(username, password)    
    page.driver.browser.manage.window.maximize#resize_to(1366,768)
    within 'form#login-form' do
      fill_in 'username',    with: username
      fill_in 'password', with: password
      click_button 'continue'
    end
  end

  # Helper for Delete Record Function (Admin Password Required)
  def show_password_modal
    page.must_have_selector('#modal_confirmation', visible: true)
    page.must_have_content "Are you sure ?"
    page.must_have_content "This action will PERMANENTLY DELETE the selected record."    
  end

  # Helper for Modal Choose report type
  def show_modal_report_type_chooser
    page.must_have_selector('#modal_report_chooser', visible: true)
    page.must_have_content "Choose what Report Type & Skill Test!"
    page.must_have_content "The report will be based on the selected type."
  end

  # Helper for sending data to CKEditor
  def fill_in_ckeditor(locator, opts)
    content = opts.fetch(:with).to_json
    page.execute_script <<-SCRIPT
      CKEDITOR.instances['#{locator}'].setData(#{content});
      $('textarea##{locator}').text(#{content});
    SCRIPT
  end

  # Helper for maximum waiting time for ajax functions to complete
  def wait_for
    Timeout.timeout(Capybara.default_max_wait_time) do
      loop until
        begin
          yield
        rescue MiniTest::Assertion
        end
    end
    yield
  end

  # Helper for unused tab/s and returns to original tab
  def closeNewTabs
    window = page.driver.browser.window_handles

    if window.size > 1 
      page.driver.browser.switch_to.window(window.last)
      page.driver.browser.close
      page.driver.browser.switch_to.window(window.first)
    end
  end

end

class ActionDispatch::IntegrationTest
  include Capybara::Screenshot::MiniTestPlugin
  # Make the Capybara DSL available in all integration tests
  include Capybara::DSL
  include Capybara::Assertions

  # Reset sessions and driver between tests
  # Use super wherever this method is redefined in your individual test classes
  def teardown
    Capybara.reset_sessions!
    Capybara.use_default_driver
  end
end

Capybara.register_driver :selenium_chrome do |app|
  Capybara::Selenium::Driver.new(app, :browser => :chrome)
end

# Capybara.register_driver :slow_selenium do |app|
#   client = Selenium::WebDriver::Remote::Http::Default.new
#   client.timeout = 120
#   Capybara::Selenium::Driver.new(app, http_client: client)
#   # driver: :slow_selenium
# end

Capybara.current_driver = :selenium_chrome
Capybara.javascript_driver = :selenium_chrome
Capybara.default_driver = :selenium_chrome

Capybara::Screenshot.register_driver(:selenium_chrome) do |driver, path|
  driver.browser.save_screenshot(path) # uncomment this to screenshot error page
  driver.autosave_on_failure = true
end


# OTHER OPTIONS:
# Capybara::Screenshot.webkit_options = { width: 1024, height: 768 }
  # Uncomment below code to change error screenshot saving location.
  # Capybara.save_path = "test-reports" #(default is $APPLICATION_ROOT/tmp/capybara)

# HAVING SOME ISSUE WITH CSS, JS (especially ajax)

  # ADD to config/environments/test.rb:
  #   config.assets.compile = true
  #   config.assets.digest = true

  # RUN rake assets:precompile RAILS_ENV=test