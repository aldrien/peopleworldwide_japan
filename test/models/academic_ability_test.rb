require 'test_helper'

class AcademicAbilityTest < ActiveSupport::TestCase
  # Invalid if duplicate english name
  test "invalid if duplicate en_name" do
    copy_academic_ability = academic_abilities(:iq_test).dup
    copy_academic_ability.save
    proc { copy_academic_ability.save! }.must_raise(ActiveRecord::RecordInvalid)
    copy_academic_ability.errors.must_include(:en_name)
  end

  test "should create new AcademicAbilityTest" do
    academic = AcademicAbility.create(en_name: 'sample ability', jp_name: '-', last_updated_by: 'admin')
    # academic should be valid
    assert academic.valid?
    assert_equal 'sample ability', academic.en_name
  end

  test "should NOT create new AcademicAbilityTest without en_name" do
    academic = AcademicAbility.create(en_name: nil, jp_name: '-', last_updated_by: 'admin')
    # academic should not be valid if en_name is nil
    assert_not_nil academic.valid?
  end

  test "should NOT create new AcademicAbilityTest with duplicate en_name" do
    academic = AcademicAbility.create(en_name: 'IQ test', jp_name: '-', last_updated_by: 'admin')
    # academic should not be valid if has duplicate
    assert_not academic.valid?, 'Found duplicate entry.'
  end

  test "should combine english and japanese name" do
    academic = academic_abilities(:pwa_japanese_skill_test)
    assert_equal 'PWJ日本語能力テスト/PWA Japanese skill test', academic.combine_jp_and_en_name
  end
end
