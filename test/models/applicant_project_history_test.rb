require 'test_helper'

class ApplicantProjectHistoryTest < ActiveSupport::TestCase
  
  setup do
    @applicant = applicants(:aldrien)
    @new_project = projects(:project2)
  end

  test "should create history if project is present in applicant data" do    
    history = ApplicantProjectHistory.create(applicant_id: @applicant, project_id: @applicant.project_id, notes: 'No notes.', last_updated_by: 'admin')
    
    assert history.valid?
  end

  test "should create new history if applicant's project was changed" do
    @applicant.project_id = @new_project
    @applicant.save

    assert @applicant
    history = ApplicantProjectHistory.create(applicant_id: @applicant, project_id: @new_project, notes: 'No notes.', last_updated_by: 'admin')
    
    assert history.valid?    
  end
end
