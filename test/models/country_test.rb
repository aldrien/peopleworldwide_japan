require 'test_helper'

class CountryTest < ActiveSupport::TestCase
  # Invalid if duplicate english name
  test "invalid if duplicate en_name" do
    copy_country = countries(:china).dup
    copy_country.save
    proc { copy_country.save! }.must_raise(ActiveRecord::RecordInvalid)
    copy_country.errors.must_include(:en_name)
  end

  test "should create new Country" do
    country = Country.create(en_name: 'sample country', country_code: 'SC', jp_name: '-', last_updated_by: 'admin')
    # country should be valid
    assert country.valid?
    assert_equal 'sample country', country.en_name
  end

  test "should NOT create new Country without en_name" do
    country = Country.create(en_name: nil, country_code: 'NIL', jp_name: '-', last_updated_by: 'admin')
    # country should not be valid if en_name is nil
    assert_not_nil country.valid?
  end

  test "should NOT create new Country with duplicate en_name" do
    country = Country.create(en_name: 'Cambodia', country_code: 'KH', jp_name: '-', last_updated_by: 'admin')
    # country should not be valid if has duplicate
    assert_not country.valid?, 'Found duplicate entry.'
  end

  test "should combine english and japanese name" do
    country = countries(:bangladesh)
    assert_equal 'バングラデシュ/Bangladesh', country.combine_jp_and_en_name
  end
end
