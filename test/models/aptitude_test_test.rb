require 'test_helper'

class AptitudeTestTest < ActiveSupport::TestCase
  # Invalid if duplicate english name
  test "invalid if duplicate en_name" do
    copy_aptitude_test = aptitude_tests(:food_processor).dup
    copy_aptitude_test.save
    proc { copy_aptitude_test.save! }.must_raise(ActiveRecord::RecordInvalid)
    copy_aptitude_test.errors.must_include(:en_name)
  end

  test "should create new Aptitude" do
    aptitude = AptitudeTest.create(en_name: 'sample aptitude', jp_name: '-', last_updated_by: 'admin')
    # aptitude should be valid
    assert aptitude.valid?
    assert_equal 'sample aptitude', aptitude.en_name
  end

  test "should NOT create new Aptitude without en_name" do
    aptitude = AptitudeTest.create(en_name: nil, jp_name: '-', last_updated_by: 'admin')
    # aptitude should not be valid if en_name is nil
    assert_not_nil aptitude.valid?
  end

  test "should NOT create new Aptitude with duplicate en_name" do
    aptitude = AptitudeTest.create(en_name: 'Food processor', jp_name: '-', last_updated_by: 'admin')
    # aptitude should not be valid if has duplicate
    assert_not aptitude.valid?, 'Found duplicate entry.'
  end

  test "should combine english and japanese name" do
    aptitude = aptitude_tests(:car_maintenace)
    assert_equal '自動車整備/Car maintenace', aptitude.combine_jp_and_en_name
  end
end
