require 'test_helper'

class UserTest < ActiveSupport::TestCase
  setup do
    @user = users(:admin)
  end

  test "the truth" do
    assert true
  end
  
  # Invalid if duplicate fullname or username
  test "invalid if duplicate fullname or username" do
    user_copy = @user.dup    
    user_copy.save
    proc { user_copy.save! }.must_raise(ActiveRecord::RecordInvalid)
    user_copy.errors.must_include(:fullname)
    user_copy.errors.must_include(:username)
  end

  # Invalid with no username
  test "invalid_without_username" do
    @user.username = nil
    refute @user.valid?, 'saved user without a username'
    assert_not_nil @user.errors[:username], 'no validation error if username is present'
  end

  # Invalid with no fullname
  test "invalid_without_fullname" do
    @user.fullname = nil
    refute @user.valid?
    assert_not_nil @user.errors[:fullname]
  end

  # Invalid with no email
  test "invalid_without_email" do
    @user.email = nil
    refute @user.valid?
    assert_not_nil @user.errors[:email]
  end

  # Invalid short password
  test "invalid_with_short_password" do
    @user = User.new
    @user.password = '123'
    refute @user.valid?
    assert_not_nil @user.errors[:password]
  end

  # Other way
  test "should not save user without username" do
    @user.username = nil
    assert_not @user.save, "Saved used without a username"
  end

  # Getting Admin Users
  test "#admins" do
    assert_includes User.admins, users(:admin)
    refute_includes User.admins, users(:client1)
  end

  # Getting Client Users
  test "#clients" do
    assert_includes User.clients, users(:client1)
    refute_includes User.clients, users(:admin)
  end

  # Getting Partner Users
  test "#partners" do
    assert_includes User.partners, users(:partner1)
    refute_includes User.partners, users(:client1)
  end

  # Before save callback set downcase
  test "role and email downcase" do
    @user.email = "ALDRIEN@skyArch.net"
    @user.role = "ADmiN"
    
    assert_equal(@user.send(:set_downcase_email), "aldrien@skyarch.net")
    assert_equal(@user.send(:set_downcase_role), "admin")
  end

  # Upload image, when required fields are valid
  test "image uploading with paperclip" do
    @user.fullname = users(:partner1).fullname
    @user.username = users(:partner1).username
    @user.password_digest = users(:partner1).password_digest
    @user.status = users(:partner1).status
    @user.email = users(:partner1).email
    @user.role = users(:partner1).role
    @user.last_updated_by = users(:partner1).last_updated_by
    @user.image = File.new("test/fixtures/2x2.jpg")
    assert @user
  end

  # Checking Admin Ability
  test "admin can manage all" do
    @admin = users(:admin)
    ability = Ability.new(@admin)
    assert ability.can?(:manage, User.new)
    assert ability.can?(:manage, Project.new)
    assert ability.can?(:destroy, users(:client1))
    assert ability.can?(:destroy, projects(:project1))
  end

  # Checking Partner Ability
  test "partners account can and cannot" do
    @partner = users(:partner1)
    ability = Ability.new(@partner)
    assert ability.can?(:create, applicants(:aldrien))
    assert ability.can?(:update, Applicant.new(creator_id: @partner.id))
    assert ability.can?(:read, Project.new(partner_id: @partner.id))
    assert ability.cannot?(:destroy, applicants(:aldrien))
    assert ability.cannot?(:manage, Project.new)
    assert ability.cannot?(:manage, User.new)
  end

  # Checking Client Ability
  test "clients account can and cannot" do
    @client = users(:client1)
    @project_failed = projects(:failed_project) #Project.new(client_id: @client.id)
    @project_inprogress = projects(:project1)
    ability = Ability.new(@client)    
    # assert ability.can?(:read, Applicant.new(project_id: @project.id))
    
    assert ability.can?(:read, @project_inprogress)
    assert ability.can?(:read, projects(:project1))
    assert ability.cannot?(:read, @project_failed)

    assert ability.cannot?(:manage, applicants(:aldrien))    
    assert ability.cannot?(:manage, Project.new)
    assert ability.cannot?(:manage, User.new)
  end

end