require 'test_helper'

class JobTypeTest < ActiveSupport::TestCase
  # Invalid if duplicate english name
  test "invalid if duplicate en_name" do
    copy_job_type = job_types(:engineer).dup
    copy_job_type.save
    proc { copy_job_type.save! }.must_raise(ActiveRecord::RecordInvalid)
    copy_job_type.errors.must_include(:en_name)
  end

  test "should create new JobType" do
    job_type = JobType.create(en_name: 'painter', jp_name: '-', last_updated_by: 'admin')
    # job_type should be valid
    assert job_type.valid?
    assert_equal 'painter', job_type.en_name
  end

  test "should NOT create new JobType without en_name" do
    job_type = JobType.create(en_name: nil, last_updated_by: 'admin')
    # job_type should not be valid if en_name is nil
    assert_not_nil job_type.valid?
  end

  test "should NOT create new JobType with duplicate en_name" do
    job_type = JobType.create(en_name: 'garment factory worker', jp_name: '-', last_updated_by: 'admin')
    # job_type should not be valid if has duplicate
    assert_not job_type.valid?, 'Found duplicate entry.'
  end

  test "should combine english and japanese name" do
    job_type = job_types(:confectioner)
    assert_equal '菓子屋/confectioner', job_type.combine_jp_and_en_name
  end

  test "should combine japanese and english name" do
    job_type = job_types(:nurse)
    assert_equal '看護士・看護師/nurse', job_type.combine_jp_and_en_name
  end
end
