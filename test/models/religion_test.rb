require 'test_helper'

class ReligionTest < ActiveSupport::TestCase
  # Invalid if duplicate english name
  test "invalid if duplicate en_name" do
    copy_religion = religions(:christianity).dup
    copy_religion.save
    proc { copy_religion.save! }.must_raise(ActiveRecord::RecordInvalid)
    copy_religion.errors.must_include(:en_name)
  end

  test "should create new Religion" do
    religion = Religion.create(en_name: 'painter', jp_name: '-', last_updated_by: 'admin')
    # religion should be valid
    assert religion.valid?
    assert_equal 'painter', religion.en_name
  end

  test "should NOT create new Religion without en_name" do
    religion = Religion.create(en_name: nil, last_updated_by: 'admin')
    # religion should not be valid if en_name is nil
    assert_not_nil religion.valid?
  end

  test "should NOT create new Religion with duplicate en_name" do
    religion = Religion.create(en_name: 'christianity', jp_name: '-', last_updated_by: 'admin')
    # religion should not be valid if has duplicate
    assert_not religion.valid?, 'Found duplicate entry.'
  end

  test "should combine japanese and english name" do
    religion = religions(:christianity)
    assert_equal 'キリスト教/Christianity', religion.combine_jp_and_en_name
  end
end
