require 'test_helper'

class WorkExperienceTest < ActiveSupport::TestCase
  setup do
    @john = applicants(:john)
    @sarah = applicants(:sarah)
    @vj = applicants(:vj)
    @aldrien = applicants(:aldrien)
  end

  test "should create new work experience" do    
    new_work_experience = WorkExperience.create(
      applicant_id: @aldrien.id,
      job_type_id: 3,
      country_id: 1,
      entering_date: '2014/12',
      leaving_date: '2016/12',
      company_name: 'The Tokyo Company',
      monthly_salary: '200'
    )

    assert new_work_experience.valid?
    assert_includes @aldrien.work_experiences, new_work_experience
  end

  test "should validate leaving_date if up_to_present is present" do    
    new_work_experience = WorkExperience.create(
      applicant_id: @aldrien.id,
      job_type_id: 3,
      country_id: 1,
      entering_date: '2014/12',
      leaving_date: '2016/12',
      up_to_present: true,
      company_name: 'The Tokyo Company',
      monthly_salary: '200'
    )

    assert new_work_experience.valid?
    assert_equal new_work_experience.leaving_date, nil
    assert_not_equal new_work_experience.leaving_date, '2016/12'
  end

  test "should return applicant's work experience country and job type" do
    assert_includes @john.work_experiences, work_experiences(:john_work_experience)
    assert_includes @sarah.work_experiences, work_experiences(:sarah_work_experience)
    assert_includes @vj.work_experiences, work_experiences(:vj_work_experience)

    @john_work_experience = work_experiences(:john_work_experience)

    assert_equal @john_work_experience.country, countries(:bangladesh)
    assert_equal @john_work_experience.job_type, job_types(:driver)    
  end
end