require 'test_helper'

class FamilyBackgroundTest < ActiveSupport::TestCase
  setup do
    @applicant = applicants(:sarah)
  end

  test "should create applicant family background" do
    new_family_background = FamilyBackground.create(
      applicant_id: @applicant.id,
      father_name: 'Will Smith',
      mother_name: 'Emelia Clarke',
      husband_name: nil,
      wife_name: 'Jessica',
      father_job_id: 1,
      mother_job_id: 2,
      husband_job_id: nil,
      wife_job_id: 4,
      father_phone_number: '+08 0967 1152 11',
      mother_phone_number: '+08 7867 1152 14',
      husband_phone_number: '+08 7567 1152 34',
      wife_phone_number: '+08 3467 1152 54',
      brother_count: 1,
      sister_count: 1
    )

    assert new_family_background.valid?
    assert_equal @applicant.family_background, new_family_background    
  end

  test "should return applicant family background" do
    @aldrien = applicants(:aldrien)
    assert_equal @aldrien.family_background, family_backgrounds(:aldrien_family_background)
  end

  test "should return applicant family background job types" do
    aldrien_family_background = family_backgrounds(:aldrien_family_background)

    assert_equal aldrien_family_background.father_occupation, job_types(:engineer)
    assert_equal aldrien_family_background.mother_occupation, job_types(:nurse)
    assert_equal aldrien_family_background.wife_occupation, job_types(:care_worker)
  end
end