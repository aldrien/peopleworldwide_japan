require 'test_helper'

class PwwLogoTest < ActiveSupport::TestCase
  test "should list all the headers" do
    headers = pww_logos(:main_logo)

    assert_not_nil headers
  end

  test "should create new header" do
    new_header = PwwLogo.create(title: 'New Header', content: 'sample new header', status: true, logo: File.new("test/fixtures/pww_japan_small.png"))
    assert new_header
  end

  test "should not create header without title" do
    new_header = PwwLogo.create(title: nil, content: 'sample new header', status: true, logo: File.new("test/fixtures/pww_japan_small.png"))
    refute new_header.valid?
    assert_not_nil new_header.errors[:title]
    assert_includes new_header.errors[:title], "can't be blank"
  end

  test "should not create header without content" do
    new_header = PwwLogo.create(title: 'New Header', content: nil, status: true, logo: File.new("test/fixtures/pww_japan_small.png"))
    refute new_header.valid?
    assert_not_nil new_header.errors[:content]
    assert_includes new_header.errors[:content], "can't be blank"
  end

  test "should not create header without logo" do
    new_header = PwwLogo.create(title: 'New Header', content: 'sample new header', status: true, logo: nil)
    refute new_header.valid?
    assert_not_nil new_header.errors[:logo]
    assert_includes new_header.errors[:logo], "can't be blank"
  end

  test "should update header's title" do
    new_title = "People World Wide Japan"
    new_content = "This is updated content"

    pww_logo = pww_logos(:main_logo)    
    pww_logo.title = new_title
    pww_logo.content = new_content
    assert pww_logo.valid?
    
    assert_equal new_title, pww_logo.title
    assert_equal new_content, pww_logo.content
  end
end