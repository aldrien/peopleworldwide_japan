require 'test_helper'

class ApplicantTest < ActiveSupport::TestCase
  setup do
    @applicant = applicants(:aldrien)
    @creator = users(:partner1)
    @project = projects(:project1)
    @job_type = job_types(:engineer)
    @country = countries(:japan)
    @religion = religions(:christianity)
    @family = family_backgrounds(:aldrien_family_background)
  end

  # Invalid if duplicate fullname or username
  test "invalid if duplicate name or email" do
    applicant_copy = @applicant.dup    
    applicant_copy.save
    proc { applicant_copy.save! }.must_raise(ActiveRecord::RecordInvalid)
    applicant_copy.errors.must_include(:name)
    applicant_copy.errors.must_include(:email)
  end

  # Invalid if custom_code is more than 6 characters
  test "invalid if custom_code is more than 6 characters" do
    @applicant.custom_code = '123456789'
    @applicant.save
    proc { @applicant.save! }.must_raise(ActiveRecord::RecordInvalid)
    @applicant.errors.must_include(:custom_code)    
  end  

  # Model Relationships
  test "should return applicant's creator or partner who registered" do
    assert_equal @applicant.creator, @creator
  end

  test "should get applicant's academic abilities" do
    assert_equal 2, @applicant.applicant_academic_abilities.size
    assert_includes @applicant.applicant_academic_abilities, applicant_academic_abilities(:aldrien_academic_ability1)
    assert_includes @applicant.applicant_academic_abilities, applicant_academic_abilities(:aldrien_academic_ability2)
    
    refute_includes @applicant.applicant_academic_abilities, applicant_academic_abilities(:jenny_academic_ability1)
    refute_includes @applicant.applicant_academic_abilities, applicant_academic_abilities(:jenny_academic_ability2)
  end

  test "should get applicant's aptitude tests" do
    assert_equal 3, @applicant.applicant_aptitude_tests.size
    assert_includes @applicant.applicant_aptitude_tests, applicant_aptitude_tests(:aldrien_aptitude_test1)
    assert_includes @applicant.applicant_aptitude_tests, applicant_aptitude_tests(:aldrien_aptitude_test2)
    assert_includes @applicant.applicant_aptitude_tests, applicant_aptitude_tests(:aldrien_aptitude_test3)

    refute_includes @applicant.applicant_aptitude_tests, applicant_aptitude_tests(:jenny_aptitude_test1)
    refute_includes @applicant.applicant_aptitude_tests, applicant_aptitude_tests(:jenny_aptitude_test2)
    refute_includes @applicant.applicant_aptitude_tests, applicant_aptitude_tests(:jenny_aptitude_test3)
  end

  test "should return applicant's country" do
    assert_equal @applicant.country, @country
  end

  test "should return applicant's religion" do
    assert_equal @applicant.religion, @religion
  end

  test "should return applicant's project" do
    assert_equal @applicant.project, @project
  end

  test "should return applicant's job type based on project" do
    assert_equal @applicant.project.job_type, @job_type
  end

  test "should return applicant's family_background" do
    assert_equal @applicant.family_background, @family
    assert_equal @applicant.family_background.father_name, @family.father_name
  end

  test "should return applicant's work experiences" do
    assert_equal 1, @applicant.work_experiences.size
    assert_includes @applicant.work_experiences, work_experiences(:aldrien_work_experience)
    
    refute_includes @applicant.work_experiences, work_experiences(:vj_work_experience)
    refute_includes @applicant.work_experiences, work_experiences(:john_work_experience)
  end

  test "should return applicant's education backgrounds" do
    assert_equal 1, @applicant.education_backgrounds.size
    assert_includes @applicant.education_backgrounds, education_backgrounds(:aldrien_education)
    
    refute_includes @applicant.education_backgrounds, education_backgrounds(:john_education)   
    refute_includes @applicant.education_backgrounds, education_backgrounds(:sarah_education)
  end

  test "should return applicant's files" do
    assert_includes @applicant.applicant_files, applicant_files(:health_check)
    assert_includes @applicant.applicant_files, applicant_files(:id_copy)
  end

  test "should return applicant's project history" do
    assert_equal 1, @applicant.applicant_project_histories.size
    assert_includes @applicant.applicant_project_histories, applicant_project_histories(:aldrien_project_history)
    
    assert_equal applicant_project_histories(:aldrien_project_history).project, projects(:project1)    
  end

  test "should return applicants which registered today" do
    today_registered = Applicant.today
    assert_includes today_registered, applicants(:aldrien)
    refute_includes today_registered, applicants(:john)
  end

  test "should return applicants based on status" do
    in_progress = Applicant.in_progress
    confirmed = Applicant.confirmed
    waiting = Applicant.waiting
    failed = Applicant.failed
    not_waiting = Applicant.not_waiting

    assert_includes in_progress, applicants(:sarah)
    assert_includes confirmed, applicants(:aldrien)
    assert_includes waiting, applicants(:john)
    assert_includes failed, applicants(:vj)
    
    assert_includes not_waiting, applicants(:jenny)
    assert_not_includes not_waiting, applicants(:john)
  end

  test "should not return applicants which is in to be deleted list" do
    list_of_to_be_deleted = requested_to_be_deleted_applicants(:delete_me_sarah)

    applicants_not_to_be_deleted = Applicant.not_to_be_deleted(list_of_to_be_deleted.applicant_id)
    assert_not_includes applicants_not_to_be_deleted, applicants(:sarah)
  end

  # Search
  test "search by applicants by name" do
    search_result = Applicant.applicant_name('aldrien')

    assert_includes search_result, applicants(:aldrien)
    assert_not_includes search_result, applicants(:sarah)
  end

  test "search by applicants by age" do
    search_result = Applicant.age_between(20, 24)

    assert_includes search_result, applicants(:aldrien)
    assert_includes search_result, applicants(:john)
    assert_not_includes search_result, applicants(:sarah)
    assert_not_includes search_result, applicants(:vj)
  end

  test "search by applicants by exact age" do
    search_result = Applicant.exact_age(27)

    assert_includes search_result, applicants(:vj)
    assert_not_includes search_result, applicants(:aldrien)
  end

  test "search by applicants by english skill" do
    conversational = Applicant.english_skill('Conversational')
    native = Applicant.english_skill('Native')
    business = Applicant.english_skill('Business')
    no = Applicant.english_skill('No')

    assert_includes conversational, applicants(:vj)
    assert_includes conversational, applicants(:aldrien)
    assert_includes native, applicants(:sarah)
    assert_includes business, applicants(:john)

    assert_empty no
  end

  test "search by applicants by work related expirience" do
    related = Applicant.is_work_related_expirience(true)
    not_related = Applicant.is_work_related_expirience(false)

    assert_includes related, applicants(:aldrien)
    assert_includes related, applicants(:vj)

    assert_includes not_related, applicants(:john)
    assert_includes not_related, applicants(:sarah)
  end

  test "search by applicants by status" do
    in_progress = Applicant.status('In-progress')
    confirmed = Applicant.status('Confirmed')
    waiting = Applicant.status('Waiting')
    failed = Applicant.status('Failed')

    assert_includes in_progress, applicants(:sarah)
    assert_includes confirmed, applicants(:aldrien)
    assert_includes waiting, applicants(:john)
    assert_includes failed, applicants(:vj)
  end

  test "search applicants by country" do
    cambodia = countries(:cambodia)
    search_result = Applicant.country(cambodia)

    assert_includes search_result, applicants(:sarah)
  end

  test "search applicants by JLPT Level" do
    search_result = Applicant.jlpt_level('N1')
    assert_includes search_result, applicants(:vj)
    assert_not_includes search_result, applicants(:sarah)
  end

  test "search applicants by marital status" do
    single = Applicant.marital_status('Single')
    married = Applicant.marital_status('Married')

    assert_includes single, applicants(:aldrien)
    assert_includes single, applicants(:john)
    assert_includes single, applicants(:vj)
    assert_includes married, applicants(:sarah)
  end

  test "search applicants by gender" do
    male = Applicant.gender('Male')
    female = Applicant.gender('Female')

    assert_includes male, applicants(:aldrien)
    assert_includes male, applicants(:vj)
    assert_includes male, applicants(:john)

    assert_includes female, applicants(:sarah)
  end

  test "search applicants by last academic background" do
    high_school_grad = Applicant.last_academic('High school graduate')
    vocational_grad = Applicant.last_academic('Vocational school graduated')
    university_grad = Applicant.last_academic('University graduate')
    university_drop = Applicant.last_academic('University drop-out')

    assert_includes high_school_grad, applicants(:aldrien)
    assert_includes vocational_grad, applicants(:john)
    assert_includes university_grad, applicants(:sarah)
    assert_includes university_drop, applicants(:vj)
  end

  test "search applicants by religion" do
    christianity = Applicant.religion(religions(:christianity))
    hinduism = Applicant.religion(religions(:hinduism))
    other = Applicant.religion(religions(:other))

    assert_includes christianity, applicants(:aldrien)
    assert_includes hinduism, applicants(:john)
    assert_includes other, applicants(:vj)
    assert_includes other, applicants(:sarah)
  end

  test "search applicants by project" do
    project = projects(:project1)
    search_result = project.applicants

    assert_includes search_result, applicants(:aldrien)
    assert_includes search_result, applicants(:john)
    assert_includes search_result, applicants(:sarah)

    refute_includes search_result, applicants(:vj)

    other = Applicant.project(projects(:project2))
    assert_includes other, applicants(:vj)
  end

  # Upload image, when required fields are valid
  test "applicant's profile & wholebody picture uploading with paperclip" do
    applicant = Applicant.create(
      creator_id: users(:partner1),
      edit_status: false,
      last_updated_by: 'admin',
      name: 'Cliff Hokage',
      project_id: projects(:project1),
      country_id: countries(:japan),
      religion_id: religions(:christianity),
      status: 'In-progress',
      email: 'anfone@skyarch.net',
      phone: '073-0000-0000',
      address: 'Tokyo Japan',
      gender: 'Male',
      birth_date: '1989/01/3',
      age: '26',
      height: '111',
      weight: '58',
      marital_status: 'Single',
      last_academic: 'University graduate',
      dominant_hand: 'Right',
      english_skill: 'Conversational',
      jlpt_level: 'No',
      is_work_related_expirience: true,
      profile: File.new("test/fixtures/2x2.jpg"),
      wholebody: File.new("test/fixtures/wholebody.jpg")
    )
    assert applicant
  end
end