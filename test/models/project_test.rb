require 'test_helper'

class ProjectTest < ActiveSupport::TestCase
  fixtures :users
  fixtures :projects

  setup do
    @project = Project.new
  end

  test "the truth" do
    assert true
  end

  # Valid saving with required fields are valid
  test 'valid project' do
    @project.title = projects(:project1).title,
    @project.company_name = projects(:project1).company_name
    @project.partner_id = projects(:project1).partner_id
    @project.client_id = projects(:project1).client_id
    @project.country_id = projects(:project1).country_id
    @project.job_type_id = projects(:project1).job_type_id
    @project.status = projects(:project1).status
    @project.no_of_needed_applicants = projects(:project1).no_of_needed_applicants
    @project.description = projects(:project1).description
    @project.notes = projects(:project1).notes
    @project.last_updated_by = projects(:project1).last_updated_by

    assert @project.valid?
    assert @project
  end

  # Other way
  test 'invalid without title' do
    @project.title = nil
    refute @project.valid?
    assert_not_nil @project.errors[:title]
  end

  # Invalid with no title
  test "should not save post without title" do
    @project.title = nil
    assert_not @project.save, "Saved the post without a title"
  end

  # Invalid with no description
  test "should not save post without description" do    
    @project.description = nil
    assert_not @project.save, "Saved the post without a description"
  end

  # Invalid with no selected partner
  test "should not save post without partner" do
    @project.partner_id = nil
    assert_not @project.save, "Saved the post without a partner"
  end

  # Invalid with no selected client
  test "should not save post without client" do
    @project.client_id = nil
    assert_not @project.save, "Saved the post without a client"
  end

  # Invalid with no selected country
  test "should not save post without country" do
    @project.country_id = nil
    assert_not @project.save, "Saved the post without a country"
  end

  # Invalid with no selected Job Type
  test "should not save post without job_type" do
    @project.job_type_id = nil
    assert_not @project.save, "Saved the post without a job_type"
  end

  # Invalid with no status
  test "should not save post without status" do
    @project.status = nil
    assert_not @project.save, "Saved the post without a status"
  end

  # Invalid with no selected number of applicants
  test "should not save post without no_of_needed_applicants" do
    @project.no_of_needed_applicants = nil
    assert_not @project.save, "Saved the post without a no_of_needed_applicants"
  end

  # Upload image, when required fields are valid
  test "file uploading with paperclip" do
    @project.title = 'New Project',
    @project.company_name = 'Sample Company',
    @project.partner_id = 2,
    @project.client_id =  5,
    @project.country_id = 5,
    @project.job_type_id = 7,
    @project.status = 'In-progress',
    @project.no_of_needed_applicants = 40,
    @project.description = 'Sample descriptions.',
    @project.notes = 'No notes!',
    @project.last_updated_by = 'admin',
    @project.document = File.new("test/fixtures/sample.doc")
    
    assert @project
    assert_equal 'sample.doc', @project.document_file_name
  end
end
