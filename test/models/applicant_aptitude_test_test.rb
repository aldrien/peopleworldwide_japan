require 'test_helper'

class ApplicantAptitudeTestTest < ActiveSupport::TestCase
  test "should NOT create without applicant" do
    applicant_aptitude = ApplicantAptitudeTest.create(applicant_id: nil, aptitude_test_id: academic_abilities(:pwa_japanese_skill_test), score: 90)
    assert_not_nil applicant_aptitude.valid?
  end

  test "should NOT create without score" do
    applicant_aptitude = ApplicantAptitudeTest.create(applicant_id: applicants(:sarah), aptitude_test_id: aptitude_tests(:food_processor), score: nil)
    assert_not_nil applicant_aptitude.valid?
  end

  test "should NOT create if score is greater than 100" do
    applicant_aptitude = ApplicantAptitudeTest.create(applicant_id: applicants(:sarah), aptitude_test_id: aptitude_tests(:food_processor), score: 999)
    assert_not_nil applicant_aptitude.valid?
  end

  test "should get data in between scores" do
    applicant = applicants(:aldrien)
    applicant_apt_tests = applicant.applicant_aptitude_tests.score_between(80, 90)

    assert_includes applicant_apt_tests, applicant_aptitude_tests(:aldrien_aptitude_test2)
    assert_includes applicant_apt_tests, applicant_aptitude_tests(:aldrien_aptitude_test3)
    refute_includes applicant_apt_tests, applicant_aptitude_tests(:aldrien_aptitude_test1)
  end

  test "should get data by exact score" do
    applicant = applicants(:aldrien)
    applicant_apt_tests = applicant.applicant_aptitude_tests.by_score(95)

    assert_includes applicant_apt_tests, applicant_aptitude_tests(:aldrien_aptitude_test1)
    refute_includes applicant_apt_tests, applicant_aptitude_tests(:aldrien_aptitude_test2)
  end
end
