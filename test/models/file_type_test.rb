require 'test_helper'

class FileTypeTest < ActiveSupport::TestCase
  # Invalid if duplicate english name
  test "invalid if duplicate en_name" do
    copy_file_type = file_types(:passport_copy).dup
    copy_file_type.save
    proc { copy_file_type.save! }.must_raise(ActiveRecord::RecordInvalid)
    copy_file_type.errors.must_include(:en_name)
  end

  test "should create new FileType" do
    file_type = FileType.create(en_name: 'sample file_type', jp_name: '-', last_updated_by: 'admin')
    # file_type should be valid
    assert file_type.valid?
    assert_equal 'sample file_type', file_type.en_name
  end

  test "should NOT create new FileType without en_name" do
    file_type = FileType.create(en_name: nil, last_updated_by: 'admin')
    # file_type should not be valid if en_name is nil
    assert_not_nil file_type.valid?
  end

  test "should NOT create new FileType with duplicate en_name" do
    file_type = FileType.create(en_name: 'ID copy', jp_name: '-', last_updated_by: 'admin')
    # file_type should not be valid if has duplicate
    assert_not file_type.valid?, 'Found duplicate entry.'
  end

  test "should combine english and japanese name" do
    file_type = file_types(:passport_copy)
    assert_equal 'パスポートコピー/Passport copy', file_type.combine_jp_and_en_name
  end
end
