require 'test_helper'

class EducationBackgroundTest < ActiveSupport::TestCase
  setup do
    @applicant = applicants(:john)
  end

  test "should create applicant education background" do
    new_education_background = EducationBackground.create(applicant_id: @applicant.id, date: '2012/03', university: 'Sample University', status: 'Graduate')

    assert new_education_background.valid?
    assert_includes @applicant.education_backgrounds, education_backgrounds(:john_education)
    assert_includes @applicant.education_backgrounds, new_education_background
  end
end
