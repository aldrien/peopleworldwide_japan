require 'test_helper'

class RequestedToBeDeletedApplicantTest < ActiveSupport::TestCase
  setup do
    @partner = users(:partner1)
    @applicant = applicants(:sarah)
  end

  # Invalid if duplicate applicant
  test "invalid if duplicate applicant" do
    copy_requested = requested_to_be_deleted_applicants(:delete_me_sarah).dup
    copy_requested.save
    proc { copy_requested.save! }.must_raise(ActiveRecord::RecordInvalid)
    copy_requested.errors.must_include(:applicant_id)
  end

  test "should create new Request" do
    request = RequestedToBeDeletedApplicant.create(partner_id: @partner, applicant_id: @applicant, reason: 'sample deletion reason')
    assert request.valid?
  end

  test "should NOT create new Request without applicant" do
    request = RequestedToBeDeletedApplicant.create(partner_id: @partner, applicant_id: nil, reason: 'sample deletion reason')
    # job_type should not be valid if en_name is nil
    assert_not_nil request.valid?
  end
end
