require 'test_helper'

class ApplicantAcademicAbilityTest < ActiveSupport::TestCase
  test "should NOT create without applicant" do
    applicant_acad_ability = ApplicantAcademicAbility.create(applicant_id: nil, academic_ability_id: academic_abilities(:pwa_japanese_skill_test), score: 90)
    assert_not_nil applicant_acad_ability.valid?
  end

  test "should NOT create without score" do
    applicant_acad_ability = ApplicantAcademicAbility.create(applicant_id: applicants(:sarah), academic_ability_id: academic_abilities(:pwa_japanese_skill_test), score: nil)
    assert_not_nil applicant_acad_ability.valid?
  end

  test "should NOT create if score is greater than 100" do
    applicant_acad_ability = ApplicantAcademicAbility.create(applicant_id: applicants(:sarah), academic_ability_id: academic_abilities(:pwa_japanese_skill_test), score: 999)
    assert_not_nil applicant_acad_ability.valid?
  end

  test "should get data in between scores" do
    applicant = applicants(:aldrien)
    applicant_acad_abilities = applicant.applicant_academic_abilities.score_between(80, 85)

    assert_includes applicant_acad_abilities, applicant_academic_abilities(:aldrien_academic_ability2)
    refute_includes applicant_acad_abilities, applicant_academic_abilities(:aldrien_academic_ability1)
  end

  test "should get data by exact score" do
    applicant = applicants(:aldrien)
    applicant_acad_abilities = applicant.applicant_academic_abilities.by_score(95)

    assert_includes applicant_acad_abilities, applicant_academic_abilities(:aldrien_academic_ability1)
    refute_includes applicant_acad_abilities, applicant_academic_abilities(:aldrien_academic_ability2)
  end
end
