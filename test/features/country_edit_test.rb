require "test_helper"

feature "Country Edit" do
  before do
   visit root_path
   @record = countries(:extra_country)
   @new_english_name = 'Country Updated'
  end

  scenario 'should update country', js: true do
    feature_login('admin', 'pwwadmin')
    visit '/en/countries'

    page.must_have_content 'Countries List & Management'
    page.must_have_content 'Add Country'
  
    select '25', from: 'dataTables-countries_length'
    
    # page.execute_script("$('div.ajax_loading').css('opacity', 0)")
    wait_for do      
      page.has_css?('div.ajax_loading', :visible => false)
    end

    within 'table#dataTables-countries' do
      page.has_css?('td', :text => @record.en_name, :visible => true)
      page.has_css?('td', :text => @record.jp_name, :visible => true)
      page.find(:xpath, "//td/a[text()='#{@record.en_name}']").click
    end

    within 'form.editableform' do
      find(:xpath, "//div[@class='editable-input']/input").set(@new_english_name)
      find(:xpath, "//div[@class='editable-buttons']/button[@type='submit']").click
    end

    page.must_have_content 'Well done! English Name was successfully updated.'
  end
end