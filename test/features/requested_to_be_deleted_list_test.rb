require "test_helper"

feature "Requested To Be Deleted Applicant's List" do
  before do
   visit root_path
   @requested = requested_to_be_deleted_applicants(:delete_me_sarah)
  end

  scenario 'should have applicant Sarah record' do
    feature_login('admin', 'pwwadmin')
    visit '/en/requests_for_applicant_record_deletion'

    page.must_have_content 'List Of Requested Applicants To Be Deleted'
    page.must_have_content 'Requested By Partners'

    within 'table#makeDataTable' do
      page.has_css?('td', :text => @requested.applicant.try(:name), :visible => true)
      page.has_css?('td', :text => @requested.partner.try(:fullname), :visible => true)
    end
  end
end