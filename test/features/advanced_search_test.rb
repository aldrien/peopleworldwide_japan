require "test_helper"

feature "Advanced Search Test" do
  before do
   visit root_path
  end

  scenario 'should return search result with applicant name like' do
    feature_login('admin', 'pwwadmin')
    visit '/en/search'
    
    page.must_have_content "Search Applicants"

    within 'form#search_form' do
      fill_in 'applicant_name', with: 'aldrien'
      click_button('Search Now')
    end

    within 'table#makeDataTable' do
      page.must_have_content applicants(:aldrien).name
    end

    within 'form#search_form' do
      fill_in 'applicant_name', with: ''
      select 'Project 101', from: 'project'

      click_button('Search Now')
    end

    within 'table#makeDataTable' do
      page.must_have_content applicants(:aldrien).name
      page.must_have_content applicants(:jenny).name
      page.must_have_content applicants(:john).name

      page.wont_have_content applicants(:sarah).name # because Sarah was in to_be_deleted_list
      page.wont_have_content applicants(:vj).name # because VJ was in Project 102
    end

    click_link 'Reset Filters'
    within 'form#search_form' do
      fill_in 'applicant_name', with: ''
      select 'Project 102', from: 'project'

      click_button('Search Now')
    end

    within 'table#makeDataTable' do
      page.must_have_content applicants(:vj).name # because VJ was in Project 102

      page.wont_have_content applicants(:aldrien).name
      page.wont_have_content applicants(:jenny).name
      page.wont_have_content applicants(:john).name
      page.wont_have_content applicants(:sarah).name # because Sarah was in to_be_deleted_list      
    end

  end
end