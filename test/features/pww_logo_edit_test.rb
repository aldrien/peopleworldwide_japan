require "test_helper"

feature "Pww Logo Header Edit" do
  before do
   visit root_path
  end

  scenario 'should edit pww logo header' do
    feature_login('admin', 'pwwadmin')
    visit '/en/pww_logos'

    page.must_have_content "PDF Header Management"

    find(:xpath, "//tbody/tr[last()]/td[5]/a[1]").click
    page.must_have_content "Edit PDF Header"

    fill_in 'pww_logo_title', with: 'New Update Pww Header Logo'
  # Upload header logo - make visible file uplaod first
    page.execute_script("$('input#pww_logo_image').css('opacity', 1)")
    attach_file('pww_logo[logo]', Rails.root.join('test/fixtures/pww_japan_small.png'))
    fill_in_ckeditor 'pww_logo_content', with: '〒105-0001 東京都港区虎ノ⾨2-7-5 BUREX虎ノ⾨7F
                                                〒105-0001 7F BUREX Toranom on, 2-7-5 Minato-ku, Tokyo Japan
                                                TEL:+81-3-3539-2237 FAX:+81-3-3539-2238'

    click_button('SAVE NOW')
    
    page.must_have_content "Well done! New Update Pww Header Logo information was successfully updated."
    assert_equal "/en/pww_logos", page.current_path #current_path
  end
end
