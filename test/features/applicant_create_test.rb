require "test_helper"

feature "Register Applicant Test" do
  before do
    visit root_path
    
    @applicant_name = 'Kobe'
    @marital_status = 'Single'
    @gender = 'Male'
    @last_academic = 'University graduate'
    @birth_date = '1991/09/28'
    @religion = 'Christianity'
    @dominant_hand = 'Right'
    @height = '153'
    @weight = '58'
    @country = 'Japan'
    @english_level = 'Conversational'
    @jlpt_level = 'No'
    @is_work_related = '有り/Yes'

    @phone_number = '1234567890'
    @email = 'new-applicant@skyarch.net'
    @home_address = 'Tokyo, Japan'
  end

  scenario 'should register new applicant record' do
    feature_login('admin', 'pwwadmin')
    visit '/en/applicants/new'

    within '#new_applicant' do
    # Basic Information
      fill_in 'applicant_name', with: @applicant_name
      select @marital_status, from: 'applicant_marital_status'
      select @gender, from: 'applicant_gender'
      select @last_academic, from: 'applicant_last_academic'
      fill_in 'applicant_birth_date', with: @birth_date
      select @religion, from: 'applicant_religion_id'
      select @dominant_hand, from: 'applicant_dominant_hand'
      fill_in 'applicant_height', with: @height
      select @country, from: 'applicant_country_id'
      fill_in 'applicant_weight', with: @weight
      select @english_level, from: 'applicant_english_skill'
      select @jlpt_level, from: 'applicant_jlpt_level'
      select @is_work_related, from: 'applicant_is_work_related_expirience'

    # Upload photo - make visible file uplaod first
      page.execute_script("$('input#applicant_profile').css('opacity', 1)")
      page.execute_script("$('input#applicant_wholebody').css('opacity', 1)")
      attach_file('applicant[profile]', Rails.root.join('test/fixtures/2x2.jpg'))
      attach_file('applicant[wholebody]', Rails.root.join('test/fixtures/wholebody.jpg'))

    # Contact Information
      click_link('連絡先 / Contact Information')
      fill_in 'applicant_phone', with: @phone_number
      fill_in 'applicant_email', with: @email
      fill_in 'applicant_address', with: @home_address

    # Education Background
      click_link('学歴 / Education Background')
      fill_in 'applicant_education_backgrounds_attributes_0_date', with: '2012/03'
      fill_in 'applicant_education_backgrounds_attributes_0_university', with: 'College'
      select '卒業/Graduate', from: 'applicant_education_backgrounds_attributes_0_status'
      click_link('ADD')

      find(:xpath, "//table[@id='table_education_background']/tbody/tr[2]/td[1]//input").set('2008/04')
      find(:xpath, "//table[@id='table_education_background']/tbody/tr[2]/td[2]//input").set('High School')
      find(:xpath, "//table[@id='table_education_background']/tbody/tr[2]/td[3]//select").select('卒業/Graduate')
      click_link('ADD')

      find(:xpath, "//table[@id='table_education_background']/tbody/tr[3]/td[1]//input").set('2004/04')
      find(:xpath, "//table[@id='table_education_background']/tbody/tr[3]/td[2]//input").set('Elementary')
      find(:xpath, "//table[@id='table_education_background']/tbody/tr[3]/td[3]//select").select('卒業/Graduate')

    # Work Experience
      click_link('職歴 / Work Experience')
      fill_in 'applicant_work_experiences_attributes_0_entering_date', with: '2014/12'
      check 'applicant_work_experiences_attributes_0_up_to_present'
      fill_in 'applicant_work_experiences_attributes_0_company_name', with: 'Company 1'
      fill_in 'applicant_work_experiences_attributes_0_monthly_salary', with: '150'
      select 'engineer/技術者', from: 'applicant_work_experiences_attributes_0_job_type_id'
      select '日本/Japan', from: 'applicant_work_experiences_attributes_0_country_id'
      click_link('ADD')
      
      find(:xpath, "//table[@id='table_work_experience']/tbody/tr[2]/td[1]//input").set('2012/06')
      find(:xpath, "//table[@id='table_work_experience']/tbody/tr[2]/td[2]/div/input").set('2014/04')
      find(:xpath, "//table[@id='table_work_experience']/tbody/tr[2]/td[3]//input").set('Company 2')
      find(:xpath, "//table[@id='table_work_experience']/tbody/tr[2]/td[4]//input").set('100')
      find(:xpath, "//table[@id='table_work_experience']/tbody/tr[2]/td[5]//select").select('engineer/技術者')
      find(:xpath, "//table[@id='table_work_experience']/tbody/tr[2]/td[6]//select").select('フィリピン/Philippines')
    
    # Family Background
      click_link('家族について / Family Background')
      fill_in 'applicant_family_background_attributes_father_name', with: 'Lebron James'
      select '機械工/mechanic', from: 'applicant_family_background_attributes_father_job_id'
      fill_in 'applicant_family_background_attributes_father_phone_number', with: '0123456789'
      fill_in 'applicant_family_background_attributes_mother_name', with: 'Ronda Rousey'
      select '看護士・看護師/nurse', from: 'applicant_family_background_attributes_mother_job_id'
      fill_in 'applicant_family_background_attributes_mother_phone_number', with: '0123456789'
      
      choose '妻/Wife:'
      fill_in 'applicant_family_background_attributes_wife_name', with: 'Meisha Tate'
      select '介護福祉士/care worker', from: 'applicant_family_background_attributes_wife_job_id'
      fill_in 'applicant_family_background_attributes_wife_phone_number', with: '0123456789'
      
      fill_in 'applicant_family_background_attributes_brother_count', with: '3'
      fill_in 'applicant_family_background_attributes_sister_count', with: '1'
      fill_in 'applicant_family_background_attributes_daugther_count', with: '1'
      fill_in 'applicant_family_background_attributes_son_count', with: '1'

    # People Worldwide Academy
      click_link('People Worldwide Academy')
      # Academic Ability
      select 'IQ test', from: 'applicant_applicant_academic_abilities_attributes_0_academic_ability_id'
      fill_in 'applicant_applicant_academic_abilities_attributes_0_score', with: '93'
      find(:xpath, "//tr[@id='add_link_academic']//a").click
      find(:xpath, "//table[@id='table_academic_ability']/tbody/tr[2]/td[1]//select").select('PWJ日本語能力テスト/PWA Japanese skill test')
      find(:xpath, "//table[@id='table_academic_ability']/tbody/tr[2]/td[2]//input").set('86')

      # Skill Check
      select '食品工場/Food processor', from: 'applicant_applicant_aptitude_tests_attributes_0_aptitude_test_id'
      fill_in 'applicant_applicant_aptitude_tests_attributes_0_score', with: '90'
      find(:xpath, "//tr[@id='add_link_skill']//a").click
      find(:xpath, "//table[@id='table_skill_check']/tbody/tr[2]/td[1]//select").select('建設業/Construction')
      find(:xpath, "//table[@id='table_skill_check']/tbody/tr[2]/td[2]//input").set('87')
      find(:xpath, "//tr[@id='add_link_skill']//a").click
      find(:xpath, "//table[@id='table_skill_check']/tbody/tr[3]/td[1]//select").select('自動車整備/Car maintenace')
      find(:xpath, "//table[@id='table_skill_check']/tbody/tr[3]/td[2]//input").set('88')

      # Test removing of fields
      find(:xpath, "//tr[@id='add_link_skill']//a").click
      find(:xpath, "//table[@id='table_skill_check']/tbody/tr[4]/td[3]//a").click

    # Upload Applicant's File
      click_link("Upload Applicant's File")
      select '健康診断書/Health check', from: 'applicant_applicant_files_attributes_0_file_type_id'
      # make visible file uplaod first
      page.execute_script("$('input#applicant_applicant_files_attributes_0_document').css('opacity', 1)")
      attach_file('applicant[applicant_files_attributes][0][document]', Rails.root.join('test/fixtures/pww_japan_small.png'))
      fill_in 'applicant_applicant_files_attributes_0_description', with: 'This is a sample applicant file.'
      click_link('ADD')

      find(:xpath, "//table[@id='table_applicant_files']/tbody/tr[2]/td[1]//select").select('パスポートコピー/Passport copy')
      # Test removing of fields      
      find(:xpath, "//table[@id='table_applicant_files']/tbody/tr[2]/td[4]//a").click
    
    # Other Info & Save
      click_link('Other Info &') 
      # select 'Project 101 | engineer', from: 'applicant_project_id'
      fill_in 'applicant_self_introduction_video_link', with: 'https://www.youtube.com/watch?v=dOlCQrRraMA'
      fill_in 'applicant_aptitude_test_link', with: 'aptitude_test_link: https://www.youtube.com/watch?v=UhdSUycEZgg' 
      fill_in 'applicant_psychometric_test_link', with: 'psychometric_test_link: https://www.youtube.com/watch?v=p2MypNWWPjM'
      select 'Partner 101', from: 'applicant_creator_id'
      select 'In-progress', from: 'applicant_status'
      fill_in 'applicant_short_notes', with: 'Short notes here and will be shown in full list pdf report.'
      fill_in_ckeditor 'applicant_notes', with: 'This is Applicant notes!'
      fill_in_ckeditor 'admin_remarks', with: 'This is Admin remarks!'

      check 'applicant_show_notes_in_partner_pdf'
      check 'applicant_show_notes_in_partner_pdf'
      check 'applicant_show_remarks_in_partner_pdf'
      check 'applicant_show_remarks_in_client_pdf'

    # Save Button
      click_button 'SAVE NOW'
    end

    page.must_have_content "#{@applicant_name}'s record was successfully created."
  end

  scenario 'should show required fields if data are not given' do
    feature_login('partner1', 'partner1')
    visit '/en/applicants/new'

    within '#new_applicant' do
      # Other Info & Save
      click_link('Other Info &')
      # Save Button
      click_button 'SAVE NOW'
    end

    page.must_have_content "* Name can't be blank."
    page.must_have_content "* Address can't be blank."
    page.must_have_content "* Gender can't be blank."
    page.must_have_content "* Birth date can't be blank."
    page.must_have_content "* Height can't be blank."
    page.must_have_content "* Weight can't be blank."
    page.must_have_content "* Age can't be blank."
    page.must_have_content "* Country can't be blank."
    page.must_have_content "* Marital status can't be blank."
    page.must_have_content "* Last academic can't be blank."
    page.must_have_content "* Religion can't be blank."
    page.must_have_content "* Dominant hand can't be blank." 
    page.must_have_content "* English skill can't be blank."
    page.must_have_content "* Jlpt level can't be blank."
  end

end