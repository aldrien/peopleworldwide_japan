require "test_helper"

feature "Country Create" do
  before do
   visit root_path
   @new_country_en = 'A New Country'
   @new_country_jp = '-'
  end

  scenario 'should create new country', js: true do
    feature_login('admin', 'pwwadmin')
    visit '/en/countries'

    page.must_have_content 'Countries List & Management'
    page.must_have_content 'Add Country'

    # page.execute_script("$('div.ajax_loading').css('opacity', 0)")
    wait_for do      
      page.has_css?('div.ajax_loading', :visible => false)
    end
    
    within 'form#countries' do
      fill_in 'en_name', with: @new_country_en
      fill_in 'jp_name', with: @new_country_jp

      page.execute_script('$("#country_code").trigger("focus")')
      click_button('ADD')
      
      fill_in 'country_code', with: 'ANC'
      click_button('ADD')
    end
    
    within 'table#dataTables-countries' do
      page.has_css?('td', :text => @new_country_en, :visible => true)
      page.has_css?('td', :text => @new_country_jp, :visible => true)
    end    
    page.must_have_content 'Well done! New country was successfully added.'
  end
end