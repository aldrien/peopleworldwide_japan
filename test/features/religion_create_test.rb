require "test_helper"

feature "Religion Create" do
  before do
   visit root_path
   @new_religion_en = 'A New Religion'
   @new_religion_jp = '-'
  end

  scenario 'should create new religion', js: true do
    feature_login('admin', 'pwwadmin')
    visit '/en/religions'

    page.must_have_content 'Religions List & Management'
    page.must_have_content 'Add Religion'

    # page.execute_script("$('div.ajax_loading').css('opacity', 0)")
    wait_for do      
      page.has_css?('div.ajax_loading', :visible => false)
    end
    
    within 'form#religions' do
      fill_in 'en_name', with: @new_religion_en
      fill_in 'jp_name', with: @new_religion_jp
      click_button('ADD')
    end
    
    within 'table#dataTables-religions' do
      page.has_css?('td', :text => @new_religion_en, :visible => true)
      page.has_css?('td', :text => @new_religion_jp, :visible => true)
    end    
    page.must_have_content 'Well done! New Religion was successfully added.'
  end
end