require "test_helper"

feature "Project Edit" do
  before do
    visit root_path
    
    @title = 'Project Z'
    @company = 'Company Z'
    @description = 'This is a sample update of Project.'
    @partner = 'Partner 101'
    @client = 'Japan Client 1'
    @needed_applicants = '30'
    @country = '日本/Japan'
    @job_type = '技術者/engineer'    
    @admin_notes = 'This is updated Admin Notes!'
  end

  scenario 'should update project with valid data' do
    feature_login('admin', 'pwwadmin')
    visit '/en/projects'

    page.must_have_content "Projects List & Management"
    
    find(:xpath, "//a[@href='#failed']").click
    find(:xpath, "//table[@id='dataTable_failed']/tbody/tr[td//text()[contains(., 'Project Failed')]]/td[last()]/a[3]").click

    page.must_have_content "Edit Project"
    
    fill_in 'project_title', with: @title
    fill_in 'project_company_name', with: @company      
    fill_in_ckeditor 'project_description', with: 'Project Long Descriptions here!!!'
    
    select @partner, from: 'project_partner_id'
    select @client, from: 'project_client_id'
    fill_in 'project_no_of_needed_applicants', with: @needed_applicants
    select @country, from: 'project_country_id'
    select @job_type, from: 'project_job_type_id'    
    
  # Upload document (doc, pdf, image) - make visible file uplaod first
    page.execute_script("$('input#project_document').css('opacity', 1)")
    attach_file('project[document]', Rails.root.join('test/fixtures/pww_japan_small.png'))

    fill_in_ckeditor 'project_notes', with: 'Admin notes here!!!'

    click_button 'SAVE NOW'

    page.must_have_content "Well done! #{@title} was successfully updated."
    assert_equal "/en/projects", page.current_path #current_path
  end
end