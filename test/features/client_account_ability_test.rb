require "test_helper"

feature "Client Account Ability" do
  before do
   visit root_path
   @project = projects(:project1).title
   @company_name = projects(:project1).company_name
  end

  # called after every single test
  teardown do    
    Rails.cache.clear # when controller is using cache it may be a good idea to reset it afterwards
  end

  scenario 'should visit client account pages' do
    feature_login('client1', 'client1')   

    page.must_have_content 'Well done! Hi Client1! you have successfully logged in.'
    page.must_have_content 'プロジェクトリスト / Projects List'

    click_link '進行中/In-Progress'

    # Visit Project Listing Page
    # Should display Project belongs to Client Account
    within 'table#dataTable_in_progress' do
      page.has_css?('td', :text => @project, :visible => true)

      page.wont_have_content(projects(:project2).title)
      page.wont_have_content(projects(:failed_project).title)
    end


    # Visit Project Applicants Page
    find(:xpath, "//table[@id='dataTable_in_progress']/tbody/tr[td//text()[contains(., '#{@project}')]]/td[last()]/a[1]").click

    page.current_path.must_equal view_project_applicants_path(locale: I18n.locale)
    page.must_have_content "#{@company_name}"
    page.must_have_content 'プロジェクトリストへ戻る / Return To Project List'
    page.must_have_content '候補者リスト / List of Applicants'

    within 'table#makeDataTable' do
      page.has_css?('td', :text => applicants(:aldrien).name, :visible => true)
      page.has_css?('td', :text => applicants(:jenny).name, :visible => true)
      page.has_css?('td', :text => applicants(:john).name, :visible => true)
      page.has_css?('td', :text => applicants(:sarah).name, :visible => true)

      page.wont_have_content(applicants(:vj).name)
      page.wont_have_content(applicants(:for_update).name)
      page.wont_have_content(applicants(:for_partner1).name)
    end

    # Client Make Comments To Applicant
    find(:xpath, "//table[@id='makeDataTable']/tbody/tr[td//text()[contains(., 'Aldrien Test')]]/td[last()]//a[last()]").click
    page.must_have_selector('#modal_make_comments', visible: true)
    page.must_have_content "Aldrien Test へのコメント作成"
    
    click_button "セーブ / Save"
    page.must_have_selector('div.invalid_result', visible: true)
    page.must_have_content "パスワード入力 & コメント入力"
    
    # Test Invalid Password
    fill_in 'client_password', with: 'wrong_password'
    fill_in 'client_comments', with: 'This is a sample comments from Client.'
    click_button "セーブ / Save"
    page.must_have_content "Invalid Password / パスワードが間違っています"

    # Valid Client Password
    fill_in 'client_password', with: 'client1'    
    click_button "セーブ / Save"
    page.must_have_content 'コメントの保存が完了しました'

    # Double Check the Comments if passed to data attribute
    find(:xpath, "//a[@data-comment='This is a sample comments from Client.']")


    # Generate Full Report
    click_link 'generate_pdf'
    show_modal_report_type_chooser
    select '一覧表/Full List Report', from: 'report_type'    
    new_pdf_window = window_opened_by { click_button 'レポート作成/Generate' }
    within_window new_pdf_window do
      page.current_path.must_equal '/en/generate_pdf_full_report.pdf'
    end
    closeNewTabs
    
    # First Set 1 Applicant ONLY (checked 1 in lists)
    uncheck 'applicant_select_all'
    find(:xpath, "//table[@id='makeDataTable']/tbody/tr[1]/td[1]/input[@type='checkbox']").click

    # Generate Individual Report
    click_link 'generate_pdf'
    show_modal_report_type_chooser
    select '個別レジュメ/Individual Report', from: 'report_type'
    new_pdf_window = window_opened_by { click_button 'レポート作成/Generate' }
    within_window new_pdf_window do
      page.current_path.must_equal '/en/generate_applicant_report.pdf'
    end
    closeNewTabs

    # Check 404 Page
    visit '/en/users'
    page.must_have_content '404 Page not found!'
  end
end