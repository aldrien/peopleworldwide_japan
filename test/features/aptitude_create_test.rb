require "test_helper"

feature "Skill Check Test Create" do
  before do
   visit root_path
   @new_aptitude_en = 'A New Skill Check Test'
   @new_aptitude_jp = '新しい適性検査'
  end

  scenario 'should create new aptitude', js: true do
    feature_login('admin', 'pwwadmin')
    visit '/en/aptitude_tests'

    page.must_have_content 'Skill Check Test List & Management'
    page.must_have_content 'Add Skill Check Test'

    # page.execute_script("$('div.ajax_loading').css('opacity', 0)")
    wait_for do      
      page.has_css?('div.ajax_loading', :visible => false)
    end

    within 'form#aptitude_tests' do
      fill_in 'en_name', with: @new_aptitude_en
      fill_in 'jp_name', with: @new_aptitude_jp

      click_button('ADD')
    end
    
    within 'table#dataTables-aptitude_tests' do
      page.has_css?('td', :text => @new_aptitude_en, :visible => true)
      page.has_css?('td', :text => @new_aptitude_jp, :visible => true)
    end    
    page.must_have_content 'Well done! New Skill Check Test was successfully added.'
  end
end