require "test_helper"

feature "Partner Account Ability" do
  before do
   visit root_path   
  end

  # called after every single test
  teardown do    
    Rails.cache.clear # when controller is using cache it may be a good idea to reset it afterwards
  end

  scenario 'should visit partner account pages' do
    feature_login('partner1', 'partner1')

    page.must_have_content 'Partner Dashboard'
    page.must_have_content 'Hello, Partner1'

    # Visit Applicant Listing Page
    click_link 'List of Applicants'
    page.must_have_content 'Applicants List'

    click_link '進行中/In-Progress'

    within 'table#dataTable_in_progress' do
      page.must_have_content applicants(:for_partner1).name

      # page.has_css?('td', :text => applicants(:for_partner1).name, :visible => true)
      # page.has_css?('td', :text => applicants(:for_update).name, :visible => true)
      
      page.wont_have_content(applicants(:for_update).name)
      page.wont_have_content(applicants(:john).name)
      page.wont_have_content(applicants(:jenny).name)
      page.wont_have_content(applicants(:sarah).name)
      page.wont_have_content(applicants(:vj).name)
    end

    # Try Update Test
    # Locked Applicants
    # find(:xpath, "//table[@id='dataTable_in_progress']/tbody/tr[td//text()[contains(., '#{applicants(:for_partner1).name}')]]/td[11]//a[1]").click
    # page.must_have_content 'ON-GOING UPDATES NOTIFICATION'
    # page.must_have_content "#{applicants(:for_partner1).name}'s data has on-going update."
    # page.must_have_content 'Worker: Admin'
    # click_link 'Try Later'
    # page.current_path.must_equal applicants_path(locale: I18n.locale)

    # Unlocked
    find(:xpath, "//a[@href='#confirmed']").click
    find(:xpath, "//table[@id='dataTable_confirmed']/tbody/tr[td//text()[contains(., '#{applicants(:aldrien).name}')]]/td[last()]//a[1]").click
    page.current_path.must_equal edit_applicant_path(id: applicants(:aldrien), locale: I18n.locale)
    page.must_have_content "Edit #{applicants(:aldrien).name} Data"
    page.must_have_content 'Well done! Locked Function was activated to this Applicant.'

    fill_in 'applicant_weight', with: '65'
    click_link 'Other Info &'
    click_button 'UPDATE NOW'
    page.must_have_content "Well done! #{applicants(:aldrien).name}'s information was successfully updated."

    # Visit My Projects Page
    click_link 'My Projects'
    page.must_have_content 'Projects List & Management'
    click_link '進行中/In-Progress'
    within 'table#dataTable_in_progress' do
      page.has_css?('td', :text => projects(:project1).title, :visible => true)
      
      page.wont_have_content(projects(:project2).title)
    end
    
    # Visit Project's Applicants List
    find(:xpath, "//table[@id='dataTable_in_progress']/tbody/tr[td//text()[contains(., '#{projects(:project1).title}')]]/td[last()]//a[1]").click
    page.current_path.must_equal view_project_applicants_path(locale: I18n.locale)
    page.must_have_content "#{projects(:project1).company_name}"
    page.must_have_content "Resumes Assigned to #{projects(:project1).title}"

    # Generate Full Report
    click_link 'generate_pdf'
    show_modal_report_type_chooser
    select '一覧表/Full List Report', from: 'report_type'    
    new_pdf_window = window_opened_by { click_button 'レポート作成/Generate' }
    within_window new_pdf_window do
      page.current_path.must_equal '/en/generate_pdf_full_report.pdf'
    end
    closeNewTabs
    
    # First Set 1 Applicant ONLY (checked 1 in lists)
    uncheck 'applicant_select_all'
    find(:xpath, "//table[@id='makeDataTable']/tbody/tr[1]/td[1]/input[@type='checkbox']").click

    # Generate Individual Report
    click_link 'generate_pdf'
    show_modal_report_type_chooser
    select '個別レジュメ/Individual Report', from: 'report_type'
    new_pdf_window = window_opened_by { click_button 'レポート作成/Generate' }
    within_window new_pdf_window do
      page.current_path.must_equal '/en/generate_applicant_report.pdf'
    end
    closeNewTabs

    # Check 404 Page
    visit '/en/users'
    page.must_have_content '404 Page not found!'
  end
end