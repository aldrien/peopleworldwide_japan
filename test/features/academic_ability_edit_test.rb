require "test_helper"

feature "Academic Ability Edit" do
  before do
   visit root_path
   @record = academic_abilities(:extra_acad_test)
   @new_english_name = 'This is a sample update'
  end

  scenario 'should update academic abilty', js: true do
    feature_login('admin', 'pwwadmin')
    visit '/en/academic_abilities'

    page.must_have_content 'Academic Ability List & Management'
    page.must_have_content 'Add Academic Ability'
  
    # page.execute_script("$('div.ajax_loading').css('opacity', 0)")
    wait_for do      
      page.has_css?('div.ajax_loading', :visible => false)
    end

    within 'table#dataTables-academic_abilities' do
      page.has_css?('td', :text => @record.en_name, :visible => true)
      page.has_css?('td', :text => @record.jp_name, :visible => true)
      page.find(:xpath, "//td/a[text()='#{@record.en_name}']").click
    end

    within 'form.editableform' do
      find(:xpath, "//div[@class='editable-input']/input").set(@new_english_name)
      find(:xpath, "//div[@class='editable-buttons']/button[@type='submit']").click
    end

    page.must_have_content 'Well done! English Name was successfully updated.'
  end
end