require "test_helper"

feature "File Type Edit" do
  before do
   visit root_path
   @record = file_types(:extra_file_type)
   @new_english_name = 'File Type Updated'
  end

  scenario 'should update file type', js: true do
    feature_login('admin', 'pwwadmin')
    visit '/en/file_types'

    page.must_have_content 'File Types List & Management'
    page.must_have_content 'Add File Type'
  
  # page.execute_script("$('div.ajax_loading').css('opacity', 0)")
    wait_for do      
      page.has_css?('div.ajax_loading', :visible => false)
    end
    
    within 'table#dataTables-file_types' do
      page.has_css?('td', :text => @record.en_name, :visible => true)
      page.has_css?('td', :text => @record.jp_name, :visible => true)
      page.find(:xpath, "//td/a[text()='#{@record.en_name}']").click
    end

    within 'form.editableform' do
      find(:xpath, "//div[@class='editable-input']/input").set(@new_english_name)
      find(:xpath, "//div[@class='editable-buttons']/button[@type='submit']").click
    end

    page.must_have_content 'Well done! English Name was successfully updated.'
  end
end