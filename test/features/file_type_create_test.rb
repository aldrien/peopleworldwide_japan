require "test_helper"

feature "File Type Create" do
  before do
   visit root_path
   @new_file_type_en = 'A New File Type'
   @new_file_type_jp = '-'
  end

  scenario 'should create new file type', js: true do
    feature_login('admin', 'pwwadmin')
    visit '/en/file_types'

    page.must_have_content 'File Types List & Management'
    page.must_have_content 'Add File Type'

    # page.execute_script("$('div.ajax_loading').css('opacity', 0)")
    wait_for do      
      page.has_css?('div.ajax_loading', :visible => false)
    end
    
    within 'form#file_types' do
      fill_in 'en_name', with: @new_file_type_en
      fill_in 'jp_name', with: @new_file_type_jp
      click_button('ADD')
    end
    
    within 'table#dataTables-file_types' do
      page.has_css?('td', :text => @new_file_type_en, :visible => true)
      page.has_css?('td', :text => @new_file_type_jp, :visible => true)
    end    
    page.must_have_content 'Well done! New File Type was successfully added.'
  end
end