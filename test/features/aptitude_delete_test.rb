require "test_helper"

feature "Aptitude Test Delete" do
  before do
   visit root_path
   @record = aptitude_tests(:delete_aptitude_test)
  end

  scenario 'should delete aptitude test', js: true do
    feature_login('admin', 'pwwadmin')
    visit '/en/aptitude_tests'

    page.must_have_content 'Skill Check Test List & Management'
    page.must_have_content 'Add Skill Check Test'
  
    # page.execute_script("$('div.ajax_loading').css('opacity', 0)")
    wait_for do      
      page.has_css?('div.ajax_loading', :visible => false)
    end
    
    within 'table#dataTables-aptitude_tests' do
      page.has_css?('td', :text => @record.en_name, :visible => true)
      page.has_css?('td', :text => @record.jp_name, :visible => true)
      page.find(:xpath, "//tr[td/a[text()='#{@record.en_name}']]/td[last()]/a[@class='delete_aptitude_test']").click
    end

    show_password_modal
    fill_in 'modal_password', with: 'pwwadmin'
    click_button('続けます/Continue')

    page.must_have_content 'Well done! Skill Check Test was successfully deleted.'
  end
end