require "test_helper"

feature "Pww Logo Header Create" do
  before do
   visit root_path
  end

  scenario 'should create new pww logo header' do
    feature_login('admin', 'pwwadmin')
    visit '/en/pww_logos/new'

    page.must_have_content "Add New PDF Header"

    within '#new_pww_logo' do
      click_button('SAVE NOW')
      page.evaluate_script("document.activeElement.id") == "pww_logo_title"

      fill_in 'pww_logo_title', with: 'New Pww Header Logo'
    # Upload header logo - make visible file uplaod first
      page.execute_script("$('input#pww_logo_image').css('opacity', 1)")
      attach_file('pww_logo[logo]', Rails.root.join('test/fixtures/pww_japan_small.png'))
      fill_in_ckeditor 'pww_logo_content', with: 'Company Address here!!!'

      click_button('SAVE NOW')
    end

    page.must_have_content "Well done! New PDF Header was successfully created."
    assert_equal "/en/pww_logos", page.current_path #current_path
  end
end
