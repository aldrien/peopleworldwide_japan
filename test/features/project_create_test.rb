require "test_helper"

feature "Project Create" do
  before do
    visit root_path
    
    @title = 'New Project'
    @company = 'New Company'
    @description = 'This is a sample creation of Project.'
    @partner = 'Partner 102'
    @client = 'Japan Client 2'
    @needed_applicants = '20'
    @country = 'Indonesia'
    @job_type = 'garment factory worker'
    @status = 'In-progress'
    @admin_notes = 'This is Admin Notes!'
  end

  scenario 'should create new project with valid data' do
    feature_login('admin', 'pwwadmin')
    visit '/en/projects/new'

    within '#new_project' do
      fill_in 'project_title', with: @title
      fill_in 'project_company_name', with: @company      
      fill_in_ckeditor 'project_description', with: 'Project Long Descriptions here!!!'
      
      select @partner, from: 'project_partner_id'
      select @client, from: 'project_client_id'
      fill_in 'project_no_of_needed_applicants', with: @needed_applicants
      select @country, from: 'project_country_id'
      select @job_type, from: 'project_job_type_id'
      select @status, from: 'project_status'
      
    # Upload document (doc, pdf, image) - make visible file uplaod first
      page.execute_script("$('input#project_document').css('opacity', 1)")
      attach_file('project[document]', Rails.root.join('test/fixtures/sample.pdf'))

      fill_in_ckeditor 'project_notes', with: 'Admin notes here!!!'

      click_button 'SAVE NOW'
    end

    page.must_have_content "Well done! New Project was successfully created."
  end
end