require "test_helper"

feature "Job Type Create" do
  before do
   visit root_path
   @new_job_type_en = 'A New Job Type'
   @new_job_type_jp = '-'
  end

  scenario 'should create new job type', js: true do
    feature_login('admin', 'pwwadmin')
    visit '/en/job_types'

    page.must_have_content 'Job Types List & Management'
    page.must_have_content 'Add Job Type'

    # page.execute_script("$('div.ajax_loading').css('opacity', 0)")
    wait_for do      
      page.has_css?('div.ajax_loading', :visible => false)
    end
    
    within 'form#job_types' do
      fill_in 'en_name', with: @new_job_type_en
      fill_in 'jp_name', with: @new_job_type_jp
      click_button('ADD')
    end
    
    within 'table#dataTables-job_types' do
      page.has_css?('td', :text => @new_job_type_en, :visible => true)
      page.has_css?('td', :text => @new_job_type_jp, :visible => true)
    end    
    page.must_have_content 'Well done! New Job Type was successfully added.'
  end
end