require "test_helper"

feature "Skill Check Test Edit" do
  before do
   visit root_path
   @record = aptitude_tests(:extra_aptitude_test)
   @new_english_name = 'This is a sample update'
  end

  scenario 'should update skill check test', js: true do
    feature_login('admin', 'pwwadmin')
    visit '/en/aptitude_tests'

    page.must_have_content 'Skill Check Test List & Management'
    page.must_have_content 'Add Skill Check Test'
  
    # page.execute_script("$('div.ajax_loading').css('opacity', 0)")
    wait_for do      
      page.has_css?('div.ajax_loading', :visible => false)
    end

    within 'table#dataTables-aptitude_tests' do
      page.has_css?('td', :text => @record.en_name, :visible => true)
      page.has_css?('td', :text => @record.jp_name, :visible => true)
      page.find(:xpath, "//td/a[text()='#{@record.en_name}']").click
    end

    within 'form.editableform' do
      find(:xpath, "//div[@class='editable-input']/input").set(@new_english_name)
      find(:xpath, "//div[@class='editable-buttons']/button[@type='submit']").click
    end

    page.must_have_content 'Well done! English Name was successfully updated.'
  end
end