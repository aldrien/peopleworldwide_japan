require "test_helper"

feature "Applicant Edit Test" do    
  before do    
    visit root_path
  end

  scenario 'should update applicant records' do
    feature_login('admin', 'pwwadmin')
    visit '/en/applicants'

    page.must_have_content 'Applicants List'

    click_link '進行中/In-Progress'
    
    find(:xpath, "//table[@id='dataTable_in_progress']/tbody/tr[td//text()[contains(., 'Por Apdayt')]]/td[11]//a[1]").click
    page.must_have_content 'Edit Por Apdayt Data'

    # Check Lock function was activated
    page.must_have_content 'Well done! Locked Function was activated to this Applicant.'

    within 'form.edit_applicant_form' do
    # Basic Information
        fill_in 'applicant_name', with: 'Por Apdayt Test Update'
        select 'Single', from: 'applicant_marital_status'
        select 'Male', from: 'applicant_gender'
        select '大学卒業/University graduate', from: 'applicant_last_academic'
        fill_in 'applicant_birth_date', with: '1991/10/20'
        select '両利き/Ambidextrous', from: 'applicant_dominant_hand'
        fill_in 'applicant_height', with: '159'        
        fill_in 'applicant_weight', with: '55'
        select 'ネイティブ/Native', from: 'applicant_english_skill'
        select 'N1', from: 'applicant_jlpt_level'
        select '無し/No', from: 'applicant_is_work_related_expirience'

    # Upload photo - make visible file uplaod first
        page.execute_script("$('input#applicant_profile').css('opacity', 1)")
        page.execute_script("$('input#applicant_wholebody').css('opacity', 1)")
        attach_file('applicant[profile]', Rails.root.join('test/fixtures/2x2.jpg'))
        attach_file('applicant[wholebody]', Rails.root.join('test/fixtures/wholebody.jpg'))

    # Contact Information
        click_link('連絡先 / Contact Information')
        fill_in 'applicant_phone', with: '073-8890-2356'
        fill_in 'applicant_email', with: 'porapdayts@skyarch.net'
        fill_in 'applicant_address', with: 'Minato-ku, Tokyo Japan'

    # Education Background
        click_link('学歴 / Education Background')
        click_link('ADD')

        find(:xpath, "//table[@id='table_education_background']/tbody/tr[1]/td[1]//input").set('2012/03')
        find(:xpath, "//table[@id='table_education_background']/tbody/tr[1]/td[2]//input").set('Por Apdayt College')
        find(:xpath, "//table[@id='table_education_background']/tbody/tr[1]/td[3]//select").select('卒業/Graduate')
        click_link('ADD')

        find(:xpath, "//table[@id='table_education_background']/tbody/tr[2]/td[1]//input").set('2008/04')
        find(:xpath, "//table[@id='table_education_background']/tbody/tr[2]/td[2]//input").set('Por Apdayt High School')
        find(:xpath, "//table[@id='table_education_background']/tbody/tr[2]/td[3]//select").select('卒業/Graduate')
        click_link('ADD')

        find(:xpath, "//table[@id='table_education_background']/tbody/tr[3]/td[1]//input").set('2004/04')
        find(:xpath, "//table[@id='table_education_background']/tbody/tr[3]/td[2]//input").set('Por Apdayt Elementary')
        find(:xpath, "//table[@id='table_education_background']/tbody/tr[3]/td[3]//select").select('卒業/Graduate')

    # People Worldwide Academy
        click_link('People Worldwide Academy')
        # Academic Ability
        @add_acad = "//tr[@id='add_link_academic']//a"

        find(:xpath, @add_acad).click
        find(:xpath, "//table[@id='table_academic_ability']/tbody/tr[1]/td[1]//select").select('IQ test')
        find(:xpath, "//table[@id='table_academic_ability']/tbody/tr[1]/td[2]//input").set('90')
        find(:xpath, @add_acad).click
        find(:xpath, "//table[@id='table_academic_ability']/tbody/tr[2]/td[1]//select").select('PWJ日本語能力テスト/PWA Japanese skill test')
        find(:xpath, "//table[@id='table_academic_ability']/tbody/tr[2]/td[2]//input").set('86')
        
        # Skill Check
        @add_skill = "//tr[@id='add_link_skill']//a"

        find(:xpath, @add_skill).click
        find(:xpath, "//table[@id='table_skill_check']/tbody/tr[1]/td[1]//select").select('食品工場/Food processor')
        find(:xpath, "//table[@id='table_skill_check']/tbody/tr[1]/td[2]//input").set('89')
        find(:xpath, @add_skill).click
        find(:xpath, "//table[@id='table_skill_check']/tbody/tr[2]/td[1]//select").select('建設業/Construction')
        find(:xpath, "//table[@id='table_skill_check']/tbody/tr[2]/td[2]//input").set('90')
        find(:xpath, @add_skill).click
        find(:xpath, "//table[@id='table_skill_check']/tbody/tr[3]/td[1]//select").select('自動車整備/Car maintenace')
        find(:xpath, "//table[@id='table_skill_check']/tbody/tr[3]/td[2]//input").set('85')
        

    # Other Info & Save
        click_link('Other Info &')
        check 'applicant_show_notes_in_partner_pdf'
        check 'applicant_show_notes_in_client_pdf'
        check 'applicant_show_remarks_in_partner_pdf'
        check 'applicant_show_remarks_in_client_pdf'

        click_button 'UPDATE NOW'
    end

    page.must_have_content "Por Apdayt Test Update's information was successfully updated."
    page.must_have_content "Applicants List"
  end
end