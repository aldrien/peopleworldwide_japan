require "test_helper"

feature "Job Type Delete" do
  before do
   visit root_path
   @record = job_types(:delete_job_type)
  end

  scenario 'should delete job type', js: true do
    feature_login('admin', 'pwwadmin')
    visit '/en/job_types'

    page.must_have_content 'Job Types List & Management'
    page.must_have_content 'Add Job Type'  

    # page.execute_script("$('div.ajax_loading').css('opacity', 0)")
    wait_for do      
      page.has_css?('div.ajax_loading', :visible => false)
    end

    within 'table#dataTables-job_types' do
      page.has_css?('td', :text => @record.en_name, :visible => true)
      page.has_css?('td', :text => @record.jp_name, :visible => true)
      page.find(:xpath, "//tr[td/a[text()='#{@record.en_name}']]/td[last()]/a[@class='delete_job_type']").click
    end

    show_password_modal
    fill_in 'modal_password', with: 'wrong_password'
    click_button('続けます/Continue')

    page.must_have_selector('.invalid_pass', visible: true)
    page.must_have_content "Sorry Invalid Password. Please Try Again!"
    page.must_have_selector('#modal_confirmation', visible: false)

    fill_in 'modal_password', with: 'pwwadmin'
    click_button('続けます/Continue')

    page.must_have_content 'Well done! Job Type was successfully deleted.'
  end
end