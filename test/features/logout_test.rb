require "test_helper"

feature "Logout" do
  before do    
    visit home_path
  end

  scenario "user should able to logout" do 
    feature_login('admin', 'pwwadmin')
    page.must_have_content "Hello, Admin| #{Time.new.strftime("%A, %d %B %Y")}"
    
    visit(logout_path(locale: I18n.locale))
    page.current_path.must_equal '/en/login'
    page.must_have_content "Well done! You have successfully logged out."
  end
end
