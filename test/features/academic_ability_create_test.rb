require "test_helper"

feature "Academic Ability Create" do
  before do
   visit root_path
   @new_acad_en = 'A New Academic Ability'
   @new_acad_jp = 'サンプルレコード'
  end

  scenario 'should create new academic abilty', js: true do
    feature_login('admin', 'pwwadmin')
    visit '/en/academic_abilities'

    page.must_have_content 'Academic Ability List & Management'
    page.must_have_content 'Add Academic Ability'

    # page.execute_script("$('div.ajax_loading').css('opacity', 0)")
    wait_for do      
      page.has_css?('div.ajax_loading', :visible => false)
    end

    within 'form#academic_abilities' do
      fill_in 'en_name', with: @new_acad_en
      fill_in 'jp_name', with: @new_acad_jp

      click_button('ADD')
    end
    
    within 'table#dataTables-academic_abilities' do
      page.has_css?('td', :text => @new_acad_en, :visible => true)
      page.has_css?('td', :text => @new_acad_jp, :visible => true)
    end    
    page.must_have_content 'Well done! New Academic Ability was successfully added.'
  end
end