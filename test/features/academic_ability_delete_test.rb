require "test_helper"

feature "Academic Ability Delete" do
  before do
   visit root_path
   @record = academic_abilities(:delete_acad_test)   
  end

  scenario 'should delete academic abilty', js: true do
    feature_login('admin', 'pwwadmin')
    visit '/en/academic_abilities'

    page.must_have_content 'Academic Ability List & Management'
    page.must_have_content 'Add Academic Ability'
  
    # page.execute_script("$('div.ajax_loading').css('opacity', 0)")
    wait_for do      
      page.has_css?('div.ajax_loading', :visible => false)
    end

    within 'table#dataTables-academic_abilities' do
      page.has_css?('td', :text => @record.en_name, :visible => true)
      page.has_css?('td', :text => @record.jp_name, :visible => true)
      page.find(:xpath, "//tr[td/a[text()='#{@record.en_name}']]/td[last()]/a[@class='delete_academic_ability']").click
    end

    show_password_modal
    fill_in 'modal_password', with: 'pwwadmin'
    click_button('続けます/Continue')

    page.must_have_content 'Well done! Academic Ability was successfully deleted.'
  end
end