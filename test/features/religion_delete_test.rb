require "test_helper"

feature "Religion Delete" do
  before do
   visit root_path
   @record = religions(:delete_religion)
  end

  scenario 'should delete religion', js: true do
    feature_login('admin', 'pwwadmin')
    visit '/en/religions'

    page.must_have_content 'Religions List & Management'
    page.must_have_content 'Add Religion'
  
    # page.execute_script("$('div.ajax_loading').css('opacity', 0)")
    wait_for do      
      page.has_css?('div.ajax_loading', :visible => false)
    end
    
    within 'table#dataTables-religions' do
      page.has_css?('td', :text => @record.en_name, :visible => true)
      page.has_css?('td', :text => @record.jp_name, :visible => true)
      page.find(:xpath, "//tr[td/a[text()='#{@record.en_name}']]/td[last()]/a[@class='delete_religion']").click
    end

    show_password_modal
    fill_in 'modal_password', with: 'wrong_password'
    click_button('続けます/Continue')

    page.must_have_selector('.invalid_pass', visible: true)
    page.must_have_content "Sorry Invalid Password. Please Try Again!"
    page.must_have_selector('#modal_confirmation', visible: false)

    fill_in 'modal_password', with: 'pwwadmin'
    click_button('続けます/Continue')

    page.must_have_content 'Well done! Religion was successfully deleted.'
  end
end