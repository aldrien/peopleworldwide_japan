require "test_helper"

feature "Job Type Edit" do
  before do
   visit root_path
   @record = job_types(:extra_job_type)
   @new_english_name = 'Job Type Updated'
  end

  scenario 'should update job type' do
    feature_login('admin', 'pwwadmin')
    visit '/en/job_types'

    page.must_have_content 'Job Types List & Management'
    page.must_have_content 'Add Job Type'
  
    # page.execute_script("$('div.ajax_loading').css('opacity', 0)")
    wait_for do      
      page.has_css?('div.ajax_loading', :visible => false)
    end
    
    within 'table#dataTables-job_types' do
      page.has_css?('td', :text => @record.en_name, :visible => true)
      page.has_css?('td', :text => @record.jp_name, :visible => true)
      page.find(:xpath, "//td/a[text()='#{@record.en_name}']").click
    end

    within 'form.editableform' do
      find(:xpath, "//div[@class='editable-input']/input").set(@new_english_name)
      find(:xpath, "//div[@class='editable-buttons']/button[@type='submit']").click
    end

    page.must_have_content 'Well done! English Name was successfully updated.'
  end
end