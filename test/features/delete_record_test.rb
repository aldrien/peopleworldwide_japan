require "test_helper"

feature "Delete Record Test" do
	before do
	 visit root_path
	end

	scenario 'should NOT delete project with INVALID admin password' do #, js: true
		feature_login('admin', 'pwwadmin')
		visit '/en/projects'

		find(:xpath, "//a[@href='#failed']").click
		find(:xpath, "//table[@id='dataTable_failed']/tbody/tr[1]/td[10]/a[2]").click
		
		show_password_modal
		click_button('続けます/Continue')		
		page.evaluate_script("document.activeElement.id") == "modal_password" # OnFocus test
		fill_in 'modal_password', with: 'wrong_password'

		click_button('続けます/Continue')		

		page.must_have_selector('.invalid_pass', visible: true)
		page.must_have_content "Sorry Invalid Password. Please Try Again!"
		page.must_have_selector('#modal_confirmation', visible: false)
	end

	scenario 'should DELETE project with VALID admin password' do
		feature_login('admin', 'pwwadmin')
		visit '/en/projects'
		sleep 0.5

		# Project Delete Me
		page.must_have_content "Projects List & Management"
		find(:xpath, "//a[@href='#failed']").click
		find(:xpath, "//table[@id='dataTable_failed']/tbody/tr[td//text()[contains(., 'Project Delete Me')]]/td[last()]/a[2]").click

		show_password_modal
		fill_in 'modal_password', with: 'pwwadmin'
		click_button('続けます/Continue')

		page.must_have_content "Well done! Project #{projects(:delete_me_project).title} was successfully deleted."
	end

end
