require "test_helper"

feature "Login" do
  before do
    visit root_path
  end

  scenario "go to login page" do    
    page.must_have_content "Connecting to Blue Sky Opportunities"
    page.wont_have_content "continue"
  end

  scenario 'user sign in with valid data' do    
    feature_login('admin', 'pwwadmin')

    page.must_have_content 'Well done! Hi Admin! you have successfully logged in.'
    page.current_path.must_equal '/en/home'
  end

  scenario 'user sign in with invalid data' do
    feature_login('admin', 'wrong_password')

    page.must_have_content 'Oh! Invalid Username or Password!'
    page.current_path.must_equal '/en/login'
  end

end
