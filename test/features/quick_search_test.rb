
require "test_helper"

feature "Quick Search & Link Test" do
  before do
   visit root_path
  end

  scenario 'should return search result with applicant name like' do
    feature_login('admin', 'pwwadmin')    
    
    page.must_have_content "Admin Dashboard"

    within 'form' do
      fill_in 'applicant_name', with: 'vj'
      find(:xpath, '//span[@class="input-group-btn"]/button').click
    end

    page.current_path.must_equal '/en/quick_search'
    page.must_have_content 'Search Applicants'

    within 'table#makeDataTable' do
      page.must_have_content applicants(:vj).name      
    end

    # Test for the links (edit and video link)
    edit_link = "//table[@id='makeDataTable']//tr[td//text()[contains(., 'VJ')]]/td[last()]/a[1]"

    find(:xpath, edit_link).click

    page.must_have_content "Edit #{applicants(:vj).name} Data"
    page.must_have_content 'Well done! Locked Function was activated to this Applicant.'
    click_link 'Other Info &'
    click_button 'UPDATE NOW'
    
    find(:xpath, "//a[@href='#failed']").click
    self_intro_link = "//tr[1]/td[last()]/a[@title='自己紹介動画 / Self introducing video']"
    new_window = window_opened_by { find(:xpath, self_intro_link).click }
    within_window new_window do
      page.current_url.must_equal applicants(:vj).self_introduction_video_link
    end
    closeNewTabs

  end
end