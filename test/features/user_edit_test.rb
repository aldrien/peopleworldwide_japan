require "test_helper"

feature "User Edit" do
  before do
    visit root_path
  end

  scenario 'should update user data' do
    feature_login('admin', 'pwwadmin')
    visit '/en/users'

    page.must_have_content 'User Accounts List'
    click_link('admin_accounts')

    find(:xpath, "//table[@id='user_table_admins']/tbody/tr[1]/td[last()]//a[@class='edit_user']").click    
    page.must_have_content 'Edit User Account'

    fill_in 'user_fullname', with: "Administrator"
    fill_in 'user_username', with: 'admin'
    fill_in 'user_password', with: 'pwwadmin'
    fill_in 'user_password_confirmation', with: 'pwwadmin'
    select 'Administrator Account', from: 'user_role'
    select 'Activate User Access', from: 'user_status'
    select 'PWW Main Logo', from: 'user_pww_logo_id'
    fill_in 'user_phone', with: '0123456789'
    fill_in 'user_description', with: 'This will update Admin records.'

    click_button 'SAVE NOW'

    page.must_have_content "Well done! Admin information was successfully updated."
  end
end