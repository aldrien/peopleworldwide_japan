require "test_helper"

feature "Password Generator Test" do
  before do
   visit root_path
  end

  scenario 'should generate random text', js: true do
    feature_login('admin', 'pwwadmin')
    visit '/en/users/new'
    # or
    # click_link 'User Access'
    # click_link 'New User'

    page.must_have_content 'New User Account'    
    find(:xpath, "//img[@id='open_pass_gen_modal']").click
    wait_for do
      page.has_css?('div#modal_generate_password', :visible => true)
    end
    
    click_button('Generate Now')

    page.execute_script("$('button#copy_generated_password').css('opacity', 1)")
    wait_for do
      page.has_css?('button#copy_generated_password', :visible => true)
    end

    click_button('Generate Again')
    click_button('Copy')
 
    wait_for do
      page.has_css?('div#modal_generate_password', :visible => false)
    end
  end
end