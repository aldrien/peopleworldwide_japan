require "test_helper"

feature "User Create" do
  before do
    visit root_path
    
    @fullname = 'sample'
    @username = 'sample'
    @email = 'robert@westeros.gov'
    @role = 'Administrator Account'
    @pww_logo = 'PWW Japan Main'
    @status = 'Activate User Access'
  end

  scenario 'check password length validation' do
    feature_login('admin', 'pwwadmin')
    visit '/en/users/new'

    within '#new_user' do
      fill_in 'user_fullname', with: @fullname
      fill_in 'user_username', with: @username      
      fill_in 'user_password', with: 'b'
      fill_in 'user_password_confirmation', with: 'b'
      fill_in 'user_email',    with: @email
      select @role, from: 'user_role'
      select pww_logos(:main_logo).title, from: 'user_pww_logo_id'
      select @status, from: 'user_status'
      click_button 'SAVE NOW'
    end

    page.must_have_content 'Password is too short.'
  end

  scenario 'PWW Logo is required' do
    feature_login('admin', 'pwwadmin')
    visit '/en/users/new'

    within '#new_user' do
      fill_in 'user_fullname', with: @fullname
      fill_in 'user_username', with: @username      
      fill_in 'user_password', with: 'b'
      fill_in 'user_password_confirmation', with: 'b'
      fill_in 'user_email',    with: @email
      select @role, from: 'user_role'      
      select @status, from: 'user_status'
      click_button 'SAVE NOW'
    end
    page.evaluate_script("document.activeElement.id") == "user_pww_logo_id" #or page.execute_script('$("#user_pww_logo_id").trigger("focus")')
  end

  scenario 'successfully created user account' do
    feature_login('admin', 'pwwadmin')
    visit '/en/users/new'

    within '#new_user' do
      fill_in 'user_fullname', with: @fullname
      fill_in 'user_username', with: @username
      fill_in 'user_email',    with: @email
      fill_in 'user_password', with: '123456'
      fill_in 'user_password_confirmation', with: '123456'
      select @role, from: 'user_role'
      select pww_logos(:main_logo).title, from: 'user_pww_logo_id'
      select @status, from: 'user_status'
      click_button 'SAVE NOW'
    end

    page.must_have_content 'Well done! New user account was successfully created.'
  end

end
