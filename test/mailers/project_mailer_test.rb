require 'test_helper'

class ProjectMailerTest < ActionMailer::TestCase
  test "Project Mailer Test" do
    user = users(:admin)
    project = projects(:project1)
    
    mail = ProjectMailer.send_job_order_mail(project, user.email)
    assert_equal "People Worldwide Japan | #{project.title} - #{Date.today.iso8601}", mail.subject
    assert_equal [user.email], mail.to
    assert_equal ["noreply@gmail.com"], mail.from
    assert_match /[a-z\d\w\-.]+/, mail.body.encoded
    # assert_match CGI::escape(user.email), mail.body.encoded
    assert_match 'Project 101', mail.body.encoded
  end
end
