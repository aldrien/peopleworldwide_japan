require 'test_helper'

class AcademicAbilitiesControllerTest < ActionController::TestCase
  setup do
    @user = users(:admin)
    @academic_ability = academic_abilities(:iq_test)
    session[:username] = @user.username
    session[:user_id] = @user.id

    @request.headers['Accept'] = Mime::JSON
    @request.headers['Content-Type'] = Mime::JSON.to_s
  end

  # called after every single test
  teardown do    
    Rails.cache.clear # when controller is using cache it may be a good idea to reset it afterwards
  end
  
  test "the truth" do
    assert true
  end

  test "should create new academic ability" do
    assert_difference('AcademicAbility.count') do
      post :add_new_academic_ability, academic_ability: {        
        en_name: 'New Acad Ability',
        jp_name: '新しい学力',
        last_updated_by: 'admin'
      }
    end
    assert_response :success, message: 'New Academic Ability was successfully added.'
  end

  # test "should update academic ability" do
  #   patch :edit_academic_ability, params: { academic_ability: { pk: @academic_ability.id, name: 'en_name', value: "IQ Test Updated" } }

  #   assert_response :success, message: 'English Name was successfully updated.'
  #   # Reload association to fetch updated data and assert that title is updated.
  #   @academic_ability.reload
  #   assert_equal "IQ Test Updated", @academic_ability.en_name # FAILS not updated
  # end

  test "update academic_ability via javascript" do
    # use xhr to simulate a ajax call
    xhr :post, :edit_academic_ability, format: :js, pk: @academic_ability.id, name: 'en_name', value: "IQ Test Updated"
    # if the controller responds with a javascript file the response will be a success
    assert_response :success, 'English Name was successfully updated.'
    # Reload association to fetch updated data and assert that title is updated.
    @academic_ability.reload
    assert_equal "IQ Test Updated", @academic_ability.en_name
  end

  test "should destroy academic ability" do
    assert_difference('AcademicAbility.count', -1) do
      delete :delete_academic_ability, id: @academic_ability.id
    end
    assert_response :success, message: "Academic Ability was successfully deleted."    
  end

  test "ajax request get all academic abilities" do
    get :get_all_academic_abilities, :format => "json"    
    json_response = JSON.parse(response.body)
    # assert_equal json_response["data"]
    assert_response :success
    assert_equal 'success', json_response["status"]
  end

end
