require 'test_helper'

class UsersControllerTest < ActionController::TestCase
  # assert_response(type, message = nil) public
  # Asserts that the response is one of the following types:

  # :success - Status code was 200
  # :redirect - Status code was in the 300-399 range
  # :missing - Status code was 404
  # :error - Status code was in the 500-599 range
  setup do
    @user = users(:admin)
    session[:username] = @user.username
    session[:user_id] = @user.id
  end

  # called after every single test
  teardown do    
    Rails.cache.clear # when controller is using cache it may be a good idea to reset it afterwards
  end
  
  test "should get index" do
    get :index
    assert_not_nil assigns(:users)
    assert_response :success
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create new user" do
    assert_difference('User.count') do
      post :create, user: {fullname: 'test', username: 'test', email: 'test@gmail.com', password: 'test1234', password_confirmation: 'test1234', role: 'client', last_updated_by: 'admin'}      
    end

    assert_redirected_to users_path(assigns(:users), locale: I18n.locale)
  end

  test "redirect to 404 page if unauthorize access" do
    assert users(:client1).role? :client
    assert_raise Exception do # CanCan::AccessDenied      
      assert_response :redirect
      assert_template layout: 'layouts/404.html', status: 404
    end
  end

  test "should get edit" do
    get :edit, id: users(:admin).id
    assert_response :success
  end

  test "should update user" do
    user = users(:admin)
    patch :update, id: user.id, user: {fullname: 'new_fullname'}
    
    assert_response :redirect
    # Reload association to fetch updated data and assert that title is updated.
    user.reload
    assert_equal "new_fullname", user.fullname
  end


  test "should destroy user" do
    client2 = users(:client2)
    assert_difference('User.count', -1) do
      delete :destroy, id: client2.id
    end

    assert_response :success, message: 'You have successfully deleted User Account.'
  end

  # Ability Test
  # user = User.create!(:admin => true) # I recommend a factory for this
  # session[:user_id] = user.id # log in user however you like, alternatively stub `current_user` method
  # get :index
  # assert_template :index # render the template since he should have access

  # def setup
  #   @ability = Object.new
  #   @ability.extend(CanCan::Ability)
  #   @controller.stubs(:current_ability).returns(@ability)
  # end

  # test "render index if have read ability on project" do
  #   @ability.can :read, Project
  #   get :index
  #   assert_template :index
  # end

end