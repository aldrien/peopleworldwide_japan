require 'test_helper'

class HomeControllerTest < ActionController::TestCase
  fixtures :users
  fixtures :projects
  fixtures :applicants

  test "the truth" do
    assert true
  end

  test "should redirect to login page if not logged in" do
    assert_raise Exception do
      assert_response :redirect
      assert_redirected_to root_path
      assert_template layout: 'layouts/login.html'
    end
  end

  test "should view home page if logged in" do
    @user = users(:admin)
    session[:username] = @user.username
    session[:user_id] = @user.id

    assert_response :success
  end

end
