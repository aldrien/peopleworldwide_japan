require 'test_helper'

class SearchControllerTest < ActionController::TestCase
  fixtures :users
  fixtures :applicants
  fixtures :projects

  setup do
    @admin = users(:admin)
    @client = users(:client1)
    @partner = users(:partner1) 
  end

  test "should not get index if not logged in" do
    get :index
    assert_redirected_to login_path(locale: I18n.locale)
  end

  test "should get index" do    
    session[:username] = @admin.username
    session[:user_id] = @admin.id
    
    get :index
    assert_response :success
  end

  test "returns a excel file" do
    session[:username] = @admin.username
    session[:user_id] = @admin.id

    get :generate_csv_full_report, applicants: [1, 2, 3, 4], format: :xlsx
    assert_response :success
    assert_equal "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", response.content_type
  end

end