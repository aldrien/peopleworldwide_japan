require 'test_helper'

class ProjectsControllerTest < ActionController::TestCase
  fixtures :users
  fixtures :projects

  # called before every single test
  # Reuse the @project, @user instance variable from setup
  setup do
    @project = projects(:project1)
    @user = users(:admin)
    session[:username] = @user.username
    session[:user_id] = @user.id    
  end
 
  # called after every single test
  teardown do    
    Rails.cache.clear # when controller is using cache it may be a good idea to reset it afterwards
  end
 
  test "should get index" do
    get :index
    assert_not_nil assigns(:projects)
    assert_response :success
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create new project" do
    assert_difference('Project.count') do
      post :create, project: {
        title: 'New Project',
        company_name: "New Skyarch Networks Inc.",
        partner_id: 2,
        client_id: 4,
        country_id: 1,
        job_type_id: 30,
        status: 'In-progress',
        no_of_needed_applicants: 5,
        description: 'Sample descriptions.',
        notes: 'No notes!',
        last_updated_by: 'admin'
      }
    end

    assert_redirected_to projects_path(assigns(:projects), locale: I18n.locale)
  end

  test "should get edit" do
    get :edit, id: @project.id
    assert_response :success
  end

  test "should update project" do
    patch :update, id: @project.id, project: { title: "updated-title" }
 
    assert_redirected_to projects_path(locale: I18n.locale)
    # Reload association to fetch updated data and assert that title is updated.
    @project.reload
    assert_equal "updated-title", @project.title
  end

  test "should show project applicants" do    
    get :view_project_applicants, id: @project.id
    assert_response :success
  end
 
  test "should destroy project" do
    assert_difference('Project.count', -1) do
      delete :delete_project, id: @project.id
    end
    assert_response :success, message: "Project #{@project.title} was successfully deleted."    
  end

  test 'send mail project job order' do    
    get :send_email_job_order, project_id: @project.id, receiver_ids: @user.id
    @project.reload
    assert_equal Time.new.strftime("%a, %e %b %Y %H"), @project.last_email_sent.to_time.strftime("%a, %e %b %Y %H")
  end

  test "should remove project file" do
    get :project_remove_document, id: @project.id
    @project.reload
    assert_nil @project.document_file_name
    assert_nil @project.document_content_type
    assert_nil @project.document_file_size
    assert_response :success, message: "Document was successfully removed."
  end

end