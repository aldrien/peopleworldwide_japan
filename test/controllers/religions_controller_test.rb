require 'test_helper'

class ReligionsControllerTest < ActionController::TestCase
  fixtures :users
  fixtures :religions

  setup do
    @user = users(:admin)
    @religion = religions(:christianity)
    session[:username] = @user.username
    session[:user_id] = @user.id
  end

  # called after every single test
  teardown do    
    Rails.cache.clear # when controller is using cache it may be a good idea to reset it afterwards
  end

  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get all religions via ajax" do
    get :get_all_religions, :format => "json"    
    json_response = JSON.parse(response.body)
    assert_response :success
    assert_equal 'success', json_response["status"]
  end

  test "should create new religion" do
    assert_difference('Religion.count') do
      post :add_new_religion, religion: {        
        en_name: 'New Religion',
        jp_name: '--',
        last_updated_by: 'admin'
      }
    end
    assert_response :success, message: 'New religion was successfully added.'
  end

  test "update religion via javascript" do
    # use xhr to simulate a ajax call
    xhr :post, :edit_religion, format: :js, pk: @religion.id, name: 'en_name', value: "Taoism"
    # if the controller responds with a javascript file the response will be a success
    assert_response :success, 'English Name was successfully updated.'
    # Reload association to fetch updated data and assert that title is updated.
    @religion.reload
    assert_equal "Taoism", @religion.en_name
  end

  test "should destroy religion" do
    assert_difference('Religion.count', -1) do
      delete :delete_religion, id: @religion.id
    end
    assert_response :success, message: "Religion was successfully deleted."    
  end

end
