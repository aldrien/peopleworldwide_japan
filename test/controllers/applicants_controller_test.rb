require 'test_helper'

class ApplicantsControllerTest < ActionController::TestCase
  fixtures :users
  fixtures :applicants
  fixtures :projects
  fixtures :countries

  # called before every single test
  # Reuse the @project, @user etc instance variable from setup
  setup do    
    @user = users(:admin)
    session[:username] = @user.username
    session[:user_id] = @user.id
    @project = projects(:project1)
    @country = countries(:japan)
    @applicant = applicants(:for_update)
  end

  # called after every single test
  teardown do    
    Rails.cache.clear # when controller is using cache it may be a good idea to reset it afterwards
  end
  
  test "should get index" do
    get :index
    assert_response :success
    assert assigns(:applicants)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create new applicant" do
    assert_difference('Applicant.count') do
      post :create, applicant: {
        creator_id: 2,
        edit_status: 0,
        last_updated_by: 'admin',
        name: 'Joeper',
        project_id: @project.id,
        country_id: @country.id,
        religion_id: 1,
        status: 'In-progress',
        email: 'joeper@skyarch.net',
        phone: '1234567890',
        address: '2-7-9 Sunny Crest Mita',
        gender: 'Male',
        birth_date: '1993/04/05',
        age: '24',
        height: '163',
        weight: '65',
        marital_status: 'Single',
        last_academic: 'University graduate',
        dominant_hand: 'Right',
        english_skill: 'Conversational',
        jlpt_level: 'No',
        is_work_related_expirience: 1
      }
    end

    assert_redirected_to applicants_path(assigns(:applicants), locale: I18n.locale)
  end

  test "should get edit" do
    get :edit, id: @applicant
    assert_response :success
  end

  test "should activate lock function when in edit page via ajax" do
    # use xhr to simulate a ajax call
    xhr :get, :lock_function, format: :js, id: @applicant.id, edit_status: 'true'
    # if the controller responds with a javascript file the response will be a success
    assert_response :success
    # Reload association to fetch updated data and assert that title is updated.
    @applicant.reload
    assert_equal true, @applicant.edit_status
  end

  test "should update applicant" do
    patch :update, id: @applicant, applicant: { name: "Joeper Serano" }
 
    assert_redirected_to applicants_path(assigns(:applicants), locale: I18n.locale)    
    # Reload association to fetch updated data and assert that title is updated.
    @applicant.reload
    assert_equal "Joeper Serano", @applicant.name
    assert_redirected_to applicants_path(locale: I18n.locale)
  end  

  test "should destroy applicant" do
    assert_difference('Applicant.count', -1) do
      delete :destroy, id: @applicant
    end
    assert_response :success, message: "You have successfully deleted #{@applicant.name} records."
  end

end