require 'test_helper'

class PwwLogosControllerTest < ActionController::TestCase
  # called before every single test
  # Reuse the @pww_logo, @user instance variable from setup
  setup do
    @pww_logo = pww_logos(:main_logo)
    @user = users(:admin)
    session[:username] = @user.username
    session[:user_id] = @user.id
  end
 
  # called after every single test
  teardown do    
    Rails.cache.clear # when controller is using cache it may be a good idea to reset it afterwards
  end
 
  test "should get index" do
    get :index
    assert_not_nil assigns(:pww_logos)
    assert_response :success
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create new header" do
    assert_difference('PwwLogo.count') do
      post :create, pww_logo: {
        title: 'New Header Today',
        content: "This is a sample creation of new header.",
        status: true,
        logo: fixture_file_upload('pww_japan_small.png', 'image/png')
      }
    end

    assert_redirected_to pww_logos_path(assigns(:pww_logos), locale: I18n.locale)
  end

  test "should get edit" do
    get :edit, id: @pww_logo.id
    assert_response :success
  end

  test "should update header" do
    patch :update, id: @pww_logo.id, pww_logo: { title: "This is new header title." }
 
    assert_redirected_to pww_logos_path(locale: I18n.locale)
    # Reload association to fetch updated data and assert that title is updated.
    @pww_logo.reload
    assert_equal "This is new header title.", @pww_logo.title
  end
 
  test "should destroy header" do
    assert_difference('PwwLogo.count', -1) do
      delete :destroy, id: @pww_logo.id
    end
    assert_response :success, message: "You have successfully deleted PDF Header."    
  end
end