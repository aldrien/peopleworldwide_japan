require 'test_helper'

class JobTypesControllerTest < ActionController::TestCase
  fixtures :users
  fixtures :job_types

  setup do
    @user = users(:admin)
    @job_type = job_types(:waiter)
    session[:username] = @user.username
    session[:user_id] = @user.id
  end

  # called after every single test
  teardown do    
    Rails.cache.clear # when controller is using cache it may be a good idea to reset it afterwards
  end
  
  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get all job_types via ajax" do
    get :get_all_job_types, :format => "json"    
    json_response = JSON.parse(response.body)
    assert_response :success
    assert_equal 'success', json_response["status"]
  end

  test "should create new job_types" do
    assert_difference('JobType.count', 1) do
      post :add_new_job_type, job_type: {        
        en_name: 'New Job Type',
        jp_name: '-',
        last_updated_by: 'admin'
      }
    end
    assert_response :success, message: 'New job type was successfully added.'
  end

  # test "should update job_type" do
  #   post :edit_job_type, id: @job_type.id, params: {job_type: { pk: @job_type.id, name: 'en_name', value: "janitor" }}, xhr: true
  #   assert_response :success, message: 'English Name was successfully updated.'
  #   # Reload association to fetch updated data and assert that title is updated.
  #   @job_type.reload
  #   assert_equal "janitor", @job_type.en_name # FAILS not updated
  # end

  test "update job_type via javascript" do
    # use xhr to simulate a ajax call
    xhr :post, :edit_job_type, format: :js, pk: @job_type.id, name: 'en_name', value: "janitor"
    # if the controller responds with a javascript file the response will be a success
    assert_response :success, 'English Name was successfully updated.'
    # Reload association to fetch updated data and assert that title is updated.
    @job_type.reload
    assert_equal "janitor", @job_type.en_name
  end

  test "should destroy job_type" do
    assert_difference('JobType.count', -1) do
      delete :delete_job_type, id: @job_type.id
    end
    assert_response :success, message: "Job Type was successfully deleted."    
  end
end
