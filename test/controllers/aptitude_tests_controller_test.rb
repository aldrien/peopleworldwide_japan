require 'test_helper'

class AptitudeTestsControllerTest < ActionController::TestCase
  fixtures :users
  fixtures :aptitude_tests

  setup do
    @user = users(:admin)
    @aptitude = aptitude_tests(:food_processor)
    session[:username] = @user.username
    session[:user_id] = @user.id

    @request.headers['Accept'] = Mime::JSON
    @request.headers['Content-Type'] = Mime::JSON.to_s
  end

  # called after every single test
  teardown do    
    Rails.cache.clear # when controller is using cache it may be a good idea to reset it afterwards
  end
  
  test "the truth" do
    assert true
  end

  test "should create new aptitude test" do
    assert_difference('AptitudeTest.count') do
      post :add_new_aptitude_test, aptitude_test: {        
        en_name: 'New Aptitude Test',
        jp_name: '新しい適性検査',
        last_updated_by: 'admin'
      }
    end
    assert_response :success, message: 'New Academic Ability was successfully added.'
  end

  # test "should update aptitude test" do
  #   patch :edit_aptitude_test, id: @aptitude.id, aptitude_test: { pk: @aptitude.id, name: 'en_name', value: "programming" }    
  #   assert_response :success, message: 'English Name was successfully updated.'
  #   # Reload association to fetch updated data and assert that title is updated.
  #   @aptitude.reload
  #   assert_equal "programming", @aptitude.en_name # FAILS not updated
  # end

  test "update aptitude_test via javascript" do
    # use xhr to simulate a ajax call
    xhr :post, :edit_aptitude_test, format: :js, pk: @aptitude.id, name: 'en_name', value: "programming"
    # if the controller responds with a javascript file the response will be a success
    assert_response :success, 'English Name was successfully updated.'
    # Reload association to fetch updated data and assert that title is updated.
    @aptitude.reload
    assert_equal "programming", @aptitude.en_name # FAILS not updated
  end

  test "should destroy aptitude test" do
    assert_difference('AptitudeTest.count', -1) do
      delete :delete_aptitude_test, id: @aptitude.id
    end
    assert_response :success, message: "Aptitude Test was successfully deleted."    
  end

  test "ajax request get all aptitude tests" do
    get :get_all_aptitude_tests, :format => "json"    
    json_response = JSON.parse(response.body)
    assert_response :success
    assert_equal 'success', json_response["status"]
  end
end
