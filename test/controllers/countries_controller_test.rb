require 'test_helper'

class CountriesControllerTest < ActionController::TestCase
  fixtures :users
  fixtures :countries

  setup do
    @user = users(:admin)
    @country = countries(:bangladesh)
    session[:username] = @user.username
    session[:user_id] = @user.id
  end

  # called after every single test
  teardown do    
    Rails.cache.clear # when controller is using cache it may be a good idea to reset it afterwards
  end
  
  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get all countries via ajax" do
    get :get_all_countries, :format => "json"    
    json_response = JSON.parse(response.body)
    assert_response :success
    assert_equal 'success', json_response["status"]
  end

  test "should create new country" do
    assert_difference('Country.count') do
      post :add_new_country, country: {        
        en_name: 'New Country',
        jp_name: '新しい国',
        last_updated_by: 'admin'
      }
    end
    assert_response :success, message: 'New country was successfully added.'
  end

  test "update country via javascript" do
    # use xhr to simulate a ajax call
    xhr :post, :edit_country, format: :js, pk: @country.id, name: 'en_name', value: "Brazil"
    # if the controller responds with a javascript file the response will be a success
    assert_response :success, 'English Name was successfully updated.'
    # Reload association to fetch updated data and assert that title is updated.
    @country.reload
    assert_equal "Brazil", @country.en_name
  end

  test "should destroy country" do
    assert_difference('Country.count', -1) do
      delete :delete_country, id: @country.id
    end
    assert_response :success, message: "Country was successfully deleted."    
  end

end
