require 'test_helper'

class FileTypesControllerTest < ActionController::TestCase
  fixtures :users
  fixtures :file_types

  setup do
    @user = users(:admin)
    @file_type = file_types(:passport_copy)
    session[:username] = @user.username
    session[:user_id] = @user.id

    @request.headers['Accept'] = Mime::JSON
    @request.headers['Content-Type'] = Mime::JSON.to_s
  end

  # called after every single test
  teardown do    
    Rails.cache.clear # when controller is using cache it may be a good idea to reset it afterwards
  end
  
  test "the truth" do
    assert true
  end

  test "should create new file type" do
    assert_difference('FileType.count') do
      post :add_new_file_type, file_type: {        
        en_name: 'New File Type',
        jp_name: '新しいファイルの種類',
        last_updated_by: 'admin'
      }
    end
    assert_response :success, message: 'New File Type was successfully added.'
  end

  test "update file type via javascript" do
    # use xhr to simulate a ajax call
    xhr :post, :edit_file_type, format: :js, pk: @file_type.id, name: 'en_name', value: "Government"
    # if the controller responds with a javascript file the response will be a success
    assert_response :success, 'English Name was successfully updated.'
    # Reload association to fetch updated data and assert that title is updated.
    @file_type.reload
    assert_equal "Government", @file_type.en_name
  end

  test "should destroy file type" do
    assert_difference('FileType.count', -1) do
      delete :delete_file_type, id: @file_type.id
    end
    assert_response :success, message: "File Type was successfully deleted."    
  end

  test "ajax request get all fiel types" do
    get :get_all_file_types, :format => "json"    
    json_response = JSON.parse(response.body)
    assert_response :success
    assert_equal 'success', json_response["status"]
  end
end
