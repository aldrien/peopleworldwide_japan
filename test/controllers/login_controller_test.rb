require 'test_helper'

class LoginControllerTest < ActionController::TestCase
  test "the truth" do
    assert true
  end

  test "should go to login page" do
    get :index
    assert_response :success
  end

  test "should authenticate valid login" do
    get :index
    @user = users(:admin)
    authenticate_user_login(@user.username, 'pwwadmin')
    # post :authenticate_login, params: { username: @user.username, password: 'pwwadmin' } # Not working

    assert_not_nil(@user)
    # assert is_logged_in?

    assert_equal session[:username], @user.username
    assert_equal session[:role], @user.role
    assert_equal session[:user_id], @user.id
    assert_equal flash[:status], 'success'    
  end

  test "login with invalid information" do
    get :index
    assert_template 'login/index'
    
    post :authenticate_login, params: { username: '', password: '' }

    assert_equal flash[:status], 'error'
    assert_not flash.empty?
    assert_response :redirect
    assert_redirected_to login_path(locale: I18n.locale)
  end

  test "should destroy sessions and update associated Applicant edit_status to false" do
    @partner = users(:partner1)
    @john = applicants(:john)

    get :destroy, id: @partner.id
    applicant = Applicant.find(@john.id)
    applicant.edit_status = false
    applicant.save

    assert_response :redirect
    
    @john.reload

    assert_equal false, @john.edit_status
    assert_equal session[:username], nil
  end

end
