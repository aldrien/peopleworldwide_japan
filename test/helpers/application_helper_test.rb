require 'test_helper'

class ApplicationHelperTest < ActionView::TestCase
  
  test "should return user profile" do
    @admin = users(:admin)
    gravatar_id = Digest::MD5.hexdigest(@admin.email.downcase)
    
    assert_equal user_profile_image(@admin, 'thumb'), "https://gravatar.com/avatar/#{gravatar_id}.png?s=50"
  end

  test "should return file icon based on type" do
    assert_equal icon_for('png'), "/filetype-icons/filetype-png.png"
    assert_equal icon_for('pdf'), "/filetype-icons/filetype-pdf.png"
    assert_equal icon_for('doc'), "/filetype-icons/filetype-doc.png"
  end

  test "should return user status in readable value" do
    assert_equal user_status(1), "Activate User Access"
    assert_equal user_status(0), "Deactivate User Access"
  end

  test "should check if nil" do
    @admin = users(:admin)

    assert_equal check_nil(work_experiences(:vj_work_experience).leaving_date), "-"
    assert_equal check_nil(@admin.fullname), "Administrator"
  end

  test "should compute work time" do
    entering_date = work_experiences(:aldrien_work_experience).entering_date
    leaving_date = work_experiences(:aldrien_work_experience).leaving_date

    assert_equal compute_work_time(entering_date, leaving_date), "10 months"
    assert_equal compute_work_time('2010/123/123', '20121/23/1'), "invalid date format"
  end

  test "should create custom applicant id" do
    assert_equal custom_id('1'), '000001'
    assert_equal custom_id('234'), '000234'
  end
  
  test "should compute the project's percentage of given data" do
    @project = projects(:project1)

    total_needed_applicants = @project.no_of_needed_applicants
    registered_applicant = @project.applicants.count

    assert_equal percent_of(total_needed_applicants, registered_applicant), 8.0
  end
  
  test "should raw string with dominant hand" do
    assert_equal helper_dominant_hand('Right'), "右<br>Right"
    assert_equal helper_dominant_hand('Left'), "左<br>Left"
    assert_equal helper_dominant_hand('Ambidextrous'), "両利き<br>Ambidextrous"
  end
  
  test "should raw string with gender" do
    assert_equal helper_gender('Male'), '男性<br>Male'
    assert_equal helper_gender('Female'), '女性<br>Female'
  end
  
  test "should raw string with marital status" do
    assert_equal helper_martial_status('Single'), '独身<br>Single'
    assert_equal helper_martial_status('Married'), '既婚<br>Married'
  end
  
  test "should raw string with japanese skill" do
    assert_equal helper_japanese_skill('N1'), 'N1'
    assert_equal helper_japanese_skill('Conversational'), '日常会話<br>Conversational'

    assert_nil helper_japanese_skill('ShouldBeNil'), nil
  end

  test "should raw string with last academic" do
    assert_equal helper_last_academic('Junior college graduated'), '短期大学卒業<br>Junior college graduated'
    assert_equal helper_last_academic('Vocational school drop out'), '専門学校中退<br>Vocational school drop out'

    assert_nil helper_last_academic('ShouldBeNil'), nil
  end
  
  test "should raw string with education level" do
    assert_equal helper_education_level('Enter'), '入学<br>Enter'
    assert_equal helper_education_level('Graduate'), '卒業<br>Graduate'
    assert_equal helper_education_level('ShouldBeDropOut'), '中退<br>Drop-out'
  end
  
  test "should raw string with is work related" do
    assert_equal helper_is_work_related(true), 'はい<br>Yes'
    assert_equal helper_is_work_related(false), '無し<br>No'
  end
  
  test "should raw string with english skill" do
    assert_equal helper_english_skill('Conversational'), '日常会話<br>Conversational'
    assert_equal helper_english_skill('Business'), 'ビジネス<br>Business'
    assert_equal helper_english_skill('No'), '無し<br>No'

    assert_nil helper_english_skill('ShouldBeNil'), nil
  end
  
  test "should return same string since not came from applicants controller" do
    assert_equal trimmer('入学<br>Enter'), '入学<br>Enter'
  end

  test "should generate icon" do
    assert_equal generate_icon('envelope'), 'pdf_icons/envelope.png'
    assert_equal generate_icon('home'), 'pdf_icons/home.png'
    assert_equal generate_icon('list'), 'pdf_icons/list.png'

    assert_nil generate_icon('car'), nil
  end

  test "should return true if admin role, in showing applicant notes in pdf report" do
    session[:role] = users(:admin).role
    applicant = applicants(:aldrien)
    assert_equal check_notes_if_to_show(applicant), applicant.show_remarks_in_admin_pdf
  end

  test "should return false if partner role, in showing applicant remarks in pdf report" do
    session[:role] = users(:partner1).role
    applicant = applicants(:aldrien)
    assert_equal check_remarks_if_to_show(applicant), applicant.show_remarks_in_partner_pdf
  end

  test "should return date with readable string" do
    sarah = applicants(:sarah)

    assert_equal check_last_sent(sarah.created_at), '1 day'
    assert_equal check_last_sent(nil), ''
  end

end