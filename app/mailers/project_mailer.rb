require 'open-uri'
class ProjectMailer < ApplicationMailer  
  default from: "noreply@gmail.com"
  
  # sends an email to selected Client and/or Partner, also attached file if present in Project
  def send_job_order_mail(project, email)
    @project = project

    if @project.document.exists?
      attachments["#{@project.title.capitalize}-Document-#{Date.today.iso8601}#{File.extname(@project.document.url)}"] = {
        # mime_type: 'multipart/mixed', #'application/pdf',
        content: open(Rails.root.join("public/#{@project.document.expiring_url}")).read
        # content: open(@project.document.expiring_url).read
      }
    end

    mail(
      to: email,
      subject: "People Worldwide Japan | #{@project.title} - #{Date.today.iso8601}",
    )
  end

  # sends an error email to administrator for debugging purposes
  def error_mail(message)
    @message = message
    mail(
      to: 'aldrienht@gmail.com',
      subject: 'PWW Mailer Error | Sending Project Job Order'
    )
  end
end
