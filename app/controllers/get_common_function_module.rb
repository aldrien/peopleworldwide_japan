# List of common function used in many controllers
module GetCommonFunctionModule
  # Updates record based on given model and other parameters
  def update_common_table(table, id, key, value)
    begin
      case table
      when 'countries'
        @record = Country.find(id)
      when 'file_types'
        @record = FileType.find(id)
      when 'religions'
        @record = Religion.find(id)
      when 'job_types'
        @record = JobType.find(id)
      when 'academic_abilities'
        @record = AcademicAbility.find(id)
      when 'aptitude_tests'
        @record = AptitudeTest.find(id)
      end
        
      case key
      when 'en_name'
        @record.en_name = params[:value]
        edit_field = 'English Name'
      when 'jp_name'
        @record.jp_name = params[:value]
        edit_field = 'Japanese Name'
      when 'country_code'
        @record.country_code = params[:value]
        edit_field = 'Country Code'
      end
      @record.last_updated_by = session[:username]
      @record.save
      
      render json: {
          status: 'success', 
          message: "#{edit_field} was successfully updated.", 
          updated_at: @record.updated_at.to_time.strftime("%a, %e %b %Y %H:%M"), 
          updated_by: session[:username]
      }
    rescue => error
      render json: {status: 'error', message: error.message}
    end
  end

  # This function filters the Applicant which is NOT listed/requested to be deleted
  def filter_not_to_be_deleted(applicants)
    applicants.not_to_be_deleted(get_all_requested_to_be_deleted_applicant.pluck(:applicant_id))
  end

  # Update Lock Function for Login User
  def update_lock_function
    @locked_applicants = Applicant.where(last_updated_by: session[:username], edit_status: true)
    unless @locked_applicants.empty?
      @locked_applicants.each do |applicant|
        applicant.edit_status = false
        applicant.save
      end
    end
  end
  
end