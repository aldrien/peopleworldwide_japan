class LoginController < ApplicationController
  layout 'login'
  include ApplicationHelper
  include CheckLoginModule
  include GetCommonFunctionModule

  # renders Login page
  def index
    @page_title = 'People Worldwide Japan | Login'
    redirect_to "/#{I18n.locale}/home" if session[:username]    
  end

  # authenticate user credentials, function from CheckLoginModule
  def authenticate_login
    user = authenticate_current_user(params[:username], params[:password])
    if user && user.status == true      
        session[:username] = user.username.downcase
        session[:user_id] = user.id
        session[:role] = user.role
        session[:profile] = user_profile_image(user, 'mini')

        flash.keep[:status] = 'success'
        
        if user.role == 'client'
          redirect_to_url = projects_path(locale: I18n.locale)
        else
          redirect_to_url = "/#{I18n.default_locale}/home"
        end
        redirect_to redirect_to_url, :notice => "Hi #{user.username.capitalize}! you have successfully logged in."

    else
      flash.keep[:status] = 'error'
      redirect_to "/#{I18n.default_locale}/login", notice: 'Invalid Username or Password!'
    end
  end

  # destroy all sessions, when Logged Out
  def destroy
    # Update Lock Function for Login User
    update_lock_function

    session.delete(:username)
    session.delete(:role)
    session.delete(:user_id)
    reset_session
    session.clear

    flash.keep[:status] = 'success'
    redirect_to "/#{I18n.default_locale}/login", notice: 'You have successfully logged out.'
  end
end
