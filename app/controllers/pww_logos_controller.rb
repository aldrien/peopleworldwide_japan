class PwwLogosController < ApplicationController
  load_and_authorize_resource

  layout 'cms'
  include CheckLoginModule
  before_action :check_login

  # renders index page
  def index
    @page_title = 'PDF Header Management'
  end
 
  # renders new page
  def new
    @page_title = 'Add New PDF Header'
  end

  # renders edit page
  def edit
    @menu = 'pww_logos'
    @page_title = 'Edit PDF Header' 
  end

  # creates new PWW Logo, from posted form in new page
  def create
    @page_title = 'Add New PDF Header'
    if @pww_logo.save
      flash.keep[:status] = 'success'
      flash.keep[:notice] = 'New PDF Header was successfully created.'
      redirect_to pww_logos_path(:locale => I18n.locale)
    else
      flash.keep[:status] = 'error'
      flash.keep[:notice] = 'Sorry, there was an error saving.'
      @menu = 'pww_logos'
      render action: 'new'
    end
  end
  
  # updates PWW Logo, from posted form in edit page
  def update
    if @pww_logo.update_attributes(pww_logo_params)
      flash[:status] = 'success'
      flash[:notice] = "#{@pww_logo.title} information was successfully updated."
      redirect_to pww_logos_path(:locale => I18n.locale)
    else
      @menu = 'pww_logos'
      @page_title = 'Edit PDF Header'
      flash[:status] = 'error'
      flash[:notice] = 'Sorry, there was an error updating.'
      render action: 'edit'
    end
  end

  # deletes PWW Logo record, submitted by ajax
  def destroy
    unless @pww_logo.nil?
      @pww_logo.destroy
      render json: {status: 'success', message: 'You have successfully deleted PDF Header.'}
    else
      render json: {status: 'error', message: 'Sorry, unable to find record.'}
    end    
  end
        
  private
  def pww_logo_params
    params.require(:pww_logo).permit!
  end
end
