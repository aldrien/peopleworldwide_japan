class ReligionsController < ApplicationController    
  require 'date'
  layout 'cms'
  include CheckLoginModule
  include GetCommonDataModule
  include GetCommonFunctionModule
  include ApplicationHelper
  
  before_action :check_login

  # renders index page
  def index
    @page_title = 'Religions List & Management'    
  end

  # returns Religion records in JSON format (for dataTable)
  def get_all_religions
    result = []
    get_religions.each do |r|
      result << common_table_rows(r.id, r.en_name, r.jp_name, nil, r.last_updated_by, r.updated_at.to_time.strftime("%a, %e %b %Y %H:%M"), 'delete_religion', 'edit_religion')
    end
    render json: {status: 'success', data: result}
  end

  # adds new Religion record, submitted via ajax
  def add_new_religion
    begin      
        religion = Religion.new
        religion.en_name = params[:en_name]
        religion.jp_name = params[:jp_name]
        religion.last_updated_by = session[:username]
        religion.save

        render json: {status: 'success', message: 'New Religion was successfully added.'}      
    rescue => error
      render json: {status: 'error', message: error.message}
    end
  end

  # update Religion, submitted via ajax (params[:pk] == id)
  def edit_religion
    update_common_table('religions', params[:pk], params[:name], params[:value])
  end

  # deletes Religion, submitted via ajax
  def delete_religion
    begin
      religion = Religion.find(params[:id])      
      religion.destroy

      render json: {status: 'success', message: 'Religion was successfully deleted.'}
    rescue => error
      render json: {status: 'error', message: error.message}
    end
  end
end
