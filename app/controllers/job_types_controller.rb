class JobTypesController < ApplicationController
  require 'date'
  layout 'cms'
  include CheckLoginModule
  include GetCommonDataModule
  include GetCommonFunctionModule
  include ApplicationHelper

  before_action :check_login

  # renders index page
  def index
    @page_title = 'Job Types List & Management'    
  end

  # returns JobType records in JSON format (for dataTable)
  def get_all_job_types
    result = []
    get_job_types.each do |r|
      result << common_table_rows(r.id, r.en_name, r.jp_name, nil, r.last_updated_by, r.updated_at.strftime("%a, %e %b %Y %H:%M"), 'delete_job_type', 'edit_job_type')
    end
    render json: {status: 'success', data: result}
  end

  # adds new JobType record, submitted via ajax
  def add_new_job_type
    begin
      job = JobType.new
      job.en_name = params[:en_name]
      job.jp_name = params[:jp_name]
      job.last_updated_by = session[:username]
      job.save

      render json: {status: 'success', message: 'New Job Type was successfully added.'}
    rescue => error
      render json: {status: 'error', message: error.message}
    end
  end

  # update JobType, submitted via ajax (params[:pk] == id)
  def edit_job_type
    update_common_table('job_types', params[:pk], params[:name], params[:value])
  end

  # deletes JobType, submitted via ajax
  def delete_job_type
    begin
      job = JobType.find(params[:id])      
      job.destroy

      render json: {status: 'success', message: 'Job Type was successfully deleted.'}
    rescue => error
      render json: {status: 'error', message: error.message}
    end
  end
end
