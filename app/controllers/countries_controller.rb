class CountriesController < ApplicationController
  require 'date'
  layout 'cms'
  include CheckLoginModule
  include GetCommonDataModule
  include GetCommonFunctionModule
  include ApplicationHelper  

  before_action :check_login

  # renders index page
  def index
    @page_title = 'Countries List & Management'    
  end

  # returns Country records in JSON format (for dataTable)
  def get_all_countries
    result = []
    get_countries.each do |r|
      result << common_table_rows(r.id, r.en_name, r.jp_name, r.country_code, r.last_updated_by, r.updated_at.to_time.strftime("%a, %e %b %Y %H:%M"), 'delete_country', 'edit_country')
    end
    render json: {status: 'success', data: result}
  end

  # adds new Country record, submitted via ajax
  def add_new_country
    begin
      country = Country.new
      country.en_name = params[:en_name]
      country.jp_name = params[:jp_name]
      country.country_code = params[:country_code]
      country.last_updated_by = session[:username]
      country.save

      render json: {status: 'success', message: 'New country was successfully added.'}
    rescue => error
      render json: {status: 'error', message: error.message}
    end
  end

  # update Country, submitted via ajax (params[:pk] == id)
  def edit_country
    update_common_table('countries', params[:pk], params[:name], params[:value])
  end

  # deletes Country, submitted via ajax
  def delete_country
    begin
      country = Country.find(params[:id])      
      country.destroy

      render json: {status: 'success', message: 'Country was successfully deleted.'}
    rescue => error
      render json: {status: 'error', message: error.message}
    end
  end
end
