# This is the core Class of the system and runs few methods before other controller works.
class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_action :set_locale, :get_required_data
  before_filter :set_cache_headers, :current_user
  rescue_from ActiveRecord::RecordNotFound, :with => :render_404
  # check_authorization
  
  rescue_from CanCan::AccessDenied do |exception|
    # flash.keep[:status] = "error"
    # flash.keep[:notice] = "Sorry, Access denied."
    # redirect_to root_url
    # render :file => "#{Rails.root}/public/404.html", :status => 404, :layout => false
    if session[:username]
      render_404
    else
      redirect_to login_path(locale: I18n.locale)
    end
  end
  
  def render_404
    render template: "layouts/404.html", layout: false, status: 404
  end  
  
  def set_locale
    I18n.locale = params[:locale] || I18n.default_locale
  end

  alias :std_redirect_to :redirect_to
  def redirect_to(*args)
     flash.keep
     std_redirect_to *args
  end

  def get_required_data
    @latest_projects = Project.where('status IN (?)', ["In-progress", "Waiting"]).order('id DESC').limit(5)
    @request_of_deletion = RequestedToBeDeletedApplicant.order('created_at DESC')
  end

  def reset_session
    @_request.reset_session
  end

  # To prevent broswer caching
  def set_cache_headers
    response.headers["Cache-Control"] = "no-cache, no-store, max-age=0, must-revalidate"
    response.headers["Pragma"] = "no-cache"
    response.headers["Expires"] = "Fri, 01 Jan 1990 00:00:00 GMT"
  end

  def current_user
    user = User.find(session[:user_id]) if session[:user_id]
    @current_user = user
  end

end
