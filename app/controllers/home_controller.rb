class HomeController < ApplicationController  
  # authorize_resource :class => false#, parent: false

  layout 'cms'
  include CheckLoginModule
  include GetCommonDataModule
  include GetCommonFunctionModule

  before_action :check_login  

  # renders index page
  def index
    if session[:role] == 'client'
      redirect_to projects_path(locale: I18n.locale)
    end

    @page_title = "#{session[:role]} Dashboard"
    @menu = 'dashboard'    
    
    get_user_applicants
    get_user_projects
  
    if session[:role] == 'admin'
      @admins = User.admins
      @partners = User.partners
      @clients = User.clients        
    end
  end

  # returns current user' project details
  def project_chart    
    result = []
    get_user_projects.each do |project|
      result << {
        title: project.title,
        registered: project.count_associated_applicants(session[:role]),
        total: project.no_of_needed_applicants
      }
    end
    render json: result    
  end

  private
  # get current user's project, based on user role
  def get_user_projects
    if session[:role] == 'admin'
      @projects = Project.not_failed.order('created_at DESC').limit(6)
    elsif session[:role] == 'partner'
      @projects = current_user.partner_projects.not_failed.order('created_at DESC').limit(6)
    else #client
      @projects = current_user.client_projects.not_failed.order('created_at DESC').limit(6)
    end
  end

  # get current user's associated applicants, based on user role
  def get_user_applicants
    if session[:role] == 'admin'
      @applicants = filter_not_to_be_deleted(Applicant.all)
    elsif session[:role] == 'partner'
      @applicants = filter_not_to_be_deleted(current_user.partner_created_applicants.has_creator)
    else #client
      @applicants = filter_not_to_be_deleted(current_user.applicants.has_creator)
    end
  end

end