class UsersController < ApplicationController
  load_and_authorize_resource except: [:authenticate_user_password]

  layout 'cms'
  include CheckLoginModule
  before_action :check_login
  before_action :get_pww_logos, only: [:new, :create, :edit, :update]

  # Note: Since I'm using load_and_authorize_resource function, it will do:
  # @users = User.all
  # @user = User.new
  # @user = User.create(user_params)
  # @user = User.find(params[:id])

  def index
    @page_title = 'User Accounts List'    
  end

  def show
    render action: 'index'
  end
  
  def new
    @page_title = 'New User Account'
  end
  
  def create
    @page_title = 'New User Account'
    @user.last_updated_by = session[:username]
    if @user.save
      flash.keep[:status] = 'success'
      flash.keep[:notice] = 'New user account was successfully created.'
      redirect_to users_path(:locale => I18n.locale)
    else
      flash.keep[:status] = 'error'
      flash.keep[:notice] = 'Sorry, there was an error saving.'
      @menu = 'users'
      render action: 'new'
    end
  end
  
  def edit
    @menu = 'users'
    @page_title = 'Edit User Account'
  end
  
  def update
    @user.last_updated_by = session[:username]
    if @user.update_attributes(user_params)   #user_params.reject{|k,v| v.blank?}            
      flash[:status] = 'success'
      flash[:notice] = "#{@user.username.capitalize} information was successfully updated."
      
      redirect_to users_path(:locale => I18n.locale)
    else
      @menu = 'users'
      @page_title = 'Edit User Account'
      flash[:status] = 'error'
      flash[:notice] = 'Sorry, there was an error updating.'
      
      render action: 'edit'
    end
  end
  
  def destroy
    unless @user.nil?
      @user.destroy
      render json: {status: 'success', message: 'You have successfully deleted User Account.'}
    else
      render json: {status: 'error', message: 'Sorry, unable to find User Account.'}
    end    
  end
  
  def delete_profile_image
    @user.last_updated_by = session[:username]
    @user.image_file_name = nil
    @user.image_file_size = nil
    @user.image_content_type = nil
    if @user.save
      render json: {status: 'success', message: 'Profile Image was successfully removed.'}
    else
      render json: {status: 'error', message: 'Unable to remover Profile Image.'}
    end
  end

  def authenticate_user_password
    user = authenticate_current_user(session[:username], params[:password])
    if user
      render json: {status: 'success', message: 'User password was authenticated!'}
    else
      render json: {status: 'error', message: 'Sorry Invalid Password. Please Try Again!'}
    end
  end
    
  private
  def user_params
    params.require(:user).permit!
  end

  def get_pww_logos
    @pww_logos = PwwLogo.active.order('title ASC')
  end
end
