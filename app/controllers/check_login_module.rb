# Used for user credential authentication
module CheckLoginModule
  # authenticate current user (username & password)
  def authenticate_current_user(username, password)
    User.find_by(username: username).try(:authenticate, password)
  end

  # checks if user was signed out and redirects to login page
  def check_login
    if session[:username].nil? || session[:username].blank?
      flash.keep[:status] = 'error'
      flash.keep[:notice] = 'Session Expired, Please Login Again.'
      redirect_to "/#{I18n.locale}/login"
    end
  end  
end