# This Class manages the Academic Ability records that will be used in Applicant records
class AcademicAbilitiesController < ApplicationController
  require 'date'
  layout 'cms'
  include CheckLoginModule
  include GetCommonDataModule
  include GetCommonFunctionModule
  include ApplicationHelper
  
  before_action :check_login

  # renders index page
  def index
    @page_title = 'Academic Ability List & Management'    
  end

  # returns AcademicAbility records in JSON format (for dataTable)
  def get_all_academic_abilities
    result = []
    get_academic_abilities.each do |r|
      result << common_table_rows(r.id, r.en_name, r.jp_name, nil, r.last_updated_by, r.updated_at.to_time.strftime("%a, %e %b %Y %H:%M"), 'delete_academic_ability', 'edit_academic_ability')
    end
    render json: {status: 'success', data: result}
  end

  # adds new AcademicAbility record, submitted via ajax
  def add_new_academic_ability
    begin
        academic = AcademicAbility.new
        academic.en_name = params[:en_name]
        academic.jp_name = params[:jp_name]
        academic.last_updated_by = session[:username]
        academic.save

        render json: {status: 'success', message: 'New Academic Ability was successfully added.'}
    rescue => error
      render json: {status: 'error', message: error.message}
    end
  end

  # update AcademicAbility, submitted via ajax (params[:pk] == id)
  def edit_academic_ability
    update_common_table('academic_abilities', params[:pk], params[:name], params[:value])
  end

  # deletes AcademicAbility, submitted via ajax
  def delete_academic_ability
    begin
      academic = AcademicAbility.find(params[:id])      
      academic.destroy

      render json: {status: 'success', message: 'Academic Ability was successfully deleted.'}
    rescue => error
      render json: {status: 'error', message: error.message}
    end
  end
end
