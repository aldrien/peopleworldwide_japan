class FileTypesController < ApplicationController
  require 'date'
  layout 'cms'
  include CheckLoginModule
  include GetCommonDataModule
  include GetCommonFunctionModule
  include ApplicationHelper
  
  before_action :check_login

  # renders index page
  def index
    @page_title = 'File Types List & Management'    
  end

  # returns FileType records in JSON format (for dataTable)
  def get_all_file_types
    result = []
    get_file_types.each do |r|
      result << common_table_rows(r.id, r.en_name, r.jp_name, nil, r.last_updated_by, r.updated_at.to_time.strftime("%a, %e %b %Y %H:%M"), 'delete_file_type', 'edit_file_type')
    end
    render json: {status: 'success', data: result}
  end

  # adds new FileType record, submitted via ajax
  def add_new_file_type
    begin      
        file_type = FileType.new
        file_type.en_name = params[:en_name]
        file_type.jp_name = params[:jp_name]
        file_type.last_updated_by = session[:username]
        file_type.save

        render json: {status: 'success', message: 'New File Type was successfully added.'}      
    rescue => error
      render json: {status: 'error', message: error.message}
    end
  end

  # update FileType, submitted via ajax (params[:pk] == id)
  def edit_file_type
    update_common_table('file_types', params[:pk], params[:name], params[:value])
  end

  # deletes FileType, submitted via ajax
  def delete_file_type
    begin
      file_type = FileType.find(params[:id])      
      file_type.destroy

      render json: {status: 'success', message: 'File Type was successfully deleted.'}
    rescue => error
      render json: {status: 'error', message: error.message}
    end
  end
end