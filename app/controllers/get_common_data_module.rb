# Returns common Database records
module GetCommonDataModule
  # Get all Country records
  def get_countries
    @countries = Country.order('en_name ASC')
  end

  # Get all JobType records
  def get_job_types
    @job_types = JobType.order('en_name ASC')  
  end

  # Get all Religion records
  def get_religions
    @religions = Religion.order('en_name ASC')
  end

  # Get all FileType records
  def get_file_types
    @file_types = FileType.order('created_at DESC')
  end

  # Get all AcademicAbility records
  def get_academic_abilities
    @academic_abilities = AcademicAbility.order('created_at DESC')
  end

  # Get all AptitudeTest records
  def get_aptitude_tests
    @aptitude_tests = AptitudeTest.order('created_at DESC')
  end

  # Get all Project records
  def get_projects
    if session[:role] == 'admin'
      @projects = Project.order('title ASC')
    elsif session[:role] == 'client'
      @projects = current_user.client_projects.order('title ASC')
    else
      @projects = current_user.partner_projects.order('title ASC')
    end
  end

  # Get all RequestedToBeDeleted records
  def get_all_requested_to_be_deleted_applicant
    @to_be_deleted = RequestedToBeDeletedApplicant.order('created_at DESC')
  end

  # Get all Current User (Partner) created applicants
  def get_partner_created_applicants
    current_user.partner_created_applicants
  end

  # Get Current User PDF Logo
  def get_pww_logo
    @pww_logo = current_user.pww_logo
  end
end