# This Class manages the Aptitude Test or Skill check records that will be used in Applicant records
class AptitudeTestsController < ApplicationController
  require 'date'
  layout 'cms'
  include CheckLoginModule
  include GetCommonDataModule
  include GetCommonFunctionModule
  include ApplicationHelper
  
  before_action :check_login

  # renders index page
  def index
    @page_title = 'Skill Check Test List & Management'    
  end

  # returns AptitudeTest records in JSON format (for dataTable)
  def get_all_aptitude_tests
    result = []
    get_aptitude_tests.each do |r|
      result << common_table_rows(r.id, r.en_name, r.jp_name, nil, r.last_updated_by, r.updated_at.to_time.strftime("%a, %e %b %Y %H:%M"), 'delete_aptitude_test', 'edit_aptitude_test')
    end
    render json: {status: 'success', data: result}
  end

  # adds new AptitudeTest record, submitted via ajax
  def add_new_aptitude_test
    begin
        aptitude = AptitudeTest.new
        aptitude.en_name = params[:en_name]
        aptitude.jp_name = params[:jp_name]
        aptitude.last_updated_by = session[:username]
        aptitude.save

        render json: {status: 'success', message: 'New Skill Check Test was successfully added.'}      
    rescue => error
      render json: {status: 'error', message: error.message}
    end
  end

  # update AptitudeTest, submitted via ajax (params[:pk] == id)
  def edit_aptitude_test
    update_common_table('aptitude_tests', params[:pk], params[:name], params[:value])
  end

  # deletes AptitudeTest, submitted via ajax
  def delete_aptitude_test
    begin
      aptitude = AptitudeTest.find(params[:id])      
      aptitude.destroy

      render json: {status: 'success', message: 'Skill Check Test was successfully deleted.'}
    rescue => error
      render json: {status: 'error', message: error.message}
    end
  end
end
