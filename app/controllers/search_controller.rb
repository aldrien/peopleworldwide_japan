require 'axlsx'
class SearchController < ApplicationController
  layout 'cms'
  include CheckLoginModule
  include GetCommonDataModule
  include GetCommonFunctionModule
  include ApplicationHelper

  before_action :check_login
  before_action :set_page_title, :set_common_data, only: [:index, :quick_search, :search_applicants]

  # renders index page
  # authorize view based on user role
  # get up to 100 latest created applicant records (not associated with to be deleted records)
  def index
    authorize! :view_advanced_search, :search_controller

    @menu = 'search_index'
    if session[:role] == 'admin'
      @applicants = Applicant.all
    elsif session[:role] == 'partner'
      @applicants = get_partner_created_applicants.has_creator.try(:in_progress)
    else #client
      @applicants = current_user.applicants.has_creator.try(:in_progress)
    end
    @applicants = filter_not_to_be_deleted(@applicants).order('created_at DESC').limit(100)
    session[:previous_controller] = 'search'
  end

  # searches applicant by name and renders in index page
  def quick_search
    @menu = 'search_quick_search'
    if session[:role] == 'admin'
      @applicants = Applicant.applicant_name(params[:applicant_name])
    elsif session[:role] == 'partner'
      @applicants = get_partner_created_applicants.has_creator.applicant_name(params[:applicant_name])
    else #client
      @applicants = current_user.applicants.has_creator.applicant_name(params[:applicant_name])
    end

    @applicants = filter_not_to_be_deleted(@applicants)
    render action: 'index'
  end

  # advanced searching of applicants
  # renders result in index page
  def search_applicants
    @menu = 'search_search_applicants'
    begin
      # Main Filter on Applicants Table
      if session[:role] == 'admin'
        @applicants = Applicant.filter(filtering_params)
      elsif session[:role] == 'partner'
        @applicants = get_partner_created_applicants.has_creator.filter(filtering_params)
      else #client
        @applicants = current_user.applicants.has_creator.filter(filtering_params)
      end
      
      # Filter applicants which is NOT listed/requested to be deleted
      @applicants = filter_not_to_be_deleted(@applicants)
      applicants_default_ids = @applicants.pluck(:id)

      # By Age
      @applicants = get_by_age(@applicants, params[:age_from], params[:age_to])
      
      # By Client or Parnter
      if session[:role] == 'admin'        
        if params[:client].present? || params[:partner].present?
          project_ids = Project.get_by_client_or_partner(params[:client], params[:partner])
          @applicants = @applicants.project(project_ids)    
        end
      end
      
      # By Academic Score Range
      from_academic_applicant_ids = []
      if params[:academic_score_from].present? && params[:academic_score_to].present?
        academic_ability = AcademicAbility.find(params[:academic_ability])
        from_academic_applicant_ids = academic_ability.applicant_academic_abilities.group(:applicant_id).score_between(params[:academic_score_from], params[:academic_score_to]).pluck(:applicant_id)
      end

      # By Aptitude Score Range
      from_aptitude_applicant_ids = []
      if params[:aptitude_score_from].present? && params[:aptitude_score_to].present?
        aptitude_test = AptitudeTest.find(params[:aptitude_test])
        from_aptitude_applicant_ids = aptitude_test.applicant_aptitude_tests.group(:applicant_id).score_between(params[:aptitude_score_from], params[:aptitude_score_to]).pluck(:applicant_id)  
      end

      got_applicant_ids = (from_academic_applicant_ids << from_aptitude_applicant_ids).flatten.uniq
      search_ids = applicants_default_ids & got_applicant_ids # intersect
      
      # unless search_ids.empty?
      if (params[:academic_score_from].present? && params[:academic_score_to].present?) || (params[:aptitude_score_from].present? && params[:aptitude_score_to].present?)
        @applicants = @applicants.where(id: search_ids)
      end
      # end

      @applicants = @applicants.includes(:religion, :country, :project, :applicant_aptitude_tests, :applicant_academic_abilities)#.flatten

      render action: 'index'

    rescue => error
      flash[:status] = 'error'
      flash[:notice] = "Sorry, there was an error. #{error.message}"
      render action: 'index'
    end
  end

  # generates PDF full listing report
  def generate_pdf_full_report
    applicant_ids = params[:applicants].split(',')
    @applicants = Applicant.includes(:applicant_aptitude_tests, :applicant_academic_abilities, :work_experiences, :education_backgrounds, :family_background).where(id: applicant_ids).sort_asc_by_code
    
    show_aptitude = nil
    if params[:skill].present?
      @aptitude_test = AptitudeTest.find(params[:skill])
      show_aptitude = "<br><br>#{@aptitude_test.jp_name}<br>#{@aptitude_test.en_name}"
    end

    if params[:company].present?
      @company_name = params[:company]
    else      
      @company_name = 'Mixed Applicants'
      unless params[:project].blank?
        project = Project.find(params[:project])
        if project
          @company_name = project.try(:company_name)
        end
      end
    end    

    @report_title = 'People World Wide Japane | PDF-Report'
    template = 'search/generate_full_report.pdf.erb'
    
    respond_to do |format|
      format.html
      format.pdf do
        render title:         @report_title,
               pdf:           @report_title,
               template:      template,
               encoding:      'UTF-8',
               orientation:   'Landscape',
               layout:        'pdf_full_report_template.html',
               header: {                        
                        spacing: 0,
                        html: {
                            template: 'layouts/pdf_full_report_header.html',
                            locals:   {
                              aptitude_test: show_aptitude,
                              company_name: @company_name,
                              pww_logo: get_pww_logo
                            }
                          }
                       },
               footer: {
                        center:    '[page] of [topage]',
                        right:     '',
                        font_name: 'Arial',
                        font_size: 8                        
                       },
               margin: {
                        top:     80, # default 10 (mm)
                        bottom:  12,
                        left:    6,
                        right:   6 
                       }
      end
    end
  end

  # generates excel file, with few applicant details
  def generate_csv_full_report
    applicant_ids = params[:applicants].split(',') if params[:applicants].present?
    @applicants = Applicant.where(id: applicant_ids.flatten).sort_asc_by_code
    
    respond_to do |format|
      format.html
      format.csv { send_data @applicants.to_csv, :filename => "People-Worldwide-Japan-CSV-Report.csv" }      
      format.xlsx do
        @applicants.each do |applicant|
            applicant.custom_code = "#{applicant.country.try(:country_code)}#{custom_id(applicant.id)}" if applicant.custom_code.blank?
        end
        generate_xlsx(@applicants)
      end
    end
  end


  private
  def generate_xlsx(applicants)
    Axlsx::Package.new do |p|
      p.workbook.add_worksheet(name: "Applicants Info") do |sheet|
        styles = p.workbook.styles        
        header = styles.add_style(sz: 14, fg_color: 'FFFFFF', bg_color: 'E22828', b: true)
        centered = styles.add_style(alignment: {horizontal: :center})

        sheet.add_row ["CODE NO.", "COUNTRY", "NAME", "EMAIL ADDRESS", "HOME ADDRESS"], style: header
        applicants.sort_by { |column| column['custom_code'] }.each do |a|
          sheet.add_row [
            a.custom_code,
            a.country.try(:en_name),
            (a.name).strip,
            (a.email).strip,
            (a.address).strip
          ], style: [centered]
        end
      end    

      send_data(p.to_stream.read,
                type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                filename: "People-Worldwide-Japan-Excel-Report.xlsx")
    end
  end

  # def filter_params(search_filters)
  #   # search_filters.each {|key,value| search_filters.delete(key) if value.blank? } 
  #   search_filters.delete_if { |key, value| value.blank? }
  # end

  def set_common_data    
    get_countries
    get_job_types
    get_religions
    get_projects
    get_academic_abilities
    get_aptitude_tests
  end

  def set_page_title    
    @page_title = 'Search Applicants'
  end  

  def filtering_params
    params.slice(
      :applicant_name, 
      :english_skill, 
      :is_work_related_expirience, 
      :status, :country, 
      :jlpt_level, 
      :marital_status, 
      :gender, 
      :last_academic, 
      :religion, 
      :project)
  end

  def get_by_age(applicants, age_from, age_to)
    # By Age Range
    if age_from.present? && age_to.present?
      applicants = applicants.age_between(age_from, age_to)
    end

    # By exact age (only params[:age_from] is present)
    if age_from.present? && !age_to.present?
      applicants = applicants.exact_age(age_from)
    end
    return applicants
  end
end
