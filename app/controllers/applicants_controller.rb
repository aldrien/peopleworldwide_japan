# This Class manages the core Applicant records
class ApplicantsController < ApplicationController        
  load_and_authorize_resource except: [:lock_function, :video_interview, :video_self_introduction, :generate_applicant_report, :request_for_delete_applicant, :restore_applicant, :make_comments_to_applicant]
  # load_and_authorize_resource param_method: :applicant_params
  # skip_authorization_check only: [:video_interview, :video_self_introduction, :generate_applicant_report]
  
  # As of CanCan 1.5 you can use the
  # skip_load_and_authorize_resource, 
  # skip_load_resource or 
  # skip_authorize_resource
  # methods to skip any of the applied behavior and specify specific actions like in a before filter.

  layout 'cms'
  include GetCommonDataModule
  include GetCommonFunctionModule
  include CheckLoginModule
  include ApplicationHelper

  before_action :check_login
  before_action :get_applicant, only: [:lock_function, :destroy, :video_self_introduction, :video_interview, :request_for_delete_applicant]

  def index
    @page_title = 'Applicants List'
    @applicants = filter_not_to_be_deleted(@applicants).includes(:country, :religion).order('created_at DESC').limit(100)
    if current_user.role != 'admin'
      @applicants = @applicants.has_creator
    end

    set_common_data
    session[:previous_controller] = 'applicants'

    # Update Lock Function for Login User
    update_lock_function
  end

  def to_be_deleted
    session[:previous_controller] = 'applicants_to_be_deleted'
    
    @page_title = 'List of Requested Applicants To Be Deleted'
    @to_be_deleted = get_all_requested_to_be_deleted_applicant
  end

  def show
    @applicant = @applicants.order('name ASC')
    set_common_data
    render action: 'index'
  end
  
  def lock_function
    begin
      @applicant.edit_status = params[:edit_status]
      @applicant.last_updated_by = session[:username]
      @applicant.save

      if params[:edit_status] == 'true'
        mesg = 'Locked Function was activated to this Applicant.'
      else
        mesg = 'Locked Function was deactivated.'
      end
      render json: {status: 'success', message: mesg}
      
    rescue => error
      render json: {status: 'error', message: "Sorry, unable to activate Locked Function. #{error.message}"}
    end
  end

  def edit
    @page_title = "Edit #{@applicant.name} Data"
    @menu = 'applicants'
    set_common_data    
  end

  def builder(applicant_instance)
    applicant_instance.education_backgrounds.build #https://github.com/plataformatec/simple_form/wiki/Nested-Models
    applicant_instance.work_experiences.build
    applicant_instance.build_family_background
    applicant_instance.applicant_files.build
    applicant_instance.applicant_academic_abilities.build
    applicant_instance.applicant_aptitude_tests.build
  end

  def new
    @page_title = 'Register New Applicant'    
    set_common_data
    if can? :manage, Project
      @projects = Project.order('title ASC')
    else
      @projects = current_user.partner_projects.order('title ASC')
    end
    
    builder(@applicant)
  end
  
  def create
    @page_title = 'New Applicant'
    set_common_data
    @applicant.creator_id = session[:user_id]
    @applicant.last_updated_by = session[:username]

    if @applicant.save
      flash.keep[:status] = 'success'
      flash.keep[:notice] = "#{@applicant.name}'s record was successfully created."
      redirect_to applicants_path(:locale => I18n.locale)
    else
      @menu = 'applicants'
      flash.keep[:status] = 'error'
      flash.keep[:notice] = 'Sorry, there was an error saving.'
      builder(@applicant)
      render action: 'new', locale: I18n.locale
    end
  end

  def update
    @page_title = "Edit #{@applicant.name} Data"
    set_common_data

    if @applicant.last_updated_by == session[:username]
      @applicant.edit_status = false
      @applicant.last_updated_by = session[:username]
    end

    if @applicant.update_attributes(applicant_params)   #applicant_params.reject{|k,v| v.blank?}
      flash[:status] = 'success'
      flash[:notice] = "#{@applicant.name}'s information was successfully updated."

      redirect_to check_previous_page
              
    else
      flash[:status] = 'error'
      flash[:notice] = 'Sorry, there was an error updating.'
      render action: 'edit', locale: I18n.locale
    end
  end

  def destroy
    unless @applicant.nil?
      @applicant.destroy
      render json: {status: 'success', message: "You have successfully deleted #{@applicant.name} records.", to_be_deleted_count: @request_of_deletion.count}
    else
      render json: {status: 'error', message: 'Sorry, unable to find Applicant.'}
    end
  end

  def video_self_introduction
    @menu = 'applicants'
    begin      
      @video_self_introduction_url = @applicant.self_introduction_video_link
      @page_title = "#{@applicant.name} 自己紹介動画 / Self Introduction Video"
    rescue
      redirect_to applicants_path(:locale => I18n.locale)
    end
  end

  def project_history
    @menu = 'applicants'
    @applicant = Applicant.find(params[:id])
    @page_title = "#{@applicant.name}'s Project History"    
  end

  # https://github.com/mileszs/wicked_pdf
  # https://www.viget.com/articles/how-to-create-pdfs-in-rails

  def generate_applicant_report
    @applicant = Applicant.find(params[:id])
    @report_title = "Applicant Report | #{@applicant.name.upcase}" 
    @pdf_title = "People World Wide | #{@applicant.name.upcase}"

    respond_to do |format|
      format.html
      format.pdf do
        render title:         @pdf_title,
               pdf:           @pdf_title,
               template:      'applicants/applicant.pdf.erb',
               encoding:      'UTF-8',               
               orientation:   'Portrait',
               layout:        'pdf_single_report_template.html',               
               header: {                        
                        spacing: 5,
                        html: {
                             template: 'layouts/pdf_single_report_header.html',
                             locals:   {
                                    applicant: @applicant,
                                    pww_logo: get_pww_logo,
                                    applicant_code: !@applicant.custom_code.blank? ? @applicant.custom_code : "#{@applicant.country.try(:country_code)}#{custom_id(@applicant.id)}"
                            }
                          }
                       },
               footer:  {
                        center:    '[page] of [topage]',
                        right:     '',
                        font_name: 'Arial',
                        font_size: 7
                        },
               margin:  {                        
                        left:    5,
                        right:   5,
                        top:     67,
                        bottom:  10
                        }               
      end
    end
  end
  
  def download_report
    @applicant = Applicant.all

    html = render_to_string(
      action: :show, 
      template: 'applicants/show.pdf.erb',
      layout: 'pdf_template.html',
      encoding:      'UTF-8',
      orientation:   'Landscape'
      )

    # html = render_to_string(:action => :show, :layout => "layouts/pdf_template.html.erb") 
    pdf = WickedPdf.new.pdf_from_string(html)
    send_data(pdf, filename: 'PWWJ-PDF-Report.pdf', disposition: 'attachment') 
  end

  def request_for_delete_applicant
    begin      
      request = RequestedToBeDeletedApplicant.create(applicant_id: @applicant.id, partner_id: current_user.id, reason: params[:reason])
      render json: {status: 'success', message: "Request to delete #{@applicant.name} record was successfully created."}
    rescue => error
      render json: {status: 'error', message: "Sorry unable to create request. #{error.message}"}  
    end
  end

  def restore_applicant
    begin
      restore = RequestedToBeDeletedApplicant.find(params[:id])
      applicant = restore.applicant.try(:name)
      if restore.destroy
        render json: {status: 'success', message: "#{applicant} record was successfully restored.", to_be_deleted_count: @request_of_deletion.count}
      end
    rescue => error
      render json: {status: 'error', message: 'Sorry, unable to restore Applicant records.'}
    end    
  end

  def make_comments_to_applicant
    begin
      client = authenticate_current_user(session[:username], params[:password])
      if client
        applicant = Applicant.find(params[:applicant_id])
        applicant.client_comments = params[:comments]
        applicant.show_client_comments = params[:is_show_to_pdf]
        applicant.save
        
        render json: {status: 'success', message: 'コメントの保存が完了しました'} #Comments was successfully saved.
      else
        render json: {status: 'error', message: 'Invalid Password / パスワードが間違っています'}
      end
    rescue => error
      render json: {status: 'error', message: error.message}
    end
  end
  
  private
  
  def get_applicant
    @applicant = Applicant.find(params[:id])
  end

  def set_common_data
    get_countries
    get_job_types
    get_religions    
    get_file_types
    get_academic_abilities
    get_aptitude_tests
    get_projects
  end

  def applicant_params
    params.require(:applicant).permit(
      :custom_code,
      :creator_id,
      :name,
      :status,
      :country_id,
      :project_id,
      :religion_id,
      :birth_date, 
      :gender, 
      :age, 
      :height,
      :weight,
      :marital_status,
      :last_academic,      
      :dominant_hand,
      :english_skill,
      :is_work_related_expirience,
      :jlpt_level,
      :profile,
      :wholebody,
      :email,
      :phone,
      :address,
      :short_notes,
      :notes,
      :show_notes_in_admin_pdf,
      :show_notes_in_partner_pdf,
      :show_notes_in_client_pdf,
      :admin_remarks,
      :show_remarks_in_admin_pdf,
      :show_remarks_in_partner_pdf,
      :show_remarks_in_client_pdf,
      :self_introduction_video_link,
      :show_client_comments,
      :client_comments,    
      :aptitude_test_link,
      :psychometric_test_link,
      family_background_attributes: [
        :id, 
        :father_name,
        :mother_name,
        :husband_name,
        :wife_name,
        :father_job_id,
        :mother_job_id,
        :husband_job_id,
        :wife_job_id,
        :father_phone_number,
        :mother_phone_number,
        :husband_phone_number,
        :wife_phone_number,
        :brother_count,
        :sister_count,
        :daugther_count,
        :son_count
      ],
      work_experiences_attributes: [
        :id,
        :job_type_id,
        :country_id,
        :entering_date,
        :leaving_date,
        :up_to_present,
        :status,
        :company_name,
        :monthly_salary,
        :_destroy
      ],
      education_backgrounds_attributes: [
        :id,  
        :date,
        :university,
        :status,
        :_destroy
      ],
      applicant_files_attributes: [
        :id,
        :document,
        :file_type_id,
        :description,
        :_destroy
      ],
      applicant_academic_abilities_attributes:[
        :id,
        :academic_ability_id,
        :score,
        :_destroy
      ],
      applicant_aptitude_tests_attributes:[
        :id,
        :aptitude_test_id,
        :score,
        :show_in_report,
        :_destroy
      ]
    )
  end
end