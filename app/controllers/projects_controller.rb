class ProjectsController < ApplicationController
  load_and_authorize_resource except: [:view_project_applicants]
  require 'date'
  layout 'cms'
  include CheckLoginModule
  include GetCommonDataModule
  include GetCommonFunctionModule
  include ApplicationHelper
  
  before_action :check_login, :set_data

  # returns Country & JobType records
  def set_data
    get_countries
    get_job_types
  end

  def index
    if session[:role] == 'client'
      @page_title = 'プロジェクトリスト / Projects List'
    else
      @page_title = '管理画面/Projects List & Management'
    end

    @projects = @projects.includes(:job_type, :country).order('created_at DESC')
  end

  def show
    @projects = @projects.order('created_at DESC')
    render :action => "index"
  end

  def edit
    @menu = 'projects'
    @page_title = "Edit Project"
  end

  def send_email_job_order
    begin
      @project = Project.find(params[:project_id])

      users = User.where(id: params[:receiver_ids])
      users.each do |u|
        if u.email
          ProjectMailer.send_job_order_mail(@project, u.email).deliver_now
        end
      end
      
      # ProjectJob.perform_later(@project, params[:receiver_ids]) # activate this when using SideKiq

      @project.last_email_sent = Time.new
      @project.save
      
      render json: {status: 'success', message: "#{@project.title} email Job Order was successfully sent!"}
    rescue => error
      render json: {status: 'error', message: error.message}
    end
  end

  def new
    @page_title = 'Create Project'
  end

  def create
    @page_title = 'Create Project'
    # @project = Project.create(project_params)
    @project.last_updated_by = session[:username]
    if @project.save
      flash.keep[:status] = 'success'
      flash.keep[:notice] = 'New Project was successfully created.'
      redirect_to projects_path(:locale => I18n.locale)
    else
      @page_title = 'Create Project'
      flash.keep[:status] = 'error'
      flash.keep[:notice] = 'Sorry, there was an error saving.'
      render :action => 'new'
    end
  end

  def update
    @project.last_updated_by = session[:username]
    if @project.update_attributes(project_params)   #project_params.reject{|k,v| v.blank?}            
      flash[:status] = 'success'
      flash[:notice] = "#{@project.title} was successfully updated."
      redirect_to projects_path(:locale => I18n.locale)
    else
      @page_title = 'Edit Project'
      flash[:status] = 'error'
      flash[:notice] = 'Sorry, there was an error updating.'
      render :action => 'edit'
    end
  end

  def project_remove_document
    begin
      @project.update_attribute(:document, nil)      

      render json: {status: 'success', message: 'Document was successfully removed.'}
    rescue => error
      render json: {status: 'error', message: "Sorry, #{error.message}"}
    end
  end


  def delete_project
    begin
      @project.destroy
      render json: {status: 'success', message: "Project #{@project.title} was successfully deleted."}
    rescue => error
      render json: {status: 'error', message: error.message}
    end
  end

  def view_project_applicants    
    @menu = 'projects'
    @project = Project.find(params[:id])

    # For default parameters in getting scores in listing
    # get_academic_abilities
    # get_aptitude_tests

    if (!['In-progress', 'Confirmed'].include? @project.try(:status)) && session[:role] == 'client'
      render_404
    end

    @page_title = "#{@project.company_name}"
    if session[:role] == 'client'
      @applicants = filter_not_to_be_deleted(@project.applicants.not_waiting).includes(:country, :religion)
    else
      @applicants = filter_not_to_be_deleted(@project.applicants).includes(:country, :religion) 
    end
    session[:previous_controller] = 'projects'
    session[:previous_project] = @project.id    
  end

  def get_last_sent_mail_date
    render json: {when: check_last_sent(@project.last_email_sent)}
  end

  private
  def project_params
    params.require(:project).permit(:title, :company_name, :partner_id, :client_id, :country_id, :job_type_id, :document, :no_of_needed_applicants, :status, :description, :notes)    
  end
end
