class ProjectJob < ActiveJob::Base
  # set queue name
  queue_as :mailers

  # Sends records to mailers, use (SideKiq) for background job
  # Use deliver_later when you're running Sidekiq
  # Run SideKiq: bundle exec sidekiq --environment development -C config/sidekiq.yml

  def perform(project, receiver_ids)
    begin
      users = User.where(id: receiver_ids)
      users.each do |u|
        ProjectMailer.send_job_order_mail(project, u.email).deliver_now #.deliver_later (used it when you're running Sidekiq)
      end
    rescue => error
      ProjectMailer.error_mail(error.message).deliver_now  #.deliver_later (used it when you're running Sidekiq)
    end
  end
end
