class AcademicAbility < ActiveRecord::Base
  has_many :applicant_academic_abilities, dependent: :destroy
  has_many :applicants, through: :applicant_academic_abilities
  
  # prevents duplication of english name, cannot be blank
  validates :en_name, uniqueness: { case_sensitive: false, message: 'Found duplicate entry.' }
  
  # combines japanese and english name, and return as String
  def combine_jp_and_en_name
    if jp_name.blank? || jp_name.nil? || jp_name == '-'
      "#{en_name}"
    else
      "#{jp_name}/#{en_name}"
    end
  end
end
