class ApplicantProjectHistory < ActiveRecord::Base
  belongs_to :applicant
  belongs_to :project

  # set default ordering to created_at descending order
  default_scope { order('created_at DESC') } 
end
