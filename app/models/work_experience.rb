class WorkExperience < ActiveRecord::Base
  belongs_to :applicant
  belongs_to :country
  belongs_to :job_type
  
  # checks if WorkExperience is up to present, then set leaving_date to nil
  before_validation(on: [:create, :update]) do
    if self.up_to_present
      self.leaving_date = nil
    end
  end
end