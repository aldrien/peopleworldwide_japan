class ApplicantAcademicAbility < ActiveRecord::Base
  belongs_to :applicant
  belongs_to :academic_ability
  
  # validate presence of the following fields, cannot be blank
  validates_presence_of :academic_ability_id, :score

  validates_inclusion_of :score, :in => 1..100, :message => "  can only be 1 to 100, but you set (%{value})"
  # validates :score, :inclusion => { :in => 1..100, :message => "  can only be 1 to 100, but you set (%{value})" }

  # gets score in between two given parameter
  scope :score_between, -> (score_from, score_to) { where('score BETWEEN ? AND ?', score_from, score_to) if score_from.present? && score_to.present? }
  
  # gets score which equals to given parameter
  scope :by_score, -> (score) {where(score: "#{score}")}

  # For Intial Display in Search Results
  # gets AcademicAbility score, if record was found or else gets the first record
  def self.get_academic_ability(id)
    if id.present?
      academic_ability_score = self.find_by(academic_ability_id: id).try(:score)
    else
      get_first = AcademicAbility.first.id
      academic_ability_score = self.find_by(academic_ability_id: get_first).try(:score)
    end
    return academic_ability_score
  end
end
