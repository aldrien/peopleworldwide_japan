class Ability
  # initialize Cancan
  include CanCan::Ability

  # initialize current user based on role
  def initialize(user)
    # new guest user (if not logged in)
    user ||= User.new

    # admin role can manage all resources
    if user.role? :admin
        can :manage, :all
    else
        if user.role? :client
            can :read, Applicant, id: user.applicants
            can :read, Project, client_id: user.id, status: ['In-progress',  'Confirmed']
            # cannot :view_dashboard, :home_controller
            cannot :view_advanced_search, :search_controller            

        elsif user.role? :partner
            can :read, Project, partner_id: user.id
            can :read, Applicant, creator_id: user.id
            can :create, Applicant
            can :update, Applicant, creator_id: user.id
            cannot :destroy, Applicant

            # can [ :index, :project_chart ], :home
            # can :view_dashboard, :home_controller
            can :view_advanced_search, :search_controller
        end
    end
    
    # TO DO
    # - Home Controller
    # - Search Controller
    # Currenty not using the Ability Functions, and uses manual IF Clause for filtering User Roles
    # Having Trouble with NON-Restful Style


    # The first argument to `can` is the action you are giving the user
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on.
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/CanCanCommunity/cancancan/wiki/Defining-Abilities
  end
end
