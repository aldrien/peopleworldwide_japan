class ApplicantAptitudeTest < ActiveRecord::Base
  belongs_to :applicant
  belongs_to :aptitude_test

  # requires aptitude_test_id and score on creation, cannot be blank
  validates_presence_of :aptitude_test_id, :score

  # validates score, should be from 0 to 100 only
  validates_inclusion_of :score, :in => 1..100, :message => "  can only be 1 to 100, but you set (%{value})"
  # validates :score, :inclusion => { :in => 1..100, :message => "  can only be 1 to 100, but you set (%{value})" }
  
  # gets score in between two given parameter
  scope :score_between, -> (score_from, score_to) { where('score BETWEEN ? AND ?', score_from, score_to) if score_from.present? && score_to.present? }

  # gets score which equals to given parameter
  scope :by_score, -> (score) {where(score: "#{score}")}

  # For Intial Display in Search Results
  # gets AptitudeTest score, if record was found or else gets the first record
  def self.get_aptitude_test(id)
    if id.present?
      aptitude = self.find_by(aptitude_test_id: id)
    else
      get_first = AptitudeTest.first.id
      aptitude = self.find_by(aptitude_test_id: get_first)
    end

    return aptitude.try(:score)
  end

end
