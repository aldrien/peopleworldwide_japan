class JobType < ActiveRecord::Base
  has_many :projects
  has_many :applicants, through: :projects
  belongs_to :family_background
  belongs_to :work_experience
  validates :en_name, uniqueness: { case_sensitive: false, message: 'Found duplicate entry.' }
  # validates :jp_name, uniqueness: { case_sensitive: false, message: "Found duplicate entry." }

  # combines japanese and english name, and return as String
  def combine_jp_and_en_name
    "#{jp_name}/#{en_name}"
    # or direct method in view lambda { |r| "#{r.jp_name} | #{r.en_name}" }
  end
  
  # combines english and japanese name, and return as String
  def combine_en_and_jp_name
    "#{en_name}/#{jp_name}"
  end
end
