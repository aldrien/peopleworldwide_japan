# Email format validator class
class EmailValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    unless value =~ /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i
      record.errors[attribute] << (options[:message] || "is invalid format")
    end
  end
end

class User < ActiveRecord::Base
  require 'iconv'
  has_many :client_projects, class_name: 'Project', :foreign_key => 'client_id'
  has_many :partner_projects, class_name: 'Project', :foreign_key => 'partner_id'
  has_many :applicants, through: :client_projects
  has_many :partner_created_applicants, class_name: 'Applicant', foreign_key: :creator_id
  belongs_to :pww_logo

  # get Admins user
  scope :admins, -> { where(role: 'admin').order('fullname asc') }
  # get Partners user
  scope :partners, -> { where(role: 'partner').order('fullname asc') }
  # get Clients user
  scope :clients, -> { where(role: 'client').order('fullname asc') }

  # calls set_downcase_email & set_downcase_role method before save
  before_save :set_downcase_email, :set_downcase_role
  
  validates_presence_of :fullname, :username, :pww_logo_id
  validates :fullname, uniqueness: { case_sensitive: false }
  validates :username, uniqueness: { case_sensitive: false }
  validates :password, :length => { minimum: 6, message: 'is too short.' }, on: :create
  validates :email, presence: true, email: true

  has_secure_password

  Roles = [ :admin , :client, :partner ]

  def role?( requested_role )
    self.role == requested_role.to_s
  end

  # set email to lower case before save
  def set_downcase_email
    self.email = email.downcase if email.present?    
  end

  # set user role to lower case before save
  def set_downcase_role
    self.role = role.downcase if role.present?
  end

  def admin_pww_logo
    self.pww_logo_id
  end
     
  has_attached_file :image, :styles => {mini: '30x30', thumb: '80x80>' # is for cropping
  },
  :url  => "/images/users/:id/:style/:basename.:extension",
  :path => ":rails_root/public/images/users/:id/:style/:basename.:extension",
  :default_url => '/assets/default_avatar.jpg',
  :default_style => :thumb
  
  # validates attachment file, should be an image
  validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/
  
  # validates_attachment :avatar, :presence => true,
  # :content_type => { :content_type => "image/jpeg" },
  # :size => { :in => 0..10.kilobytes }
  
  before_post_process :transliterate_file_name
  
  def transliterate_file_name
    extension = File.extname(image.original_filename).gsub(/^\.+/, '')
    filename = image.original_filename.gsub(/\.#{extension}$/, '')
    self.image.instance_write(:file_name, "#{transliterate(filename)}.#{transliterate(extension)}")
  end
  
  def transliterate(str)
    s = Iconv.iconv('ascii//ignore//translit', 'utf-8', str).to_s
    s.downcase!
    s.gsub!(/'/, '')
    s.gsub!(/[^A-Za-z0-9]+/, ' ')
    s.strip!
    s.gsub!(/\ +/, '-')
    return s
  end
end