class FamilyBackground < ActiveRecord::Base
  belongs_to :applicant
  
  belongs_to :father_occupation, :class_name => 'JobType', :foreign_key => :father_job_id
  belongs_to :mother_occupation, :class_name => 'JobType', :foreign_key => :mother_job_id
  belongs_to :husband_occupation, :class_name => 'JobType', :foreign_key => :husband_job_id
  belongs_to :wife_occupation, :class_name => 'JobType', :foreign_key => :wife_job_id
end
