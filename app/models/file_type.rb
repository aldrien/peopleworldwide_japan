class FileType < ActiveRecord::Base
  belongs_to :applicant
  has_many :applicant_files

  # prevents duplication of english name
  validates :en_name, uniqueness: { case_sensitive: false, message: 'Found duplicate entry.' }
  
  # combines japanese and english name, and return as String
  def combine_jp_and_en_name
    "#{jp_name}/#{en_name}"
    # or direct method in view lambda { |r| "#{r.jp_name} | #{r.en_name}" }
  end
end
