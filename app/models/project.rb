class Project < ActiveRecord::Base
  include GetCommonDataModule
  has_many :applicants
  has_many :applicant_project_history, dependent: :destroy
  belongs_to :partner, :class_name => 'User', :foreign_key => :partner_id
  belongs_to :client, :class_name => 'User', :foreign_key => :client_id
  # belongs_to :user

  belongs_to :country
  belongs_to :job_type

  scope :by_client, -> (client_id) {where(client_id: client_id)}
  scope :by_partner, -> (partner_id) {where(partner_id: partner_id)}
  scope :by_partner_or_client, -> (user_id) { where('client_id = ? OR partner_id = ?', user_id, user_id) }
  scope :not_failed, -> { where.not(status: 'Failed') }
  scope :in_progress, -> { where(status: 'In-progress') }
  scope :waiting, -> { where(status: 'Waiting') }
  scope :failed, -> { where(status: 'Failed') }
  scope :done, -> { where(status: 'Done') }

  # allows the system to uplaod file, under document prefix
  has_attached_file :document,
  :url  => "/documents/projects/:id/:basename.:extension",
  :path => ":rails_root/public/documents/projects/:id/:basename.:extension"

  # validates presence of the following fields
  validates_presence_of :title, :description, :partner_id, :client_id, :country_id, :job_type_id, :status, :no_of_needed_applicants

  # validates file type before uplaoding
  validates_attachment :document, 
    :content_type => {
    :content_type => %w(image/jpeg image/jpg image/png application/pdf application/msword application/vnd.openxmlformats-officedocument.wordprocessingml.document)
  }

  # this is for selection purposes, combines Project's title and job type
  def select_project_with_description
    "#{title}  |  #{self.job_type.try(:en_name)}"
    # or direct method in view lambda { |r| "#{r.title} | #{r.description}" }
  end

  # counts Project's associated applicants
  def count_associated_applicants(role)
    if role == 'client'
      self.applicants.not_waiting.has_creator.not_to_be_deleted(get_all_requested_to_be_deleted_applicant.pluck(:applicant_id)).count
    else
      self.applicants.not_to_be_deleted(get_all_requested_to_be_deleted_applicant.pluck(:applicant_id)).count
    end
  end

  # for Advanced Searching, either by client or partner
  def self.get_by_client_or_partner(client_id, partner_id)    
    clients = Project.by_client(client_id).group(:id).pluck(:id).flatten
    partners = Project.by_partner(partner_id).group(:id).pluck(:id).flatten
    
    project_ids = (clients<<partners).flatten.uniq
  end
end
