class RequestedToBeDeletedApplicant < ActiveRecord::Base
  belongs_to :applicant
  belongs_to :partner, class_name: 'User', foreign_key: 'partner_id'

  # prevents duplication of applicant_id
  validates_uniqueness_of :applicant_id
end
