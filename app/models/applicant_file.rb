class ApplicantFile < ActiveRecord::Base
  belongs_to :applicant
  belongs_to :file_type

  # validates presence of file_type, cannot be blank
  validates_presence_of :file_type_id

  # validates attachment size
  validate :check_file_size

  has_attached_file :document,
  :url  => "/documents/applicant_files/:id/:basename.:extension",
  :path => ":rails_root/public/documents/applicant_files/:id/:basename.:extension"

  # validates attachment type
  validates_attachment :document, 
    :content_type => {
    :content_type => %w(image/jpeg image/jpg image/png application/pdf application/msword application/vnd.openxmlformats-officedocument.wordprocessingml.document)
  }

  # 3 megabytes
  NUM_BYTES_IN_MEGABYTE = 3145728
  
  # validates attachment file size
  def check_file_size
    if (document.size.to_f / NUM_BYTES_IN_MEGABYTE) > 1
      errors.add(:file, 'File size cannot be over 3 megabyte.')
    end
  end
end

# UPLOADING DOCUMENT AND CONVERT TO BINARY
# http://ryan.endacott.me/2014/06/10/rails-file-upload.html
# Migration File Contents
# t.string :filename
# t.string :content_type
# t.binary :file_contents

# def initialize(params = {})
#   file = params.delete(:file)
#   super
#   if file
#     self.filename = sanitize_filename(file.original_filename)
#     self.content_type = file.content_type
#     self.file_contents = file.read
#   end
# end

# private
# def sanitize_filename(filename)
#   return File.basename(filename)
# end