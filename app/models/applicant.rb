class Applicant < ActiveRecord::Base
  # includes Filterable module, under concerns directory
  include Filterable

  include ApplicationHelper
  include GetCommonDataModule

  belongs_to :user
  belongs_to :job_type
  belongs_to :country
  belongs_to :religion
  belongs_to :project
  has_one :requested_to_be_deleted_applicant, dependent: :destroy
  has_many :academic_abilities
  belongs_to :creator, class_name: 'User', foreign_key: 'creator_id'
  
  has_one :family_background, dependent: :destroy
  has_many :work_experiences, dependent: :destroy
  has_many :education_backgrounds, dependent: :destroy
  has_many :applicant_files, dependent: :destroy
  has_many :applicant_academic_abilities, dependent: :destroy
  has_many :applicant_aptitude_tests, dependent: :destroy
  has_many :applicant_project_histories, dependent: :destroy

  # set default record sorting/ordering, by name ascending
  default_scope { order('name ASC') }

  # override the default record sorting/ordering, by custom_code ascending
  scope :sort_asc_by_code, -> { reorder(custom_code: :asc) }

  # getting Today's created applicant record for Dashboard
  scope :today, -> { where('created_at >= ?', Time.zone.now.beginning_of_day) }

  # get Applicants by given status
  scope :in_progress, -> { where(status: 'In-progress') }
  scope :waiting, -> { where(status: 'Waiting') }
  scope :failed, -> { where(status: 'Failed') }
  scope :confirmed, -> { where(status: 'Confirmed') }
  scope :not_waiting, -> {where.not(status: 'Waiting')}

  # Not Included in Requested to be deleted applicant records
  scope :not_to_be_deleted, -> (applicant_ids) { where.not(id: applicant_ids) }

  # For filtering Applicants, shows ONLY for associated user (Partner or Client) account.
  scope :has_creator, -> { where.not(creator_id: nil) }
  scope :self_created, -> (user_id) { where(creator_id: user_id) }
  scope :linked_via_project, -> (user_project_ids) { where("project_id IN (?)", user_project_ids).has_creator }

  #Search Filters
  scope :applicant_name, -> (applicant_name) { where('lower(name) LIKE ?', "%#{applicant_name.downcase}%") }
  scope :age_between, -> (age_from, age_to) { where('age BETWEEN ? AND ?', age_from, age_to) }
  scope :exact_age, -> (age_from) { where(age: age_from) }
  scope :english_skill, -> (english_skill) { where(english_skill: english_skill) }
  scope :is_work_related_expirience, -> (is_work_related_expirience) { where(is_work_related_expirience: is_work_related_expirience) }
  scope :status, -> (status) { where(status: status) }
  scope :country, -> (country) { where(country: country) }  
  scope :jlpt_level, -> (jlpt_level) { where(jlpt_level: jlpt_level) }  
  scope :marital_status, -> (marital_status) { where(marital_status: marital_status) }
  scope :gender, -> (gender) {where(gender: gender)}
  scope :last_academic, -> (last_academic) {where(last_academic: last_academic)}
  scope :religion, -> (religion) {where(religion_id: religion)}
  scope :project, -> (project) {where(project_id: project)}
  
  before_validation :clean_data

  # only runs when project_id was changed or currently blank
  after_save :create_history, :if => :project_id_changed? # :if => :new_record?

  # trim whitespace from beginning and end of string attributes
  def clean_data    
    attribute_names().each do |name|
      if self.send(name.to_sym).respond_to?(:strip)
        self.send("#{name}=".to_sym, self.send(name).strip)
      end
    end
  end
  
  # creates applciant project history, only if project_is was changed or updated
  def create_history    
    self.applicant_project_histories.create(project_id: self.project_id, notes: "#{self.notes.blank? ? 'No notes.' : self.notes}", last_updated_by: self.last_updated_by) if self.project_id
  end

  # get applicant academic ability score, based on given test name
  def applicant_academic_ability_score(what_test)
    begin
      acad = AcademicAbility.where("LOWER(en_name) = ?", what_test.downcase).first      
      "#{acad.applicant_academic_abilities.where(applicant_id: self.id).first.score}/100"      
    rescue => err
      # err.message
      "-"
    end
  end

  # get applicant aptitude test score by given aptitude_id
  def get_aptitude_score(aptitude_id)
    begin
      apt = self.applicant_aptitude_tests.where(aptitude_test_id: aptitude_id).first
      apt.nil? ? '-' : "#{apt.score}/100"
    rescue => err
      err.message
      # "-"
    end
  end  

  # Reference urls:
  # - http://railscasts.com/episodes/197-nested-model-form-part-2?autoplay=true
  # - https://github.com/plataformatec/simple_form/wiki/Nested-Models

  accepts_nested_attributes_for :education_backgrounds, :reject_if =>  :reject_condition, :allow_destroy => true
  accepts_nested_attributes_for :work_experiences, :reject_if =>  :reject_condition, :allow_destroy => true
  accepts_nested_attributes_for :family_background
  accepts_nested_attributes_for :applicant_files, :reject_if =>  :reject_condition, :allow_destroy => true
  accepts_nested_attributes_for :applicant_academic_abilities, :reject_if =>  :reject_condition, :allow_destroy => true
  accepts_nested_attributes_for :applicant_aptitude_tests, :reject_if =>  :reject_condition, :allow_destroy => true

  # validates deleted records and allows blank record
  def reject_condition(attributes)
    exists = attributes['id'].present?
    # empty = attributes.slice(:field_name, :field_name).values.all?(&:blank)
    empty = attributes.except(:id).values.all?(&:blank?)
    attributes.merge!({:_destroy => 1}) if exists and empty # destroy empty
    return (!exists and empty) # reject empty attributes
  end

  # validates presence of the following fields
  validates_presence_of :name,
                        :status,                        
                        :address,
                        :gender,
                        :birth_date,
                        :height,
                        :weight,
                        :age,
                        :country_id,
                        :marital_status,
                        :last_academic,
                        :religion_id,
                        :dominant_hand,
                        :english_skill,
                        :jlpt_level,
                        :last_updated_by
  
  # prevents duplication of name & email on create
  validates_uniqueness_of :name, :email, on: :create
  # validates length of short notes
  validates_length_of :short_notes, :maximum => 25
  # validates length of custom code
  validates_length_of :custom_code, is: 6, message: 'should be 6 characters.', allow_blank: true #, :maximum => 6
  # validates_attachment_size :profile, :less_than => 2.megabytes

  # allows the system to uplaod file, under profile prefix
  has_attached_file :profile, :styles => { 
      :thumb => '80x80>',
      :medium => "150x150>" # is for cropping
    },
    :url  => "/images/applicants/:id/profile/:style/:basename.:extension",
    :path => ":rails_root/public/images/applicants/:id/profile/:style/:basename.:extension",
    :default_url => '/assets/default_avatar.jpg',
    :default_style => :thumb
  

  # allows the system to uplaod file, under wholebody prefix
  has_attached_file :wholebody, :styles => { 
      :thumb => '80x80>',
      :medium => "150x150>" # is for cropping
    },
    :url  => "/images/applicants/:id//wholebody/:style/:basename.:extension",
    :path => ":rails_root/public/images/applicants/:id/wholebody/:style/:basename.:extension",
    :default_url => '/assets/default_avatar.jpg',
    :default_style => :thumb

  # validates profile and wholebody file type, before uplaoding
  validates_attachment :profile, :wholebody, 
    :content_type => {
    :content_type => %w(image/jpeg image/jpg image/png)
  }

  # Reference urls: 
  # https://gorails.com/episodes/export-to-csv
  # http://railscasts.com/episodes/362-exporting-csv-and-excel?view=asciicast
  
  # use to generate CSV file
  def self.to_csv  
    attributes = %w{name email address phone country}
    # CSV.generate(headers: true) do |csv|      
    #   csv << attributes
    #   all.each do |applicant|
    #     csv << applicant.attributes.values_at(*attributes)
    #   end
    # end
    CSV.generate(headers: true) do |csv|      
      csv << attributes
      all.each do |applicant|        
        row = applicant.attributes.values_at(*attributes)
        row << Country.find(applicant.country_id).en_name
        row = row.reject(&:blank?)
        csv << row      
      end      
    end
  end
end