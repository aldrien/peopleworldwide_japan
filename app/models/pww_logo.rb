class PwwLogo < ActiveRecord::Base
  has_many :users
  scope :active, -> { where(status: true) }

  # validates presence of the following, cannot be blank
  validates_presence_of :title, :content, :logo

  # allows the system to uplaod file, under logo prefix
  has_attached_file :logo, :styles => {:medium => "300x150>"}, # is for cropping
    :url  => "/images/pww_logo/:id/:style/:basename.:extension",
    :path => ":rails_root/public/images/pww_logo/:id/:style/:basename.:extension",
    :default_url => '/assets/default_avatar.jpg',
    :default_style => :medium
  
  # validates attachment, should be an image only
  validates_attachment_content_type :logo, :content_type => /\Aimage\/.*\Z/
  before_post_process :transliterate_file_name
  
  # validates file name for readable name
  def transliterate_file_name
    extension = File.extname(logo.original_filename).gsub(/^\.+/, '')
    filename = logo.original_filename.gsub(/\.#{extension}$/, '')
    self.logo.instance_write(:file_name, "#{transliterate(filename)}.#{transliterate(extension)}")
  end
  
  def transliterate(str)
    s = Iconv.iconv('ascii//ignore//translit', 'utf-8', str).to_s
    s.downcase!
    s.gsub!(/'/, '')
    s.gsub!(/[^A-Za-z0-9]+/, ' ')
    s.strip!
    s.gsub!(/\ +/, '-')
    return s
  end
end
