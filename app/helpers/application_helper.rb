require 'date'
require 'action_view'
include ActionView::Helpers::DateHelper
module ApplicationHelper

  # Use for Getting User Image
  def use_gravatar(email, type)
    gravatar_id = Digest::MD5.hexdigest(email.downcase)
    "https://gravatar.com/avatar/#{gravatar_id}.png?s=#{type == 'thumb' ? 50 : 25}"
  end

  # Returns user image based on given type
  def user_profile_image(user, type)
    if user.image_file_name.present?      
      type == 'thumb' ? user.image.url(:thumb) : user.image.url(:mini)
    else      
      use_gravatar(user.email, type)
    end
  end

  # Use for Academic Ability, Skill Check, FileTypes, Countries, Religions and JobTypes for DataTables
  def common_table_rows(id, en_name, jp_name, country_code, last_updated_by, updated_at, delete_url, edit_url)
    data = Array.new
    unless country_code.nil?
      data = [      
        "<a href='#' class='common_page_editable' data-name='en_name' data-url='/en/#{edit_url}' data-pk='#{id}'>#{!en_name.blank? ? en_name : '-'}</a>",
        "<a href='#' class='common_page_editable' data-name='jp_name' data-url='/en/#{edit_url}' data-pk='#{id}'>#{!jp_name.blank? ? jp_name : '-'}</a>",
        "<a href='#' class='common_page_editable' data-name='country_code' data-url='/en/#{edit_url}' data-pk='#{id}'>#{!country_code.blank? ? country_code : '-'}</a>",
        "<label id='last_updated_by_#{id}'>#{last_updated_by}</label>",
        "<label id='last_updated_at_#{id}'>#{updated_at}</label>",
        "<a href='#' class='#{delete_url}' data-id='#{id}' title='Remove #{en_name}'>"+
          "<i class='glyphicon glyphicon-remove font20 remove_added' aria-hidden='true'></i>"+
        "</a>"
      ]
    else
      data = [      
        "<a href='#' class='common_page_editable' data-name='en_name' data-url='/en/#{edit_url}' data-pk='#{id}'>#{!en_name.blank? ? en_name : '-'}</a>",
        "<a href='#' class='common_page_editable' data-name='jp_name' data-url='/en/#{edit_url}' data-pk='#{id}'>#{!jp_name.blank? ? jp_name : '-'}</a>",
        "<label id='last_updated_by_#{id}'>#{last_updated_by}</label>",
        "<label id='last_updated_at_#{id}'>#{updated_at}</label>",
        "<a href='#' class='#{delete_url}' data-id='#{id}' title='Remove #{en_name}'>"+
          "<i class='glyphicon glyphicon-remove font20 remove_added' aria-hidden='true'></i>"+
        "</a>"
      ]
    end
  end

  # Setting File Icons
  def icon_for(type)
      type = type.remove('.')
      '/' + (FAENZA_ICONS_FILES[type] || FAENZA_ICONS_FILES['unknown'])
  end  

  # User for setting selected
  def user_status(status)
    status == 1 ? 'Activate User Access' : 'Deactivate User Access'
  end

  # Applicant Nested Model Attributes Functions
  # Creates associated nested fields
  def link_to_function(name, *args, &block)
     html_options = args.extract_options!.symbolize_keys

     function = block_given? ? update_page(&block) : args[0] || ''
     onclick = "#{"#{html_options[:onclick]}; " if html_options[:onclick]}#{function}; return false;"
     href = html_options[:href] || '#'

     content_tag(:a, name, html_options.merge(:href => href, :onclick => onclick))
  end
  
  # Used in Nested Attributes, remove field/s function
  def link_to_remove_fields(name, f)
    f.hidden_field(:_destroy) + link_to_function(name, "remove_fields(this)")
  end
  
  # Used in Nested Attributes, add field/s function
  def link_to_add_fields(name, f, association)
    new_object = f.object.class.reflect_on_association(association).klass.new
    fields = f.fields_for(association, new_object, :child_index => "new_#{association}") do |builder|
      render(association.to_s.singularize + "_fields", :f => builder)
    end
    link_to_function(name, "add_fields(this, \"#{association}\", \"#{escape_javascript(fields)}\")")
  end

  # Checks nil or blank data and return default string "-"
  def check_nil(data)
    if !data.nil? && !data.blank?
      data
    else
      '-'
    end
  end

  # Used in Applicant's Video Url
  def youtube_embed(youtube_url)
    if youtube_url[/youtu\.be\/([^\?]*)/]
      youtube_id = $1
    else
      # Regex from # http://stackoverflow.com/questions/3452546/javascript-regex-how-to-get-youtube-video-id-from-url/4811367#4811367
      youtube_url[/^.*((v\/)|(embed\/)|(watch\?))\??v?=?([^\&\?]*).*/]
      youtube_id = $5
    end

    %Q{<iframe title="YouTube video player" width="640" height="390" src="http://www.youtube.com/embed/#{ youtube_id }" frameborder="0" allowfullscreen></iframe>}
  end

  # Used in PDF Generation
  def compute_work_time(entering_date, leaving_date)
    begin
      entering_date = entering_date.to_date
      leaving_date = leaving_date.to_date
      distance_of_time_in_words(entering_date, leaving_date)
    rescue
      'invalid date format'
    end
  end

  # Used in Full List PDF Report
  def custom_id(id)
    default_id = '000000' << id.to_s
    default_id.last(6)
  end

  # Used in Projects
  def percent_of(total_needed_applicants, registered_applicant)
    percentage = (registered_applicant.to_f / total_needed_applicants.to_f * 100.0).to_i
    percentage > 100 ? 100 : percentage
  end

  # Checks data if more than or less than 100% and returns proper display
  def project_percentage(val)
    if val.to_i < 100
      "#{val}% 進行中"
    else
      val.to_i > 100 ? '100% 完了' : "#{val}% 完了"
    end
  end

  # Used in Advanced Search PDF Full Report
  def helper_dominant_hand(params)
    if params == 'Right'
      data = '右<br>Right'
    elsif params == 'Left'
      data = '左<br>Left'
    else
      data = '両利き<br>Ambidextrous'
    end

    return trimmer(data)
  end

  # Returns gender in jp/en format
  def helper_gender(params)
    if params == 'Male'
      data = '男性<br>Male'
    else
      data = '女性<br>Female'
    end

    return trimmer(data)
  end

  # Returns marital status in jp/en format
  def helper_martial_status(params)
    case params
    when 'Single'
      data = '独身<br>Single'
    when 'Married'
      data = '既婚<br>Married'
    when 'Divorced'
      data = '離婚<br>Divorced'
    when 'Widowed'
      data = '未亡人<br>Widowed'
    end

    return trimmer(data)
  end

  # Returns japanese skill in jp/en format
  def helper_japanese_skill(params)
    case params
    when 'N1'
      data = 'N1'
    when 'N2'
      data = 'N2'
    when 'N3'
      data = 'N3'
    when 'N4'
      data = 'N4'
    when 'N5'
      data = 'N5'
    when 'Conversational'
      data = '日常会話<br>Conversational'
    when 'No'
      data = '無し<br>No'
    end

    return trimmer(data)
  end

  # Returns last academic in jp/en format
  def helper_last_academic(params)
    case params
    when 'Junior college graduated'
      data = '短期大学卒業<br>Junior college graduated'
    when 'Junior school graduate'
      data = '中学卒業<br>Junior school graduate'
    when 'Junior school drop out'
      data = '中学中退<br>Junior school drop out'
    when 'High school graduate'
      data = '高校卒業<br>High school graduate'
    when 'High school drop-out'
      data = '高校中退<br>High school drop out'
    when 'University graduate'
      data = '大学卒業<br>University graduate'
    when 'University drop-out'
      data = '大学中退<br>University drop out'    
    when 'Vocational school graduated'
      data = '専門学校卒業<br>Vocational school graduated'
    when 'Vocational school drop out'
      data = '専門学校中退<br>Vocational school drop out'
    end

    return trimmer(data)
  end

  # Returns education level in jp/en format
  def helper_education_level(params)
    if params == 'Enter'
      data = '入学<br>Enter'
    elsif params == 'Graduate'
      data = '卒業<br>Graduate'
    else
      data = '中退<br>Drop-out'
    end

    return trimmer(data)
  end

  # Returns is work related in jp/en format
  def helper_is_work_related(params)
    params == true ? trimmer('はい<br>Yes') : trimmer('無し<br>No')
  end

  # Returns english skill in jp/en format
  def helper_english_skill(params)
    case params
    when 'Conversational'
      data = '日常会話<br>Conversational'
    when 'Business'
      data = 'ビジネス<br>Business'
    when 'Native'
      data = 'ネイティブ<br>Native'
    when 'No'
      data = '無し<br>No'
    end

    return trimmer(data)
  end

  # Returns progress status in jp/en format
  def helper_progress_status(params)
    case params
    when 'In-progress'
      data = '進行中<br>In-progress'
    when 'Waiting'
      data = '保留<br>Waiting'
    when 'Failed'
      data = 'ボツ<br>Failed'
    when 'Confirmed'
      data = '終了<br>Confirmed'
    when 'Done'
      data = '完了<br>Done'
    end

    return data
  end

  # This function is used for PDF Single Reporting, so it will render data oneliner
  def trimmer(data)    
    if controller_name == 'applicants'
      data = data.gsub '<br>', '/' unless data.nil?
    end

    return data
  end

  # Change slash (/) to break
  def to_break(data)
    begin
      data.gsub '/', '<br>' unless data.nil?
    rescue
      data
    end
  end

  # Used in Single Report PDF Header
  def generate_icon(icon_name)
    case icon_name
    when 'envelope'
      'pdf_icons/envelope.png'
    when 'globe'
      'pdf_icons/globe.png'
    when 'home'
      'pdf_icons/home.png'
    when 'list'
      'pdf_icons/list.png'
    when 'phone'
      'pdf_icons/phone.png'
    when 'video-camera'
      'pdf_icons/video-camera.png'
    end
  end  

  # Used in Single PDF Reports, Showing of Notes
  def check_notes_if_to_show(applicant)
    case session[:role]
    when 'admin'
      applicant.show_notes_in_admin_pdf
    when 'client'
      applicant.show_notes_in_client_pdf
    when 'partner'
      applicant.show_notes_in_partner_pdf
    end
  end

  # Used in Single PDF Reports, Showing of Remarks
  def check_remarks_if_to_show(applicant)
    case session[:role]
    when 'admin'
      applicant.show_remarks_in_admin_pdf
    when 'client'
      applicant.show_remarks_in_client_pdf
    when 'partner'
      applicant.show_remarks_in_partner_pdf
    end
  end

  # Used in Projects
  def check_last_sent(data)
    !data.blank? ? time_ago_in_words(data) : ''
  end

  # Checks previous controller using session for proper redirection of page
  def check_previous_page
    case session[:previous_controller]
    when 'search'
      search_path(:locale => I18n.locale)
    when 'projects'
      view_project_applicants_path(:locale => I18n.locale, id: session[:previous_project])
    when 'applicants_to_be_deleted'
      requests_for_applicant_record_deletion_path(:locale => I18n.locale)
    else
      applicants_path(:locale => I18n.locale)
    end
  end

  # Sum up the family background record count
  def helper_for_siblings_table(siblings)
    siblings.try(:brother_count).to_i + siblings.try(:sister_count).to_i + siblings.try(:daugther_count).to_i + siblings.try(:son_count).to_i
  end

  # Dashboard Notification Panel
  def helper_check_last_record(data)
    !data.last.try(:created_at).nil? ? time_ago_in_words(data.last.try(:created_at)) : '0'
  end

  def helper_default_academic_ability(academic_ability)
    if controller_name == 'search'
      !academic_ability.nil? ? academic_ability : @academic_abilities.first.id
    else
      academic_ability
    end
  end

  def helper_default_aptitude_test(aptitude_test)
    if controller_name == 'search'
      !aptitude_test.nil? ? aptitude_test : @aptitude_tests.first.id
    else
      aptitude_test
    end
  end

  def print_env_on_development
    Rails.env if Rails.env != 'production'
  end
end