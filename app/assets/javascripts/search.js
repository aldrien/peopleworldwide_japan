$(function() {
    var $searchForm = $('form#search_form');
    var $modalReportChooser = $('#modal_report_chooser');

    // Setting Background Color to fields with parameters
    function addRemoveActiveField() {
        $searchForm.find(":text, select").each(function() {
            if ($(this).val() === "")
                $(this).removeClass('with_params');
            else
                $(this).addClass('with_params');
        });
    }
    addRemoveActiveField();

    // Reset Filters
    $('a.reset_filters').on('click', function(e) {
        e.preventDefault();
        $searchForm.find(":text, select").each(function() {
            if ($(this).is('select')) {
                $(this).prop("selectedIndex", 0);
            } else {
                $(this).val('');
            }
        });

        // Reseting Fields Background
        addRemoveActiveField();
    });

    // Selecting All or Not
    $('#applicant_select_all').on('change', function() {
        $('input[name="applicant[]"]').prop('checked', $(this).prop("checked"));
    });

    var get_controller = $('input#controller_name').val();
    var get_project_company = $('input#project_company_name').val();

    function generateIndividualPdfReport(array_ids) {
        var patternUrl = "/en/generate_applicant_report.pdf?id=";

        $.each(array_ids, function(k, v) {
            window.open(patternUrl + v, '_blank');
        });
    }

    function generateReport(type) {
        var allCheckedIds = [];
        var url = '';
        var aptitude_id = $('#aptitude_id').val();
        var report_type = $('select#report_type').val();
        var selected_project = $("select#project").val();

        $('input[name="applicant[]"]:checked').each(function() {
            allCheckedIds.push($(this).val());
        });

        switch (type) {
            case 'pdf':
                url = '/en/generate_pdf_full_report.pdf';
                break;
            case 'excel':
                url = '/en/generate_csv_full_report.xlsx';
                break;
        }

        if (report_type == 'individual' && type == 'pdf') {
            // Open Individual Applicant's PDF Report in new tab
            generateIndividualPdfReport(allCheckedIds);

        } else {
            // Based on Origin
            // if(get_controller == 'projects'){ // no need aptitude test
            //   generatedUrl = url + '?applicants=' + allCheckedIds + '&company=' + get_project_company + '&date=' + new Date();
            // }else if(get_controller == 'search'){
            // generatedUrl = url + '?applicants=' + allCheckedIds + '&skill=' + aptitude_id + '&project=' + selected_project + '&date=' + new Date();
            // }

            if (get_project_company) {
                generatedUrl = url + '?applicants=' + allCheckedIds + '&company=' + get_project_company + '&skill=' + aptitude_id + '&project=' + selected_project + '&date=' + new Date();
            } else {
                generatedUrl = url + '?applicants=' + allCheckedIds + '&skill=' + aptitude_id + '&project=' + selected_project + '&date=' + new Date();
            }

            window.open(generatedUrl, '_blank');
        }
    }

    // Generating PDF based on selected Applicant's ID
    $('a#generate_pdf').on('click', function(e) {
        e.preventDefault();
        var areHeader = $('h2.are_you_sure');
        var areDesription = $('b.are_you_sure_description');

        // if(get_controller == 'projects'){ // no need aptitude test      
        // areHeader.text('Choose what Report Type to Generate!');
        // areDesription.text('The report will be based on the selected type.');
        // $('div#skill_test_selection').remove();

        // }else{ // search controller
        areHeader.text('Choose what Report Type & Skill Test!');
        areDesription.text('The report will be based on the selected type.');
        // }

        $modalReportChooser.modal({
            backdrop: 'static',
            keyboard: false
        });
    });

    // Generate CSV Report based on selected Applicant's ID
    $('a#generate_excel').on('click', function(e) {
        e.preventDefault();
        if ($('input[name="applicant[]"]:checked').length == 0) {
            alert('No Applicant Selected. Please check box to select the Applicant.');
            return;
        }
        generateReport('excel');

    });

    // Generate Button
    $('#generate_report').click(function() {
        generateReport('pdf');
        $modalReportChooser.modal('hide');
    });

    // No Button
    $('#no_cancel').click(function() {
        $modalReportChooser.modal('hide');
    });

    // Collpasing Search Filters
    $('.btn_collapse').on('click', function() {
        if ($(this).children('i').hasClass('glyphicon-chevron-up')) {
            $(this).children('i').attr('class', 'glyphicon glyphicon-chevron-down');
            $('a.reset_filters').css('display', 'none');
        } else {
            $(this).children('i').attr('class', 'glyphicon glyphicon-chevron-up');
            $('a.reset_filters').css('display', 'inline-block');
        }
    });

    // Removing parameters to input fields, return normal input background
    $('input.user_input').on('keyup', function(e) {
        if ($(this).val() === '') {
            $(this).removeClass('with_params');
        }
    });

    // Select Report Type
    $('select#report_type').on('change', function() {
        if ($(this).val() == 'individual') {
            $('div#skill_test_selection').addClass('hidden');
        } else {
            $('div#skill_test_selection').removeClass('hidden');
        }
    });

});
