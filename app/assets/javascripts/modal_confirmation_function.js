$(function(){    
    var $message = '';
    var $data_action = '';
    var $modalConfirmation = $('#modal_confirmation');
    var $requestReason = $('textarea.request_reason');
    var $inputPassword = $('input[name="password"]');
    var $yesBtn = $('#yes_continue');
    var $noBtn = $('.no_cancel_close_modal');
    var $passwordDivMsg = $('div.invalid_pass');
    var $successAlert = $('#success_alert');
    var $successAlertMessage = $('#success_alert_message');
    var $errorAlert = $('#error_alert');
    var $errorAlertMessage = $("#error_alert_message");
    var $spanCurrentCount = $('span.request_of_deletion_count');
    
    var $controllerName = $('input#controller_name').val();
    var $controllerAction = $('input#controller_action').val();

    var $separatedTables = ['delete_academic_ability', 'delete_aptitude_test', 'delete_country', 'delete_file_type', 'delete_job_type', 'delete_religion']; //see ajax_request_dataTable/index
    var $multiTabTables = ['applicants', 'projects', 'users'];
    var setTable = '', $noSort, $setOrderAsc = 0;

    var APPLICANT_COLUMN_PROFILE = 0,
        APPLICANT_SORT_COLUMN_NAME = 1;

    var COMMON_COLUMN_ACTIONS = -1;

    var SEARCH_COLUMN_CHECK_BOX = 0,
        SEARCH_COLUMN_PROFILE = 1,
        SEARCH_COLUMN_INDEX_NAME = 3;

    var PROJECT_COLUMN_FILE = 5,
        PROJECT_COLUMN_TITLE = 0;
        PROJECT_APPLICANTS_CODE = 2;

    var PWW_COLUMN_LOGO = 1,
        PWW_COLUMN_TITLE = 0;

    var USER_COLUMN_PROFILE = 0,
        USER_COLUMN_FULLNAME = 2;

    // Setting NoSort & Order columns    
    if($controllerName == 'applicants'){
      $noSort = [APPLICANT_COLUMN_PROFILE, COMMON_COLUMN_ACTIONS];
      $setOrderAsc = APPLICANT_SORT_COLUMN_NAME;

    }else if($controllerName == 'search'){
      $noSort = [SEARCH_COLUMN_CHECK_BOX,  SEARCH_COLUMN_PROFILE, COMMON_COLUMN_ACTIONS];
      $setOrderAsc = SEARCH_COLUMN_INDEX_NAME;

    }else if($controllerName == 'projects'){
      if($controllerAction == 'index'){
          $noSort = [PROJECT_COLUMN_FILE, COMMON_COLUMN_ACTIONS];
          $setOrderAsc = PROJECT_COLUMN_TITLE;

        }else if($controllerAction == 'view_project_applicants'){
            $noSort = [SEARCH_COLUMN_CHECK_BOX, SEARCH_COLUMN_PROFILE, COMMON_COLUMN_ACTIONS];
          $setOrderAsc = PROJECT_APPLICANTS_CODE;
        }

    }else if($controllerName == 'pww_logos'){
      $noSort = [PWW_COLUMN_LOGO, COMMON_COLUMN_ACTIONS];
      $setOrderAsc = PWW_COLUMN_TITLE;

    }else if($controllerName == 'users'){
      $noSort = [USER_COLUMN_PROFILE, COMMON_COLUMN_ACTIONS];
      $setOrderAsc = USER_COLUMN_FULLNAME;
    }

    // Checking what table to be used
    if($.inArray($controllerName, $multiTabTables) >= 0){
      if($controllerAction == 'view_project_applicants'){ // for Project's Applicant page
        setTable = $('table#makeDataTable');
      }else{
        setTable = $('table.makeDataTable'); // for Users, Projects and Applicants index page
      }
    }else{
      setTable = $('table#makeDataTable'); // for other data tables w/c doesn't used tabs
    }

    // Initialize DataTable
    var $makeDataTable = setTable.DataTable({
      pageLength: 50,
      responsive: true,
      // bLengthChange: false, // hide Showing SELECT-BOX entries
      order: [[ $setOrderAsc, 'asc' ]],
      columnDefs: [{ orderable: false, "targets": $noSort }]
      // aoColumnDefs: [{bSortable: false, aTargets: $noSort}]
    });
      

    // Check DataTable Version
    // alert($.fn.dataTable.version);

    // Setup Modal
    function set_up_modal(element){
      $data_action = $(element).attr('class').split(' ')[0];
      data_id = $(element).attr("data-id");
      
      if(element.attr('class') == 'request_for_delete_applicant'){
        $message = "<p class='modal_descrip'>This action will <b>MAKE DELETE REQUEST</b> of Applicant record to Administrator.</p>";
        $requestReason.val('').fadeIn('fast');
      }else if(element.attr('class') == 'restore_applicant'){
        $message = "<p class='modal_descrip'>This action will <b>RESTORE</b> Applicant's record and will be shown back in the list.</p>";
      }else{
        $message = "<p class='modal_descrip'>This action will <b>PERMANENTLY DELETE</b> the selected record.</p>";
      }

      $('#modal_message_content').html($message);
      $yesBtn.attr("data-action", $data_action);
      $yesBtn.attr("data-id", data_id); 
      $yesBtn.css({"background-image":"none", "cursor":"pointer"});
      $noBtn.show();
      $passwordDivMsg.text('').css('display', 'none');

      $modalConfirmation.modal({
        backdrop: 'static',
        keyboard: false
      }).on('shown.bs.modal', function () {
        $inputPassword.focus();   
      });
    }

    // Delete User Account, Religion, Job Description
    $(document).on('click', 
     '.delete_user_account,\
      .delete_religion,\
      .delete_job_type,\
      .delete_academic_ability,\
      .delete_aptitude_test,\
      .delete_country,\
      .delete_file_type,\
      .delete_project,\
      .delete_job_order,\
      .delete_applicant_file,\
      .project_remove_document,\
      .delete_applicant,\
      .request_for_delete_applicant,\
      .restore_applicant,\
      .delete_pww_logo'
      , function(e){
      e.preventDefault();
      $inputPassword.val('');
      set_up_modal($(this));
    });

    var onSuccessRequest = function(message){
      $errorAlert.fadeOut('fast');
      $successAlertMessage.text(message);
      $successAlert.addClass('alert-success').fadeIn('fast');
    }

    var onErrorRequest = function(message){
      $errorAlert.addClass('alert-danger').fadeIn('fast');
      $errorAlertMessage.text(message);
    }

    // For Updating Upper Links (Delete Request Count & List)
    var updateUpperLinks = function(get_id){
      $('li.request_of_deletion_'+get_id).remove();
      var current_request_count = $spanCurrentCount.text();
      var new_request_count = parseInt(current_request_count) - 1;
      
      // If New Count is == 0, then remove the dropdown list
      if( new_request_count == 0 ){
        $('ul.view_all_deletion_request').remove();
      }
      // Setting New Request Count
      $spanCurrentCount.text(new_request_count);
    }

    // Request Applicant Record Deletion
    var makeRequestDeletion = function(get_id){
      $.ajax({
        type: 'GET',
        url: '/en/request_for_delete_applicant',
        data: {id: get_id, reason: $requestReason.val()}
      }).done(function(data){
        $modalConfirmation.modal('hide');
        
        if (data.status == 'success'){
          $makeDataTable.row($('[data-id="'+get_id+'"]').closest('tr')).remove().draw();
          onSuccessRequest(data.message);

        }else{
          onErrorRequest(data.message);
        }
      }); 
    }

    // I decided to make two AJAX functions instead of one, to separate the authentication & deletetion method.
    // For easy code management and less styling modification.
    // Feel free to modify if you want. It's easy.

    // Deleting Record
    var deleteRecordNow = function(get_action_url, get_id){
      $.ajax({
        type: 'GET',
        url: '/en/' + get_action_url,
        data: {id: get_id}
      }).done(function(data){   
        if (data.status == 'success'){
          if(get_action_url == 'project_remove_document'){
            $('label.show_main_document').remove();
            $('label.show_icon_document').fadeIn('fadeIn');

          }else{            
            if($.inArray(get_action_url, $separatedTables) >= 0){
                $('[data-id="'+get_id+'"]').closest('tr').remove();
            }else{
                $makeDataTable.row($('[data-id="'+get_id+'"]').closest('tr')).remove().draw();              
            }
          }

          if(get_action_url == 'restore_applicant' || get_action_url == 'delete_applicant'){
            updateUpperLinks(get_id);
          }

          onSuccessRequest(data.message);
          
        }else{
          onErrorRequest(data.message);
        }

        $modalConfirmation.modal('hide');

      }).error(function(data){
        $modalConfirmation.modal('hide');
        $successAlert.fadeOut('fast');
        $errorAlert.fadeIn('fast');
        $errorAlertMessage.text(data.status + " " + data.statusText);
      });
    }

    // Authenticate Admin Before Record Deletion
    var authenticateAdmin = function(){
      $.ajax({
        type: 'POST',
        dataType: 'json',
        beforeSend: function(xhr) {
          xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
        },
        url: '/en/authenticate_user_password',
        data: {password: $inputPassword.val()}
      }).done(function(data){
        if(data.status == 'error'){
          $inputPassword.val('').focus();
          $passwordDivMsg.text(data.message).fadeIn('fast');

        }else{
          $passwordDivMsg.text('').css('display', 'none');
          $yesBtn.addClass('set-loader');
          $noBtn.hide();

          var get_action = $yesBtn.attr("data-action");
          var get_id = $yesBtn.attr("data-id");
          if(get_action == 'request_for_delete_applicant'){
            makeRequestDeletion(get_id);
          }else{        
            deleteRecordNow(get_action, get_id);
          }
        }
      });
    }

    // Yes Button
    $yesBtn.click(function(){ 
      if($inputPassword.val() == ''){
        $inputPassword.focus();
        return;
      }
      if($(this).attr("data-action") == 'request_for_delete_applicant'){
        if($requestReason.val() == ''){
          $requestReason.focus();
          return;
        } 
      } 
      authenticateAdmin();
    });

    // On Enter Key in Password Field (auto-submission)
    $inputPassword.keyup(function (e) {
        if (e.keyCode == 13) {
            $yesBtn.trigger('click');
        }
    });

});