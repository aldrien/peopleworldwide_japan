$(function() {
    var action = $('input#hidden-action').val();
    var dataTableUrl, formUrl;

    switch (action) {
        case 'academic_abilities':
            dataTableUrl = '/en/get_all_academic_abilities';
            formUrl = '/en/add_new_academic_ability';
            break;
        case 'aptitude_tests':
            dataTableUrl = '/en/get_all_aptitude_tests';
            formUrl = '/en/add_new_aptitude_test';
            break;
        case 'countries':
            dataTableUrl = '/en/get_all_countries';
            formUrl = '/en/add_new_country';
            break;
        case 'file_types':
            dataTableUrl = '/en/get_all_file_types';
            formUrl = '/en/add_new_file_type';
            break;
        case 'job_types':
            dataTableUrl = '/en/get_all_job_types';
            formUrl = '/en/add_new_job_type';
            break;
        case 'religions':
            dataTableUrl = '/en/get_all_religions';
            formUrl = '/en/add_new_religion';
            break;
        case 'projects':
            dataTableUrl = '/en/get_all_projects';
            formUrl = '/en/add_new_project';
            break;
    }

    // Loader GIF
    function displayLoader(set) {
        if (set == true) {
            $('div.ajax_loading').css("display", "block");
        } else {
            setTimeout(function() {
                $('div.ajax_loading').fadeOut('fast');
            }, 400);
        }
    }

    // Inline Editable Functions
    function initializeEditables() {
        // https://vitalets.github.io/x-editable/docs.html
        // http://vitalets.github.io/x-editable/docs.html#newrecord
        $(document).on('mouseover', 'a.common_page_editable', function() {
            $(this).editable({
                type: 'text',
                ajaxOptions: {
                    type: 'post',
                    dataType: 'json',
                    beforeSend: function(xhr) {
                        xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
                    }
                },
                clear: true,
                success: function(response, newValue) {
                    if (response.status == 'error') { //return response.message;
                        // Error Notification
                        $("#error_alert_message").text(response.message);
                        $("#error_alert").css({ "display": "block" });
                        $("#success_alert").css({ "display": "none" });
                    } else {
                        // Success Notification
                        $("#success_alert_message").text(response.message);
                        $("#success_alert").css({ "display": "block" });
                        $("#error_alert").css({ "display": "none" });

                        // Updating Values on Table
                        $('label#last_updated_by_' + $(this).data('pk')).text(response.updated_by);
                        $('label#last_updated_at_' + $(this).data('pk')).text(response.updated_at);
                    }
                }
            });
        });
    }


    displayLoader(true);

    // Data Table Gets Records
    $fillDataTable = $('#dataTables-' + action).DataTable({
        responsive: true,
        pageLength: 10,
        order: [
            [0, 'asc']
        ],
        columnDefs: [{
            targets: 4,
            orderable: false
        }],
        ajax: dataTableUrl,
        dataSrc: 'data',
        fnInitComplete: function(oSettings, json) {
            displayLoader(false);
            initializeEditables();
        }
    });

    // Form On Submit
    $('form#' + action).on('submit', function(e) {
        e.preventDefault();

        $.ajax({
            type: 'GET',
            url: formUrl,
            data: $(this).serialize()
        }).done(function(data) {
            if (data.status == 'success') {
                $('#error_alert').fadeOut('fast');
                $('#success_alert_message').text(data.message);
                $('#success_alert').addClass('alert-success').fadeIn('fast');
            } else {
                $('#error_alert').addClass('alert-error').fadeIn('fast');
                $("#error_alert_message").text(data.message);
            }

            $fillDataTable.ajax.reload();

        }).error(function(data) {
            $('#success_alert').fadeOut('fast');
            $('#error_alert').fadeIn('fast');
            $("#error_alert_message").text(data.status + " " + data.statusText);
        });

        // Clear Form
        $(this).find("input[type=text], textarea").val("");
    });

});
