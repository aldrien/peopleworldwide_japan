$(function() {
    var showCommentsModal = $('div#modal_make_comments');
    var clientPassword = $('input#client_password');
    var commentsTextArea = $('textarea#client_comments');
    var isShowCheckBox = $('input#show_client_comments_in_pdf');
    var invalidResult = $('div.invalid_result');
    var applicantID = '',
        isShowInPdf;

    var isShowCommentToPdf = function(current_value) {
        if (current_value == 'true') {
            isShowCheckBox.prop('checked', true);
            isShowInPdf = true
        } else {
            isShowCheckBox.prop('checked', false);
            isShowInPdf = false
        }
    }

    isShowCheckBox.on('change', function() {
        if ($(this).is(":checked")) {
            isShowInPdf = true
        } else {
            isShowInPdf = false
        }
    });

    var onSuccessRequest = function(message) {
        $('#success_alert_message').text(message);
        $('#success_alert').addClass('alert-success').fadeIn('fast');
    }

    var resetFields = function() {
        clientPassword.val('');
        commentsTextArea.val('');
        isShowCheckBox.prop('checked', false);
    }

    // OnClick icon comment
    $('a.make_comments').on('click', function(e) {
        e.preventDefault();
        $('h2#render_applicant_name').text($(this).data('name') + ' へのコメント作成')
        commentsTextArea.val($(this).attr('data-comment'));
        applicantID = $(this).attr('id');
        invalidResult.text('').fadeOut('fast');
        isShowCommentToPdf($(this).attr('data-isShow'));
        clientPassword.val('');

        showCommentsModal.modal({
            backdrop: 'static',
            keyboard: false
        }).on('shown.bs.modal', function() {
            clientPassword.focus();
        });
    });

    // Click Save Comment button
    $('button#save_comments').on('click', function() {
        if (!clientPassword.val() || !commentsTextArea.val()) {
            clientPassword.focus();
            invalidResult.text('パスワード入力 & コメント入力').fadeIn('fast');
            return false;

        } else {
            $.ajax({
                type: 'GET',
                url: '/en/make_comments_to_applicant',
                data: {
                    password: clientPassword.val(),
                    applicant_id: applicantID,
                    comments: commentsTextArea.val(),
                    is_show_to_pdf: isShowInPdf
                }
            }).done(function(data) {
                if (data.status == 'success') {
                    $('a#' + applicantID)
                        .attr('data-comment', commentsTextArea.val())
                        .attr('data-isShow', isShowInPdf);

                    resetFields();

                    onSuccessRequest(data.message);
                    showCommentsModal.modal('hide');
                } else {
                    invalidResult.text(data.message).fadeIn('fast');
                    clientPassword.focus();
                }

            });
        }
    });

    // Close/Hide Modal
    $('img.modal_close_btn').on('click', function() {
        showCommentsModal.modal('hide');
    });

});
