$(window).on('load', function() {
    // This checks if some update happens and showing alert before leaving the page!
    // var form_original_data = $("form.edit_applicant_form").serialize();
    // window.onbeforeunload = function(){  
    //     var msg = 'You haven\'t saved your changes.';        
    //     if ( ($("form.edit_applicant_form").serialize() != form_original_data) ) {
    //         if (!$("form.edit_applicant_form:submit")){ //not working here
    //             return msg;
    //         }            
    //     }
    // }; 
    // End


    var checkStatus = $('div#modal_edit_status').data('edit-status');
    var applicantID = $('input#hidden_applicant_id').val();
    var sessionUsername = $('input#hidden_username').val();
    var lastUpdatedBy = $('input#hidden_last_updated_by').val();

    // Used in window unload function
    localStorage.setItem('sessionUsername', sessionUsername);
    localStorage.setItem('lastUpdatedBy', lastUpdatedBy);

    // Update Edit Status for locked function
    function updateLockStatus(status) {
        $.ajax({
            type: 'GET',
            url: '/en/lock_function',
            // async: false,
            data: { id: applicantID, edit_status: status }
        }).done(function(data) {
            if (data.status == 'success') {
                $('#error_alert').fadeOut('fast');
                $('#success_alert_message').text(data.message);
                $('#success_alert').addClass('alert-success').fadeIn('fast');
            } else if (data.status == 'error') {
                $('#error_alert').addClass('alert-danger').fadeIn('fast');
                $("#error_alert_message").text(data.message);
            }
        });
    }

    // When User Leave the Edit Applicant's Page, it will update the edit_status to false (so other user can edit)
    // Not wroking when logout (another function in logout, see controller!)
    $(window).on('beforeunload', function() {
        // return "You're about to leave the page, Locked-Function will be disabled if you leave.";    
        if (localStorage.getItem('sessionUsername') == localStorage.getItem('lastUpdatedBy')) {
            updateLockStatus(false);
        }
    });

    // Checks if Locked Function was activated, show modal
    if (checkStatus == true && (sessionUsername != lastUpdatedBy)) {
        $('#modal_edit_status').modal({
            backdrop: 'static',
            keyboard: false
        });
    } else {
        updateLockStatus(true);
    }

    // Proceed Anyway
    $('#proceed_anyway').on('click', function(e) {
        e.preventDefault();
        updateLockStatus(true);
        $('#modal_edit_status').modal('hide');
    })

});
