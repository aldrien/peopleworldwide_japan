$(function() {
    $(document).on('click', '.generate_pdf', function(e) {
        e.preventDefault();
        $('#generate_report').data('id', $(this).data('id'));

        $('#modal_show_notes_and_remarks').modal({
            backdrop: 'static',
            keyboard: false
        });
    });

    // Generate Button
    $('#generate_report').click(function() {
        var id = $(this).data('id');
        var show_admin_remarks = $('input[name="admin_remarks"').is(":checked");
        var show_notes = $('input[name="notes"').is(":checked");
        var url = '/en/generate_applicant_report.pdf?id=' + id + '&show_notes=' + show_notes + '&show_admin_remarks=' + show_admin_remarks;

        $('#modal_show_notes_and_remarks').modal('hide');
        window.open(url, '_blank');
    });

    // No Button
    $('#no_cancel').click(function() {
        $('#modal_show_notes_and_remarks').modal('hide');
    });

});


// <a href="#" class="generate_pdf" data-id="<%=a.id%>" data-toggle="tooltip" title="Download PDF"><%= image_tag("/assets/icons/pdf.png", class: 'applicants_link') %></a>&nbsp;

//<!-- Modal for Selecting whether to show Notes & Admin Remarks -->
// <div class="modal fade" id="modal_show_notes_and_remarks" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
//   <div class="modal-dialog">
//     <div class="modal-content text-center" style="margin-top: 140px;">      
//       <div class="fade in margin_btm30" role="alert">
//         <h2 class="are_you_sure">Choose if to show in report!</h2>        
//         <div class="text-center"><b>This selection is whether to display or not the notes & remarks.</b></div>
//         <div style="width: 100%; margin-top: 30px; margin-bottom: 30px;" class="text-center">
//           <div class="text-left" style="margin: 0 auto 0 auto; width: 300px; padding-left: 45px;">            
//             <label><input type="checkbox" name="admin_remarks" value="true" checked=""> Show Admin Remarks</label>
//             <br>
//             <label><input type="checkbox" name="notes" value="true" checked=""> Show Applicant's Notes</label>
//           </div>
//           <br>
//         </div>
//         <button id="generate_report" data-id="" type="button" class="btn btn-lg btn-danger modal_btn_size"><%=t :generate_report %></button>
//         &nbsp;&nbsp;&nbsp;
//         <button id="no_cancel" type="button" class="btn btn-lg btn-warning modal_btn_size"><%=t :no_cancel %></button>        
//       </div>
//     </div>
//   </div>
// </div>
