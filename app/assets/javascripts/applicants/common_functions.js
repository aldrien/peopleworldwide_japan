$(function() {

    // Preventing Form To Submit on Enter Key Press
    $(document).on('keypress', function(e) {
        if (e.which == 13) {
            e.preventDefault();
            return false;
        }
    });

    // Function for selecting Applicant's Aptitude test    
    $(document).on('change', '.check_show_in_report', function() {
        if (this.checked) {
            $('.check_show_in_report').prop('checked', false).prop('disabled', 'disabled');
            $(this).removeAttr("disabled").prop('checked', true);
        } else {
            $('.check_show_in_report').removeAttr("disabled");
        }
    });

    var todayDate = moment().format();

    var computeAge = function(birth_date) {
        var DOB = new Date(birth_date);
        var today = new Date();
        var age = today.getTime() - DOB.getTime();
        age = Math.floor(age / (1000 * 60 * 60 * 24 * 365.25));
        return age;
    }


    function intializeBirthDatePicker() {
        $('#birth_date').datetimepicker({
            viewMode: 'years',
            format: 'YYYY/MM/DD',
            defaultDate: moment('1990/01/01', 'YYYY/MM/DD'), //moment(todayDate).days(0).months(0).year(0),
            showClear: true,
            useCurrent: false,
            maxDate: moment(todayDate) //.format('ddd DD MMM YYYY HH:mm:ss ZZ')
        }).on('dp.change', function(e) {
            var age = computeAge($(this).data('date'));
            $('input#your_age').val(age);
            $('#applicant_age').val(age);
        });
    }

    function intializeYearMonthPicker() {
        $('.date_year_month').datetimepicker({
            viewMode: 'years',
            format: 'YYYY/MM',
            showClear: true,
            maxDate: moment(todayDate)
        });
    }


    intializeBirthDatePicker();
    intializeYearMonthPicker();

    // Spouse Seelction (husband or Wife)
    var showHideHusbandOrWife = function(data) {
        if (data == 'husband') {
            $('.wife_field').addClass('hidden').removeClass('show');
            $('.husband_field').addClass('show').removeClass('hidden');
        } else {
            $('.husband_field').addClass('hidden').removeClass('show');
            $('.wife_field').addClass('show').removeClass('hidden');
        }
    }

    // Setting Option to be checked
    var checkUncheckOption = function(data) {
        if (data == 'wife') {
            $('input[name="spouse_selection"][value="wife"]').prop('checked', true);
        } else {
            $('input[name="spouse_selection"][value="husband"]').prop('checked', true);
        }
    }

    // Option on Change function
    $('input[name="spouse_selection"]').on('change', function() {
        showHideHusbandOrWife($(this).val());
    });

    // Setting Showed data between Husband & Wife selection
    var get_gender = $('input#hidden_gender_data').val();
    if (get_gender == 'Male') {
        showHideHusbandOrWife('wife');
        checkUncheckOption('wife');
    } else {
        showHideHusbandOrWife('husband');
        checkUncheckOption('husband');
    }

    // Quick To Go Save (scroll down)
    $('a#scroll_to_save').click(function() {
        $('html, body').animate({ scrollTop: $(document).height() }, 'slow');
        return false;
    });

    // Reserved Code for scroll up
    $('.scroll_up').click(function() {
        $('html, body').animate({ scrollTop: 0 }, 'slow');
        return false;
    });

    // Function for selecting Work Experience To Present
    $(document).on('click', 'input.up_to_preset', function() {
        if ($(this).is(':checked')) {
            $(this).closest('td').children('div.date').children('input').prop('disabled', true).val('');
        } else {
            $(this).closest('td').children('div.date').children('input').prop('disabled', false);
        }
    });

});
