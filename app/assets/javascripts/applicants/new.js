// $(function(){
// intializeYearMonthPicker();

// // Dynamic Adding of Education Background
// var $addEducationBackground = $('button#add_education_background');
// var $tableEducationBackground = $('table#table_education_background');
// var $educationBackgroundLastTr;

// $addEducationBackground.on('click', function(){
//   $educationBackgroundLastTr = $tableEducationBackground.find('tr:last');
//   var cloneEducationTr = $('table#table_education_background>tbody tr:first');

//   $educationBackgroundLastTr.after(cloneEducationTr.clone());
//   $tableEducationBackground.find('tr:last').find('.disabled_color').removeClass('disabled_color').addClass('remove_added');

//   // Update Names
//   var namePrefix = 'applicant[education_backgrounds_attributes]';
//   var countAllRows = $('.education_date').length - 1;

//   $tableEducationBackground.find('tr:last').find('.education_date').attr('name', namePrefix + '['+countAllRows+']' + '[date]').val('');
//   $tableEducationBackground.find('tr:last').find('.university').attr('name', namePrefix + '['+countAllRows+']' + '[university]').val('');
//   $tableEducationBackground.find('tr:last').find('.status').attr('name', namePrefix + '['+countAllRows+']' + '[status]');

//   intializeYearMonthPicker();
// });

// // Dynamic Adding of Work Experience
// var $addWorkExperience = $('button#add_work_experience');
// var $tableWorkExperience = $('table#table_work_experience');
// var $workExperienceLastTr;

// $addWorkExperience.on('click', function(){
//   $workExperienceLastTr = $tableWorkExperience.find('tr:last');
//   var cloneWorkTr = $('table#table_work_experience>tbody tr:first');    

//   $workExperienceLastTr.after(cloneWorkTr.clone());
//   $tableWorkExperience.find('tr:last').find('.disabled_color').removeClass('disabled_color').addClass('remove_added');

//   // Update Names
//   var namePrefix = 'applicant[work_experiences_attributes]';
//   var countAllRows = $('.work_experience_date').length - 1;

//   $tableWorkExperience.find('tr:last').find('.work_experience_date').attr('name', namePrefix + '['+countAllRows+']' + '[date]').val('');
//   $tableWorkExperience.find('tr:last').find('.company_name').attr('name', namePrefix + '['+countAllRows+']' + '[company_name]').val('');
//   $tableWorkExperience.find('tr:last').find('.status').attr('name', namePrefix + '['+countAllRows+']' + '[status]');
//   $tableWorkExperience.find('tr:last').find('.monthly_salary').attr('name', namePrefix + '['+countAllRows+']' + '[monthly_salary]');
//   $tableWorkExperience.find('tr:last').find('.job_description_id').attr('name', namePrefix + '['+countAllRows+']' + '[job_description_id]');
//   $tableWorkExperience.find('tr:last').find('.country_id').attr('name', namePrefix + '['+countAllRows+']' + '[country_id]');    

//   intializeYearMonthPicker();
// });

// // Remove Recently Added on click event
// $(document).on('click', '.remove_added', function(){
//   $(this).closest('tr').remove();
// });

// });
