$(function() {

    $('#side-menu').metisMenu();
    
});

//Loads the correct sidebar on window load,
//collapses the sidebar on window resize.
// Sets the min-height of #page-wrapper to window size
$(function() {
    $(window).bind("load resize", function() {
        topOffset = 50;
        width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
        if (width < 768) {
            $('div.navbar-collapse').addClass('collapse');
            topOffset = 100; // 2-row-menu
        } else {
            $('div.navbar-collapse').removeClass('collapse');
        }
        
        height = ((this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height) - 1;
        height = height - topOffset;
        if (height < 1) height = 1;
        if (height > topOffset) {
            $("#page-wrapper").css("min-height", (height) + "px");
        }

        // For Non-Admin Menu
        if(this.window.innerWidth < 715){
            $('ul.non_admin_ul li>a').css("margin-right", "0");
        }
    });
    
    // Setting Active Menu by aldrien
    var url = window.location.pathname;
    var element = $('ul.nav a').filter(function(){    
        return $(this).attr('href') == url;
    }).addClass('active_menu').parent().parent().addClass('in').parent().addClass('active');


    var menu = $('input#menu_title').val();
    if(menu){
      $('.nav').find('li[id='+menu+']').addClass("active").children('a').addClass("active_menu");
      $('.nav').find('li[id='+menu+']').children('ul').addClass('collapse in');
    }

    // For Partner & Client Navigation
    var applicant_other_coverage = ['applicants_edit', 'applicants_video_interview', 'applicants_video_self_introduction'];
    var search_other_coverage = ['search_search_applicants', 'search_quick_search'];

    var non_admin_menu = $('input#non_admin_nav').val();
    if(non_admin_menu){
        if(non_admin_menu == 'projects_view_project_applicants'){
            non_admin_menu = 'projects_index'
        }else if($.inArray(non_admin_menu, applicant_other_coverage) >= 0){
            non_admin_menu = 'applicants_index';
        }else if($.inArray(non_admin_menu, search_other_coverage) >= 0){
            non_admin_menu = 'search_index';
        }

        $('ul.non_admin_ul').find('li[id='+non_admin_menu+']').addClass("active")
    }
    
    // Closing Alert Notification
    $('button#success_alert_x, button#error_alert_x').on('click', function(e){
      e.preventDefault();
      $(this).parent().css( "display", "none" );
    });

    // Catch All Broken Image
    var setNewImg;
    $('img').on('error', function() {
        if( $(this).attr('class') == 'height_width50' || $(this).attr('class') == 'profile_small' ){            
            setNewImg = '/assets/unknown.png'
        }else{
            setNewImg = '/assets/broken_image.png'
        }
        $(this).attr({src: setNewImg, class: $(this).attr('class')});
    });


    // No Button for All Modal
    $('.no_cancel_close_modal').click(function(){
        var get_id = $(this).closest('.modal').attr('id');
        $('#'+get_id).modal('hide');
    });    
});
