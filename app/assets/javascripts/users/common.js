$(function() {
    var $pwwLogoHolder = $('div#pww_logo_holder');
    var $roleSelector = $('select#user_role');
    var $pwwLogoSelector = $('select#user_pww_logo_id');

    function setSelectors(param) {
        if (param == 'admin') {
            $pwwLogoHolder.removeClass('hidden');
            $pwwLogoSelector.prop('required', true);
        } else {
            $pwwLogoHolder.addClass('hidden');
            $pwwLogoSelector.removeAttr('required').val('');
        }
    }

    // Onload
    setSelectors($roleSelector.val());

    // OnChange
    $roleSelector.on('change', function() {
        setSelectors($roleSelector.val());
    });
});
