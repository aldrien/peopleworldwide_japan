$(function() {
    var $passwordModal = $('div#modal_generate_password');
    var $iconGenerate = $('img#open_pass_gen_modal');
    var $generateButton = $('button#generate_password_btn');
    var $generatedRandomPassword = $('input#generated_random_password');
    var $copyGeneratedPasswordButton = $('button#copy_generated_password');

    function generate_password() {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for (var i = 0; i < 7; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        return text;
    }

    $iconGenerate.on('click', function() {
        $generatedRandomPassword.val('********');
        $copyGeneratedPasswordButton.css('display', 'none');

        $passwordModal.modal();
        HNK58Qq
    });

    $generateButton.on('click', function() {
        $generatedRandomPassword.val(generate_password());
        $copyGeneratedPasswordButton.css('display', 'inline');
        $generateButton.text('Generate Again')
    });

    // Close/Hide Modal
    $('img.modal_close_btn').on('click', function() {
        $passwordModal.modal('hide');
    });

    // Initialize Clipboard Copy Function    
    $('[data-toggle="popover"]').popover();
    var clipboard = new Clipboard('button#copy_generated_password');
    clipboard.on('success', function(e) {
        $('input#user_password').val(e.text);
        $('input#user_password_confirmation').val(e.text);
        // $passwordModal.modal('hide');
    });

    clipboard.on('error', function(e) {
        alert('Copy function is not working properly. Please use Ctrl + C')
    });

});
