$(function() {
    function getDateofLastSentMail(project_id) {
        $.get("/en/get_last_sent_mail_date", { id: project_id }, function(data) {
            if (data.when != '') {
                msg = '" Last sent mail was ' + data.when + ' ago. "';
                $('p.last_sent_mail').text(msg).css('display', 'block');
            } else {
                $('p.last_sent_mail').text('').css('display', 'none');
            }

        });

    }

    // Send Mail
    $(document).on('click', '.send_mail', function(e) {
        e.preventDefault();
        var project_id = $(this).attr("data-id");

        getDateofLastSentMail(project_id);

        $('#modal_email_chooser').modal({
            backdrop: 'static',
            keyboard: false
        });

        // Passing ID to Button data-id attribute
        $('#send_mail_now').attr("data-id", project_id);

        $('input#client_id').val($(this).attr("data-client").split('|')[0]);
        $('input#partner_id').val($(this).attr("data-partner").split('|')[0]);
        $('label#client_name').text($(this).attr("data-client").split('|')[1]);
        $('label#partner_name').text($(this).attr("data-partner").split('|')[1]);

        $('p.message_here').text('');
        $('div.now_sending_mail').css('display', 'none');
    });

    // Send Mail Now Button

    function sendMailAjax(project_id, receiver_ids) {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            beforeSend: function(xhr) {
                xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
            },
            url: '/en/send_email_job_order',
            data: { project_id: project_id, receiver_ids: receiver_ids }
        }).done(function(data) {
            setTimeout(function() {
                $('#modal_email_chooser').modal('hide');
                if (data.status == 'success') {
                    $('#error_alert').fadeOut('fast');
                    $('#success_alert_message').text(data.message);
                    $('#success_alert').addClass('alert-success').fadeIn('fast');
                } else {
                    $('#error_alert').addClass('alert-danger').fadeIn('fast');
                    $("#error_alert_message").text(data.message);
                }
            }, 3000);

        }).error(function(data) {
            $('#modal_email_chooser').modal('hide');
            $('#success_alert').fadeOut('fast');
            $('#error_alert').fadeIn('fast');
            $("#error_alert_message").text(data.status + " " + data.statusText);
        });
    }

    $('#send_mail_now').click(function() {
        var allCheckedIds = [];
        var project_id = $(this).attr("data-id");

        $('input[name="receiver_id[]"]:checked').each(function() {
            allCheckedIds.push($(this).val());
        });

        if (allCheckedIds.length == 0) {
            $('p.message_here').text('Warning: Please choose email receiver!')
        } else {
            $('div.now_sending_mail').fadeIn('fast');
            sendMailAjax(project_id, allCheckedIds);
        }

    });

});
