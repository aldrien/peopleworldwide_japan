$(function() {

    // Closing Alert Notification
    $('button#success_alert_x, button#error_alert_x').on('click', function(e) {
        e.preventDefault();
        $(this).parent().css("display", "none");
    });

    // Showing of Icon Descriptions
    var ulLegend = $('ul.ul_custom_icon_legend');
    var divLegendHolder = $('div.legend_holder');
    var iLegendIcon = $('i#legend_hover');

    divLegendHolder.on('mouseover click', function() {
        divLegendHolder.css('min-width', '310px');
        ulLegend.show();
    }).on('mouseout', function() {
        divLegendHolder.css('min-width', '0px');
        ulLegend.hide();
    });
});

$(window).load(function() {
    // Auto Hide Success & Error Alert
    function hideAlertNotification(element) {
        setTimeout(function() { element.css('display', 'none') }, 5000);
    }

    var $successAlert = $('div#success_alert');
    var $errorAlert = $('div#error_alert');

    if ($successAlert.css('display') == 'block') { hideAlertNotification($successAlert) }
    if ($errorAlert.css('display') == 'block') { hideAlertNotification($errorAlert) }
});
