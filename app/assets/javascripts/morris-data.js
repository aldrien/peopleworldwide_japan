$(function() {

    // Morris.Area({
    //     element: 'morris-area-chart',
    //     data: [{
    //         period: '2010 Q1',
    //         iphone: 2666,
    //         ipad: null,
    //         itouch: 2647
    //     }, {
    //         period: '2010 Q2',
    //         iphone: 2778,
    //         ipad: 2294,
    //         itouch: 2441
    //     }, {
    //         period: '2010 Q3',
    //         iphone: 4912,
    //         ipad: 1969,
    //         itouch: 2501
    //     }, {
    //         period: '2010 Q4',
    //         iphone: 3767,
    //         ipad: 3597,
    //         itouch: 5689
    //     }, {
    //         period: '2011 Q1',
    //         iphone: 6810,
    //         ipad: 1914,
    //         itouch: 2293
    //     }, {
    //         period: '2011 Q2',
    //         iphone: 5670,
    //         ipad: 4293,
    //         itouch: 1881
    //     }, {
    //         period: '2011 Q3',
    //         iphone: 4820,
    //         ipad: 3795,
    //         itouch: 1588
    //     }, {
    //         period: '2011 Q4',
    //         iphone: 15073,
    //         ipad: 5967,
    //         itouch: 5175
    //     }, {
    //         period: '2012 Q1',
    //         iphone: 10687,
    //         ipad: 4460,
    //         itouch: 2028
    //     }, {
    //         period: '2012 Q2',
    //         iphone: 8432,
    //         ipad: 5713,
    //         itouch: 1791
    //     }],
    //     xkey: 'period',
    //     ykeys: ['iphone', 'ipad', 'itouch'],
    //     labels: ['iPhone', 'iPad', 'iPod Touch'],
    //     pointSize: 2,
    //     hideHover: 'auto',
    //     resize: true
    // });

    // Morris.Donut({
    //     element: 'morris-donut-chart',
    //     data: [{
    //         label: "Download Sales",
    //         value: 12
    //     }, {
    //         label: "In-Store Sales",
    //         value: 30
    //     }, {
    //         label: "Mail-Order Sales",
    //         value: 20
    //     }],
    //     resize: true
    // });

    // Fire off an AJAX request to load the data
    $.ajax({
      type: "GET",
      dataType: 'json',
      url: "/en/project_chart", // This is the URL to the API
    })
    .done(function( data ) {
      // When the response to the AJAX request comes back render the chart with new data
        if ((typeof data === "undefined" || data === null) || data.length === 0) {
            // should disable hover (in-progress)
            // chart.setData(null);
            return;
        }else{
            var chart = Morris.Bar({
                element: 'morris-bar-chart',
                data: [],
                xkey: 'title',
                ykeys: ['total', 'registered'],
                labels: ['Needed Applicant', 'Registered Applicant'],
                hideHover: 'auto',
                resize: true
                // barColors: function (row, series, type) { return 'red'}
            });
            chart.setData(data);
        }
    })
    .fail(function() {
      // If there is no communication between the server, show an error
      console.log( "error occured..unable to get records." );
    });

});
