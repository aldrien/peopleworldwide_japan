var resizing = function() {
    if ($(window).width() <= 991) {
        $('div.pww_bg').css("background-size", "100%");

        // Login
        $("input[value='continue']").css('font-size', '21px');
        $('input#username').css('font-size', '15px');
        $('input#password').css('font-size', '15px');
        // Homepage
        $('img.nav_logo').css('height', '32px');
        $('h1.page-header-custom').css('font-size', '30px');

    } else {
        $('div.pww_bg').css("background-size", "50%");
    }

    if ($(window).height() <= 720) {
        $('div#login_form_holder').removeClass('margintop10pers');
    }
}

$(function() {
    resizing();
});

$(window).on('resize', function() {
    resizing();
});
