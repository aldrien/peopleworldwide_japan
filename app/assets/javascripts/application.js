//= require ckeditor/init

var todayDate = moment().format();

function intializeYearMonthPicker() {
    $('.date_year_month').datetimepicker({
        viewMode: 'years',
        format: 'YYYY/MM',
        defaultDate: moment('2000/01', 'YYYY/MM'),
        maxDate: moment(todayDate),
        showClear: true
    });
}

function remove_fields(link) {
    // $(link).prev("input[type=hidden]").val("1");
    // $(link).parent().parent('.fields').find("input[type=text], textarea").val("");
    // $(link).parent().parent('.fields').find("select").val("");
    // $(link).closest(".fields").hide();
    $(link).closest(".fields").remove();
}

function add_fields(link, association, content) {
    var new_id = new Date().getTime();
    var regexp = new RegExp("new_" + association, "g");
    $(link).closest('tr').before(content.replace(regexp, new_id));

    // Reinitialize datePicker for newly added field
    intializeYearMonthPicker();

    // Used in Applicant's Uploading Files
    if (association == 'applicant_files') {
        // Reinitialize File Upload for newly added field
        $(".file").fileinput({ 'showUpload': false, 'showRemove': true });

        $('a[href="/documents/original/missing.png"]').remove();
        $(link).closest('tr').prev('tr').children("td:eq( 1 )").addClass('text-center').find('img').attr({ src: "/assets/icons/upload.png", class: 'avatar_size' });
    }

}
