
require 'socket'
hostname = Socket.gethostname

if hostname == 'aldrien-OptiPlex-990'
  location = '/root/.rbenv/shims/wkhtmltopdf'
elsif hostname == 'hrs' || Rails.env == 'production'
  location = '/sky/local/bin/wkhtmltopdf'
end

WickedPdf.config = {
  exe_path: location
}