Rails.application.configure do
  # Settings specified here will take precedence over those in config/application.rb.

  # In the development environment your application's code is reloaded on
  # every request. This slows down response time but is perfect for development
  # since you don't have to restart the web server when you make code changes.
  config.cache_classes = false

  # Do not eager load code on boot.
  config.eager_load = false

  # Show full error reports and disable caching.
  config.consider_all_requests_local       = true
  config.action_controller.perform_caching = false

  # Don't care if the mailer can't send.
  # config.action_mailer.raise_delivery_errors = false

  # Print deprecation notices to the Rails logger.
  config.active_support.deprecation = :log

  # Raise an error on page load if there are pending migrations.
  config.active_record.migration_error = :page_load

  # Debug mode disables concatenation and preprocessing of assets.
  # This option may cause significant delays in view rendering with a large
  # number of complex assets.
  config.assets.debug = true

  # Asset digests allow you to set far-future HTTP expiration dates on all assets,
  # yet still be able to expire them through the digest params.
  config.assets.digest = true

  # Adds additional error checking when serving assets at runtime.
  # Checks for improperly declared sprockets dependencies.
  # Raises helpful error messages.
  config.assets.raise_runtime_errors = true

  # Raises error for missing translations
  # config.action_view.raise_on_missing_translations = true

  # Added by Aldrien
  # Sidekiq Reference url: https://gist.github.com/maxivak/690e6c353f65a86a4af9
  config.active_job.queue_adapter = :sidekiq
  config.active_job.queue_name_prefix = "pww"
  config.active_job.queue_name_delimiter = "_"
  config.action_mailer.perform_deliveries = true
  config.action_mailer.raise_delivery_errors = true

  # GMAIL SMTP Setup
  config.action_mailer.delivery_method = :smtp
  config.action_mailer.smtp_settings = {
    address: 'smtp.gmail.com',
    port: 587,
    user_name: ENV['GMAIL_USERNAME_DEVELOPMENT'],
    password: ENV['GMAIL_PASSWORD_DEVELOPMENT'],
    authentication: 'plain',
    enable_starttls_auto: true 
  }
  config.action_mailer.default_options = {
    from: 'noreply@gmail.com'
  }
  # Paperclip config:
  Paperclip.options[:image_magick_path] = "/usr/local/bin"
  Paperclip.options[:command_path] = "/usr/local/bin"

  # Default in development environment is in public/images || public/documents
  # Amazon Web Services S3 (Uncomment below codes if you plan to use Amazon S3 in development environment)
  # config.paperclip_defaults = {
  #   storage: :s3,
  #   s3_protocol: 'https',
  #   s3_host_name: "s3-ap-northeast-1.amazonaws.com",
  #   s3_region: 'ap-northeast-1',
  #   s3_credentials: {
  #     bucket: ENV['AWS_BUCKET'],
  #     access_key_id: ENV['AWS_ACCESS_KEY_ID'],
  #     secret_access_key: ENV['AWS_SECRET_ACCESS_KEY']
  #   }
  # }

  # Add Rack::LiveReload to the bottom of the middleware stack with the default options:
  config.middleware.insert_after ActionDispatch::Static, Rack::LiveReload
  # or, if you're using better_errors:
  config.middleware.insert_before Rack::Lock, Rack::LiveReload
  # Specifying Rack::LiveReload options.
  # config.middleware.use(Rack::LiveReload,
  #   min_delay        : 500,    # default 1000
  #   max_delay        : 10_000, # default 60_000
  #   live_reload_port : 56789,  # default 35729
  #   host             : 'myhost.cool.wow',
  #   ignore           : [ %r{dont/modify\.html$} ]
  # )
  
end