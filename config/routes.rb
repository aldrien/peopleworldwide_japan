Rails.application.routes.draw do      
  scope '(:locale)', locale: /en|ja/ do
    resources :users, :applicants, :religions, :countries, :job_types, :projects, :job_orders, :file_types, :academic_abilities, :aptitude_tests, :pww_logos
    
    # AJAX REQUESTS
    ## Religions
    get 'get_all_religions' => 'religions#get_all_religions'
    get 'add_new_religion' => 'religions#add_new_religion'
    post 'edit_religion' => 'religions#edit_religion'
    get 'delete_religion' => 'religions#delete_religion'

    ## Job Types
    get 'get_all_job_types' => 'job_types#get_all_job_types'
    get 'add_new_job_type' => 'job_types#add_new_job_type'
    post 'edit_job_type' => 'job_types#edit_job_type'
    get 'delete_job_type' => 'job_types#delete_job_type'

    ## Countries
    get 'get_all_countries' => 'countries#get_all_countries'
    get 'add_new_country' => 'countries#add_new_country'
    post 'edit_country' => 'countries#edit_country'
    get 'delete_country' => 'countries#delete_country'

    ## Projects
    get 'get_all_projects' => 'projects#get_all_projects'
    get 'add_new_project' => 'projects#add_new_project'
    get 'delete_project' => 'projects#delete_project'
    get 'project_remove_document' => 'projects#project_remove_document'
    get 'view_project_applicants' => 'projects#view_project_applicants'
    post 'send_email_job_order' => 'projects#send_email_job_order'
    get 'project_chart' => 'home#project_chart'
    get 'get_last_sent_mail_date' => 'projects#get_last_sent_mail_date'

    ## Users
    get 'delete_user_account' => 'users#destroy'
    get 'delete_applicant' => 'applicants#destroy'

    ## File Type
    get 'get_all_file_types' => 'file_types#get_all_file_types'
    get 'add_new_file_type' => 'file_types#add_new_file_type'
    post 'edit_file_type' => 'file_types#edit_file_type'
    get 'delete_file_type' => 'file_types#delete_file_type'

    ## Academic Abilities
    get 'get_all_academic_abilities' => 'academic_abilities#get_all_academic_abilities'
    get 'add_new_academic_ability' => 'academic_abilities#add_new_academic_ability'
    post 'edit_academic_ability' => 'academic_abilities#edit_academic_ability'
    get 'delete_academic_ability' => 'academic_abilities#delete_academic_ability'

    ## Aptitude Tests
    get 'get_all_aptitude_tests' => 'aptitude_tests#get_all_aptitude_tests'
    get 'add_new_aptitude_test' => 'aptitude_tests#add_new_aptitude_test'
    post 'edit_aptitude_test' => 'aptitude_tests#edit_aptitude_test'
    get 'delete_aptitude_test' => 'aptitude_tests#delete_aptitude_test'

    ## Applicant Files
    get 'applicant_files' => 'applicant_files#index'
    get 'delete_applicant_file' => 'applicant_files#destroy'

    ## Search
    get 'search' => 'search#index'
    get 'quick_search' => 'search#quick_search', as: 'quick_search'
    post 'search_applicants' => 'search#search_applicants', as: 'search_applicants'    

    ## Applicants
    get 'lock_function' => 'applicants#lock_function'
    get 'video_interview' => 'applicants#video_interview'
    get 'video_self_introduction' => 'applicants#video_self_introduction'    
    get 'project_history' => 'applicants#project_history'
    get 'requests_for_applicant_record_deletion' => 'applicants#to_be_deleted'
    get 'request_for_delete_applicant' => 'applicants#request_for_delete_applicant'
    get 'restore_applicant' => 'applicants#restore_applicant'
    get 'make_comments_to_applicant' => 'applicants#make_comments_to_applicant'

    # PWW Logo
    get 'delete_pww_logo' => 'pww_logos#destroy'

    ## Generate PDF Reports
    get 'generate_applicant_report' => 'applicants#generate_applicant_report', as: 'generate_applicant_report'
    match 'generate_pdf_full_report' => 'search#generate_pdf_full_report', via: :get
    match 'generate_csv_full_report' => 'search#generate_csv_full_report', as: 'generate_csv_full_report', via: :get

    ## Authenticate Admin Password Before Record Deletion
    post 'authenticate_user_password' => 'users#authenticate_user_password'

    ## User Auth Functions    
    controller :login do
        get 'login' => :index, as: :login
        post 'authenticate_login' => :authenticate_login
        get 'logout' => :destroy
        # delete 'logout' => 'login#destroy'
    end
    
    mount Ckeditor::Engine => '/ckeditor'
    match 'home' => 'home#index', via: :get, as: 'home'
    root :to => 'login#index'
  end

  match '*path', :to => 'application#render_404', :via => :all, :locale => 'en' # render 404 page when routes not found
  match '/*locale/*path', to: redirect('/#{I18n.default_locale}/%{path}'), :via => :get # handles /bad-locale|anything/valid-path
  match '/*path', to: redirect('/#{I18n.default_locale}/%{path}'), :via => :get # handles /anything|valid-path-but-no-locale  
end
