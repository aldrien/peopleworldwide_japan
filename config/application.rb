require File.expand_path('../boot', __FILE__)

require 'csv'
require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module PeopleWorldWide
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    # config.time_zone = 'Central Time (US & Canada)'

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]    
    config.i18n.enforce_available_locales = false
    config.i18n.available_locales = [:en, :ja]
    config.i18n.default_locale = :en  

    # Do not swallow errors in after_commit/after_rollback callbacks.
    config.active_record.raise_in_transactional_callbacks = true

    # Added by aldrien    
    config.encoding = 'utf-8'
    config.assets.enabled = true

    config.assets.precompile += %w(*.js)

    config.generators do |g|
      g.assets false
      # Use Minitest for generating new tests.
      g.test_framework :minitest, spec: true
    end

    config.before_configuration do
      local_env_file = File.join(Rails.root, 'config', 'local_env.yml')
      YAML.load(File.open(local_env_file)).each do |key, value|
        ENV[key.to_s] = value
      end if File.exist?(local_env_file)

      aws_env_file = File.join(Rails.root, 'config', 'aws.yml')
      YAML.load(File.open(aws_env_file)).each do |key, value|
        ENV[key.to_s] = value
      end if File.exist?(aws_env_file)
    end

    # Paperclip config:
    if Rails.env.production?
      Paperclip.options[:image_magick_path] = "/usr/local/bin"
      Paperclip.options[:command_path] = "/usr/local/bin"
    end
    # end of added
  end
end
