source 'https://rubygems.org'


# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.2.6'
# Use sqlite3 as the database for Active Record
# gem 'sqlite3'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.1.0'
# See https://github.com/rails/execjs#readme for more supported runtimes
gem 'therubyracer', platforms: :ruby

gem 'mysql2'
gem 'bcrypt', '3.1.7' # Use ActiveModel has_secure_password
gem 'paperclip'
gem 'iconv', '~> 1.0.3'
gem 'execjs'

gem 'wicked_pdf'
gem 'wkhtmltopdf-binary'

gem 'simple_form'
gem 'nested_form'
gem 'faenza-file-icons-rails'

gem 'sidekiq'
gem 'redis'
gem 'cancancan'
gem 'ckeditor', github: 'galetahub/ckeditor'

gem 'aws-sdk'

# Use jquery as the JavaScript library
gem 'jquery-rails'
# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'
# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0', group: :doc

# Use Unicorn as the app server
# gem 'unicorn'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

gem 'zip-zip'
gem 'axlsx', '~> 2.0'
gem 'axlsx_rails'

group :development do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug'
  # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'web-console', '~> 2.0'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'

  gem 'minitest-rails', '~> 2.0'

  gem 'capybara'
  gem 'selenium-webdriver'
  gem 'chromedriver-helper'

  gem 'guard' # NOTE: this is necessary in newer versions
  gem 'guard-minitest'
  gem 'guard-livereload', '~> 2.5', require: false
  gem 'rack-livereload'
end

group :test do
  gem 'minitest-rails-capybara'
  gem 'minitest-reporters'
  gem 'capybara-screenshot'
  gem 'ci_reporter_minitest'
  gem 'metric_fu', '4.11.3'
end
