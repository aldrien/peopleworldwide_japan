##################################################
Steps on How To SetUp AWS Service
##################################################

  1. Gemfile
    gem 'aws-sdk'
    gem 'paperclip' #suggested to be the latest version


  2. Set up local environments, for development environment
    
    - make file, aws.yml
    - put the following:

      # Amazon Web Service S3
      AWS_NAME: 'pww'
      AWS_BUCKET: 'pwwbucket'
      AWS_ACCESS_KEY_ID: 'AKIAJSIH44QD6ZCC2QTQ'
      AWS_SECRET_ACCESS_KEY: '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'


  3. Load the aws.yml
     Put in config/application.rb

      config.before_configuration do
        env_file = File.join(Rails.root, 'config', 'aws.yml')
        YAML.load(File.open(env_file)).each do |key, value|
          ENV[key.to_s] = value
        end if File.exist?(env_file)
      end 

  4. Put in config/environments/development.rb
     Set up for default storage in paperclip.
      
      # Amazon Web Services S3
      config.paperclip_defaults = {
        storage: :s3,
        s3_host_name: "s3-ap-northeast-1.amazonaws.com",
        s3_region: 'ap-northeast-1',
        s3_credentials: {
          bucket: ENV['AWS_BUCKET'],
          access_key_id: ENV['AWS_ACCESS_KEY_ID'],
          secret_access_key: ENV['AWS_SECRET_ACCESS_KEY']
        }
      }


##################################################
FOR Production Environment:
##################################################

  The ONLY difference, is that it is NOT advisable to display AWS credentials in codes or yml files.
  Instead set in in System Environment.


  vi ~/.bashrc
    export SECRET_KEY_BASE='xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'

    export AWS_NAME='xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'
    export AWS_BUCKET='pwwbucket'
    export AWS_ACCESS_KEY_ID='xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'
    export AWS_SECRET_ACCESS_KEY='xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'

  check id System sets the environment:
      echo $AWS_BUCKET
      > pwwbucket

  Done.!!!


##################################################
NOTE FOR UPGRADING FROM 4.3.0 OR EARLIER
##################################################

  Paperclip is now compatible with aws-sdk >= 2.0.0.

  If you are using S3 storage, aws-sdk >= 2.0.0 requires you to make a few small
  changes:

  * You must set the `s3_region`
  * If you are explicitly setting permissions anywhere, such as in an initializer,
    note that the format of the permissions changed from using an underscore to
    using a hyphen. For example, `:public_read` needs to be changed to
    `public-read`.

  For a walkthrough of upgrading from 4 to 5 and aws-sdk >= 2.0 you can watch
  http://rubythursday.com/episodes/ruby-snack-27-upgrade-paperclip-and-aws-sdk-in-prep-for-rails-5
