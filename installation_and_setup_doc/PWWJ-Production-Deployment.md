

# Install Latest Ruby Version
  Since ruby it pre-installed using Omnibus Installer, we just need to bind it to system default.

  Now, make a symbolic link to /urs/bin/ruby from /sky/local/bin/ruby
```
  ln -s /sky/local/bin/ruby /usr/bin/ruby

  To check:
    ls -la /usr/bin/ruby 
    

    Result like:
      lrwxrwxrwx 1 root root 19 Oct 21 11:40 /usr/bin/ruby -> /sky/local/bin/ruby

  NOTE: If you other ruby folder under /usr/bin/ , please remove it to avoid conflict.

```

# Install Passenger
```
  /sky/local/bin/gem install passenger --no-ri --no-rdoc
  /sky/local/bin/passenger-install-apache2-module

  Validate Passenger Installation using:
    > /sky/local/bin/passenger-config validate-install
    (Should all checked, No errors...)
```

  Once done installing, dont close the current terminal, it will ask you to update httpd config file.
  
  Thus, open a new terminal, then:
  ```
    vi /etc/httpd/conf/httpd.conf
  ```

  At the bottom part of httpd.conf, paste the LIKE code below, given in your terminal

    LoadModule passenger_module /sky/local/lib64/ruby/gems/2.3.0/gems/passenger-5.0.30/buildout/apache2/mod_passenger.so
    <IfModule mod_passenger.c>
      PassengerRoot /sky/local/lib64/ruby/gems/2.3.0/gems/passenger-5.0.30
      PassengerDefaultRuby /sky/local/bin/ruby
    </IfModule>


# Install ImageMagick
  This is for image management.

```
  yum install ImageMagick.x86_64
```

# Create Virtual Host
```
  vi /etc/httpd/sites-available/worker.db.peopleworldwide.jp

  Copy & Paste the code below:

  <VirtualHost *:80>
    ServerAdmin webadmin@worker.db.peopleworldwide.jp
    DocumentRoot /prj/people_world_wide/public 
    ServerName worker.db.peopleworldwide.co.jp

    ErrorLog "|/usr/sbin/rotatelogs /var/www_logs/worker.db.peopleworldwide.co.jp-error_log.%Y%m%d 86400 540"
    CustomLog "|/usr/sbin/rotatelogs /var/www_logs/worker.db.peopleworldwide.co.jp-access_log.%Y%m%d 86400 540" combined

    RailsEnv production

    <Directory /prj/people_world_wide/public>

      SetEnv SECRET_KEY_BASE 81c2ed2f38f6b5f23384ac1aac11f21ab142ecf055d9876fb99f06ce07879d8577561cf7de72fc21dbea82c891184a0e3e8c6d71fd2f44a2034b26ec99bcd8fd
      SetEnv AWS_NAME pww
      SetEnv AWS_BUCKET pwwbucket
      SetEnv AWS_ACCESS_KEY_ID AKIAJSIH44QD6ZCC2QTQ
      SetEnv AWS_SECRET_ACCESS_KEY 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

      Options         Indexes FollowSymLinks MultiViews
      AllowOverride   none
      Require all granted
      Order           allow,deny
      Allow from      all
    </Directory>
  </VirtualHost>
```

# Create Symbolic Link to /etc/httpd/sites-enabled/
```
  ln -s /etc/httpd/sites-available/worker.db.peopleworldwide.co.jp /etc/httpd/sites-enabled/
  service httpd restart
```

# Installing Japanese Fonts
```
  yum install -y ipa-pgothic-fonts
  yum install -y vlgothic-p-fonts
```

# Set Up SSH Access
```
  Step 1: Make public key
    > ssh-keygen -t rsa

  Step 2: Update ssh config
    > vi ~/.ssh/config

      Copy & Paste this code:
          Host git.skyarch.net
          HostName git.skyarch.net
          IdentityFile ~/.ssh/id_rsa
          StrictHostKeyChecking no
          User git
          Port 10022

  Step 3: Update file permission.
    > chmod 600 ~/.ssh/*

  Step 4: Get Public Key
    > cat ~/.ssh/id_rsa.pub

    Copy & Paste to your SSH Keys in Git
```

# Clone PWWJ from Skyarch Git Repo
```
  Step 1: Make /prj directory
    > mkdir /prj

  Step 2: Clone
    > git clone git@git.skyarch.net:skyarchnetworks/people_world_wide.git
```

# Export /sky/local/bin & Add AWS S3 Bucket Options
```
  vi /etc/profile
  
  Then, add below:
    export PATH="$PATH:/sky/local/bin"

    # Amazon S3 Bucket Options, used when seeding default data.
    export AWS_NAME='pww'
    export AWS_BUCKET='pwwbucket'
    export AWS_ACCESS_KEY_ID='AKIAJSIH44QD6ZCC2QTQ'
    export AWS_SECRET_ACCESS_KEY='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'

  Reload using:
    > source /etc/profile

  Or Ctrl + D, to logout & login again.


  Check Environment Variables:

    > echo $AWS_NAME
      should return: pww
```

# SetUp PWWJ Production Database
  It will do the following:
    - Create production database
    - Run migration
    - Import seed data

  NOTE: The below command (rake db:setup) ONLY possible on initial creation of database, unless do [ /sky/local/bin/rake db:drop RAILS_ENV=production ] first.

```
  cd /prj/people_world_wide
  /sky/local/bin/rake db:setup RAILS_ENV=production
```