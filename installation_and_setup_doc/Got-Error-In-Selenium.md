# IF YOU ENCOUNTER ERROR IN SELENIUM TEST, WITH CHROMEDRIVER LIKE:

    > EOFError:         EOFError: end of file reached
  
  This happens when your Google Chrome Browser updates and your installed chromedriver doesn't.

  To fix this you need to update your [chromedriver], which you have in Gemfile [gem 'chromedriver-helper'].

    > Documentation: https://github.com/flavorjones/chromedriver-helper

# Updating Chromedriver

  If you'd like to force-upgrade to the latest version of chromedriver, run the script [chromedriver-update] that also comes packaged with this gem.

  Step 1: Go to your root application directory
    > cd /prj/people_world_wide

  Step 2: Run update command
    > chromedriver-update

    You should not get any error, nor response.    

  Step 3: Try to re-run your test. (2 ways)

    1. rake test (single run of all tests)
    2. bundle exec guard (only if you want to listen to any changes you will made in file and run automatic test)



  That's it!!!