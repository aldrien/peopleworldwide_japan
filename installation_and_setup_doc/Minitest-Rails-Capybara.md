# Set Up Minitest-Rails-Capybara


1. Gemfile:
    group :test do
      gem 'minitest-rails-capybara'
      gem 'minitest-reporters'
    end


2. In test/test_helper.rb
    ENV["RAILS_ENV"] = "test"
    require File.expand_path("../../config/environment", __FILE__)
    require "rails/test_help"
    require "minitest/rails"
    require "minitest/rails/capybara"

    require "minitest/reporters"
    Minitest::Reporters.use!(
      Minitest::Reporters::SpecReporter.new,
      ENV,
      Minitest.backtrace_filter
    )
    class ActiveSupport::TestCase
      ActiveRecord::Migration.check_pending!
      fixtures :all
    end


3. Generator command, ex.:
    rails generate minitest:feature Login

    Result like:

      require "test_helper"

      feature "Login" do
        scenario "the test is sound" do
          visit root_path
          page.must_have_content "Hello World"
          page.wont_have_content "Goodbye All!"
        end
      end