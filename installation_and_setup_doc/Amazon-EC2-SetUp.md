# Installing Latest Ruby version
  yum search ruby
  yum install ruby23
  ruby --version
  alternatives --config ruby
    > sample output:
      There are 2 programs which provide 'ruby'.

        Selection    Command
      -----------------------------------------------
      *  1           /usr/bin/ruby2.0
       + 2           /usr/bin/ruby2.3

    > enter 2

  *check again: ruby --version
    > should likely be: ruby 2.3.0p0 (2015-12-25 revision 53290) [x86_64-linux-gnu]

# Installing ImageMagick (source: https://gist.github.com/ARolek/9199329)
  *Download the most recent package
    > wget http://www.imagemagick.org/download/ImageMagick.tar.gz
  
  *Uncomress the package
    > tar -vxf ImageMagick.tar.gz
  
  *Install the devel packages for png, jpg, tiff. these are dependencies of ImageMagick
    > sudo yum -y install libpng-devel libjpeg-devel libtiff-devel
  
  *Configure ImageMagick without X11. this is a server without a display (headless) so we don't need X11
    > cd ImageMagick
    > ./configure --without-x
    > make && make install

  * Check installation:
    > convert -version
    > convert: command not found then, it is not installed.


# Auto run HTTPD & MYSQLD on boot
Checking the list:
> chkconfig --list
  If not active in list, do:
  > chkconfig httpd on
  > chkconfig mysqld on


https://www.digitalocean.com/community/tutorials/how-to-setup-a-rails-4-app-with-apache-and-passenger-on-centos-6

# Installing APACHE
  sudo yum update
  sudo yum install https mod_ssl mysql mysql-server php php-mysql php-xml
  sudo yum install phpmyadmin
  cd /etc/rc.d/init.d/
  sudo ./mysqld start
  sudo /usr/bin/mysql_secure_installation
    * check:
    mysql -u root -p
      > enter your password

  cd /etc/rc.d/rc3.d
  sudo rm K15httpd 
  sudo rm K36mysqld 
  sudo ln -s ../init.d/mysqld S30mysql
  sudo ln -s ../init.d/httpd S85httpd
  cd /etc/httpd/conf
  vi httpd.conf 
  sudo service http restart  
  sudo /etc/rc.d/init.d/httpd start
  cd ~
  yum install ruby-devel
  yum install mysql-devel
  yum install make
  yum install gcc46
  yum install curl-devel
  yum install openssl-devel
  yum install httpd-devel
  yum install gcc-c++
  yum install rubygems

# Installing Passenger
  sudo gem install passenger
  sudo passenger-install-apache2-module
    > * if you got error here like (sudo: passenger-install-apache2-module: command not found)
    > Find your passenger:
      which passenger
        >  output like: /sky/local/bin/passenger
  
  (locate it and run using the exact path like:)
  /sky/local/bin/passenger-install-apache2-module

  (Now check it succefully installed:)
  passenger -v
  > output like: Phusion Passenger 5.0.29

  Visit page using your IP/ElasticIP:
  > you should see Amazon Linux AMI Test Page, powered by Apache 2.2
  


# Set Up Git
  sudo yum update
  sudo yum install git
  vi ~/.ssh/config
  ssh-keygen -t rsa -C "aldrien@skyarch.net"
  eval "$(ssh-agent -s)"
  ssh-add ~/.ssh/id_rsa
  yum install xclip
  vi ~/.ssh/config
    > add below code:
      Host git.skyarch.net
      HostName git.skyarch.net
      IdentityFile ~/.ssh/id_rsa
      StrictHostKeyChecking no
      User git
      Port 10022
  cat ~/.ssh/id_rsa.pub 
    > add to Git keys

# Download Git Project
  mkdir /prj/
  chmod -R 777 /prj
  git clone git@git.skyarch.net:aldrien/people_world_wide.git
  cd people_world_wide/


# Install Bundler & Rails
  gem install bundler
    > to check:
      gem list bundler

  try:
    > rails -v
    > if rails command not found, /sky/local/bin is not loaded
    
    echo $PATH
      > if there is no /sky/local/bin in PATH 
      > add below code to (~/.bashrc)
      export PATH="/sky/local/bin:$PATH"


# Install PHPMyAdmin (requires PHP5.5)
  cd /var/www/html
  sudo chown ec2-user .
  wget https://files.phpmyadmin.net/phpMyAdmin/4.5.0.2/phpMyAdmin-4.5.0.2-all-languages.tar.bz2
  tar -jxf phpMyAdmin-4.5.0.2-all-languages.tar.bz2 -C /var/www/html
  mv phpMyAdmin-4.5.0.2-all-languages phpmyadmin
  rm -rf phpMyAdmin-4.5.0.2-all-languages.tar.bz2

# Check current PHP version:
  > php -v (if it's less than 5.5 we need to uninstall it, as well as the other dependencies)

# Removing Lower Versions
  You might uninstall all the lower versions installed:
    > yum remove php53
    > yum remove php-*
    > yum remove httpd
    > yum remove httpd-tools-2.2.31-1.7.amzn1.x86_64

# Installing PHP5.5 versions & other dependecies:
  > yum install php55.x86_64 php55-cli php55-mbstring php55-devel php55-mysqlnd  


# Restart Server:
  > service httpd restart

# Browseand check in web:
  Ip or elesticIP/phpmyadmin
  ex. http://52.197.141.6/phpmyadmin
