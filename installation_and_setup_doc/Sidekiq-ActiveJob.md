SETUP BACKGROUND JOB USING SIDEKIQ & REDIS

Reference url:
# https://gist.github.com/maxivak/690e6c353f65a86a4af9

STEP1: Set in Gemfile
      gem 'sidekiq'
      gem 'redis'

STEP2: Update config/environments/development.rb
      config.active_job.queue_adapter = :sidekiq
      config.active_job.queue_name_prefix = "pww"
      config.active_job.queue_name_delimiter = "_"
      config.action_mailer.perform_deliveries = true
      config.action_mailer.raise_delivery_errors = true

STEP3: Create file config/initializer/active_job.rb
      Rails.application.config.active_job.queue_adapter = :sidekiq

      or

      Add in config/application.rb
        config.active_job.queue_adapter = :sidekiq

STEP4: Create file config/sidekiq.yml
    :concurrency: 1
    :queues:
      - default
      - mailers
      - pww_default
      - pww_mailers

# Usage:

    Development Environment: bundle exec sidekiq -C config/sidekiq.yml

    -No ActiveJob: 
      > ProjectMailer.send_job_order_mail(project, u.email).deliver_now
    -With Activejob using SideKiq: 
      > ProjectMailer.send_job_order_mail(project, u.email).deliver_later

# Run SideKiq:
    bundle exec sidekiq --environment development -C config/sidekiq.yml

    Other options:

    bundle exec sidekiq -d -L log/sidekiq.log -C config/sidekiq.yml -e production
      -d, Daemonize process (requies log file)

      -L, path to writable logfile

      -C, path to YAML config file

      -e, Application environment


# Install Redis in Amazon EC2
    Reference url: https://gist.github.com/dstroot/2776679

    cd bin/
    ./install_redis.sh


# Removing / Uninstalling REDIS-server in Ubuntu
# http://grainier.net/how-to-uninstall-redis-server-from-ubuntu/

    sudo apt-get purge --auto-remove redis-server
    sudo rm /usr/local/bin/redis-*

