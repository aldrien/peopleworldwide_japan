To be able to use same SYSTEM Font with your Local and Amazon EC2 Linux, please follow the steps:

STEP 1:
  Back up your EC2 fonts, like:
    cp -r /etc/fonts /etc/fonts.old

STEP 2:
  Copy your local fonts to EC2 (requires permission)
    scp -r -i aldrienhate.pem /etc/fonts  ec2-user@ec2-52-197-141-6.ap-northeast-1.compute.amazonaws.com:/etc/fonts

STEP 3:
  Reset Cache & Check system font error
    fc-cache
    fc-cache -v | grep err

    If you encounter error with bitmap-fonts...
      
      *The default config restricts the use of bitmap fonts, do the following.
        
        sudo rm /etc/fonts/conf.d/70-no-bitmaps.conf
        sudo ln -s /etc/fonts/conf.avail/70-yes-bitmaps.conf /etc/fonts/conf.d/70-yes-bitmaps.conf

      Do the reseting of cache, you shouldn't get any error.

STEP 4:
  In you applicantion directory:
  
  mkdir fonts
    > add all the fonts you will use

  cd fonts
  mkdir .ebextensions
  vi copy_fonts.config
    > put the code below:

    container_commands:
      copy_fonts:
        command: "cp fonts/*.ttf /usr/share/fonts/"


STEP 5:
  Restart your server