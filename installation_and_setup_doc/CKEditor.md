# Reference URL: https://github.com/galetahub/ckeditor

STEPS:

#1 Gemfile:
  gem 'ckeditor', github: 'galetahub/ckeditor'
  bundle install


#2 How to generate models to store uploaded files
  > ForActiveRecord + paperclip

    To use the active_record orm with paperclip (i.e. the default settings):
    
      gem 'paperclip'
      rails generate ckeditor:install --orm=active_record --backend=paperclip

#3 For Rails 4, add the following to config/initializers/assets.rb:
  Rails.application.config.assets.precompile += %w( ckeditor/* )


#4 Mount the Ckeditor::Engine in your routes (config/routes.rb):
  mount Ckeditor::Engine => '/ckeditor'
  restart server


#5 Usage
  Load editor from gem vendor
  Include ckeditor javascripts in your app/assets/javascripts/application.js:

  //= require ckeditor/init


#6 Load editor via CKEditor CDN
  Setup editor version to load (more info here http://cdn.ckeditor.com/)

  # in config/initializers/ckeditor.rb

  Ckeditor.setup do |config|
    # //cdn.ckeditor.com/<version.number>/<distribution>/ckeditor.js
    config.cdn_url = "//cdn.ckeditor.com/4.5.10/standard/ckeditor.js"
  end


#7 In view template/layout include ckeditor CDN:
  <%= javascript_include_tag Ckeditor.cdn_url %>

#8 Sample use in FORM
  <%= f.cktext_area :description, class: 'form-control', autofocus: true, required: '' %>