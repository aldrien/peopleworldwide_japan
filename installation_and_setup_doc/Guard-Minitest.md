# I used gem guard-minitest for automatic running of all tests once I update some codes.

In Gemfile:

  group :development do
    gem 'guard-minitest'
  end

Run:
  > bundle install


Next, to generate codes in Guardfile (located in your application directory): 
  > guard init minitest


Finally, in separate terminal tab/window run:
  > guard


Notes: If you're using Rails 4 (or other version), don't forget to uncomment stuff in Guardfile:

  Looks like the following:

    # Rails 4
    watch(%r{^app/(.+)\.rb$})                               { |m| "test/#{m[1]}_test.rb" }
    watch(%r{^app/controllers/application_controller\.rb$}) { 'test/controllers' }
    watch(%r{^app/controllers/(.+)_controller\.rb$})        { |m| "test/integration/#{m[1]}_test.rb" }
    watch(%r{^app/views/(.+)_mailer/.+})                    { |m| "test/mailers/#{m[1]}_mailer_test.rb" }
    watch(%r{^lib/(.+)\.rb$})                               { |m| "test/lib/#{m[1]}_test.rb" }
    watch(%r{^test/.+_test\.rb$})
    watch(%r{^test/test_helper\.rb$}) { 'test' }

    # Additional, custom listener (CHOOSE ONE based on what you NEED most)
    # Listen to fixture files when updated and auto run the model test, when guard-livereload is running...
    
    # Checks ONLY for [s]  plural form
      watch(%r{^test/fixtures/(.+)s\.yml$})                   { |m| "test/models/#{m[1]}_test.rb"}  

    # Checks BOTH [s, ies, es] plural form
      watch(%r{^test/fixtures/(.+(?:s|ies|es)).yml$}) do |m|
       "test/models/#{ActiveSupport::Inflector.singularize(m[1])}_test.rb"
      end 