#Using rbenv

https://github.com/rbenv/ruby-build/wiki

*Ubuntu/Debian/Mint:
  apt-get install autoconf bison build-essential libssl-dev libyaml-dev libreadline6-dev zlib1g-dev libncurses5-dev libffi-dev libgdbm3 libgdbm-dev

*CentOS/Fedora:
  yum install -y gcc bzip2 openssl-devel libyaml-devel libffi-devel readline-devel zlib-devel gdbm-devel ncurses-devel


  - Check the list
    > rbenv install -l
    (we're looking for verion 2.3.1, if not on the list you have to update your ruby-build)

    First locate it on your system:

      > which ruby-build
      > ls "$(rbenv root)"/plugins
      > cd "$(rbenv root)"/plugins/ruby-build && git pull

  - Once, finished, let's install ruby 2.3.1
    https://www.digitalocean.com/community/tutorials/how-to-install-ruby-on-rails-with-rbenv-on-ubuntu-16-04

    > rbenv install 2.3.1
    > rbenv global 2.3.1

      Check ruby version:

      > ruby -v
      ruby 2.3.1p112 (2016-04-26 revision 54768) [x86_64-linux]


# Note: If you encounter error like below when running bunlde or bundle install:

  rbenv: bundle: command not found

  The `bundle' command exists in these Ruby versions:
    2.2.3


  Solution:  http://stackoverflow.com/questions/28056595/bundle-not-working-with-rbenv
  
  Change to a directory that is not your app, and that doesn't have a Gemfile.
  Then do the usual gem install bundle (and use sudo if you need it).
  That's All!!!