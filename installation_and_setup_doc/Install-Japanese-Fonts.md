# Installing Japanese Font in Amazon Linux

  Amazon Linuxで
    sudo yum install -y ipa-pgothic-fonts
    とコマンド打って、日本語フォントをインストールしようとすると

    読み込んだプラグイン:priorities, update-motd, upgrade-helper
    パッケージ ipa-pgothic-fonts は利用できません。

    と怒られます。

    ところが
    このあたりのパッケージはおｋなようです。
    sudo yum install -y vlgothic-p-fonts
    sudo yum install -y ipa-gothic-fonts