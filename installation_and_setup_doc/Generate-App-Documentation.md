STEP 1: AFTER ADDING REMARKS (using # comment_here) IN METHODS, FUNCTIONS, MODULES AND CLASSES

STEP 2: RUN: rake doc:app

        RESULT LOOKS LIKE BELOW:

          Parsing sources...
          100% [61/61]  lib/simple_form_extensions/button_components.rb 

          Generating Darkfish format into /prj/people_world_wide/doc/app...

            Files:       61

            Classes:     41 ( 35 undocumented)
            Modules:     25 ( 21 undocumented)
            Constants:    2 (  1 undocumented)
            Attributes:   0 (  0 undocumented)
            Methods:    174 ( 51 undocumented)

            Total:      242 (108 undocumented)
             55.37% documented

            Elapsed: 1.0s

STEP 3: OPEN GENERATED DOCUMENTATION
        
        RUN: xdg-open doc/app/index.html
        (install xdg-... if not installed)