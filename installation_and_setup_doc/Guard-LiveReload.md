# I used gem guard-livereload for auto refreshing browser when I changed/updated some codes in view, js or css.

https://github.com/guard/guard-livereload
https://github.com/johnbintz/rack-livereload
https://github.com/guard/guard/wiki/List-of-available-Guards

Gemfile:

  group :development do
    gem 'guard-livereload', '~> 2.5', require: false
    gem "rack-livereload"
  end

After:
  > bundle install

Add guard definition to your Guardfile by running this command:
  > guard init livereload

Options:
  1. To get everything running in the browser, use "gem rack-livereload"
  2. Or install the LiveReload Safari/Chrome/Firefox extension.

  Instead of adding browser extension, I used "gem rack-livereload", this automatically adds livereload.js that listens to our application resources.

Finally, in separate terminal tab/window and run:
  > guard or bundle exec guard


Other reference: http://railscasts.com/episodes/264-guard?autoplay=true