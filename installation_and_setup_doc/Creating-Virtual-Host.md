vi /etc/httpd/conf/httpd.conf

# People Worldwide Configurations
<VirtualHost *:80>
  ServerAdmin webadmin@52.197.141.6
  DocumentRoot /prj/people_world_wide/public
  ServerName 52.197.141.6

  #ErrorLog "|/usr/sbin/rotatelogs /var/www-logs/pww-error.log.%Y%m%d 86400 540"
  #CustomLog "|/usr/sbin/rotatelogs /var/www-logs/pww-access.log.%Y%m%d 86400 540" combined

  RailsEnv production

  <Directory /prj/people_world_wide/public>
    Options         Indexes FollowSymLinks MultiViews
    SetEnv SECRET_KEY_BASE b6a3fbc7a4016a386586d7bf26a1bcaf460c3a7af5bd63a38138db9024f7fcace9ba15f89b7b108f1467101617ee2a71a43ec5e2301dbb92921e295567a14707
    AllowOverride   none
    Order           allow,deny
    Allow from      all
  </Directory>
</VirtualHost>