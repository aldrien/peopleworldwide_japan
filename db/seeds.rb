User.create([
  {
    fullname: 'Administrator', username: 'admin',
    password: 'pwwadmin', password_confirmation: 'pwwadmin',
    status: true, email: 'aldrien@skyarch.net', role: 'admin',
    pww_logo_id: 1, last_updated_by: 'admin'
  }
])

PwwLogo.create(
  title: 'PWW Japan Main',
  content: '<div>〒105-0001 東京都港区虎ノ⾨2-7-5 BUREX虎ノ⾨7F</div>
            <div>〒105-0001 7F BUREX Toranom on, 2-7-5 Minato-ku, Tokyo Japan</div>
            <div>TEL:+81-3-3539-2237 FAX:+81-3-3539-2238</div>',
  status: true,
  logo: File.new("#{Rails.root}/app/assets/images/pww_japan.png")
)

JobType.create([
  {en_name: 'waiter', jp_name: 'ウエイター', last_updated_by: 'admin'},
  {en_name: 'waitress', jp_name: 'ウエイトレス', last_updated_by: 'admin'},
  {en_name: 'driver', jp_name: '運転手', last_updated_by: 'admin'},
  {en_name: 'care worker', jp_name: '介護福祉士', last_updated_by: 'admin'},
  {en_name: 'confectioner', jp_name: '菓子屋', last_updated_by: 'admin'},
  {en_name: 'garment factory worker', jp_name: '縫製工場作業員', last_updated_by: 'admin'},
  {en_name: 'nurse', jp_name: '看護士・看護師', last_updated_by: 'admin'},
  {en_name: 'mechanic', jp_name: '機械工', last_updated_by: 'admin'},
  {en_name: 'engineer, engine driver', jp_name: '機関士', last_updated_by: 'admin'},
  {en_name: 'engineer', jp_name: '技術者', last_updated_by: 'admin'},
  {en_name: 'soldier, serviceman', jp_name: '軍人', last_updated_by: 'admin'},
  {en_name: 'guard, security guard', jp_name: '警備員、ガードマン', last_updated_by: 'admin'},
  {en_name: 'construction worker', jp_name: '建設作業員', last_updated_by: 'admin'},
  {en_name: 'builder', jp_name: '建築業者', last_updated_by: 'admin'},
  {en_name: 'public servant', jp_name: '公務員', last_updated_by: 'admin'},
  {en_name: 'cook', jp_name: 'コック', last_updated_by: 'admin'},
  {en_name: 'plaster', jp_name: '左官', last_updated_by: 'admin'},
  {en_name: 'craftsman, artisan', jp_name: '職人', last_updated_by: 'admin'},
  {en_name: 'carpenter', jp_name: '大工', last_updated_by: 'admin'},
  {en_name: 'cab driver, taxi driver', jp_name: 'タクシー運転手', last_updated_by: 'admin'},
  {en_name: 'interpreter', jp_name: '通訳', last_updated_by: 'admin'},
  {en_name: 'truck driver', jp_name: 'トラック運転手', last_updated_by: 'admin'},
  {en_name: 'butcher', jp_name: '肉屋', last_updated_by: 'admin'},
  {en_name: 'grdener', jp_name: '庭師', last_updated_by: 'admin'},
  {en_name: 'farmer', jp_name: '農家、農夫', last_updated_by: 'admin'},
  {en_name: 'bartender, barman', jp_name: 'バーテンダー', last_updated_by: 'admin'},
  {en_name: 'plumber', jp_name: '配管工', last_updated_by: 'admin'},
  {en_name: 'florist', jp_name: '花屋', last_updated_by: 'admin'},
  {en_name: 'hairdresser, beautician', jp_name: '美容師', last_updated_by: 'admin'},
  {en_name: '(hourse) painter', jp_name: 'ペンキ屋', last_updated_by: 'admin'},
  {en_name: 'maid', jp_name: 'メイド', last_updated_by: 'admin'},
  {en_name: 'welder', jp_name: '溶接工', last_updated_by: 'admin'},
  {en_name: 'fisherman', jp_name: '漁師', last_updated_by: 'admin'},
  {en_name: 'foreman', jp_name: '作業長、班長', last_updated_by: 'admin'},
  {en_name: 'worker', jp_name: '工員', last_updated_by: 'admin'},
  {en_name: 'manual labor', jp_name: '肉体労働', last_updated_by: 'admin'},
  {en_name: 'factory, works, plant, mill', jp_name: '工場', last_updated_by: 'admin'}
])

Country.create([
  {en_name: 'Bangladesh', jp_name: 'バングラデシュ', country_code: 'BD', last_updated_by: 'admin'},
  {en_name: 'Bhutan', jp_name: 'ブータン', country_code: 'BT', last_updated_by: 'admin'},
  {en_name: 'Cambodia', jp_name: 'カンボジア', country_code: 'KH', last_updated_by: 'admin'},
  {en_name: 'China', jp_name: '中華人民共和国', country_code: 'CN', last_updated_by: 'admin'},
  {en_name: 'Indonesia', jp_name: 'インドネシア', country_code: 'ID', last_updated_by: 'admin'},
  {en_name: 'India', jp_name: 'インド', country_code: 'IN', last_updated_by: 'admin'},
  {en_name: 'Japan', jp_name: '日本', country_code: 'JP', last_updated_by: 'admin'},  
  {en_name: 'Laos', jp_name: 'ラオス', country_code: 'LA', last_updated_by: 'admin'},
  {en_name: 'Malaysia', jp_name: 'マレーシア', country_code: 'MY', last_updated_by: 'admin'},
  {en_name: 'Myanmar', jp_name: 'ミャンマー', country_code: 'MM', last_updated_by: 'admin'},
  {en_name: 'Mongolia', jp_name: 'モンゴル', country_code: 'MN', last_updated_by: 'admin'},
  {en_name: 'Nepal', jp_name: 'ネパール', country_code: 'NP', last_updated_by: 'admin'},
  {en_name: 'Palau', jp_name: 'パラオ', country_code: 'PW', last_updated_by: 'admin'},
  {en_name: 'Pakistan', jp_name: 'パキスタン', country_code: 'PK', last_updated_by: 'admin'},
  {en_name: 'Philippines', jp_name: 'フィリピン', country_code: 'PH', last_updated_by: 'admin'},
  {en_name: 'Republic of Korea', jp_name: '韓国', country_code: 'KR', last_updated_by: 'admin'},
  {en_name: 'Singapore', jp_name: 'シンガポール', country_code: 'SG', last_updated_by: 'admin'},
  {en_name: 'Sri Lanka', jp_name: 'スリランカ', country_code: 'LK', last_updated_by: 'admin'},
  {en_name: 'Thailand', jp_name: 'タイ', country_code: 'TH', last_updated_by: 'admin'},
  {en_name: 'Viet Nam', jp_name: 'ベトナム', country_code: 'VN', last_updated_by: 'admin'}  
])

Religion.create([
  {en_name: '-', jp_name: '-', last_updated_by: 'admin'},
  {en_name: 'Christianity', jp_name: 'キリスト教', last_updated_by: 'admin'},
  {en_name: 'Islam', jp_name: 'イスラム教', last_updated_by: 'admin'},
  {en_name: 'Hinduism', jp_name: 'ヒンドゥー教', last_updated_by: 'admin'},
  {en_name: 'Buddhism', jp_name: '仏教', last_updated_by: 'admin'},
  {en_name: 'Other', jp_name: 'その他', last_updated_by: 'admin'}
])

FileType.create([
  {en_name: 'Passport copy', jp_name: 'パスポートコピー', last_updated_by: 'admin'},
  {en_name: 'ID copy', jp_name: '身分証明書', last_updated_by: 'admin'},
  {en_name: 'Health check', jp_name: '健康診断書', last_updated_by: 'admin'}
])

AptitudeTest.create([
  {en_name: 'Food processor', jp_name: '食品工場', last_updated_by: 'admin'},
  {en_name: 'Construction', jp_name: '建設業', last_updated_by: 'admin'},
  {en_name: 'Car maintenace', jp_name: '自動車整備', last_updated_by: 'admin'}
])

AcademicAbility.create([
  {en_name: 'IQ test', jp_name: '-', last_updated_by: 'admin'},
  {en_name: 'PWA Japanese skill test', jp_name: 'PWA日本語能力テスト', last_updated_by: 'admin'}
])