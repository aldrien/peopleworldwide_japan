# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160930014016) do

  create_table "academic_abilities", force: :cascade do |t|
    t.string   "en_name",         limit: 100
    t.string   "jp_name",         limit: 100
    t.string   "last_updated_by", limit: 20
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  create_table "applicant_academic_abilities", force: :cascade do |t|
    t.integer  "applicant_id",        limit: 4
    t.integer  "academic_ability_id", limit: 4
    t.integer  "score",               limit: 3
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  add_index "applicant_academic_abilities", ["applicant_id", "academic_ability_id"], name: "idx_applicant_academic", using: :btree

  create_table "applicant_aptitude_tests", force: :cascade do |t|
    t.integer  "applicant_id",     limit: 4
    t.integer  "aptitude_test_id", limit: 4
    t.integer  "score",            limit: 3
    t.boolean  "show_in_report",             default: false
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
  end

  add_index "applicant_aptitude_tests", ["applicant_id", "aptitude_test_id"], name: "idx_applicant_aptitude", using: :btree

  create_table "applicant_files", force: :cascade do |t|
    t.integer  "applicant_id",          limit: 4
    t.integer  "file_type_id",          limit: 4
    t.text     "description",           limit: 65535
    t.string   "document_file_name",    limit: 255
    t.string   "document_content_type", limit: 255
    t.integer  "document_file_size",    limit: 4
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "applicant_files", ["applicant_id", "file_type_id"], name: "idx_applicant", using: :btree

  create_table "applicant_project_histories", force: :cascade do |t|
    t.integer  "applicant_id",    limit: 4
    t.integer  "project_id",      limit: 4
    t.text     "notes",           limit: 65535
    t.string   "last_updated_by", limit: 30
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  add_index "applicant_project_histories", ["applicant_id"], name: "idx_applicant", using: :btree

  create_table "applicants", force: :cascade do |t|
    t.string   "custom_code",                  limit: 6
    t.integer  "creator_id",                   limit: 4
    t.boolean  "edit_status",                                default: false
    t.string   "last_updated_by",              limit: 20
    t.string   "name",                         limit: 30
    t.integer  "project_id",                   limit: 4
    t.integer  "country_id",                   limit: 4
    t.integer  "religion_id",                  limit: 4
    t.string   "status",                       limit: 20,    default: "Waiting"
    t.string   "email",                        limit: 50
    t.string   "phone",                        limit: 20
    t.text     "address",                      limit: 255
    t.string   "gender",                       limit: 8
    t.string   "birth_date",                   limit: 10
    t.integer  "age",                          limit: 3
    t.string   "height",                       limit: 10
    t.string   "weight",                       limit: 10
    t.string   "marital_status",               limit: 10
    t.string   "last_academic",                limit: 50
    t.string   "dominant_hand",                limit: 20
    t.string   "english_skill",                limit: 30
    t.string   "jlpt_level",                   limit: 50
    t.boolean  "is_work_related_expirience"
    t.string   "profile_file_name",            limit: 255
    t.string   "profile_content_type",         limit: 255
    t.integer  "profile_file_size",            limit: 4
    t.string   "wholebody_file_name",          limit: 255
    t.string   "wholebody_content_type",       limit: 255
    t.integer  "wholebody_file_size",          limit: 4
    t.string   "short_notes",                  limit: 255
    t.text     "notes",                        limit: 65535
    t.boolean  "show_notes_in_admin_pdf",                    default: true
    t.boolean  "show_notes_in_partner_pdf",                  default: false
    t.boolean  "show_notes_in_client_pdf",                   default: false
    t.text     "admin_remarks",                limit: 65535
    t.boolean  "show_remarks_in_admin_pdf",                  default: true
    t.boolean  "show_remarks_in_partner_pdf",                default: false
    t.boolean  "show_remarks_in_client_pdf",                 default: false
    t.boolean  "show_client_comments",                       default: false
    t.text     "client_comments",              limit: 65535
    t.text     "self_introduction_video_link", limit: 65535
    t.text     "aptitude_test_link",           limit: 65535
    t.text     "psychometric_test_link",       limit: 65535
    t.datetime "created_at",                                                     null: false
    t.datetime "updated_at",                                                     null: false
  end

  add_index "applicants", ["creator_id", "project_id", "country_id", "religion_id"], name: "searchable_by_idx", using: :btree

  create_table "aptitude_tests", force: :cascade do |t|
    t.string   "en_name",         limit: 100
    t.string   "jp_name",         limit: 100
    t.string   "last_updated_by", limit: 20
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  create_table "ckeditor_assets", force: :cascade do |t|
    t.string   "data_file_name",    limit: 255, null: false
    t.string   "data_content_type", limit: 255
    t.integer  "data_file_size",    limit: 4
    t.string   "data_fingerprint",  limit: 255
    t.integer  "assetable_id",      limit: 4
    t.string   "assetable_type",    limit: 30
    t.string   "type",              limit: 30
    t.integer  "width",             limit: 4
    t.integer  "height",            limit: 4
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  add_index "ckeditor_assets", ["assetable_type", "assetable_id"], name: "idx_ckeditor_assetable", using: :btree
  add_index "ckeditor_assets", ["assetable_type", "type", "assetable_id"], name: "idx_ckeditor_assetable_type", using: :btree

  create_table "countries", force: :cascade do |t|
    t.string   "en_name",         limit: 100
    t.string   "jp_name",         limit: 100
    t.string   "country_code",    limit: 10
    t.string   "last_updated_by", limit: 20
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  create_table "education_backgrounds", force: :cascade do |t|
    t.integer  "applicant_id", limit: 4,   null: false
    t.string   "date",         limit: 10
    t.string   "university",   limit: 255
    t.string   "status",       limit: 20
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "education_backgrounds", ["applicant_id"], name: "idx_applicant", using: :btree

  create_table "family_backgrounds", force: :cascade do |t|
    t.integer  "applicant_id",         limit: 4,  null: false
    t.string   "father_name",          limit: 50
    t.string   "mother_name",          limit: 50
    t.string   "husband_name",         limit: 50
    t.string   "wife_name",            limit: 50
    t.integer  "father_job_id",        limit: 4
    t.integer  "mother_job_id",        limit: 4
    t.integer  "husband_job_id",       limit: 4
    t.integer  "wife_job_id",          limit: 4
    t.string   "father_phone_number",  limit: 20
    t.string   "mother_phone_number",  limit: 20
    t.string   "husband_phone_number", limit: 20
    t.string   "wife_phone_number",    limit: 20
    t.integer  "brother_count",        limit: 2
    t.integer  "sister_count",         limit: 2
    t.integer  "daugther_count",       limit: 2
    t.integer  "son_count",            limit: 2
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
  end

  add_index "family_backgrounds", ["applicant_id", "father_job_id", "mother_job_id", "husband_job_id", "wife_job_id"], name: "idx_searchables", using: :btree

  create_table "file_types", force: :cascade do |t|
    t.string   "en_name",         limit: 100
    t.string   "jp_name",         limit: 100
    t.string   "last_updated_by", limit: 20
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  create_table "job_types", force: :cascade do |t|
    t.string   "en_name",         limit: 255
    t.string   "jp_name",         limit: 255
    t.string   "last_updated_by", limit: 20
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  create_table "projects", force: :cascade do |t|
    t.string   "title",                   limit: 50
    t.string   "company_name",            limit: 50
    t.integer  "partner_id",              limit: 4
    t.integer  "client_id",               limit: 4
    t.integer  "country_id",              limit: 4
    t.integer  "job_type_id",             limit: 4
    t.string   "status",                  limit: 255,   default: "In-progress"
    t.string   "no_of_needed_applicants", limit: 5
    t.text     "description",             limit: 65535
    t.text     "notes",                   limit: 65535
    t.string   "document_file_name",      limit: 255
    t.string   "document_content_type",   limit: 255
    t.integer  "document_file_size",      limit: 4
    t.datetime "last_email_sent"
    t.string   "last_updated_by",         limit: 20
    t.datetime "created_at",                                                    null: false
    t.datetime "updated_at",                                                    null: false
  end

  add_index "projects", ["partner_id", "client_id"], name: "idx_user", using: :btree

  create_table "pww_logos", force: :cascade do |t|
    t.string   "title",             limit: 255
    t.text     "content",           limit: 65535
    t.boolean  "status",                          default: true
    t.string   "logo_file_name",    limit: 255
    t.string   "logo_content_type", limit: 255
    t.integer  "logo_file_size",    limit: 4
    t.datetime "created_at",                                     null: false
    t.datetime "updated_at",                                     null: false
  end

  create_table "religions", force: :cascade do |t|
    t.string   "en_name",         limit: 100
    t.string   "jp_name",         limit: 100
    t.string   "last_updated_by", limit: 20
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  create_table "requested_to_be_deleted_applicants", force: :cascade do |t|
    t.integer  "applicant_id", limit: 4
    t.integer  "partner_id",   limit: 4
    t.text     "reason",       limit: 65535
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  add_index "requested_to_be_deleted_applicants", ["applicant_id"], name: "idx_applicant", unique: true, using: :btree
  add_index "requested_to_be_deleted_applicants", ["partner_id"], name: "idx_user", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "username",           limit: 255
    t.string   "password_digest",    limit: 255
    t.string   "fullname",           limit: 255
    t.string   "role",               limit: 255
    t.boolean  "status"
    t.string   "email",              limit: 255
    t.string   "phone",              limit: 255
    t.text     "description",        limit: 65535
    t.string   "image_file_name",    limit: 255
    t.string   "image_content_type", limit: 255
    t.integer  "image_file_size",    limit: 4
    t.integer  "pww_logo_id",        limit: 4,     default: 1
    t.string   "last_updated_by",    limit: 255
    t.datetime "created_at",                                   null: false
    t.datetime "updated_at",                                   null: false
  end

  create_table "work_experiences", force: :cascade do |t|
    t.integer  "applicant_id",   limit: 4,                   null: false
    t.integer  "job_type_id",    limit: 4
    t.integer  "country_id",     limit: 4
    t.string   "entering_date",  limit: 10
    t.string   "leaving_date",   limit: 10
    t.boolean  "up_to_present",              default: false
    t.string   "company_name",   limit: 255
    t.string   "status",         limit: 20
    t.string   "monthly_salary", limit: 20
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
  end

  add_index "work_experiences", ["applicant_id", "job_type_id", "country_id"], name: "idx_applicant_nature", using: :btree

end
