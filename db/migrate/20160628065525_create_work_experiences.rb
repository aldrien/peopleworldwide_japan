class CreateWorkExperiences < ActiveRecord::Migration
  def self.up
    create_table :work_experiences do |t|
      t.integer :applicant_id, null: false
      t.integer :job_type_id
      t.integer :country_id
      t.string :entering_date, limit: 10
      t.string :leaving_date, limit: 10
      t.boolean :up_to_present, default: false
      t.string :company_name
      t.string :status, limit: 20
      t.string :monthly_salary, limit: 20

      t.timestamps null: false
    end
    
    add_index :work_experiences, [:applicant_id, :job_type_id, :country_id], name: :idx_applicant_nature
  end

  def self.down
    drop_table :work_experiences
  end  
end
