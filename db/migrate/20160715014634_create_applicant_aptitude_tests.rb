class CreateApplicantAptitudeTests < ActiveRecord::Migration
  def self.up
    create_table :applicant_aptitude_tests do |t|
      t.integer :applicant_id
      t.integer :aptitude_test_id
      t.integer :score, limit: 3
      t.boolean :show_in_report, default: false
      
      t.timestamps null: false
    end

    add_index :applicant_aptitude_tests, [:applicant_id, :aptitude_test_id], name: :idx_applicant_aptitude
  end

  def self.down
    drop_table :applicant_aptitude_tests
  end
  
end