class CreateUsers < ActiveRecord::Migration
  def self.up
    create_table :users do |t|
      t.string :username
      t.string :password_digest
      t.string :fullname
      t.string :role
      t.boolean :status
      t.string :email
      t.string :phone
      t.text :description
      t.string :image_file_name
      t.string :image_content_type
      t.integer :image_file_size
      t.integer :pww_logo_id, default: 1
      t.string :last_updated_by
      
      t.timestamps null: false
    end
  end
  
  def self.down
    drop_table :users
  end  
end
