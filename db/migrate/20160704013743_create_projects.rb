class CreateProjects < ActiveRecord::Migration
  def self.up
    create_table :projects do |t|
      t.string :title, limit: 50
      t.string :company_name, limit: 50
      t.integer :partner_id
      t.integer :client_id
      t.integer :country_id
      t.integer :job_type_id
      t.string :status, default: 'In-progress'
      t.string :no_of_needed_applicants, limit: 5
      t.text :description
      t.text :notes
      t.string :document_file_name
      t.string :document_content_type
      t.integer :document_file_size
      t.datetime :last_email_sent

      t.string :last_updated_by, limit: 20

      t.timestamps null: false
    end

    add_index :projects, [:partner_id, :client_id], name: :idx_user
  end

  def self.down
    drop_table :projects
  end  

end