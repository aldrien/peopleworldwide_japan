class CreateAcademicAbilities < ActiveRecord::Migration
  def self.up
    create_table :academic_abilities do |t|
      t.string :en_name, limit: 100
      t.string :jp_name, limit: 100      
      t.string :last_updated_by, limit: 20
      
      t.timestamps null: false
    end
  end

  def self.down
    drop_table :academic_abilities
  end
end