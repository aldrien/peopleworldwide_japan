class CreateApplicantProjectHistories < ActiveRecord::Migration
  def self.up
    create_table :applicant_project_histories do |t|
      t.integer :applicant_id
      t.integer :project_id
      t.text :notes
      t.string :last_updated_by, limit: 30

      t.timestamps null: false
    end

    add_index :applicant_project_histories, :applicant_id, name: :idx_applicant
  end

  def self.down
    drop_table :applicant_project_histories
  end  

end