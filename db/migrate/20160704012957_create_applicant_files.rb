class CreateApplicantFiles < ActiveRecord::Migration
  def self.up
    create_table :applicant_files do |t|
      t.integer :applicant_id
      t.integer :file_type_id
      t.text :description
      t.string :document_file_name
      t.string :document_content_type
      t.integer :document_file_size
      
      t.timestamps null: false
    end
    
    add_index :applicant_files, [:applicant_id, :file_type_id], name: :idx_applicant
  end

  def self.down
    drop_table :applicant_files
  end
end