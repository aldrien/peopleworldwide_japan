class CreateApplicants < ActiveRecord::Migration
  def self.up
    create_table :applicants do |t|
      t.integer :creator_id
      t.boolean :edit_status, default: false
      t.string :last_updated_by, limit: 20
      t.string :name, limit: 30
      t.integer :project_id
      t.integer :country_id
      t.integer :religion_id
      t.string :status, limit: 20, default: 'Waiting'
      t.string :email, limit: 50
      t.string :phone, limit: 20
      t.text :address, limit: 100
      t.string :gender, limit: 8
      t.string :birth_date, limit: 10
      t.integer :age, limit: 3
      t.string :height, limit: 10
      t.string :weight, limit: 10
      t.string :marital_status, limit: 10
      t.string :last_academic, limit: 50      
      t.string :dominant_hand, limit: 20
      t.string :english_skill, limit: 30
      t.string :jlpt_level, limit: 50
      t.boolean :is_work_related_expirience
      t.string :profile_file_name
      t.string :profile_content_type
      t.integer :profile_file_size
      t.string :wholebody_file_name
      t.string :wholebody_content_type
      t.integer :wholebody_file_size
      t.string :short_notes
      t.text :notes
      t.boolean :show_notes_in_admin_pdf, default: true
      t.boolean :show_notes_in_partner_pdf, default: false
      t.boolean :show_notes_in_client_pdf, default: false
      t.text :admin_remarks
      t.boolean :show_remarks_in_admin_pdf, default: true
      t.boolean :show_remarks_in_partner_pdf, default: false
      t.boolean :show_remarks_in_client_pdf, default: false
      t.boolean :show_client_comments, default: false
      t.text :client_comments
      t.text :self_introduction_video_link
      t.text :aptitude_test_link
      t.text :psychometric_test_link
      
      t.timestamps null: false
    end

    add_index :applicants, [:creator_id, :project_id, :country_id, :religion_id], name: :searchable_by_idx
  end
  
  def self.down
    drop_table :applicants
  end
end