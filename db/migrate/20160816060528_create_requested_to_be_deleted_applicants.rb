class CreateRequestedToBeDeletedApplicants < ActiveRecord::Migration
  def self.up
    create_table :requested_to_be_deleted_applicants do |t|
      t.integer :applicant_id
      t.integer :partner_id
      t.text :reason

      t.timestamps null: false
    end

    add_index :requested_to_be_deleted_applicants, :applicant_id, name: :idx_applicant, unique: true
    add_index :requested_to_be_deleted_applicants, :partner_id, name: :idx_user
  end

  def self.down
    drop_table :requested_to_be_deleted_applicants
  end

end