class CreatePwwLogos < ActiveRecord::Migration
  def self.up
    create_table :pww_logos do |t|
      t.string :title
      t.text :content
      t.boolean :status, default: true
      t.string :logo_file_name
      t.string :logo_content_type
      t.integer :logo_file_size

      t.timestamps null: false
    end
  end

  def self.down
    drop_table :pww_logos
  end  
end
