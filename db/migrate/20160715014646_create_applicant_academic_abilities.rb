class CreateApplicantAcademicAbilities < ActiveRecord::Migration
  def self.up
    create_table :applicant_academic_abilities do |t|
      t.integer :applicant_id
      t.integer :academic_ability_id
      t.integer :score, limit: 3

      t.timestamps null: false
    end

    add_index :applicant_academic_abilities, [:applicant_id, :academic_ability_id], name: :idx_applicant_academic
  end

  def self.down
    drop_table :applicant_academic_abilities
  end  

end