class CreateEducationBackgrounds < ActiveRecord::Migration
  def self.up
    create_table :education_backgrounds do |t|
      t.integer :applicant_id, null: false
      t.string :date, limit: 10
      t.string :university
      t.string :status, limit: 20

      t.timestamps null: false
    end

    add_index :education_backgrounds, :applicant_id, name: :idx_applicant
  end

  def self.down
    drop_table :education_backgrounds
  end  
end