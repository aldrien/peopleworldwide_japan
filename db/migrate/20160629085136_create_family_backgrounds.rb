class CreateFamilyBackgrounds < ActiveRecord::Migration
  def self.up
    create_table :family_backgrounds do |t|
      t.integer :applicant_id, null: false
      t.string :father_name, limit: 50
      t.string :mother_name, limit: 50
      t.string :husband_name, limit: 50
      t.string :wife_name, limit: 50
      t.integer :father_job_id
      t.integer :mother_job_id
      t.integer :husband_job_id
      t.integer :wife_job_id
      t.string :father_phone_number, limit: 20
      t.string :mother_phone_number, limit: 20
      t.string :husband_phone_number, limit: 20
      t.string :wife_phone_number, limit: 20
      t.integer :brother_count, limit: 2
      t.integer :sister_count, limit: 2
      t.integer :daugther_count, limit: 2
      t.integer :son_count, limit: 2

      t.timestamps null: false
    end

    add_index :family_backgrounds, [:applicant_id, :father_job_id, :mother_job_id, :husband_job_id, :wife_job_id], name: :idx_searchables
  end

  def self.down
    drop_table :family_backgrounds
  end
  
end