class CreateAptitudeTests < ActiveRecord::Migration
  def self.up
    create_table :aptitude_tests do |t|
      t.string :en_name, limit: 100
      t.string :jp_name, limit: 100      
      t.string :last_updated_by, limit: 20
      
      t.timestamps null: false
    end
  end

  def self.down
    drop_table :aptitude_tests
  end  
end