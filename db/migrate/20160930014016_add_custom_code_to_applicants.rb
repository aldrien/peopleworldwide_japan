class AddCustomCodeToApplicants < ActiveRecord::Migration
  def self.up
    add_column :applicants, :custom_code, :string, limit: 6, after: :id
  end

  def self.down
    remove_column :applicants, :custom_code
  end
end
