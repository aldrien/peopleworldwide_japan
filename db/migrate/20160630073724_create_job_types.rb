class CreateJobTypes < ActiveRecord::Migration
  def self.up
    create_table :job_types do |t|
      t.string :en_name
      t.string :jp_name
      t.string :last_updated_by, limit: 20
      
      t.timestamps null: false
    end
  end

  def self.down
    drop_table :job_types
  end
end
